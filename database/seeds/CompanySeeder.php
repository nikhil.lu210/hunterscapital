<?php

use Illuminate\Database\Seeder;

class CompanySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('my_companies')->insert([
            'company_name' =>'Your Company Name',
            'company_website' =>null,
            'phone_number' =>null,
            'mobile_number' =>'01234567891',
            'vat_status' =>0,
            'principal_email' =>'abc@mail.com',
            'filechecking_email' =>'filecheck@mail.com',
            'office_email' =>'official@mail.com',
            'secondary_filechecking_email' =>'secondary@mail.com',
            'county' =>'Lodon',
            'town_city' =>'Your Town City',
            'postal_code' =>'abc1020',
            'area_location' =>'your area location',
            'house_plot_no' =>'comapony plot number',
            'address' =>'compony address',
            'fca_number' =>'compony fca number',
            'financial_year_end' =>now(),
            'pii_renewal_date' =>now(),
            'business' =>null,
            'company_footer' =>'your company footer',
            'created_at' =>now(),
            'updated_at' =>now(),
            'company_logo' =>null
        ]);
    }
}


