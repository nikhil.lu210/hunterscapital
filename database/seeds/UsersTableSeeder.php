<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            'role_id' => '1',
            'saluation' => 'Mr.',
            'forname'   =>  'Super',
            'surname'   =>  'Admin',
            'email_address' => 'superadmin@mail.com',
            'mobile_number' => '01234567899',
            'job_role'  =>  'Super',
            'email' => 'superadmin@mail.com',
            'password' => bcrypt('12345678'),
            'created_at' => now(),
            'updated_at' => now(),
        ]);
        DB::table('users')->insert([
            'role_id' => '3',
            'saluation' => 'Mr.',
            'forname'   =>  'Director',
            'surname'   =>  'Babu',
            'email_address' => 'director@mail.com',
            'mobile_number' => '01234567899',
            'job_role'  =>  'Director',
            'email' => 'director@mail.com',
            'password' => bcrypt('12345678'),
            'created_at' => now(),
            'updated_at' => now(),
        ]);
    }
}
