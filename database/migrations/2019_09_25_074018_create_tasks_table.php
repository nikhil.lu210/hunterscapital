<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTasksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tasks', function (Blueprint $table) {
            $table->bigIncrements('id');

            $table->string('task_title');

            $table->bigInteger('task_type')->unsigned();
            $table->foreign('task_type')->references('id')->on('task_types')->onDelete('cascade'); //Relation With "TASK_TYPES" Table

            $table->bigInteger('assigned_to')->unsigned();
            $table->foreign('assigned_to')->references('id')->on('users')->onDelete('cascade'); //Relation With "users" Table

            $table->date('start_date');
            $table->date('end_date')->nullable();

            $table->bigInteger('client')->unsigned()->nullable();
            $table->foreign('client')->references('id')->on('clients')->onDelete('cascade'); //Relation With "clients" Table

            $table->string('task_status');

            $table->text('note');

            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tasks');

        Schema::table("tasks", function ($table) {
            $table->dropSoftDeletes();
        });
    }
}
