<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFactFindModulesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('fact_find_modules', function (Blueprint $table) {
            $table->bigIncrements('id');

            $table->string('factfind_name');
            $table->json('factfinds')->nullable();

            $table->boolean('isDefault')->default(false);

            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('fact_find_modules');

        Schema::table("fact_find_modules", function ($table) {
            $table->dropSoftDeletes();
        });
    }
}
