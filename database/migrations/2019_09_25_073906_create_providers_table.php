<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProvidersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('providers', function (Blueprint $table) {
            $table->bigIncrements('id');

            $table->string('provider_name');
            $table->text('address')->nullable();
            $table->string('county')->nullable();
            $table->string('postcode')->nullable();
            $table->string('email_address')->nullable();
            $table->text('note')->nullable();

            $table->timestamp('approved_at')->nullable();

            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('providers');

        Schema::table("providers", function ($table) {
            $table->dropSoftDeletes();
        });
    }
}
