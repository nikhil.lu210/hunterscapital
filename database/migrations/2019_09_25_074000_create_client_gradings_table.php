<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateClientGradingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('client_gradings', function (Blueprint $table) {
            $table->bigIncrements('id');

            $table->string('name');
            $table->boolean('status')->default(true);
            $table->timestamps();
            $table->softDeletes();
        });

        DB::statement("ALTER TABLE client_gradings ADD icon MEDIUMBLOB NULL"); //Insert Image in Binary format
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('client_gradings');

        Schema::table("client_gradings", function ($table) {
            $table->dropSoftDeletes();
        });
    }
}
