<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePromotionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('promotions', function (Blueprint $table) {
            $table->bigIncrements('id');

            $table->string('promotion_category');
            $table->string('promotion_type');
            $table->string('promotion_medium');
            $table->string('promotion_title');
            $table->date('requested_date');
            $table->date('deadline')->nullable();

            $table->boolean('promotion_closed')->default(false);

            $table->bigInteger('closed_by')->unsigned()->nullable();
            $table->foreign('closed_by')->references('id')->on('authors')->onDelete('cascade'); //Relation With "authors" Table

            $table->date('closed_date')->nullable();
            $table->json('overall_result')->nullable();
            $table->text('note')->nullable();

            $table->bigInteger('requested_by')->unsigned();
            $table->foreign('requested_by')->references('id')->on('users')->onDelete('cascade'); //Relation With "users" Table

            $table->bigInteger('ifac_advisor')->unsigned()->nullable();
            $table->foreign('ifac_advisor')->references('id')->on('authors')->onDelete('cascade'); //Relation With "authors" Table

            $table->string('jurisdiction')->nullable();

            // $table->bigInteger('jurisdiction')->unsigned()->nullable();
            // $table->foreign('jurisdiction')->references('id')->on('authors')->onDelete('cascade'); //Relation With "authors" Table

            $table->text('product_area_covered')->nullable();

            $table->bigInteger('reviewer')->unsigned()->nullable();
            $table->foreign('reviewer')->references('id')->on('authors')->onDelete('cascade'); //Relation With "authors" Table

            $table->date('reviewed_date')->nullable();

            $table->bigInteger('signed_of_by')->unsigned()->nullable();
            $table->foreign('signed_of_by')->references('id')->on('authors')->onDelete('cascade'); //Relation With "authors" Table

            $table->date('approval_date')->nullable();
            $table->date('approval_to')->nullable();

            $table->json('documents')->nullable();

            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('promotions');

        Schema::table("promotions", function ($table) {
            $table->dropSoftDeletes();
        });
    }
}
