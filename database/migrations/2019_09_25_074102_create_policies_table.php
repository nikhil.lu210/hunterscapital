<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePoliciesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('policies', function (Blueprint $table) {
            $table->bigIncrements('id');

            $table->string('policy_name');
            $table->bigInteger('client')->unsigned();
            $table->foreign('client')->references('id')->on('clients')->onDelete('cascade'); //Relation With "clients" Table

            $table->bigInteger('advisor')->unsigned();
            $table->foreign('advisor')->references('id')->on('users')->onDelete('cascade'); //Relation With "users" Table

            $table->string('specific_policy_type');
            $table->string('new_business_type');
            $table->string('adviser_reference')->nullable();

            $table->float('policy_valuation_amount')->nullable();

            $table->date('policy_request_date')->nullable();
            $table->date('policy_valuation_date')->nullable();
            $table->date('policy_start_date')->nullable();
            $table->date('policy_end_date')->nullable();
            $table->date('policy_cancellation_date')->nullable();

            $table->date('policy_review_date')->nullable();
            $table->enum('is_send_mail',['Yes', 'No'])->default('No');

            $table->boolean('other_reason')->default(false);
            $table->text('note')->nullable();

            $table->bigInteger('provider')->unsigned();
            $table->foreign('provider')->references('id')->on('providers')->onDelete('cascade'); //Relation With "providers" Table

            $table->bigInteger('trust')->unsigned()->nullable();
            $table->foreign('trust')->references('id')->on('trusts')->onDelete('cascade'); //Relation With "trusts" Table

            $table->string('beneficiaries')->nullable();

            $table->json('funds')->nullable();

            $table->string('policy_status');
            $table->string('policy_number')->nullable();
            $table->string('risk_attitude');

            $table->boolean('ucis_product')->default(false);
            $table->boolean('direct_stock_share')->default(false);

            $table->json('commission_fees')->nullable();
            $table->json('payments')->nullable();

            //upper section common factor




            // there is investment section
            $table->json('withdrawals')->nullable();
            // investment section

            // there is mortgages section
            $table->float('mortgage_amount')->nullable();
            $table->float('mortgage_monthly_amount')->nullable();

            // $table->string('interest_rate_type')->nullable();
            $table->string('mortgage_term')->nullable();

            $table->date('mortgage_fixed_until')->nullable();

            $table->boolean('mortgage_protected')->nullable();

            $table->string('county')->nullable();
            $table->string('postal_code')->nullable();
            $table->text('address')->nullable();

            $table->float('interest')->nullable();
            $table->float('loan_to_value')->nullable();    //amount_outstanding
            $table->float('property_value')->nullable();
            $table->float('deposit_amount')->nullable();  //mortgage_fund_source
            $table->date('valuation_date')->nullable();  //mortgage_reason
            $table->date('mortgage_offer_date')->nullable();  //repayment_penalty

            $table->boolean('willing_to_pay')->default(false);
            $table->boolean('repayment_on_track')->default(false);
            $table->boolean('mortgage_portable')->default(false);
            // there is mortgages section


            // there is pension section
            $table->string('indexed_criteria')->nullable(); // also be a protection section
            $table->float('widow_pension')->nullable();
            $table->boolean('waiver_included')->default(false); // also be a protection section
            $table->boolean('contracted_out')->default(false);
            // there is pension section

            // there is protection section
            $table->string('product_type')->nullable();
            $table->string('protection_basis')->nullable();
            $table->date('renewal_date')->nullable();
            $table->string('premium_period')->nullable();
            $table->string('total_protection_sum')->nullable();
            $table->string('protection_excess')->nullable();
            $table->string('plan_purpose')->nullable();
            // there is protection section

            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('policies');

        Schema::table("policies", function ($table) {
            $table->dropSoftDeletes();
        });
    }
}
