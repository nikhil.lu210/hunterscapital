<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOnlineAccountsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('online_accounts', function (Blueprint $table) {
            $table->bigIncrements('id');

            $table->string('site');
            $table->string('email');
            $table->string('password');

            $table->string('decision')->nullable();
            $table->text('decision_note')->nullable();

            $table->boolean('site_checked')->default(true);

            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('online_accounts');

        Schema::table("online_accounts", function ($table) {
            $table->dropSoftDeletes();
        });
    }
}
