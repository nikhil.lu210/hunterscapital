<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateClientsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('clients', function (Blueprint $table) {
            $table->bigIncrements('id');

            $table->string('client_status');
            $table->string('client_access')->default(false);

            $table->bigInteger('advisor')->unsigned()->nullable();
            $table->foreign('advisor')->references('id')->on('users')->onDelete('cascade'); //Relation With User Table

            $table->string('corporate_name')->nullable();

            $table->string('client_nationality')->nullable();
            $table->string('house_no_name')->nullable();
            $table->string('town_city')->nullable();
            $table->string('county')->nullable();
            $table->string('post_code')->nullable();
            $table->text('address')->nullable();

            $table->string('client_title')->nullable();
            $table->string('client_forname');
            $table->string('client_middle_name')->nullable();
            $table->string('client_surname');
            $table->date('date_of_birth')->nullable();
            $table->string('home_phone')->nullable();
            $table->string('mobile_phone');
            $table->string('email');

            $table->bigInteger('grading')->unsigned()->nullable();
            $table->foreign('grading')->references('id')->on('client_gradings')->onDelete('cascade'); //Relation With User Table

            $table->json('contactable_by')->nullable();

            $table->json('partners')->nullable();
            $table->json('notes')->nullable();

            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('clients');

        Schema::table("clients", function ($table) {
            $table->dropSoftDeletes();
        });
    }
}
