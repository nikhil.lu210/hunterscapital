<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateComplaintsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('complaints', function (Blueprint $table) {
            $table->bigIncrements('id');

            $table->bigInteger('advisor')->unsigned();
            $table->foreign('advisor')->references('id')->on('users')->onDelete('cascade'); //Relation With "users" Table

            $table->string('business_type');
            $table->string('complaint_type');
            $table->date('client_advice_date')->nullable();

            $table->bigInteger('client')->unsigned();
            $table->foreign('client')->references('id')->on('clients')->onDelete('cascade'); //Relation With "clients" Table

            $table->bigInteger('assessor')->unsigned()->nullable();
            $table->foreign('assessor')->references('id')->on('assessors')->onDelete('cascade'); //Relation With "assessors" Table

            $table->date('client_received');
            $table->date('internally_received');
            $table->date('confirmation_sent')->nullable();
            $table->boolean('verbar_complaint')->default(true);
            $table->text('details')->nullable();

            $table->date('four_week_later')->nullable();
            $table->date('eight_week_later')->nullable();
            $table->date('final_response')->nullable();

            // Assessor Decision
            $table->date('to_assessor')->nullable();
            $table->date('from_assessor')->nullable();
            $table->date('to_client_assessor')->nullable();
            $table->integer('redress_amount_assessor')->nullable();
            $table->string('assessor_decision')->nullable();
            $table->text('assessor_note')->nullable();

            // FOS Adjudicator Decision
            $table->date('to_adjudicator')->nullable();
            $table->date('from_adjudicator')->nullable();
            $table->date('to_client_adjudicator')->nullable();
            $table->integer('redress_amount_adjudicator')->nullable();
            $table->string('adjudicator_decision')->nullable();
            $table->text('adjudicator_note')->nullable();

            // FOS Ombudsman Decision
            $table->date('to_ombudsman')->nullable();
            $table->date('from_ombudsman')->nullable();
            $table->date('to_client_ombudsman')->nullable();
            $table->integer('redress_amount_ombudsman')->nullable();
            $table->string('ombudsman_decision')->nullable();
            $table->text('ombudsman_note')->nullable();

            $table->json('documents')->nullable();

            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('complaints');

        Schema::table("complaints", function ($table) {
            $table->dropSoftDeletes();
        });
    }
}
