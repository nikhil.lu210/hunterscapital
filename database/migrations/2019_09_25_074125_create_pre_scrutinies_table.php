<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePreScrutiniesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pre_scrutinies', function (Blueprint $table) {
            $table->bigIncrements('id');

            $table->bigInteger('advisor')->unsigned();
            $table->foreign('advisor')->references('id')->on('users')->onDelete('cascade'); //Relation With "users" Table

            $table->bigInteger('client')->unsigned();
            $table->foreign('client')->references('id')->on('clients')->onDelete('cascade'); //Relation With "clients" Table

            $table->string('specific_business_type');

            $table->binary('file');

            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pre_scrutinies');

        Schema::table("pre_scrutinies", function ($table) {
            $table->dropSoftDeletes();
        });
    }
}
