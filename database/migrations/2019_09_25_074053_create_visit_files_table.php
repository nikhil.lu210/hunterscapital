<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateVisitFilesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('visit_files', function (Blueprint $table) {
            $table->bigIncrements('id');

            $table->bigInteger('visit_id')->unsigned();
            $table->foreign('visit_id')->references('id')->on('visits')->onDelete('cascade'); //Relation With "visits" Table

            $table->string('file_type');
            $table->text('description');
            $table->binary('file');

            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('visit_files');

        Schema::table("visit_files", function ($table) {
            $table->dropSoftDeletes();
        });
    }
}
