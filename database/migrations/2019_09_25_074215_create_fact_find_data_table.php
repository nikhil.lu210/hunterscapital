<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFactFindDataTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('fact_find_data', function (Blueprint $table) {
            $table->bigIncrements('id');



            $table->bigInteger('factfind_id')->unsigned();
            $table->foreign('factfind_id')->references('id')->on('fact_find_modules')->onDelete('cascade'); //Relation With "fact_find_modules" Table

            $table->bigInteger('client_id')->unsigned();
            $table->foreign('client_id')->references('id')->on('clients')->onDelete('cascade'); //Relation With "fact_find_modules" Table

            $table->json('factfinds')->nullable();

            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('fact_find_data');

        Schema::table("fact_find_data", function ($table) {
            $table->dropSoftDeletes();
        });
    }
}
