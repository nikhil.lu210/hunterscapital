<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateVisitsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('visits', function (Blueprint $table) {
            $table->bigIncrements('id');

            $table->date('visit_date');
            $table->date('signed_off_on')->nullable();
            $table->string('nature_of_visit')->nullable();

            $table->bigInteger('signed_off_by')->unsigned()->nullable();
            $table->foreign('signed_off_by')->references('id')->on('authors')->onDelete('cascade'); //Relation With "authors" Table

            $table->string('grading')->nullable();

            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('visits');

        Schema::table("visits", function ($table) {
            $table->dropSoftDeletes();
        });
    }
}
