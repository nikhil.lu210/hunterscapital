<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSPSSTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('s_p_s_s', function (Blueprint $table) {
            $table->bigIncrements('id');

            $table->bigInteger('user')->unsigned();
            $table->foreign('user')->references('id')->on('users')->onDelete('cascade'); //Relation With "users" Table

            $table->string('issued_by');
            $table->date('date_issued');
            $table->date('expire_date');
            $table->json('files')->nullable();

            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('s_p_s_s');

        Schema::table("s_p_s_s", function ($table) {
            $table->dropSoftDeletes();
        });
    }
}
