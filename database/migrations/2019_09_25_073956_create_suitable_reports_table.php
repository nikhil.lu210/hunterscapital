<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSuitableReportsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('suitable_reports', function (Blueprint $table) {
            $table->bigIncrements('id');

            $table->string('name');
            $table->integer('download');
            $table->binary('file');
            $table->text('note')->nullable();

            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('suitable_reports');

        Schema::table("suitable_reports", function ($table) {
            $table->dropSoftDeletes();
        });
    }
}
