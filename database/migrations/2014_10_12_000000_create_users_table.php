<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->bigIncrements('id');

            $table->bigInteger('role_id')->unsigned()->default(3);
            $table->foreign('role_id')->references('id')->on('roles')->onDelete('cascade'); //Relation With Role Table

            $table->bigInteger('client_id')->unsigned()->nullable();

            $table->string('saluation')->nullable();
            $table->string('forname');
            $table->string('middle_name')->nullable();
            $table->string('surname');

            $table->date('date_of_birth')->nullable();

            $table->string('email_address');
            $table->string('mobile_number')->nullable();
            $table->string('phone_number')->nullable();
            $table->string('fax_number')->nullable();

            $table->string('ni_number')->nullable();
            $table->string('pre_sale_type')->nullable();
            $table->string('fca_number')->nullable();

            $table->string('county_old')->nullable();
            $table->string('town_city')->nullable();
            $table->string('post_code')->nullable();
            $table->text('address')->nullable();

            $table->string('job_title')->nullable();
            $table->json('specials')->nullable();
            $table->json('basics')->nullable();
            $table->json('mortgages')->nullable();
            $table->json('lifetimes')->nullable();
            $table->json('others')->nullable();
            $table->json('heigher_levels')->nullable();

            $table->json('professionals')->nullable();

            $table->string('job_role');
            $table->boolean('user_status')->default(true); //Return Active User

            $table->string('email')->unique();
            $table->timestamp('email_verified_at')->nullable();
            $table->string('password');
            $table->rememberToken();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');

        Schema::table("users", function ($table) {
            $table->dropSoftDeletes();
        });
    }
}
