<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTurnoversTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('turnovers', function (Blueprint $table) {
            $table->bigIncrements('id');

            $table->bigInteger('client')->unsigned()->nullable();
            $table->foreign('client')->references('id')->on('clients')->onDelete('cascade'); //Relation With "clients" Table

            $table->bigInteger('client_policy')->unsigned()->nullable();
            $table->foreign('client_policy')->references('id')->on('policies')->onDelete('cascade'); //Relation With "clients" Table

            $table->bigInteger('advisor')->unsigned();
            $table->foreign('advisor')->references('id')->on('users')->onDelete('cascade'); //Relation With "users" Table

            $table->date('turnover_date')->nullable();
            $table->string('regulation_type');
            $table->string('regulation_type_spec');
            $table->string('turnover_type');
            $table->string('turnover_frequency')->nullable();
            $table->string('adviser_reference')->nullable();
            $table->string('rdr_status')->nullable();
            $table->string('revenue_source')->nullable();
            $table->string('payment_frequency')->nullable();
            $table->float('amount');
            $table->boolean('vat_applied')->default(false);
            $table->string('revenue_source_name')->nullable();
            $table->text('note')->nullable();

            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('turnovers');

        Schema::table("turnovers", function ($table) {
            $table->dropSoftDeletes();
        });
    }
}
