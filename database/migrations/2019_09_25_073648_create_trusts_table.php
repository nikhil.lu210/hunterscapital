<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTrustsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('trusts', function (Blueprint $table) {
            $table->bigIncrements('id');

            $table->string('trust_name');
            $table->string('forename');
            $table->string('surname');
            $table->text('address')->nullable();
            $table->string('county');
            $table->string('postcode');
            $table->string('contact_number')->nullable();
            $table->text('note')->nullable();

            $table->timestamp('approved_at')->nullable();

            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('trusts');

        Schema::table("trusts", function ($table) {
            $table->dropSoftDeletes();
        });
    }
}
