<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMyCompaniesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('my_companies', function (Blueprint $table) {
            $table->bigIncrements('id');

            $table->string('company_name');
            $table->string('company_website')->nullable();
            $table->string('phone_number')->nullable();
            $table->string('mobile_number');
            $table->boolean('vat_status');

            $table->string('principal_email');
            $table->string('filechecking_email');
            $table->string('office_email');
            $table->string('secondary_filechecking_email')->nullable();

            $table->string('county');
            $table->string('town_city');
            $table->string('postal_code')->nullable();
            $table->string('area_location')->nullable();
            $table->string('house_plot_no')->nullable();
            $table->text('address')->nullable();
            $table->string('fca_number')->nullable();
            $table->date('financial_year_end')->nullable();
            $table->date('pii_renewal_date')->nullable();
            $table->json('business')->nullable();

            $table->text('company_footer')->nullable();

            $table->timestamps();
            $table->softDeletes();
        });

        DB::statement("ALTER TABLE my_companies ADD company_logo MEDIUMBLOB NULL"); //Insert Image in Binary format
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('my_companies');

        Schema::table("my_companies", function ($table) {
            $table->dropSoftDeletes();
        });
    }
}
