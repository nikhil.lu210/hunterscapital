<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFundsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('funds', function (Blueprint $table) {
            $table->bigIncrements('id');

            $table->string('fund_name');
            $table->text('note')->nullable();
            $table->string('mex_code')->nullable();
            $table->string('isin_code')->nullable();
            $table->string('sedol_code')->nullable();
            $table->string('citi_code')->nullable();
            $table->string('bb_ticker_code')->nullable();

            $table->timestamp('approved_at')->nullable();

            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('funds');

        Schema::table("funds", function ($table) {
            $table->dropSoftDeletes();
        });
    }
}
