<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateVisitActionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('visit_actions', function (Blueprint $table) {
            $table->bigIncrements('id');

            $table->bigInteger('visit_id')->unsigned();
            $table->foreign('visit_id')->references('id')->on('visits')->onDelete('cascade'); //Relation With "visits" Table

            $table->string('agenda');
            $table->string('outcome');
            $table->date('deadline');
            $table->date('completed_on')->nullable();
            $table->text('issue');
            $table->text('related_action');
            $table->text('note')->nullable();

            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('visit_actions');

        Schema::table("visit_actions", function ($table) {
            $table->dropSoftDeletes();
        });
    }
}
