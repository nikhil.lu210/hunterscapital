<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRiskListsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('risk_lists', function (Blueprint $table) {
            $table->bigIncrements('id');

            $table->string('name');
            $table->boolean('status');


            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('risk_lists');

        Schema::table("risk_lists", function ($table) {
            $table->dropSoftDeletes();
        });
    }
}
