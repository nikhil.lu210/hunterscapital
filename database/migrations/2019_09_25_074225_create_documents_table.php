<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDocumentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('documents', function (Blueprint $table) {
            $table->bigIncrements('id');

            $table->string('category');
            $table->string('file_name');

            $table->bigInteger('client')->unsigned()->nullable();
            $table->foreign('client')->references('id')->on('clients')->onDelete('cascade'); //Relation With "clients" Table

            $table->bigInteger('client_policy')->unsigned()->nullable();
            $table->foreign('client_policy')->references('id')->on('policies')->onDelete('cascade'); //Relation With "clients" Table

            $table->text('description');

            $table->binary('file');


            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('documents');

        Schema::table("documents", function ($table) {
            $table->dropSoftDeletes();
        });
    }
}
