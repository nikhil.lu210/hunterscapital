<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLetterBuildersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('letter_builders', function (Blueprint $table) {
            $table->bigIncrements('id');

            $table->string('name');

            $table->text('letter_content');
            $table->boolean('footer_display');
            $table->string('footer_display_content');

            $table->bigInteger('client')->unsigned();
            $table->foreign('client')->references('id')->on('clients')->onDelete('cascade'); //Relation With "clients" Table


            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('letter_builders');

        Schema::table("letter_builders", function ($table) {
            $table->dropSoftDeletes();
        });
    }
}
