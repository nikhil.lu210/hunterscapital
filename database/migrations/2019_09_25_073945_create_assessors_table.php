<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAssessorsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('assessors', function (Blueprint $table) {
            $table->bigIncrements('id');

            $table->string('assessor_name');
            $table->text('note')->nullable();
            $table->text('address')->nullable();
            $table->string('county')->nullable();
            $table->string('postcode')->nullable();
            $table->string('contact_number')->nullable();

            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('assessors');

        Schema::table("assessors", function ($table) {
            $table->dropSoftDeletes();
        });
    }
}
