<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCPDSTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('c_p_d_s', function (Blueprint $table) {
            $table->bigIncrements('id');

            $table->bigInteger('user')->unsigned();
            $table->foreign('user')->references('id')->on('users')->onDelete('cascade'); //Relation With "users" Table

            $table->string('cpd_topic');

            $table->bigInteger('dev_set_by_user')->unsigned()->nullable();
            $table->foreign('dev_set_by_user')->references('id')->on('users')->onDelete('cascade'); //Relation With "users" Table

            $table->bigInteger('dev_set_by_author')->unsigned()->nullable();
            $table->foreign('dev_set_by_author')->references('id')->on('authors')->onDelete('cascade'); //Relation With "authors" Table

            $table->string('cpd_activity_type');
            $table->text('development_needed');
            $table->date('planned_completion_date');

            $table->text('cpd_activity_completed');
            $table->date('completed_date')->nullable();

            $table->text('note_reflective_statement');

            $table->float('structured_hour');
            $table->float('unstructured_hour');

            $table->json('files')->nullable();

            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('c_p_d_s');

        Schema::table("c_p_d_s", function ($table) {
            $table->dropSoftDeletes();
        });
    }
}
