<?php

// assessors
Route::group([
    'prefix' => 'assessor', //URL
    'as' => 'assessor.', //Route
    'namespace' => 'Assessor', // Controller
],
    function(){
        Route::get('/all_assessors', 'AssessorController@index')->name('index');
        Route::get('/create', 'AssessorController@create')->name('create');
        Route::post('/store', 'AssessorController@store')->name('store');
        Route::get('/all_assessors/show/{id}', 'AssessorController@show')->name('show');
        Route::post('/update/{id}', 'AssessorController@update')->name('update');
    }
);
