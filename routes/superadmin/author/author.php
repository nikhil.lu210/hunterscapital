<?php

// authors
Route::group([
    'prefix' => 'author', //URL
    'as' => 'author.', //Route
    'namespace' => 'Author', // Controller
],
    function(){
        Route::get('/all_authors', 'AuthorController@index')->name('index');
        Route::get('/create', 'AuthorController@create')->name('create');
        Route::post('/store', 'AuthorController@store')->name('store');
        Route::get('/all_authors/show/{id}', 'AuthorController@show')->name('show');
        Route::post('/update/{id}', 'AuthorController@update')->name('update');
    }
);
