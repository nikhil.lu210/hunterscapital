<?php

// funds
Route::group([
    'prefix' => 'fund', //URL
    'as' => 'fund.', //Route
    'namespace' => 'Fund', // Controller
],
    function(){
        Route::get('/all_funds', 'FundController@index')->name('index');
        Route::get('/create', 'FundController@create')->name('create');
        Route::post('/store', 'FundController@store')->name('store');
        Route::get('/all_funds/show/{id}', 'FundController@show')->name('show');
        Route::post('/update/{id}', 'FundController@update')->name('update');

        Route::get('/non_approved_fund', 'FundController@nonApprovedFund')->name('non_approved_fund');
        Route::get('/non_approved_fund/approve/{id}', 'FundController@approveFund')->name('non_approved_fund.approve');
        Route::get('/non_approved_fund/refuse/{id}', 'FundController@refuseFund')->name('non_approved_fund.refuse');
    }
);
