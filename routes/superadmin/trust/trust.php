<?php

// trusts
Route::group([
    'prefix' => 'trust', //URL
    'as' => 'trust.', //Route
    'namespace' => 'Trust', // Controller
],
    function(){
        Route::get('/all_trusts', 'TrustController@index')->name('index');
        Route::get('/create', 'TrustController@create')->name('create');
        Route::post('/store', 'TrustController@store')->name('store');
        Route::get('/all_trusts/show/{id}', 'TrustController@show')->name('show');
        Route::post('/update/{id}', 'TrustController@update')->name('update');

        Route::get('/non_approved_trust', 'TrustController@nonApprovedTrust')->name('non_approved_trust');
        Route::get('/non_approved_trust/approve/{id}', 'TrustController@approveTrust')->name('non_approved_trust.approve');
        Route::get('/non_approved_trust/refuse/{id}', 'TrustController@refuseTrust')->name('non_approved_trust.refuse');
    }
);
