<?php

// Providers
Route::group([
    'prefix' => 'provider', //URL
    'as' => 'provider.', //Route
    'namespace' => 'Provider', // Controller
],
    function(){
        Route::get('/all_providers', 'ProviderController@index')->name('index');
        Route::get('/create', 'ProviderController@create')->name('create');
        Route::post('/store', 'ProviderController@store')->name('store');
        Route::get('/all_providers/show/{id}', 'ProviderController@show')->name('show');
        Route::post('/update/{id}', 'ProviderController@update')->name('update');

        Route::get('/non_approved_provider', 'ProviderController@nonApprovedProvider')->name('non_approved_provider');
        Route::get('/non_approved_provider/approve/{id}', 'ProviderController@approveProvider')->name('non_approved_provider.approve');
        Route::get('/non_approved_provider/refuse/{id}', 'ProviderController@refuseProvider')->name('non_approved_provider.refuse');
    }
);
