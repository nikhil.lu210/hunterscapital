<?php

// Super Admin Routes
Route::group([
    'prefix' => 'superadmin', // URL
    'as' => 'superadmin.', // Route
    'namespace' => 'SuperAdmin', // Controller
],
    function(){
        /* ==================================
        ============< Dashboard >============
        ===================================*/
        // Dashboard
        include_once 'dashboard/dashboard.php';



        /* ==================================
        ============< Providers >============
        ===================================*/
        // Providers
        include_once 'provider/provider.php';



        /* ==================================
        ============< authors >============
        ===================================*/
        // authors
        include_once 'author/author.php';



        /* ==================================
        ============< assessors >============
        ===================================*/
        // assessors
        include_once 'assessor/assessor.php';



        /* ==================================
        ============< trusts >============
        ===================================*/
        // trusts
        // include_once 'trust/trust.php';



        /* ==================================
        ============< funds >============
        ===================================*/
        // funds
        // include_once 'fund/fund.php';
    }
);
