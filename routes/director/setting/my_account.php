<?php

// Settings
Route::group([
    'prefix' => 'setting', //URL
    'as' => 'setting.', //Route
    'namespace' => 'Setting', // Controller
],
    function(){
        Route::get('/my_account', 'MyAccountController@index')->name('my_account.index');
        Route::post('/my_account/update', 'MyAccountController@update')->name('my_account.update');

        // Password Changing
        Route::post('/my_account/change_password', 'MyAccountController@changePassword')->name('my_account.password.change');
    }
);
