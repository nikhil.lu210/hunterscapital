<?php

// Settings
Route::group([
    'prefix' => 'setting', //URL
    'as' => 'setting.', //Route
    'namespace' => 'Setting', // Controller
],
    function(){
        Route::get('/my_credential', 'MyCredentialController@index')->name('my_credential.index');
    }
);
