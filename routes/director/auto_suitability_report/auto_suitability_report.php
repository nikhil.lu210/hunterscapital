<?php

// auto_suitability_report
Route::group([
    'prefix' => 'auto_suitability_report', //URL
    'as' => 'auto_suitability_report.', //Route
    'namespace' => 'AutoSuitabilityReport', // Controller
],
    function(){
        Route::get('/', 'AutoSuitabilityReportController@index')->name('index');
    }
);
