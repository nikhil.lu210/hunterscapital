<?php

// gdpr
Route::group([
    'prefix' => 'gdpr', //URL
    'as' => 'gdpr.', //Route
    'namespace' => 'GDPR', // Controller
],
    function(){
        Route::get('/', 'GDPRController@index')->name('index');
    }
);
