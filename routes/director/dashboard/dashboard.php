<?php

// Customer Routes
Route::group([
    'prefix' => 'dashboard', //URL
    'as' => 'dashboard.', //Route
    'namespace' => 'Dashboard', // Controller
],
    function(){
        Route::get('/', 'DashboardController@index')->name('index');

        Route::get('/send_scheduling_mail', 'DashboardController@sendMail')->name('mailsend');
    }
);
