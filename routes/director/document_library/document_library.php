<?php

// Terms and Conditions
Route::group([
    'prefix' => 'document_library', //URL
    'as' => 'document_library.', //Route
    'namespace' => 'DocumentLibrary', // Controller
],
    function(){
        Route::get('/', 'DocumentLibraryController@index')->name('index');
    }
);
