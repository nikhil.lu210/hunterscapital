<?php

// Terms and Conditions
Route::group([
    'prefix' => 'term_and_condition', //URL
    'as' => 'term_and_condition.', //Route
    'namespace' => 'TermAndCondition', // Controller
],
    function(){
        Route::get('/sps', 'SPSController@index')->name('sps.index');
        Route::post('/sps/store', 'SPSController@store')->name('sps.store');
        Route::post('/sps/update', 'SPSController@update')->name('sps.update');

        Route::get('/sps/destroy/{id}', 'SPSController@destroy')->name('sps.destroy');


        Route::post('/sps/upload', 'SPSController@upload')->name('sps.upload');
        Route::get('/sps/download/{id}/{sps_id}', 'SPSController@download')->name('sps.download');
        Route::get('/sps/destroy_file/{id}/{sps_id}', 'SPSController@destroyFile')->name('sps.destroyFile');
    }
);
