<?php

// Terms and Conditions
Route::group([
    'prefix' => 'term_and_condition', //URL
    'as' => 'term_and_condition.', //Route
    'namespace' => 'TermAndCondition', // Controller
],
    function(){
        Route::get('/professional_body', 'ProfessionalBodyController@index')->name('professional_body.index');
    }
);
