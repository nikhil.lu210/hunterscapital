<?php

// Terms and Conditions
Route::group([
    'prefix' => 'term_and_condition', //URL
    'as' => 'term_and_condition.', //Route
    'namespace' => 'TermAndCondition', // Controller
],
    function(){
        Route::get('/cpd_analysis', 'CPDAnalysisController@index')->name('cpd_analysis.index');
    }
);
