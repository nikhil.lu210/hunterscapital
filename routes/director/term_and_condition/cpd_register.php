<?php

// Terms and Conditions
Route::group([
    'prefix' => 'term_and_condition', //URL
    'as' => 'term_and_condition.', //Route
    'namespace' => 'TermAndCondition', // Controller
],
    function(){
        Route::get('/cpd_register', 'CPDRegisterController@index')->name('cpd_register.index');
        Route::post('/cpd_register/store', 'CPDRegisterController@store')->name('cpd_register.store');
        Route::post('/cpd_register/update', 'CPDRegisterController@update')->name('cpd_register.update');
        Route::get('/cpd_register/destroy/{id}', 'CPDRegisterController@destroy')->name('cpd_register.destroy');


        Route::post('/cpd_register/upload', 'CPDRegisterController@upload')->name('cpd_register.upload');
        Route::get('/cpd_register/download/{id}/{cpd_id}', 'CPDRegisterController@download')->name('cpd_register.download');
        Route::get('/cpd_register/destroy_file/{id}/{cpd_id}', 'CPDRegisterController@destroyFile')->name('cpd_register.destroyFile');
    }
);
