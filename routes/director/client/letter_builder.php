<?php

// Customer Routes
Route::group([
    'prefix' => 'client', //URL
    'as' => 'client.', //Route
    'namespace' => 'Client', // Controller
],
    function(){
        Route::get('/letter_builder', 'LetterBuilderController@index')->name('letter_builder.index');
    }
);
