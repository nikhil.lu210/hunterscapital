<?php

// Customer Routes
Route::group([
    'prefix' => 'client', //URL
    'as' => 'client.', //Route
    'namespace' => 'Client', // Controller
],
    function(){
        Route::get('/my_clients', 'ClientController@index')->name('index');
        Route::get('/create', 'ClientController@create')->name('create');
        Route::get('/my_clients/show/{id}', 'ClientController@show')->name('show');


        Route::post('/my_clients/store', 'ClientController@storeClient')->name('store');
        Route::post('/my_clients/update/{id}', 'ClientController@updateClient')->name('update');


        Route::post('/my_clients/assign_partner/{client_id}', 'ClientController@assign_partner')->name('assign_partner');
        Route::get('/my_clients/delete_partner/{client}/{partner_id}', 'ClientController@delete_partner')->name('delete_partner');


        Route::post('/my_clients/add_note/{client}', 'ClientController@add_note')->name('add_note');
        Route::get('/my_clients/delete_note/{client}/{key}', 'ClientController@delete_note')->name('delete_note');

        Route::post('my_clients/upload_checklist/{client_id}', 'ClientController@uploadChecklist')->name('upload.checklist');
        Route::get('my_clients/delete_checklist/{list_id}', 'ClientController@deleteChecklist')->name('delete.checklist');
        Route::get('my_clients/download_checklist/{list_id}', 'ClientController@downloadChecklist')->name('download.checklist');
    }
);
