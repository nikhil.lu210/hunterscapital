<?php

// Customer Routes
Route::group([
    'prefix' => 'client', //URL
    'as' => 'client.', //Route
    'namespace' => 'Client', // Controller
],
    function(){
        Route::get('/risk_list', 'RiskListController@index')->name('risk_list.index');
        Route::get('/risk_list/create', 'RiskListController@create')->name('risk_list.create');
        Route::get('/risk_list/show/{id}', 'RiskListController@show')->name('risk_list.show');
        Route::get('/risk_list/destroy/{id}', 'RiskListController@destroy')->name('risk_list.destroy');


        Route::post('/risk_list/store', 'RiskListController@store')->name('risk_list.store');
        Route::post('/risk_list/update/{id}', 'RiskListController@update')->name('risk_list.update');
    }
);
