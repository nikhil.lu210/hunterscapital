<?php

// Customer Routes
Route::group([
    'prefix' => 'client', //URL
    'as' => 'client.', //Route
    'namespace' => 'Client', // Controller
],
    function(){
        Route::get('/fact_find', 'FactFindController@index')->name('fact_find.index');
        Route::post('/fact_find/search', 'FactFindController@searchFactFind')->name('fact_find.search');
        Route::get('/fact_find/searchupdate/{client_id}/{fact_module_id}', 'FactFindController@searchFactFindUpdate')->name('fact_find.searchupdate');

        Route::post('/fact_find/user/store/{client_id}/{fact_module_id}/{section_id}', 'FactFindController@userStoreFactFind')->name('fact_find.user_store');
    }
);
