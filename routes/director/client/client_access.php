<?php

// Customer Routes
Route::group([
    'prefix' => 'client', //URL
    'as' => 'client.', //Route
    'namespace' => 'Client', // Controller
],
    function(){
        Route::get('/client_access', 'ClientAccessController@index')->name('client_access.index');
        Route::get('/client_access/show/{id}', 'ClientAccessController@show')->name('client_access.show');
        Route::get('/client_access/refuse/{id}', 'ClientAccessController@refuse')->name('client_access.refuse');

        Route::post('/client_access/add_as_user/{id}', 'ClientAccessController@addAsUser')->name('client_access.add_as_user');
    }
);
