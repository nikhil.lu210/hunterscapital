<?php

// Customer Routes
Route::group([
    'prefix' => 'client', //URL
    'as' => 'client.', //Route
    'namespace' => 'Client', // Controller
],
    function(){
        Route::get('/grading_list', 'GradingListController@index')->name('grading_list.index');
        Route::get('/grading_list/create', 'GradingListController@create')->name('grading_list.create');
        Route::get('/grading_list/show/{id}', 'GradingListController@show')->name('grading_list.show');


        Route::post('/grading_list/store', 'GradingListController@storeGrading')->name('grading_list.store');
        Route::post('/grading_list/update/{id}', 'GradingListController@updateGrading')->name('grading_list.update');
    }
);
