<?php

// Customer Routes
Route::group([
    'prefix' => 'client', //URL
    'as' => 'client.', //Route
    'namespace' => 'Client', // Controller
],
    function(){
        Route::get('/document_center', 'DocumentCenterController@index')->name('document_center.index');
        Route::get('/document_center/create', 'DocumentCenterController@create')->name('document_center.create');
        // Route::get('/document_center/show/{id}', 'DocumentCenterController@show')->name('document_center.show');

        Route::get('/document_center/policies/{id}', 'DocumentCenterController@getPolicies')->name('document_getpolicies');
        Route::get('/document_center/download/{id}', 'DocumentCenterController@download')->name('document_center.download');
        Route::get('/document_center/destroy/{id}', 'DocumentCenterController@destroy')->name('document_center.destroy');


        Route::post('/document_center/store', 'DocumentCenterController@store')->name('document_center.store');
    }
);
