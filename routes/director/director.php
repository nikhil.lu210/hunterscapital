<?php

// Customer Routes
Route::group([
    'prefix' => 'director', // URL
    'as' => 'director.', // Route
    'namespace' => 'Director', // Controller
],
    function(){
        /* ==================================
        ============< Dashboard >============
        ===================================*/
        // Dashboard
        include_once 'dashboard/dashboard.php';



        /* ================================
        ============< Clients >============
        =================================*/
        // all_clients
        include_once 'client/client.php';
        // fact_find
        include_once 'client/fact_find.php';
        // quote_center
        // include_once 'client/quote_center.php';
        // letter_builder
        // include_once 'client/letter_builder.php';
        // document_center
        include_once 'client/document_center.php';
        // client_access
        // include_once 'client/client_access.php';
        // grading_list
        // include_once 'client/grading_list.php';
        // risk_list
        // include_once 'client/risk_list.php';



        /* =================================
        ============< Business >============
        ==================================*/
        // all_business
        include_once 'business/all_business.php';
        // turnover
        include_once 'business/turnover.php';
        // pre_scrutiny
        // include_once 'business/pre_scrutiny.php';
            // ************{ Add New Policy }************
            // Investment
            include_once 'business/add_new_policy/investment.php';
            // mortgage
            include_once 'business/add_new_policy/mortgage.php';
            // pension
            // include_once 'business/add_new_policy/pension.php';
            // protection
            include_once 'business/add_new_policy/protection.php';



        /* =================================
        ============< Comapny >=============
        ===================================*/
        // my_company
        include_once 'company/my_company.php';
        // user
        include_once 'company/user.php';
        // business_model
        include_once 'company/business_model.php';
        // task_manager
        include_once 'company/task_manager.php';
        // task_type
        include_once 'company/task_type.php';
            // ************{ Compliance }************
            // Online Account
            include_once 'company/compliance/online_account.php';
            // Complaints
            include_once 'company/compliance/complaint.php';
            // Promotions
            include_once 'company/compliance/promotion.php';
            // visits
            include_once 'company/compliance/visit.php';
            // file_checks
            // include_once 'company/compliance/file_check.php';
                // ************{ In & Out }************
                // Application
                // include_once 'company/compliance/in_and_out/application.php';
                // approval
                // include_once 'company/compliance/in_and_out/approval.php';
                // leaving
                // include_once 'company/compliance/in_and_out/leaving.php';
        // fact_find
        include_once 'company/fact_find.php';



        /* =================================
        ============< Setting >=============
        ===================================*/
        // my_account
        include_once 'setting/my_account.php';
        // my_credential
        // include_once 'setting/my_credential.php';



        /* =================================
        ============< Terms & Conditions >=============
        ===================================*/
        // cpd_register
        include_once 'term_and_condition/cpd_register.php';
        // cpd_analysis
        include_once 'term_and_condition/cpd_analysis.php';
        // financial_exam
        // include_once 'term_and_condition/financial_exam.php';
        // professional_body
        // include_once 'term_and_condition/professional_body.php';
        // qualification
        // include_once 'term_and_condition/qualification.php';
        // sps
        include_once 'term_and_condition/sps.php';



        /* =================================
        ============< Document Library >=============
        ===================================*/
        // document_library
        // include_once 'document_library/document_library.php';



        /* =================================
        ============< Auto Suitability Report >=============
        ===================================*/
        // auto_suitability_report
        // include_once 'auto_suitability_report/auto_suitability_report.php';



        /* =================================
        ============< GDPR >=============
        ===================================*/
        // gdpr
        // include_once 'gdpr/gdpr.php';
    }
);
