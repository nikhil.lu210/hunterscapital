<?php

// Customer Routes

Route::group([
    'prefix' => 'company', //URL
    'as' => 'company.', //Route
    'namespace' => 'Company', // Controller
],
    function(){
        Route::get('/users', 'UserController@index')->name('user.index');
        Route::get('/user/create', 'UserController@create')->name('user.create');

        Route::post('/user/store/{id}','UserController@userStore')->name('user.store');

        Route::get('/users/show/{id}', 'UserController@showUser')->name('user.show');
    }
);
