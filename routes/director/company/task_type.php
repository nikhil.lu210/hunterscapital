<?php

// Customer Routes
Route::group([
    'prefix' => 'company', //URL
    'as' => 'company.', //Route
    'namespace' => 'Company', // Controller
],
    function(){
        Route::get('/task_type', 'TaskTypeController@index')->name('task_type.index');
        Route::post('/task_type/store', 'TaskTypeController@store')->name('task_type.store');
        Route::post('/task_type/update', 'TaskTypeController@update')->name('task_type.update');
    }
);
