<?php

// Customer Routes
Route::group([
    'prefix' => 'company', //URL
    'as' => 'company.', //Route
    'namespace' => 'Company', // Controller
],
    function(){
        Route::get('/fact_find', 'FactFindController@index')->name('fact_find.index');
        Route::post('/fact_find/store', 'FactFindController@store')->name('fact_find.store');
        Route::post('/fact_find/update', 'FactFindController@update')->name('fact_find.update');

        Route::get('/fact_find/destroy/{id}', 'FactFindController@destroy')->name('fact_find.destroy');

        Route::post('/fact_find/store_new_section', 'FactFindController@storeNewSection')->name('fact_find.store_new_section');

        Route::post('/fact_find/edit_section', 'FactFindController@editSection')->name('fact_find.edit_section');
        Route::get('/fact_find/destroy_section/{module_id}/{section_id}', 'FactFindController@destroySection')->name('fact_find.destroy_section');


        Route::post('/fact_find/add_new_question', 'FactFindController@addNewQuestion')->name('fact_find.add_new_question');
        Route::post('/fact_find/update_question/{module_id}/{section_id}/{question_id}', 'FactFindController@updateQuestion')->name('fact_find.update_question');

        Route::get('/fact_find/destroy_question/{module_id}/{section_id}/{question_id}', 'FactFindController@destroyQuestion')->name('fact_find.destroy_question');
    }
);
