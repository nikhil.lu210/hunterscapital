<?php

// Compliance Routes
Route::group([
    'prefix' => 'company/compliance', //URL
    'as' => 'company.compliance.', //Route
    'namespace' => 'Company\Compliance', // Controller
],
    function(){
        Route::get('/visit', 'VisitController@index')->name('visit.index');
        Route::get('/visit/create', 'VisitController@create')->name('visit.create');
        Route::post('/visit/store', 'VisitController@store')->name('visit.store');
        Route::get('/visit/show/{id}', 'VisitController@show')->name('visit.show');
        Route::post('/visit/update/{id}', 'VisitController@update')->name('visit.update');
        Route::get('/visit/destroy/{id}', 'VisitController@destroy')->name('visit.destroy');

        Route::get('/visit/action/{id}', 'VisitController@actionCreate')->name('visit.action.create');
        Route::post('/visit/action/{id}', 'VisitController@actionStore')->name('visit.action.store');
        Route::get('/visit/action/destroy/{id}', 'VisitController@actionDestroy')->name('visit.action.destroy');

        Route::post('/visit/document_upload', 'VisitController@documentUpload')->name('visit.document_upload');
        Route::get('/visit/document_download/{id}', 'VisitController@documentDownload')->name('visit.document_download');
        Route::get('/visit/document_destroy/{id}', 'VisitController@documentDestroy')->name('visit.document_destroy');
    }
);
