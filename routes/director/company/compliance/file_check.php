<?php

// Compliance Routes
Route::group([
    'prefix' => 'company/compliance', //URL
    'as' => 'company.compliance.', //Route
    'namespace' => 'Company\Compliance', // Controller
],
    function(){
        Route::get('/file_check', 'FileCheckController@index')->name('file_check.index');
    }
);
