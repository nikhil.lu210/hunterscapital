<?php

// Compliance Routes
Route::group([
    'prefix' => 'company/compliance', //URL
    'as' => 'company.compliance.', //Route
    'namespace' => 'Company\Compliance', // Controller
],
    function(){
        Route::get('/complaint', 'ComplaintController@index')->name('complaint.index');
        Route::get('/complaint/create', 'ComplaintController@create')->name('complaint.create');
        Route::get('/complaint/show/{id}', 'ComplaintController@show')->name('complaint.show');
        Route::post('/complaint/update/{id}', 'ComplaintController@update')->name('complaint.update');

        Route::get('/complaint/destroy/{id}', 'ComplaintController@destroy')->name('complaint.destroy');

        Route::post('/complaint/store', 'ComplaintController@store')->name('complaint.store');
        Route::post('/complaint/uploadFile', 'ComplaintController@uploadFile')->name('complaint.uploadFile');

        Route::get('/complaint/destoryFile/{id}/{complaint_id}', 'ComplaintController@destoryFile')->name('complaint.destoryFile');
        Route::get('/complaint/downloadFile/{id}/{complaint_id}', 'ComplaintController@downloadFile')->name('complaint.downloadFile');
    }
);
