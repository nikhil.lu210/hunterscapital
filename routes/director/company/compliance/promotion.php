<?php

// Compliance Routes
Route::group([
    'prefix' => 'company/compliance', //URL
    'as' => 'company.compliance.', //Route
    'namespace' => 'Company\Compliance', // Controller
],
    function(){
        Route::get('/promotion', 'PromotionController@index')->name('promotion.index');
        Route::get('/promotion/create', 'PromotionController@create')->name('promotion.create');
        Route::post('/promotion/store', 'PromotionController@store')->name('promotion.store');
        Route::post('/promotion/document_upload', 'PromotionController@documentUpload')->name('promotion.document_upload');

        Route::get('/promotion/downloadFile/{id}/{promotion_id}', 'PromotionController@downloadFile')->name('promotion.downloadFile');
        Route::get('/promotion/destoryFile/{id}/{promotion_id}', 'PromotionController@destoryFile')->name('promotion.destoryFile');

        Route::get('/promotion/show/{id}', 'PromotionController@show')->name('promotion.show');
        Route::post('/promotion/update/{id}', 'PromotionController@update')->name('promotion.update');
    }
);
