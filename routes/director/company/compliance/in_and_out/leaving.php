<?php

// Compliance Routes
Route::group([
    'prefix' => 'company/compliance/in_and_out', //URL
    'as' => 'company.compliance.in_and_out.', //Route
    'namespace' => 'Company\Compliance\InAndOut', // Controller
],
    function(){
        Route::get('/leaving', 'LeavingController@index')->name('leaving.index');
    }
);
