<?php

// Compliance Routes
Route::group([
    'prefix' => 'company/compliance/in_and_out', //URL
    'as' => 'company.compliance.in_and_out.', //Route
    'namespace' => 'Company\Compliance\InAndOut', // Controller
],
    function(){
        Route::get('/application', 'ApplicationController@index')->name('application.index');
    }
);
