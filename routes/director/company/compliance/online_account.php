<?php

// Compliance Routes
Route::group([
    'prefix' => 'company/compliance', //URL
    'as' => 'company.compliance.', //Route
    'namespace' => 'Company\Compliance', // Controller
],
    function(){
        Route::get('/online_account', 'OnlineAccountController@index')->name('online_account.index');

        Route::post('/online_account/store', 'OnlineAccountController@store')->name('online_account.store');
        Route::post('/online_account/update', 'OnlineAccountController@update')->name('online_account.update');
        Route::post('/online_account/decision', 'OnlineAccountController@decision')->name('online_account.decision');

        Route::get('/online_account/destroy/{id}', 'OnlineAccountController@destroy')->name('online_account.destroy');
    }
);
