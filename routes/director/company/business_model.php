<?php

// Customer Routes
Route::group([
    'prefix' => 'company', //URL
    'as' => 'company.', //Route
    'namespace' => 'Company', // Controller
],
    function(){
        Route::get('/business_model', 'BusinessModelController@index')->name('business_model.index');
    }
);
