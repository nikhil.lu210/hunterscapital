<?php

// Customer Routes
Route::group([
    'prefix' => 'company', //URL
    'as' => 'company.', //Route
    'namespace' => 'Company', // Controller
],
    function(){
        Route::get('/task_manager', 'TaskManagerController@index')->name('task_manager.index');
        Route::post('/task_manager/store', 'TaskManagerController@store')->name('task_manager.store');
        Route::post('/task_manager/update', 'TaskManagerController@update')->name('task_manager.update');
    }
);
