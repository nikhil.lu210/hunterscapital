<?php

// Customer Routes
Route::group([
    'prefix' => 'company', //URL
    'as' => 'company.', //Route
    'namespace' => 'Company', // Controller
],
    function(){
        Route::get('/my_company', 'MyCompanyController@index')->name('my_company.index');
        Route::get('/my_company/edit', 'MyCompanyController@edit')->name('my_company.edit');

        Route::post('/my_company/update', 'MyCompanyController@update')->name('my_company.update');
    }
);
