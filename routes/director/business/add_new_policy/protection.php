<?php
Route::group([
    'prefix' => 'business/add_new_business', //URL
    'as' => 'business.add_new_policy.', //Route
    'namespace' => 'Business\AddNewPolicy', // Controller
],
    function(){
        Route::get('/protection', 'ProtectionController@index')->name('protection.index');
        Route::post('/protection/store', 'ProtectionController@store')->name('protection.store');

        Route::post('/protection/update/{id}', 'ProtectionController@update')->name('protection.update');
    }
);
