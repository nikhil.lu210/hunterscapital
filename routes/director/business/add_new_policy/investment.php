<?php
Route::group([
    'prefix' => 'business/add_new_business', //URL
    'as' => 'business.add_new_policy.', //Route
    'namespace' => 'Business\AddNewPolicy', // Controller
],
    function(){
        // Route::get('/investment', 'InvestmentController@index')->name('investment.index');

        // Route::post('/investment/store', 'InvestmentController@store')->name('investment.store');

        // Route::post('/investment/update/{id}', 'InvestmentController@update')->name('investment.update');


        //for provider request
        Route::post('/request_for_provider','InvestmentController@requestProvider')->name('request_for_provider');
        Route::post('/request_for_fund','InvestmentController@requestFund')->name('request_for_fund');
        Route::post('/request_for_trust','InvestmentController@requestTrust')->name('request_for_trust');
    }
);
