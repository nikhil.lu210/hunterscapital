<?php
Route::group([
    'prefix' => 'business/add_new_business', //URL
    'as' => 'business.add_new_policy.', //Route
    'namespace' => 'Business\AddNewPolicy', // Controller
],
    function(){
        Route::get('/mortgage', 'MortgageController@index')->name('mortgage.index');


        Route::post('/mortgage/store', 'MortgageController@store')->name('mortgage.store');
        Route::post('/mortgage/update/{id}', 'MortgageController@update')->name('mortgage.update');
    }
);
