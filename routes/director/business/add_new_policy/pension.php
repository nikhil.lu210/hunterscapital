<?php
Route::group([
    'prefix' => 'business/add_new_business', //URL
    'as' => 'business.add_new_policy.', //Route
    'namespace' => 'Business\AddNewPolicy', // Controller
],
    function(){
        Route::get('/pension', 'PensionController@index')->name('pension.index');


        Route::post('/pension/store', 'PensionController@store')->name('pension.store');
        Route::post('/pension/update/{id}', 'PensionController@update')->name('pension.update');
    }
);
