<?php
Route::group([
    'prefix' => 'business', //URL
    'as' => 'business.', //Route
    'namespace' => 'Business', // Controller
],
    function(){
        Route::get('/pre_scrutiny', 'PreScrutinyController@index')->name('pre_scrutiny.index');
    }
);
