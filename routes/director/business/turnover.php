<?php
Route::group([
    'prefix' => 'business', //URL
    'as' => 'business.', //Route
    'namespace' => 'Business', // Controller
],
    function(){
        Route::get('/turnovers', 'TurnoverController@index')->name('turnover.index');
        Route::get('/turnover/create', 'TurnoverController@create')->name('turnover.create');
        Route::post('/turnover/store', 'TurnoverController@store')->name('turnover.store');
        Route::get('/turnovers/show/{id}', 'TurnoverController@show')->name('turnover.show');
        Route::post('/turnovers/update/{id}', 'TurnoverController@update')->name('turnover.update');
    }
);
