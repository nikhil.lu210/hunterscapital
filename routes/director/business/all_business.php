<?php
Route::group([
    'prefix' => 'business', //URL
    'as' => 'business.', //Route
    'namespace' => 'Business', // Controller
],
    function(){
        Route::get('/all_business', 'AllBusinessController@index')->name('all_business.index');

        // Route::get('/all_business/show/investemnt/{id}', 'AllBusinessController@showInvestment')->name('all_business.showInvestment');

        Route::get('/all_business/show/mortgage/{id}', 'AllBusinessController@showMortgage')->name('all_business.showMortgage');

        // Route::get('/all_business/show/pension/{id}', 'AllBusinessController@showPension')->name('all_business.showPension');

        Route::get('/all_business/show/protection/{id}', 'AllBusinessController@showProtection')->name('all_business.showProtection');
    }
);
