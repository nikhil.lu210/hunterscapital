<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('login');
// });

Auth::routes();

Route::get('/', 'HomeController@index')->name('home');


/*===================================
========< superadmin Routes >=======
===================================*/
Route::group(
    [
        'middleware' => ['auth', 'superadmin'],
    ],
    function () {
        include_once 'superadmin/superadmin.php';
    }
);


/*===================================
========< admin Routes >=======
===================================*/
Route::group(
    [
        'middleware' => ['auth', 'admin'],
    ],
    function () {
        include_once 'admin/admin.php';
    }
);


/*===================================
========< director Routes >=======
===================================*/
Route::group(
    [
        'middleware' => ['auth', 'director'],
    ],
    function () {
        include_once 'director/director.php';
    }
);


/*===================================
========< client Routes >=======
===================================*/
Route::group(
    [
        'middleware' => ['auth', 'client'],
    ],
    function () {
        include_once 'client/client.php';
    }
);
