<?php

// Client Routes
Route::group([
    'prefix' => 'client', // URL
    'as' => 'client.', // Route
    'namespace' => 'Client', // Controller
],
    function(){
        /* ==================================
        ============< Dashboard >============
        ===================================*/
        // Dashboard
        include_once 'dashboard/dashboard.php';
    }
);
