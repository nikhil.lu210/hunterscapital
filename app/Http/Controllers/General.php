<?php

namespace App\Http\Controllers;

use DateTime;
use Illuminate\Http\Request;

class General extends Controller
{
    public static function dateFormatSlashToDash($date){
        $formated = explode("/",$date);
        return $formated[2]. "-" .$formated[1]."-".$formated[0];
    }
    public static function dateFormatDashToSlash($date){
        $formated = explode("-",$date);
        return $formated[2]. "/" .$formated[1]."/".$formated[0];
    }

    public static function reverseDateFormatSlashToDash($date){
        $formated = explode("/",$date);
        return $formated[2]. "-" .$formated[1]."-".$formated[0];
    }

    public static function reverseDateFormatDashToSlash($date){
        // $formated = explode("-",$date);
        // dd($formated);
        $datetime = new DateTime($date);

        return $datetime->format('d/m/Y');

        // return $formated[1]. "/" .$formated[2]."/".$formated[0];
    }
}
