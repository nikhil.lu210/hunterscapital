<?php

namespace App\Http\Controllers\Director\Dashboard;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Models\Director\Business\Policy\Policy;
use App\Models\Director\Clients\Client\Client;
use Carbon\Carbon;
use Mail;

use Auth;


class DashboardController extends Controller
{
    // Contstructor
    public function __construct(){

    }

    /**
     * ===================================================
     * ===============< Dashboard >=================
     * ===================================================
     */
    // Dashboard Index
    public function index(){
        $all_business = Policy::all()->count();
        $my_business = Policy::where('advisor', Auth::user()->id)->get()->count();

        $all_clients = Client::all()->count();
        $my_clients = Client::where('advisor', Auth::user()->id)->get()->count();
        return view("director.dashboard.index", compact(['all_business', 'my_business', 'all_clients', 'my_clients']));
    }

    /**
     * ===================================================
     * ===============< Send Mail >=================
     * ===================================================
     */
    // Send Mail sendMail
    public function sendMail(){
        $polices = Policy::with(['clientDetails' => function($query){
                                $query->select('id', 'email');
                            }])->select('id', 'client', 'policy_review_date', 'is_send_mail')->where('policy_review_date', '!=', NULL)
                            ->where('is_send_mail', 'No')->get();
        // dd($polices);

        foreach($polices as $policy){
            $review_date = new Carbon($policy->policy_review_date);
            $review_date->parse($review_date);

            $today = Carbon::now();

            $diff = $review_date->diffInDays($today);
            if($diff <= 10 && $policy->is_send_mail == 'No'){

                $data = [
                    'name'	 =>	"test name",
                    'email'	 => $policy->clientDetails->email,
                    'subject' => "Test Mail Information",
                    'password' => "asdfafafaf",
                ];

                Mail::send('emails.mail', $data, function($message) use ($data) {
                    $message->to($data['email']);
                    $message->subject($data['subject']);
                    // $message->from('sperrowmailtest@gmail.com');
                    $message->from('sperrowmailtest@gmail.com', 'HUNTER CAPITAL');
                });

                $policy->is_send_mail = "Yes";
                $policy->save();

            }
        }
        return response('done');
    }

}
