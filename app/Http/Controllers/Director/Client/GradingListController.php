<?php

namespace App\Http\Controllers\Director\Client;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Models\Director\Clients\ClientGrading\ClientGrading;

use Session;

class GradingListController extends Controller
{
    // Contstructor
    public function __construct(){

    }

    /**
     * ===================================================
     * ===============< Fact Find >=================
     * ===================================================
     */
    // Fact Find Index
    public function index(){
        $gradings = ClientGrading::with('clients')->get();

        return view("director.client.grading_list.index", compact(['gradings']));
    }


    // Fact Find create
    public function create(){
        return view("director.client.grading_list.create");
    }


    // Fact Find show
    public function show($id){
        $grading = ClientGrading::with('clients')->where('id', $id)->first();

        return view("director.client.grading_list.show", compact(['grading']));
    }

    public function storeGrading(Request $request){
        // dd($request->all());
        $this->validate($request, array(
            'greading_name' => 'required | string',
            'greading_status' => 'required',
            'greading_icon' => 'required | image | mimes:jpeg,png,jpg,svg | max:200',
        ));
        // dd($request->all());

        $grading = new ClientGrading();

        $grading->name = $request->greading_name;
        $grading->status = $request->greading_status;


        if($request->hasFile('greading_icon')){
            $image_data=file_get_contents($request->greading_icon);

            $encoded_image=base64_encode($image_data);

            $grading->icon = $encoded_image;
        }

        $grading->save();

        //confirmaiton here
        Session::flash('success', 'New Greading Added');

        return redirect()->route('director.client.grading_list.index');

    }


    public function updateGrading(Request $request, $id){
        // dd($request->all());
        $this->validate($request, array(
            'greading_name' => 'required | string',
            'greading_status' => 'required'
        ));

        if($request->hasFile('greading_icon')){
            $this->validate($request, array(
                'greading_icon' => 'required | image | mimes:jpeg,png,jpg,svg | max:200'
            ));
        }
        // dd($request->all());

        $grading = ClientGrading::find($id);

        $grading->name = $request->greading_name;
        $grading->status = $request->greading_status;


        if($request->hasFile('greading_icon')){
            $image_data=file_get_contents($request->greading_icon);

            $encoded_image=base64_encode($image_data);

            $grading->icon = $encoded_image;
        }

        $grading->save();

        return redirect()->route('director.client.grading_list.index');

    }
}
