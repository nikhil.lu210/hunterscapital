<?php

namespace App\Http\Controllers\Director\Client;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Director\Business\Policy\Policy;
use App\Models\Director\Clients\Client\Client;
use App\Models\Director\Clients\Document\Document;

use Session;

class DocumentCenterController extends Controller
{
    // Contstructor
    public function __construct(){

    }

    /**
     * ===================================================
     * ===============< Fact Find >=================
     * ===================================================
     */
    // Fact Find Index
    public function index(){
        $documents = Document::with(['documentClient', 'policy'])->get();
        // dd($documents);
        return view("director.client.document_center.index", compact(['documents']));
    }
    // Fact Find create
    public function create(){
        $clients = Client::where('client_status', 1)->get();
        return view("director.client.document_center.create", compact(['clients']));
    }


    public function store(Request $request)
    {
        // dd($request->all());
        $this->validate($request, array(
            'category' => 'required',
            'document' => 'required | max:50024'
        ));

        if($request->client){
            $this->validate($request, array(
                'policy' => 'required'
            ));
        }

        $document = new Document();
        $document->category = $request->category;
        if($request->client) $document->client = $request->client;
        if($request->policy) $document->client_policy = $request->policy;

        if($request->note) $document->description = $request->note;

        $document->file = $request->file('document')->store('documentCenter', 'public');
        $document->file_name = $request->files->get('document')->getClientOriginalName();

        $document->save();

        //confirmaiton here
        Session::flash('success', 'New Doc File Uploaded...');

        if($request->client_page == 'on') return redirect()->back();

        return redirect()->route('director.client.document_center.index');

    }

    public function getPolicies($id){

        $policies = Policy::where('client', $id)->get();

        return response()->json($policies);
    }

    public function download($id){

        $document = Document::find($id);

        $path = storage_path('app/public/'.$document->file);
        $headers = array(
                'Content-Type: application',
                );
        return \Response::download($path, $document->file_name, $headers);
    }


    public function destroy($id){
        $document = Document::find($id);

        unlink(storage_path('app/public/'.$document->file));

        $document->delete();

        //confirmaiton here
        Session::flash('danger', 'File has been Deleted');

        return redirect()->back();
    }
}
