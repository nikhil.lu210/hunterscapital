<?php

namespace App\Http\Controllers\Director\Client;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Director\Clients\RiskList\RiskList;

use Session;

class RiskListController extends Controller
{
    // Contstructor
    public function __construct(){

    }

    /**
     * ===================================================
     * ===============< risk list >=================
     * ===================================================
     */
    // risk list Index
    public function index(){

        $risk_lists = RiskList::all();
        return view("director.client.risk_list.index", compact(['risk_lists']));
    }
    // risk list create
    public function create(){
        return view("director.client.risk_list.create");
    }
    // risk list show
    public function show($id){
        $risk_list = RiskList::find($id);
        return view("director.client.risk_list.show", compact(['risk_list']));
    }


    // risk list store
    public function store(Request $request){
        // dd($request->all());

        $this->validate($request, array(
            'risk_name' => 'required | string',
            'risk_status' => 'required'
        ));
        // dd($request->all());

        $risk_list = new RiskList();

        $risk_list->name    = $request->risk_name;
        $risk_list->status  = $request->risk_status;

        $risk_list->save();

        //confirmaiton here
        Session::flash('success', 'New Risk Added');

        return redirect()->route('director.client.risk_list.index');
    }


    // risk list store
    public function update(Request $request, $id){
        // dd($request->all());

        $this->validate($request, array(
            'risk_name' => 'required | string',
            'risk_status' => 'required'
        ));
        // dd($request->all());

        $risk_list = RiskList::find($id);

        $risk_list->name    = $request->risk_name;
        $risk_list->status  = $request->risk_status;

        $risk_list->save();

        //confirm messege here

        return redirect()->route('director.client.risk_list.index');
    }


}
