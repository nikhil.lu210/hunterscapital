<?php

namespace App\Http\Controllers\Director\Client;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class LetterBuilderController extends Controller
{
    // Contstructor
    public function __construct(){

    }

    /**
     * ===================================================
     * ===============< Fact Find >=================
     * ===================================================
     */
    // Fact Find Index
    public function index(){
        return view("director.client.letter_builder.index");
    }
}
