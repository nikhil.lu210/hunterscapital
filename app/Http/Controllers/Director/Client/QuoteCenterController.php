<?php

namespace App\Http\Controllers\Director\Client;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class QuoteCenterController extends Controller
{
    // Contstructor
    public function __construct(){

    }

    /**
     * ===================================================
     * ===============< Quote Center >=================
     * ===================================================
     */
    // Quote Center Index
    public function index(){
        return view("director.client.quote_center.index");
    }
}
