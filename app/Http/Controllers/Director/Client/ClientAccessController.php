<?php

namespace App\Http\Controllers\Director\Client;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Director\Clients\Client\Client;
use Illuminate\Support\Facades\Hash;
use App\User;

use Session;

class ClientAccessController extends Controller
{
    // Contstructor
    public function __construct(){

    }

    /**
     * ===================================================
     * ===============< Fact Find >=================
     * ===================================================
     */
    // Fact Find Index
    public function index(){
        $clients = Client::all();
        return view("director.client.client_access.index", compact(['clients']));
    }
    // Fact Find show
    public function show($id){
        $client = Client::find($id);
        return view("director.client.client_access.show", compact(['client']));
    }

    public function addAsUser(Request $request, $id)
    {
        // dd($request->all(), $id);
        $this->validate($request, array(
            'email' => 'required | string | email | max:255 | unique:users',
        ));

        $client = Client::find($id);

        $user = new User();

        $user->role_id = 4;
        $user->client_id = $client->id;
        $user->saluation = $client->client_title;
        $user->forname = $client->client_forname;
        $user->surname = $client->client_surname;
        $user->date_of_birth = $client->date_of_birth;
        $user->email_address = $client->email;
        $user->email = $request->email;
        $user->password = Hash::make($request->password);
        $user->job_role = "Client Access";
        $user->user_status = true;

        $user->save();

        $client->client_access = true;
        $client->save();

        //email to client



        //confirmaiton here
        Session::flash('success', 'User Access Done');

        return redirect()->route('director.client.client_access.index');

    }


    public function refuse($id)
    {
        $client = Client::find($id);

        $user = User::where('client_id', $id)->first();

        $user->delete();

        $client->client_access = false;
        $client->save();

        //email to client

        //confirmaiton here
        Session::flash('danger', 'User Access Denied');

        return redirect()->route('director.client.client_access.index');
    }
}
