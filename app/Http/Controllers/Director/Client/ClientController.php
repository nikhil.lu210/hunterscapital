<?php

namespace App\Http\Controllers\Director\Client;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;


use App\Models\Director\Business\Policy\Policy;
use Illuminate\Support\Facades\Validator;
use App\Models\Director\Clients\Client\Client;
use App\Models\Director\Clients\ClientGrading\ClientGrading;
use App\Models\Director\Clients\Document\Document;
use App\Models\Director\Clients\Checklist;
use App\User;

use Auth;
use Session;
use DateTime;

class ClientController extends Controller
{
    // Contstructor
    public function __construct(){

    }

    /**
     * ===================================================
     * ===============< Clients >=================
     * ===================================================
     */
    // Clients Index
    public function index(){
        $clients = Client::all(); //with(['clientGrading'])->
        // dd($clients);
        return view("director.client.my_client.index", compact(['clients']));
    }

    // Clients create
    public function create(){
        $clients = Client::select('id','client_title','client_surname','client_forname')->get();
        // $gradings = ClientGrading::where('status', 1)->select('id','name')->get();
        $advisors = User::where('role_id', 3)->where('user_status', 1)->select('id','saluation','forname','surname')->get();

        return view("director.client.my_client.create", compact(['advisors', 'clients']));  //'gradings',
    }

    // Clients show
    public function show($id){

        $clients = Client::select('id','client_title','client_surname','client_forname')->where('id', '!=', $id)->get();
        // $gradings = ClientGrading::where('status', 1)->select('id','name')->get();
        $advisors = User::where('role_id', 3)->where('user_status', 1)->select('id','saluation','forname','surname')->get();
        $policies = Policy::where('client', $id)->get();

        $documents = Document::where('client', $id)->get();

        $client = Client::with('checklists')->where('id', $id)->first();

        return view("director.client.my_client.show", compact(['advisors', 'clients', 'client', 'policies', 'documents'])); //'gradings',
    }

    public function storeClient(Request $request){
        // dd($request->all());
        $this->validator($request->all());

        $client = new Client();

        $client->client_access = 0;
        if($request->client_title)
            $client->client_title = $request->client_title;

        if($request->client_forname)
            $client->client_forname = $request->client_forname;

        if($request->client_middle_name)
            $client->client_middle_name = $request->client_middle_name;

        if($request->client_surname)
            $client->client_surname = $request->client_surname;

        if($request->home_phone)
            $client->home_phone = $request->home_phone;

        if($request->mobile_phone)
            $client->mobile_phone = $request->mobile_phone;

        if($request->email)
            $client->email = $request->email;

        if($request->date_of_birth)
            $client->date_of_birth = $this->dateFormat($request->date_of_birth);

        if($request->county)
            $client->county = $request->county;

        if($request->client_nationality)
            $client->client_nationality = $request->client_nationality;

        if($request->town_city)
            $client->town_city = $request->town_city;

        if($request->post_code)
            $client->post_code = $request->post_code;

        if($request->house_no_name)
            $client->house_no_name = $request->house_no_name;

        if($request->address)
            $client->address = $request->address;

        if($request->corporate_name)
            $client->corporate_name = $request->corporate_name;

        if(isset($request->client_status))
            $client->client_status = $request->client_status;

        // if($request->grading)
        //     $client->grading = $request->grading;

        if($request->advisor)
            $client->advisor = $request->advisor;


        if($request->contactable_by)
            $client->contactable_by = json_encode($request->contactable_by);
        else
            $client->contactable_by = NULL;

        $client->save();



        //partner section
        if($request->partner){
            $client = Client::orderBy('id', 'desc')->first();
            $partners = array();
            $partners[] = $request->partner;

            $r_data = json_encode($partners);
            $client->partners = $r_data;
            $client->save();

            //partner section
            $partner = Client::find($request->partner);
            $partner_partners = json_decode($partner->partners, true);

            if($partner_partners == null){
                $partner_partners = array();
                $partner_partners[] = $client->id;
            } else {
                $partner_partners[] = $client->id;
            }
            $r_partner_data = json_encode($partner_partners);
            $partner->partners = $r_partner_data;

            $partner->save();

        }

        //confirmaiton here
        Session::flash('success', 'New Client Added...');

        return redirect()->route('director.client.index');
    }



    public function updateClient(Request $request, $id){
        // dd($request->all());
        $this->validator($request->all());

        $client = Client::find($id);

        $client->client_access = 0;
        if($request->client_title)
            $client->client_title = $request->client_title;

        if($request->client_forname)
            $client->client_forname = $request->client_forname;

        if($request->client_middle_name)
            $client->client_middle_name = $request->client_middle_name;

        if($request->client_surname)
            $client->client_surname = $request->client_surname;

        if($request->home_phone)
            $client->home_phone = $request->home_phone;

        if($request->mobile_phone)
            $client->mobile_phone = $request->mobile_phone;

        if($request->email)
            $client->email = $request->email;

        if($request->date_of_birth)
            $client->date_of_birth = $this->dateFormat($request->date_of_birth);

        if($request->county)
            $client->county = $request->county;

        if($request->client_nationality)
            $client->client_nationality = $request->client_nationality;

        if($request->town_city)
            $client->town_city = $request->town_city;

        if($request->post_code)
            $client->post_code = $request->post_code;

        if($request->house_no_name)
            $client->house_no_name = $request->house_no_name;

        if($request->address)
            $client->address = $request->address;

        if($request->corporate_name)
            $client->corporate_name = $request->corporate_name;

        if(isset($request->client_status))
            $client->client_status = $request->client_status;

        // if($request->grading)
        //     $client->grading = $request->grading;

        if($request->advisor)
            $client->advisor = $request->advisor;

        // if($request->partner){
        //     $partners = array();
        //     $partners[] = $request->partner;

        //     $r_data = json_encode($partners);
        //     $client->partners = $r_data;
        // }

        if($request->contactable_by)
            $client->contactable_by = json_encode($request->contactable_by);
        else
            $client->contactable_by = NULL;

        $client->save();

        //confirmaiton here
        Session::flash('success', 'Client Information Updated');

        return redirect()->route('director.client.index');
    }


    public function assign_partner(Request $request, $client_id)
    {
        $client = Client::find($client_id);

        $partners = json_decode($client->partners, true);

        if($partners == null){
            $partners = array();
            $partners[] = $request->partner;
        } else {
            $partners[] = $request->partner;
        }
        $r_data = json_encode($partners);
        $client->partners = $r_data;

        // dd($client);
        // dd($client->partners);

        $client->save();


        //there is partner relation custom
        $partner = Client::find($request->partner);
        if($partner != null){
            $partner_partners = json_decode($partner->partners, true);

            if($partner_partners == null){
                $partner_partners = array();
                $partner_partners[] = $client_id;
            } else {
                $partner_partners[] = $client_id;
            }
            $r_partner_data = json_encode($partner_partners);
            $partner->partners = $r_partner_data;

            $partner->save();
        }


        //confirmaiton here
        Session::flash('success', 'New Partner Assigned');

        return redirect()->back();
    }

    public function delete_partner( $client, $partner_id)
    {
        $client = Client::find($client);

        $partners = json_decode($client->partners, true);

        $key = array_search($partner_id, $partners);

        if(gettype( $key ) != 'boolean') array_splice($partners,$key,1);

        if(empty($partners)){
            $client->partners = null;
        } else{
            $r_partners = json_encode($partners);
            $client->partners = $r_partners;
        }

        $client->save();

        //partner delete relation custom
        $partner = Client::find($partner_id);
        if($partner != null){
            $partner_partners = json_decode($partner->partners, true);

            $p_key = array_search($client->id, $partner_partners);

            if(gettype( $p_key) != 'boolean' ) array_splice($partner_partners, $p_key, 1);

            if(empty($partner_partners)){
                $partner->partners = null;
            } else{
                $r_partner_partners = json_encode($partner_partners);
                $partner->partners = $r_partner_partners;
            }

            $partner->save();
        }

        //confirmaiton here
        Session::flash('danger', 'Partner Has Been Romoved');

        return redirect()->back();
    }

    public function add_note(Request $request, $client){
        $client = Client::find($client);
        $notes = json_decode($client->notes, true);

        $time = new DateTime(now());

        if($notes == null){
            $notes = array();
            $data = array(
                'date'  => date('d M Y'),
                'time'  => $time->format('H:i:s'),
                'note'  => $request->note,
                'admin' => Auth::user()->id
            );

            $notes[] = $data;
        } else{
            $data = array(
                'date'  => date('d M Y'),
                'time'  => $time->format('H:i:s'),
                'note'  => $request->note,
                'admin' => Auth::user()->id
            );
            $notes[] = $data;
        }

        $r_notes = json_encode($notes);
        $client->notes = $r_notes;

        $client->save();

        //confirmaiton here
        Session::flash('success', 'New Note Added');

        return redirect()->back();
    }



    public function delete_note($client, $key){
        $client = Client::find($client);

        $notes = json_decode($client->notes, true);
        if(array_key_exists($key, $notes)){
            array_splice($notes, $key, 1);
        }

        if(empty($notes)){
            $client->notes = null;
        } else{
            $r_notes = json_encode($notes);
            $client->notes = $r_notes;
        }

        $client->save();

        //confirmaiton here
        Session::flash('danger', 'Note Deleted Successfully.');

        return redirect()->back();
    }



    /**
     * upload checklist file
     */
    public function uploadChecklist(Request $request, $client_id)
    {
        // dd($request->all());
        $this->validate($request, array(
            'checklist_id' => 'required',
            'checklist_file' => 'required | max:50024'
        ));


        $checklist = new Checklist();

        $checklist->client_id = $client_id;
        $checklist->checklist_id = $request->checklist_id;

        $checklist->file = $request->file('checklist_file')->store('checklist', 'public');
        $checklist->file_name = $request->files->get('checklist_file')->getClientOriginalName();

        $checklist->save();

        //confirmaiton here
        Session::flash('success', 'New Checklist File Uploaded...');

        return redirect()->back();
    }


    /**
     * delete checklist file
     */
    public function deleteChecklist($list_id)
    {
        $checklist = Checklist::findOrFail($list_id);

        unlink(storage_path('app/public/'.$checklist->file));

        $checklist->delete();

        //confirmaiton here
        Session::flash('danger', 'Checklist File Deleted...');

        return redirect()->back();
    }


    /**
     * download checklist file
     */
    public function downloadChecklist($list_id)
    {
        $checklist = Checklist::findOrFail($list_id);

        $path = storage_path('app/public/'.$checklist->file);
        $headers = array(
                'Content-Type: application',
                );

        //confirmaiton here
        // Session::flash('info', 'Checklist File Downloaded...');

        return \Response::download($path, $checklist->file_name, $headers);
    }





    protected function validator(array $data)
    {
        return Validator::make($data, [
            'client_title' => ['required', 'string'],
            'client_status' => ['required', 'string'],
            'client_forname' => ['required', 'string'],
            'client_surname' => ['required', 'string'],
            'mobile_phone' => ['required', 'string'],
            'email' => ['required', 'string', 'email']
        ]);
    }

    protected function dateFormat($date){
        $formated = explode("/",$date);
        return $formated[2]. "-" .$formated[1]."-".$formated[0];
    }

    public static function reverseDateFormat($date){
        $formated = explode("-",$date);
        return $formated[2]. "/" .$formated[1]."/".$formated[0];
    }
}
