<?php

namespace App\Http\Controllers\Director\Client;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Director\Clients\Client\Client;
use App\Models\Director\Clients\FactFindData\FactFindData;
use App\Models\Director\Company\FactFindModule\FactFindModule;

use Session;

class FactFindController extends Controller
{
    // Contstructor
    public function __construct(){

    }

    /**
     * ===================================================
     * ===============< Fact Find >=================
     * ===================================================
     */
    // Fact Find Index
    public function index(){
        $clients = Client::where('client_status', 1)->get();
        $fact_modules = FactFindModule::all();
        return view("director.client.fact_find.index", compact(['clients', 'fact_modules']));
    }
    // Fact Find Search
    public function searchFactFind(Request $request){

        $this->validate($request, array(
            'client_id' => 'required',
            'factfind_id' => 'required',
        ));

        $client = Client::find($request->client_id);
        $fact_module = FactFindModule::find($request->factfind_id);
        $fact_data = FactFindData::where('client_id', $request->client_id)->where('factfind_id', $request->factfind_id)->first();

        return view("director.client.fact_find.show", compact(['client', 'fact_module', 'fact_data']));
    }

    public function userStoreFactFind(Request $request, $client_id, $fact_module_id, $section_id)
    {
        // dd($request->all());
        $client = Client::find($client_id);
        $fact_module = FactFindModule::find($fact_module_id);
        // dd($fact_module);

        $fact_data = FactFindData::where('client_id', $client_id)->where('factfind_id', $fact_module_id)->first();

        if($fact_data == null){
            $fact_data = new FactFindData();

            $m_factfinds = json_decode($fact_module->factfinds, true);
            // dd($m_factfinds);
            $d_factfinds = array();
            foreach($m_factfinds as $key => $m_factfind){
                $d_factfinds[$key] = array();
                // dd($m_factfind);
                $m_questions = array();
                $m_questions = (array)$m_factfind['module'];
                $d_questions = array();
                foreach($m_questions as $list => $question){
                    $d_questions[$list] = null;
                }
                // dd($d_questions);
                $d_factfinds[$key] = $d_questions;
            }
            // dd($d_factfinds);
            $fact_data->client_id = $client_id;
            $fact_data->factfind_id = $fact_module_id;

            $j_factfinds = json_encode($d_factfinds);
            $fact_data->factfinds = $j_factfinds;

            $fact_data->save();

        }

        // dd($fact_data);
        $d_factfinds = json_decode($fact_data->factfinds, true);
        // dd($d_factfinds);
        if(array_key_exists($section_id, $d_factfinds)){
            $s_quesions = $d_factfinds[$section_id];
            // dd($s_quesions);
            // dd($d_factfinds, $request->question);
            foreach($request->question as $index => $question){
                $s_quesions[$index] = $question;
            }
            // dd($s_quesions);
            $d_factfinds[$section_id] = $s_quesions;

            // dd($d_factfinds);
            $j_factfinds = json_encode($d_factfinds);
            $fact_data->factfinds = $j_factfinds;
        } else {
            $s_quesions = array();
            // dd($s_quesions);
            // dd($d_factfinds, $request->question);
            foreach($request->question as $index => $question){
                $s_quesions[$index] = $question;
            }
            // dd($s_quesions);
            $d_factfinds[$section_id] = $s_quesions;

            // dd($d_factfinds);
            $j_factfinds = json_encode($d_factfinds);
            $fact_data->factfinds = $j_factfinds;
        }
        // dd($fact_data);

        $fact_data->save();

        //confirmaiton here
        Session::flash('success', 'New Fact Find Added');

        return redirect()->route('director.client.fact_find.searchupdate', ['client_id' => $client_id, 'fact_module_id' => $fact_module_id]);
    }

    public function searchFactFindUpdate($client_id, $fact_module_id)
    {
        $client = Client::find($client_id);
        $fact_module = FactFindModule::find($fact_module_id);
        $fact_data = FactFindData::where('client_id', $client_id)->where('factfind_id', $fact_module_id)->first();

        return view("director.client.fact_find.show", compact(['client', 'fact_module', 'fact_data']));
    }
}
