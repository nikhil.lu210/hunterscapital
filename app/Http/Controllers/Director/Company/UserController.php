<?php

namespace App\Http\Controllers\Director\Company;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;

use App\User;
use Mail;

class UserController extends Controller
{
    private $user;
    // Contstructor
    public function __construct(){
        $this->user = new User();
    }

    /**
     * ===================================================
     * ===============< User >=================
     * ===================================================
     */
    // User Index
    public function index(){
        $users = $this->user->with('role')->get();
        return view("director.company.user.index", compact(['users']));
    }
    // User Create
    public function create(){
        return view("director.company.user.create");
    }


    //store company new user with role
    public function userStore(Request $request, $id){
        // dd($request);

        if($id == 'store'){
            $user = new User();
            $this->validator($request->all(), $id)->validate();
        } else {
            $user = User::find($id);
            $this->validator($request->all(), $id)->validate();
        }

        // dd($user);


        if($request->forname)
            $user->forname = $request->forname;

        if($request->middle_name)
            $user->middle_name = $request->middle_name;

        if($request->surname)
            $user->surname = $request->surname;

        if($request->salutation)
            $user->saluation = $request->salutation;

        if($request->birthdate)
            $user->date_of_birth = date("Y-m-d", strtotime(str_replace('/', '-', $request->birthdate )));

        if($request->email_address)
            $user->email_address = $request->email_address;

        if($request->phone_number)
            $user->phone_number = $request->phone_number;

        if($request->mobile_number)
            $user->mobile_number = $request->mobile_number;

        if($request->fax_number)
            $user->fax_number = $request->fax_number;

        if($request->ni_number)
            $user->ni_number = $request->ni_number;

        if($request->pre_sale_type)
            $user->pre_sale_type = $request->pre_sale_type;

        if($request->fca_number)
            $user->fca_number = $request->fca_number;

        if($request->county_old)
            $user->county_old = $request->county_old;

        if($request->town_city)
            $user->town_city = $request->town_city;

        if($request->post_code)
            $user->post_code = $request->post_code;

        if($request->address)
            $user->address = $request->address;

        if($request->job_title)
            $user->job_title = $request->job_title;

        if($request->special)
            $user->specials = json_encode($request->special);

        if($request->basic)
            $user->basics = json_encode($request->basic);

        if($request->mortgage)
            $user->mortgages = json_encode($request->mortgage);

        if($request->lifetime)
            $user->lifetimes = json_encode($request->lifetime);

        if($request->other)
            $user->others = json_encode($request->other);

        if($request->heigher_level)
            $user->heigher_levels = json_encode($request->heigher_level);

        $professionals = array();

        if($request->pre_sale_100_percent)
            $professionals['pre_sale_100_percent'] = true;

        if($request->exclude_from_auto_filecheck)
            $professionals['exclude_from_auto_filecheck'] = true;

        if($request->exclude_from_commision_check)
            $professionals['exclude_from_commision_check'] = true;

        if($request->exclude_from_adviser_list)
            $professionals['exclude_from_adviser_list'] = true;

        if($request->personal_finance)
            $professionals['personal_finance'] = true;

        if($request->institute_of_financial_planners)
            $professionals['institute_of_financial_planners'] = true;

        if($request->school_of_finance)
            $professionals['school_of_finance'] = true;

        if($request->institute_of_actuaries)
            $professionals['institute_of_actuaries'] = true;

        if($request->institute_of_bankers)
            $professionals['institute_of_bankers'] = true;

        if(!empty($professionals)) $user->professionals = json_encode($professionals);

        if($request->email)
            $user->email = $request->email;

        if($request->password)
            $user->password = Hash::make($request->password);

        if($request->job_role)
            $user->role_id = $request->job_role;

        if($request->job_role == 1) $user->job_role = 'Super Admin';
        if($request->job_role == 2) $user->job_role = 'Admin';
        if($request->job_role == 3) $user->job_role = 'Director';
        if($request->job_role == 4) $user->job_role = 'Client';

        if(isset($request->user_status)){
            // dd('sdfdf');
            $user->user_status = $request->user_status;
        }

        // there is accessing email
        if(isset($request->send_access_info) && $request->send_access_info == 'on'){
            $fullname = $request->forname . ' ' . $request->middle_name . ' ' . $request->surname;

            $data = [
                'name'	 =>	$fullname,
                'email'	 => $request->email,
                'subject' => "User Authentication Information",
                'password' => $request->password,
            ];

            Mail::send('emails.mail', $data, function($message) use ($data) {
                $message->to($data['email']);
                $message->subject($data['subject']);
                // $message->from('sperrowmailtest@gmail.com');
                $message->from('sperrowmailtest@gmail.com', 'HUNTER CAPITAL');
            });
        }


        $user->save();

        return redirect()->route('director.company.user.index');


    }

    public function showUser($id){
        $user = $this->user->find($id);
        return view("director.company.user.show",compact(['user']));
    }






    protected function validator(array $data, $id)
    {
        // dd($data);
        if($id == 'store'){
            return Validator::make($data, [
                'forname' => ['required', 'string', 'max:60'],
                'surname' => ['required', 'string', 'max:60'],
                'mobile_number' => ['required', 'string', 'max:15'],
                'email' => ['required', 'string', 'email', 'max:100', 'unique:users'],
                'email_address' => ['required', 'string', 'email', 'max:100'],
                'job_role' => ['required'],
                'password' => ['required', 'string', 'min:8'],
            ]);
        }
        return Validator::make($data, [
            'forname' => ['required', 'string', 'max:60'],
            'surname' => ['required', 'string', 'max:60'],
            'mobile_number' => ['required', 'string', 'max:15'],
            'email_address' => ['required', 'string', 'email', 'max:100'],
            'job_role' => ['required']
        ]);
    }
}
