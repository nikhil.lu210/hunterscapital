<?php

namespace App\Http\Controllers\Director\Company;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Director\Company\MyCompany\MyCompany;

class MyCompanyController extends Controller
{
    // Contstructor
    public function __construct(){

    }

    /**
     * ===================================================
     * ===============< My Company >=================
     * ===================================================
     */
    // My Company Index
    public function index(){
        $company = MyCompany::find(1);
        return view("director.company.my_company.index", compact(['company']));
    }
    // My Company edit
    public function edit(){
        $company = MyCompany::find(1);
        return view("director.company.my_company.edit", compact(['company']));
    }

    public function update(Request $request)
    {
        $this->validate($request, array(
            "company_name" => 'required | string',
            "company_website" => 'required | string',
            "mobile_number" => 'required | string',
            "vat_status" => 'required',
            "principal_email" => 'required | string | email',
            "filechecking_email" => 'required | string | email',
            "office_email" => 'required | string | email',
            "secondary_filechecking_email" => 'required | string | email',
            "county" => 'required | string',
            "town_city" => 'required | string',
            "postal_code" => 'required | string',
            "area_location" => 'required | string',
            "house_plot_no" => 'required | string',
            "address" => 'required | string',
            "fca_number" => 'required | string',
            "financial_year_end" => 'required',
            "pii_renewal_date" => 'required',
            "company_footer" => 'required | string'
        ));

        if($request->hasFile('company_logo')){
            $this->validate($request, array(
                "company_logo" => 'required | max:1024'
            ));
        }


        $company = MyCompany::find(1);

        if($request->company_name)
            $company->company_name = $request->company_name;
        if($request->company_website)
            $company->company_website = $request->company_website;
        if($request->phone_number)
            $company->phone_number = $request->phone_number;
        if($request->mobile_number)
            $company->mobile_number = $request->mobile_number;
        if($request->vat_status)
            $company->vat_status = $request->vat_status;
        if($request->principal_email)
            $company->principal_email = $request->principal_email;
        if($request->filechecking_email)
            $company->filechecking_email = $request->filechecking_email;
        if($request->office_email)
            $company->office_email = $request->office_email;
        if($request->secondary_filechecking_email)
            $company->secondary_filechecking_email = $request->secondary_filechecking_email;
        if($request->county)
            $company->county = $request->county;
        if($request->town_city)
            $company->town_city = $request->town_city;
        if($request->postal_code)
            $company->postal_code = $request->postal_code;
        if($request->area_location)
            $company->area_location = $request->area_location;
        if($request->house_plot_no)
            $company->house_plot_no = $request->house_plot_no;
        if($request->address)
            $company->address = $request->address;
        if($request->fca_number)
            $company->fca_number = $request->fca_number;
        if($request->financial_year_end)
            $company->financial_year_end = $this->dateFormat($request->financial_year_end);
        if($request->pii_renewal_date)
            $company->pii_renewal_date = $this->dateFormat($request->pii_renewal_date);
        if($request->business)
            $company->business = json_encode($request->business);
        if($request->company_footer)
            $company->company_footer = $request->company_footer;


        if($request->hasFile('company_logo')){
            $image_data=file_get_contents($request->company_logo);

            $encoded_image=base64_encode($image_data);

            $company->company_logo = $encoded_image;
        }

        $company->save();

        // confirmation

        return redirect()->route('director.company.my_company.index');

    }


    protected function dateFormat($date){
        $formated = explode("/",$date);
        return $formated[2]. "-" .$formated[1]."-".$formated[0];
    }

    public static function reverseDateFormat($date){
        $formated = explode("-",$date);
        return $formated[2]. "/" .$formated[1]."/".$formated[0];
    }
}
