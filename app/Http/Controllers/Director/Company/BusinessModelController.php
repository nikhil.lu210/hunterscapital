<?php

namespace App\Http\Controllers\Director\Company;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class BusinessModelController extends Controller
{
    // Contstructor
    public function __construct(){

    }

    /**
     * ===================================================
     * ===============< Business Model >=================
     * ===================================================
     */
    // Business Model Index
    public function index(){
        return view("director.company.business_model.index");
    }
}
