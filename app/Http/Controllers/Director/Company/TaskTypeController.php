<?php

namespace App\Http\Controllers\Director\Company;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Models\Director\Company\TaskType\TaskType;

class TaskTypeController extends Controller
{
    private $task_type;
    // Contstructor
    public function __construct(){
        $this->task_type = new TaskType();
    }

    /**
     * ===================================================
     * ===============< Task Type >=================
     * ===================================================
     */
    // Task Type Index
    public function index(){
        $task_types = $this->task_type->all();
        return view("director.company.task_type.index", compact('task_types'));
    }

    // Task Type Store
    public function store(Request $request){

        // dd($request);

        $this->validate($request, [
            'title'     =>      'required | string | max:191'
        ]);

        $data = new TaskType();

        $data->title            =   $request->title;
        $data->description      =   $request->description;
        if($request->status)
            $data->status = false;

        $data->save();

        return redirect()->route('director.company.task_type.index');
    }

    // Task Type update
    public function update(Request $request){
        if($request->title){
            $this->validate($request, [
                'title'     =>      'required | string | max:191'
            ]);
        }

        $data = TaskType::find($request->id);

        $data->title            =   $request->title;
        $data->description      =   $request->description;
        if($request->status == 'on')
            $data->status = false;
        else
            $data->status = true;

        $data->save();

        return redirect()->back();
    }
}
