<?php

namespace App\Http\Controllers\Director\Company\Compliance;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Controllers\General;
use App\Models\Director\Clients\Client\Client;
use App\Models\Director\Company\Compliance\Complaint\Complaint;
use App\Models\SuperAdmin\Assessor\Assessor;
use App\User;

class ComplaintController extends Controller
{
    // Contstructor
    public function __construct(){

    }

    /**
     * ===================================================
     * ===============< Complaint Controller >============
     * ===================================================
     */
    // Complaint Index
    public function index(){
        $complaints = Complaint::with(['advisorDetails', 'clientDetails', 'assessorDetails'])->get();

        return view("director.company.compliance.complaint.index", compact(['complaints']));
    }
    // Complaint create
    public function create(){
        $advisors = User::where('role_id', 3)->where('user_status', 1)->get();
        $clients = Client::where('client_status', 1)->get();
        $assessors = Assessor::all();


        return view("director.company.compliance.complaint.create", compact(['advisors', 'clients','assessors']));
    }
    // Complaint show
    public function show($id){
        $advisors = User::where('role_id', 3)->where('user_status', 1)->get();
        $clients = Client::where('client_status', 1)->get();
        $assessors = Assessor::all();
        $complaint = Complaint::with(['advisorDetails', 'clientDetails', 'assessorDetails'])->where('id', $id)->first();

        // dd($advisors, $clients, $assessors, $complaint);


        return view("director.company.compliance.complaint.show", compact(['advisors', 'clients','assessors', 'complaint']));
    }


    public function store(Request $request)
    {
        //validation here

        $complaint = new Complaint();

        if($request->advisor_id)
            $complaint->advisor  = $request->advisor_id;

        if($request->business_type)
            $complaint->business_type  = $request->business_type;

        if($request->complaint_type)
            $complaint->complaint_type  = $request->complaint_type;

        if($request->client_advice_date)
            $complaint->client_advice_date  = General::dateFormatSlashToDash($request->client_advice_date);

        if($request->client)
            $complaint->client  = $request->client;

        if($request->assessor)
            $complaint->assessor  = $request->assessor;

        if($request->client_received)
            $complaint->client_received  = General::dateFormatSlashToDash($request->client_received);

        if($request->internally_received)
            $complaint->internally_received  = General::dateFormatSlashToDash($request->internally_received);

        if($request->confirmation_sent)
            $complaint->confirmation_sent  = General::dateFormatSlashToDash($request->confirmation_sent);

        if($request->verbar_complaint)
            $complaint->verbar_complaint  = ($request->verbar_complaint == 'on') ? true : false;

        if($request->details)
            $complaint->details  = $request->details;

        if($request->four_week_later)
            $complaint->four_week_later  = General::dateFormatSlashToDash($request->four_week_later);

        if($request->eight_week_later)
            $complaint->eight_week_later  = General::dateFormatSlashToDash($request->eight_week_later);

        if($request->final_response)
            $complaint->final_response  = General::dateFormatSlashToDash($request->final_response);

        if($request->to_assessor)
            $complaint->to_assessor  = General::dateFormatSlashToDash($request->to_assessor);

        if($request->from_assessor)
            $complaint->from_assessor  = General::dateFormatSlashToDash($request->from_assessor);

        if($request->to_client_assessor)
            $complaint->to_client_assessor  = General::dateFormatSlashToDash($request->to_client_assessor);

        if($request->redress_amount_assessor)
            $complaint->redress_amount_assessor  = $request->redress_amount_assessor;

        if($request->assessor_decision)
            $complaint->assessor_decision  = $request->assessor_decision;

        if($request->assessor_note)
            $complaint->assessor_note  = $request->assessor_note;

        if($request->to_adjudicator)
            $complaint->to_adjudicator  = General::dateFormatSlashToDash($request->to_adjudicator);

        if($request->from_adjudicator)
            $complaint->from_adjudicator  = General::dateFormatSlashToDash($request->from_adjudicator);

        if($request->to_client_adjudicator)
            $complaint->to_client_adjudicator  = General::dateFormatSlashToDash($request->to_client_adjudicator);

        if($request->redress_amount_adjudicator)
            $complaint->redress_amount_adjudicator  = $request->redress_amount_adjudicator;

        if($request->adjudicator_decision)
            $complaint->adjudicator_decision  = $request->adjudicator_decision;

        if($request->adjudicator_note)
            $complaint->adjudicator_note  = $request->adjudicator_note;

        if($request->to_ombudsman)
            $complaint->to_ombudsman  = General::dateFormatSlashToDash($request->to_ombudsman);

        if($request->from_ombudsman)
            $complaint->from_ombudsman  = General::dateFormatSlashToDash($request->from_ombudsman);

        if($request->to_client_ombudsman)
            $complaint->to_client_ombudsman  = General::dateFormatSlashToDash($request->to_client_ombudsman);

        if($request->redress_amount_ombudsman)
            $complaint->redress_amount_ombudsman  = $request->redress_amount_ombudsman;

        if($request->ombudsman_decision)
            $complaint->ombudsman_decision  = $request->ombudsman_decision;

        if($request->ombudsman_note)
            $complaint->ombudsman_note  = $request->ombudsman_note;


        $complaint->save();

        return redirect()->route('director.company.compliance.complaint.index');

    }



    // Complaint update
    public function update(Request $request, $id){
        $complaint = Complaint::find($id);

        if($request->advisor_id)
            $complaint->advisor  = $request->advisor_id;

        if($request->business_type)
            $complaint->business_type  = $request->business_type;

        if($request->complaint_type)
            $complaint->complaint_type  = $request->complaint_type;

        if($request->client_advice_date)
            $complaint->client_advice_date  = General::dateFormatSlashToDash($request->client_advice_date);

        if($request->client)
            $complaint->client  = $request->client;

        if($request->assessor)
            $complaint->assessor  = $request->assessor;

        if($request->client_received)
            $complaint->client_received  = General::dateFormatSlashToDash($request->client_received);

        if($request->internally_received)
            $complaint->internally_received  = General::dateFormatSlashToDash($request->internally_received);

        if($request->confirmation_sent)
            $complaint->confirmation_sent  = General::dateFormatSlashToDash($request->confirmation_sent);

        if($request->verbar_complaint)
            $complaint->verbar_complaint  = ($request->verbar_complaint == 'on')? true:false;

        if($request->details)
            $complaint->details  = $request->details;

        if($request->four_week_later)
            $complaint->four_week_later  = General::dateFormatSlashToDash($request->four_week_later);

        if($request->eight_week_later)
            $complaint->eight_week_later  = General::dateFormatSlashToDash($request->eight_week_later);

        if($request->final_response)
            $complaint->final_response  = General::dateFormatSlashToDash($request->final_response);

        if($request->to_assessor)
            $complaint->to_assessor  = General::dateFormatSlashToDash($request->to_assessor);

        if($request->from_assessor)
            $complaint->from_assessor  = General::dateFormatSlashToDash($request->from_assessor);

        if($request->to_client_assessor)
            $complaint->to_client_assessor  = General::dateFormatSlashToDash($request->to_client_assessor);

        if($request->redress_amount_assessor)
            $complaint->redress_amount_assessor  = $request->redress_amount_assessor;

        if($request->assessor_decision)
            $complaint->assessor_decision  = $request->assessor_decision;

        if($request->assessor_note)
            $complaint->assessor_note  = $request->assessor_note;

        if($request->to_adjudicator)
            $complaint->to_adjudicator  = General::dateFormatSlashToDash($request->to_adjudicator);

        if($request->from_adjudicator)
            $complaint->from_adjudicator  = General::dateFormatSlashToDash($request->from_adjudicator);

        if($request->to_client_adjudicator)
            $complaint->to_client_adjudicator  = General::dateFormatSlashToDash($request->to_client_adjudicator);

        if($request->redress_amount_adjudicator)
            $complaint->redress_amount_adjudicator  = $request->redress_amount_adjudicator;

        if($request->adjudicator_decision)
            $complaint->adjudicator_decision  = $request->adjudicator_decision;

        if($request->adjudicator_note)
            $complaint->adjudicator_note  = $request->adjudicator_note;

        if($request->to_ombudsman)
            $complaint->to_ombudsman  = General::dateFormatSlashToDash($request->to_ombudsman);

        if($request->from_ombudsman)
            $complaint->from_ombudsman  = General::dateFormatSlashToDash($request->from_ombudsman);

        if($request->to_client_ombudsman)
            $complaint->to_client_ombudsman  = General::dateFormatSlashToDash($request->to_client_ombudsman);

        if($request->redress_amount_ombudsman)
            $complaint->redress_amount_ombudsman  = $request->redress_amount_ombudsman;

        if($request->ombudsman_decision)
            $complaint->ombudsman_decision  = $request->ombudsman_decision;

        if($request->ombudsman_note)
            $complaint->ombudsman_note  = $request->ombudsman_note;


        $complaint->save();

        return redirect()->route('director.company.compliance.complaint.index');
    }



    public function uploadFile(Request $request)
    {
        // dd($request->all());
        if($request->hasFile('document')){
            $this->validate($request,array(
                'document'    =>  'max:50024',
            ));
        }
        // dd($request->all());

        $complaint = Complaint::find($request->id);

        if($request->hasFile('document')){
            $document = $request->file('document')->store('complaintDocuments', 'public');
            $filename = $request->files->get('document')->getClientOriginalName();

            $temp = json_decode($complaint->documents, true);
            $temp[] = array(
                        'name'      => $filename,
                        'document'  => $document,
                        'date'      => date('d M Y'),
                        'note'      => $request->note
                    );
            $r_temp = json_encode($temp);
            $complaint->documents = $r_temp;

            // $request->avatar->move(public_path('uploaded_image'), $path);

            // $course->avatar = $path;
        }

        $complaint->save();

        //confirmation here

        return redirect()->back();
    }

    public function destoryFile($id, $complaint_id)
    {
        $complaint = Complaint::find($complaint_id);

        $documents = json_decode($complaint->documents, true);

        $deletable_file = $documents[$id];
        unlink(storage_path('app/public/'.$deletable_file['document']));

        array_splice($documents, $id, 1);

        $reverse_documents = json_encode($documents);

        $complaint->documents = $reverse_documents;

        $complaint->save();

        return redirect()->back();

    }


    public function downloadFile($id, $complaint_id)
    {
        $complaint = Complaint::find($complaint_id);

        $documents = json_decode($complaint->documents, true);
        $dowloadable_file = $documents[$id];

        $path = storage_path('app/public/'.$dowloadable_file['document']);
        $headers = array(
                'Content-Type: application',
                );
        return \Response::download($path, $dowloadable_file['name'], $headers);
    }


    public function destroy($id){
        $complaint = Complaint::find($id);

        $documents = json_decode($complaint->documents, true);

        for($i =0; $i<sizeof($documents); $i++){
            unlink(storage_path('app/public/'.$documents[$i]['document']));
        }

        $complaint->delete();

        return redirect()->back();
    }
}


