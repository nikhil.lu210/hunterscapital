<?php

namespace App\Http\Controllers\Director\Company\Compliance\InAndOut;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class LeavingController extends Controller
{
    // Contstructor
    public function __construct(){

    }

    /**
     * ===================================================
     * ===============< Leaving Controller >============
     * ===================================================
     */
    // leaving Index
    public function index(){
        return view("director.company.compliance.in_and_out.leaving.index");
    }
}
