<?php

namespace App\Http\Controllers\Director\Company\Compliance\InAndOut;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ApprovalController extends Controller
{
    // Contstructor
    public function __construct(){

    }

    /**
     * ===================================================
     * ===============< Approval Controller >============
     * ===================================================
     */
    // approval Index
    public function index(){
        return view("director.company.compliance.in_and_out.approval.index");
    }
}
