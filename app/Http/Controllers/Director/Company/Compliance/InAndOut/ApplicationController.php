<?php

namespace App\Http\Controllers\Director\Company\Compliance\InAndOut;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ApplicationController extends Controller
{
    // Contstructor
    public function __construct(){

    }

    /**
     * ===================================================
     * ===============< Application Controller >============
     * ===================================================
     */
    // Application Index
    public function index(){
        return view("director.company.compliance.in_and_out.application.index");
    }
}
