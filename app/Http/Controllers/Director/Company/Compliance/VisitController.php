<?php

namespace App\Http\Controllers\Director\Company\Compliance;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Controllers\General;
use App\Models\Director\Company\Compliance\Visit\Visit;
use App\Models\Director\Company\Compliance\Visit\VisitAction\VisitAction;
use App\Models\Director\Company\Compliance\Visit\VisitFile\VisitFile;
use App\Models\SuperAdmin\Author\Author;

class VisitController extends Controller
{
    // Contstructor
    public function __construct(){

    }

    /**
     * ===================================================
     * ===============< Visit Controller >============
     * ===================================================
     */
    // Visit Index
    public function index(){
        $visits = Visit::with([
                    'signedOfBy',
                    'visitActions',
                    'visitFiles',
                ])->get();
        // dd($visits);
        return view("director.company.compliance.visit.index", compact(['visits']));
    }
    // Visit create
    public function create(){
        $authors = Author::all();
        return view("director.company.compliance.visit.create", compact(['authors']));
    }
    // Visit store
    public function store(Request $request){

        $visit = new Visit();

        if($request->visit_date)
            $visit->visit_date = General::dateFormatSlashToDash($request->visit_date);

        if($request->signed_off_on)
            $visit->signed_off_on = General::dateFormatSlashToDash($request->signed_off_on);

        if($request->nature_of_visit)
            $visit->nature_of_visit = $request->nature_of_visit;

        if($request->signed_off_by)
            $visit->signed_off_by = $request->signed_off_by;

        if($request->grading)
            $visit->grading = $request->grading;

        $visit->save();

        return redirect()->route('director.company.compliance.visit.index');
    }
    // Visit show
    public function show($id){
        $visit = Visit::with([
            'signedOfBy',
        ])->where('id', $id)->first();
        $authors = Author::all();
        // dd($visit);
        return view("director.company.compliance.visit.show", compact(['visit', 'authors']));
    }
    // Visit update
    public function update(Request $request, $id){

        $visit = Visit::find($id);

        if($request->visit_date)
            $visit->visit_date = General::dateFormatSlashToDash($request->visit_date);

        if($request->signed_off_on)
            $visit->signed_off_on = General::dateFormatSlashToDash($request->signed_off_on);

        if($request->nature_of_visit)
            $visit->nature_of_visit = $request->nature_of_visit;

        if($request->signed_off_by)
            $visit->signed_off_by = $request->signed_off_by;

        if($request->grading)
            $visit->grading = $request->grading;

        $visit->save();

        return redirect()->route('director.company.compliance.visit.index');
    }


    // Visit action create
    public function actionCreate($id){
        $visit = Visit::find($id);
        // dd($visit);
        return view("director.company.compliance.visit.action", compact(['visit']));
    }

    // Visit action store
    public function actionStore(Request $request, $id){

        $action = new VisitAction();

        $action->visit_id = $id;

        if($request->agenda)
            $action->agenda = $request->agenda;

        if($request->outcome)
            $action->outcome = $request->outcome;

        if($request->deadline)
            $action->deadline = General::dateFormatSlashToDash($request->deadline);

        if($request->completed_on)
            $action->completed_on = General::dateFormatSlashToDash($request->completed_on);

        if($request->issue)
            $action->issue = $request->issue;

        if($request->related_action)
            $action->related_action = $request->related_action;

        if($request->note)
            $action->note = $request->note;

        // dd($action);

        $action->save();

        return redirect()->route('director.company.compliance.visit.index');

    }

    // Visit action Destry
    public function actionDestroy($id){
        $visit = VisitAction::find($id);
        $visit->delete();
        return redirect()->back();
    }


    public function documentUpload(Request $request){
        // dd($request);

        if($request->hasFile('document')){
            $this->validate($request,array(
                'document'    =>  'max:50024',
            ));
        }

        $doc = new VisitFile();

        $doc->visit_id = $request->visit_id;
        $doc->description = $request->note;

        if($request->hasFile('document')){
            $document = $request->file('document')->store('visitDocuments', 'public');
            $filename = $request->files->get('document')->getClientOriginalName();

            $doc->file_type = $filename;
            $doc->file = $document;
        }

        $doc->save();

        //Confirmation Message

        return redirect()->back();
    }

    public function documentDownload($id){
        $doc = VisitFile::find($id);
        // dd($doc);

        $path = storage_path('app/public/'.$doc->file);
        $headers = array(
                        'Content-Type: application',
                    );
        return \Response::download($path, $doc->file_type, $headers);
    }

    public function documentDestroy($id){

        $doc = VisitFile::find($id);
        // dd($doc);

        unlink(storage_path('app/public/'.$doc->file));
        $doc->delete();

        //Confirmation Message

        return redirect()->back();
    }


    public function destroy($id){
        $visit = Visit::with(['visitActions','visitFiles'])->where('id', $id)->first();

        foreach($visit->visitActions as $action){
            $d_action = VisitAction::find($action->id);
            $d_action->delete();
        }

        foreach($visit->visitFiles as $file){
            $d_file = VisitFile::find($file->id);
            unlink(storage_path('app/public/'.$d_file->file));
            $d_file->delete();
        }

        $visit->delete();

        return redirect()->back();

    }

}
