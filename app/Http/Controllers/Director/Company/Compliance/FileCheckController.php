<?php

namespace App\Http\Controllers\Director\Company\Compliance;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class FileCheckController extends Controller
{
    // Contstructor
    public function __construct(){

    }

    /**
     * ===================================================
     * ===============< File Check Controller >============
     * ===================================================
     */
    // File Check Index
    public function index(){
        return view("director.company.compliance.file_check.index");
    }
}
