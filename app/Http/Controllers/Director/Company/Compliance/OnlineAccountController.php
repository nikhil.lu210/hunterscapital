<?php

namespace App\Http\Controllers\Director\Company\Compliance;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Director\Company\Compliance\OnlineAccount\OnlineAccount;

class OnlineAccountController extends Controller
{
    // Contstructor
    public function __construct(){

    }

    /**
     * ===================================================
     * ===============< Online Account Controller >============
     * ===================================================
     */
    // Online Account Index
    public function index(){
        $online_accounts = OnlineAccount::all();
        return view("director.company.compliance.online_account.index", compact(['online_accounts']));
    }


    /**
     * Online account store
     *
     * @return dirctor\company\compliance\onlineaccountindex
     */

     public function store(Request $request)
     {
         $this->validate($request, array(
            'site'      =>  'required | string',
            'email'     =>  'required | email',
            'password'  =>  'required | string | min:8'
         ));


        $online_account = new OnlineAccount();

        $online_account->site       = $request->site;
        $online_account->email      = $request->email;
        $online_account->password   = $request->password;

        $online_account->decision   = "Pending";

        $online_account->save();

        //confirmation notification

        return redirect()->route('director.company.compliance.online_account.index');
     }



    /**
     * Online account update
     *
     * @return dirctor\company\compliance\onlineaccountindex
     */

     public function update(Request $request)
     {
         $this->validate($request, array(
            'site'      =>  'required | string',
            'email'     =>  'required | email',
            'password'  =>  'required | string | min:8'
         ));


        $online_account = OnlineAccount::find($request->id);

        $online_account->site       = $request->site;
        $online_account->email      = $request->email;
        $online_account->password   = $request->password;

        $online_account->save();

        //confirmation notification

        return redirect()->route('director.company.compliance.online_account.index');
     }



     /**
     * Online account update
     *
     * @return dirctor\company\compliance\onlineaccountindex
     */

     public function decision(Request $request)
     {

         if($request->decision_note){
            $this->validate($request, array(
                'decision_note'      =>  'required | string'
             ));
         }


        $online_account = OnlineAccount::find($request->id);

        $online_account->decision   = $request->decision;
        if($request->decision_note) $online_account->decision_note   = $request->decision_note;

        $online_account->site_checked = ($request->site_checked == 'on') ? true:false;

        $online_account->save();

        //confirmation notification

        return redirect()->route('director.company.compliance.online_account.index');
     }




    /**
     * Online account update
     *
     * @return dirctor\company\compliance\onlineaccountindex
     */

     public function destroy($id)
     {
         $online_account = OnlineAccount::find($id);
         $online_account->delete();

        //confirmation notification

        return redirect()->route('director.company.compliance.online_account.index');
     }


}
