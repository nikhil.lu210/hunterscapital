<?php

namespace App\Http\Controllers\Director\Company\Compliance;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Controllers\General;
use App\Models\Director\Company\Compliance\Promotion\Promotion;
use App\Models\SuperAdmin\Author\Author;
use App\User;

class PromotionController extends Controller
{
    // Contstructor
    public function __construct(){

    }

    /**
     * ===================================================
     * ===============< Promotion Controller >============
     * ===================================================
     */
    // Promotion Index
    public function index(){
        $promotions = Promotion::with([
                                    'requestedBy',
                                    'closedBy',
                                    'ifacAdvisor',
                                    'reviewedBy',
                                    'signedOfBy',
                                ])->get();

        return view("director.company.compliance.promotion.index", compact('promotions'));
    }


    // Promotion create
    public function create(){
        $authors = Author::all();
        $advisors = User::where('role_id', 3)->where('user_status', 1)->get();

        return view("director.company.compliance.promotion.create", compact(['authors', 'advisors']));
    }


    // Promotion Store
    public function store(Request $request){

        $promotion = new Promotion();

        if($request->promotion_category)
            $promotion->promotion_category = $request->promotion_category;

        if($request->promotion_type)
            $promotion->promotion_type = $request->promotion_type;

        if($request->promotion_medium)
            $promotion->promotion_medium = $request->promotion_medium;

        if($request->promotion_title)
            $promotion->promotion_title = $request->promotion_title;

        if($request->requested_date)
            $promotion->requested_date = General::dateFormatSlashToDash($request->requested_date);

        if($request->deadline)
            $promotion->deadline = General::dateFormatSlashToDash($request->deadline);

        if($request->promotion_closed)
            $promotion->promotion_closed = $request->promotion_closed;

        if($request->closed_by)
            $promotion->closed_by = $request->closed_by;

        if($request->closed_date)
            $promotion->closed_date = General::dateFormatSlashToDash($request->closed_date);

        if($request->overall_result)
            $promotion->overall_result = json_encode($request->overall_result);
        else
            $promotion->overall_result = NULL;

        if($request->note)
            $promotion->note = $request->note;

        if($request->requested_by)
            $promotion->requested_by = $request->requested_by;

        if($request->ifac_advisor)
            $promotion->ifac_advisor = $request->ifac_advisor;

        if($request->jurisdiction)
            $promotion->jurisdiction = $request->jurisdiction;

        if($request->product_area_covered)
            $promotion->product_area_covered = $request->product_area_covered;

        if($request->reviewer)
            $promotion->reviewer = $request->reviewer;

        if($request->reviewed_date)
            $promotion->reviewed_date = General::dateFormatSlashToDash($request->reviewed_date);

        if($request->signed_of_by)
            $promotion->signed_of_by = $request->signed_of_by;

        if($request->approval_date)
            $promotion->approval_date = General::dateFormatSlashToDash($request->approval_date);

        if($request->approval_to)
            $promotion->approval_to = General::dateFormatSlashToDash($request->approval_to);

        $promotion->save();

        return redirect()->route('director.company.compliance.promotion.index');

    }


    public function documentUpload(Request $request){

        $this->validate($request, array(
            'note' => 'required | string'
        ));

        if($request->hasFile('document')){
            $this->validate($request,array(
                'document'    =>  'max:50024',
            ));
        }

        $promotion = Promotion::find($request->hidden_promotion_id);

        // dd($promotion);

        if($request->hasFile('document')){
            $document = $request->file('document')->store('promotionDocuments', 'public');
            $filename = $request->files->get('document')->getClientOriginalName();

            $temp = json_decode($promotion->documents, true);
            $temp[] = array(
                        'name'      => $filename,
                        'document'  => $document,
                        'date'      => date('d M Y'),
                        'note'      => $request->note
                    );
            $r_temp = json_encode($temp);
            $promotion->documents = $r_temp;

            // $request->avatar->move(public_path('uploaded_image'), $path);

            // $course->avatar = $path;
        }

        $promotion->save();

        return redirect()->back();

    }


    public function destoryFile($id, $promotion_id)
    {
        $promotion = Promotion::find($promotion_id);

        $documents = json_decode($promotion->documents, true);

        $deletable_file = $documents[$id];
        unlink(storage_path('app/public/'.$deletable_file['document']));

        array_splice($documents, $id, 1);

        $reverse_documents = json_encode($documents);

        $promotion->documents = $reverse_documents;

        $promotion->save();

        return redirect()->back();

    }


    public function downloadFile($id, $promotion_id)
    {
        $promotion = Promotion::find($promotion_id);

        $documents = json_decode($promotion->documents, true);
        $dowloadable_file = $documents[$id];

        $path = storage_path('app/public/'.$dowloadable_file['document']);
        $headers = array(
                'Content-Type: application',
                );
        return \Response::download($path, $dowloadable_file['name'], $headers);
    }


    // Promotion show
    public function show($id){
        $authors = Author::all();
        $advisors = User::where('role_id', 3)->where('user_status', 1)->get();
        $promotion = Promotion::with([
                                        'requestedBy',
                                        'closedBy',
                                        'ifacAdvisor',
                                        'reviewedBy',
                                        'signedOfBy',
                                    ])->where('id', $id)->first();

        // dd($authors, $advisors, $promotion);

        return view("director.company.compliance.promotion.show", compact(['authors', 'advisors', 'promotion']));
    }

    // Promotion Update
    public function update(Request $request, $id){

        $promotion = Promotion::find($id);

        // dd($request, $id);

        if($request->promotion_category)
            $promotion->promotion_category = $request->promotion_category;

        if($request->promotion_type)
            $promotion->promotion_type = $request->promotion_type;

        if($request->promotion_medium)
            $promotion->promotion_medium = $request->promotion_medium;

        if($request->promotion_title)
            $promotion->promotion_title = $request->promotion_title;

        if($request->requested_date)
            $promotion->requested_date = General::dateFormatSlashToDash($request->requested_date);

        if($request->deadline)
            $promotion->deadline = General::dateFormatSlashToDash($request->deadline);

        if($request->promotion_closed)
            $promotion->promotion_closed = $request->promotion_closed;

        if($request->closed_by)
            $promotion->closed_by = $request->closed_by;

        if($request->closed_date)
            $promotion->closed_date = General::dateFormatSlashToDash($request->closed_date);

        if($request->overall_result)
            $promotion->overall_result = json_encode($request->overall_result);
        else
            $promotion->overall_result = NULL;

        if($request->note)
            $promotion->note = $request->note;

        if($request->requested_by)
            $promotion->requested_by = $request->requested_by;

        if($request->ifac_advisor)
            $promotion->ifac_advisor = $request->ifac_advisor;

        if($request->jurisdiction)
            $promotion->jurisdiction = $request->jurisdiction;

        if($request->product_area_covered)
            $promotion->product_area_covered = $request->product_area_covered;

        if($request->reviewer)
            $promotion->reviewer = $request->reviewer;

        if($request->reviewed_date)
            $promotion->reviewed_date = General::dateFormatSlashToDash($request->reviewed_date);

        if($request->signed_of_by)
            $promotion->signed_of_by = $request->signed_of_by;

        if($request->approval_date)
            $promotion->approval_date = General::dateFormatSlashToDash($request->approval_date);

        if($request->approval_to)
            $promotion->approval_to = General::dateFormatSlashToDash($request->approval_to);

        $promotion->save();

        return redirect()->route('director.company.compliance.promotion.index');

    }
}
