<?php

namespace App\Http\Controllers\Director\Company;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Models\Director\Company\Task\Task;
use App\Models\Director\Company\TaskType\TaskType;
use App\User;
use App\Models\Director\Clients\Client\Client;

class TaskManagerController extends Controller
{
    private $task;
    private $task_type;
    private $user;
    private $client;
    // Contstructor
    public function __construct(){
        $this->task = new Task();
        $this->task_type = new TaskType();
        $this->user = new User();
        $this->client = new Client();
    }

    /**
     * ===================================================
     * ===============< Task >=================
     * ===================================================
     */
    // Task Index
    public function index(){
        $tasks = $this->task->with(['director', 'taskClient', 'taskType'])->get();
        $task_types = $this->task_type->select('id', 'title')->get();
        // dd($tasks);
        $users = $this->user->select('id', 'saluation', 'forname', 'surname')->where('role_id', 3)->get();
        $clients = $this->client->select('id', 'client_forname', 'client_middle_name', 'client_surname')->where('client_status', 1)->get();
        return view("director.company.task_manager.index", compact('tasks', 'task_types', 'users', 'clients'));
    }

    //Task Store
    public function store(Request $request){

        if($request->task_title){
            $this->validate($request, [
                'task_title'     =>      'required | string | max:191'
            ]);
        }


        $task = new Task();

        $task->task_title = $request->task_title;
        $task->task_type = $request->task_type;
        $task->assigned_to = $request->assigned_to;
        $task->client = $request->client;
        $task->task_status = $request->task_status;
        $task->start_date = $this->dateFormat($request->start_date);

        if (!$request->disable_end_date)
            $task->end_date = $this->dateFormat($request->end_date);

        $task->note = $request->note;

        $task->save();

        return redirect()->back();
    }

    //Task Update
    public function update(Request $request){

        if($request->task_title){
            $this->validate($request, [
                'task_title'     =>      'required | string | max:191'
            ]);
        }


        $task = Task::find($request->hiddenID);

        $task->task_title = $request->task_title;
        $task->task_type = $request->task_type;
        $task->assigned_to = $request->assigned_to;
        $task->client = $request->client;
        $task->task_status = $request->task_status;
        $task->start_date = $this->dateFormat($request->start_date);


        if ($request->disable_end_date == 'on'){
            $task->end_date = NULL;
        }else{
            $task->end_date = $this->dateFormat($request->end_date);
        }

        $task->note = $request->note;

        $task->save();

        return redirect()->back();
    }

    protected function dateFormat($date){
        $formated = explode("/",$date);
        return $formated[2]. "-" .$formated[1]."-".$formated[0];
    }
}
