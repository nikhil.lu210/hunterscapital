<?php

namespace App\Http\Controllers\Director\Company;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Director\Company\FactFindModule\FactFindModule;

class FactFindController extends Controller
{
    // Contstructor
    public function __construct(){

    }

    /**
     * ===================================================
     * ===============< Fact Find >=================
     * ===================================================
     */
    // Fact Find Index
    public function index(){
        $fact_modules = FactFindModule::all();
        return view("director.company.fact_find.index", compact(['fact_modules']));
    }

    public function store(Request $request)
    {

        $this->validate($request, array(
            'fact_find_name' => 'required | string'
        ));

        $fact_module = new FactFindModule();

        $fact_module->factfind_name = $request->fact_find_name;

        $fact_module->save();

        //confirmation here

        return redirect()->route('director.company.fact_find.index');

    }


    public function storeNewSection(Request $request)
    {

        $this->validate($request, array(
            'fact_find_section_name' => 'required | string'
        ));

        $fact_module = FactFindModule::find($request->fact_id);

        $factfinds = json_decode($fact_module->factfinds, true);
        if($factfinds == null) $factfinds = array();

        $factfinds[] = array(
            'name' => $request->fact_find_section_name,
            'module' => null
        );

        $r_facfinds = json_encode($factfinds);

        $fact_module->factfinds = $r_facfinds;

        $fact_module->save();

        //confirmation here

        return redirect()->route('director.company.fact_find.index');

    }


    public function addNewQuestion(Request $request)
    {
        $this->validate($request, array(
            'question_name' => 'required | string',
            'field_type' => 'required | string',
        ));

        // dd($request->all());

        $fact_module = FactFindModule::find($request->module_id);

        $factfinds = json_decode($fact_module->factfinds, true);

        $section_question = $factfinds[$request->section_id]['module'];
        if($section_question == null) $section_question = array();

        if($request->field_type == 'select'){
            $options = $request->options;
        } else $options = null;

        $temp = array(
            'question_name' => $request->question_name,
            'field_type' => $request->field_type,
            'options'   => $options,
            'print_by_default' => $request->print_by_default,
            'mendatory' => $request->mendatory,
            'hidden' => $request->hidden,
            'allow_client_edit' => $request->allow_client_edit,
        );

        $section_question[] = $temp;

        // dd($section_question);

        $factfinds[$request->section_id]['module'] = $section_question;

        $r_facfinds = json_encode($factfinds);

        $fact_module->factfinds = $r_facfinds;

        $fact_module->save();

        //confirmation nofification here

        return redirect()->back();
    }


    public function updateQuestion(Request $request, $module_id, $section_id, $question_id)
    {
        $this->validate($request, array(
            'question_name' => 'required | string',
            'field_type' => 'required | string',
        ));
        // dd($request->all());

        $fact_module = FactFindModule::find($module_id);

        $factfinds = json_decode($fact_module->factfinds, true);

        $section_question = $factfinds[$section_id]['module'];
        if($section_question == null) $section_question = array();

        if($request->field_type == 'select'){
            $options = $request->options;
        } else $options = null;

        $temp = array(
            'question_name' => $request->question_name,
            'field_type' => $request->field_type,
            'options'   => $options,
            'print_by_default' => $request->print_by_default,
            'mendatory' => $request->mendatory,
            'hidden' => $request->hidden,
            'allow_client_edit' => $request->allow_client_edit,
        );

        $section_question[$question_id] = $temp;

        // dd($section_question);

        $factfinds[$section_id]['module'] = $section_question;

        $r_facfinds = json_encode($factfinds);

        $fact_module->factfinds = $r_facfinds;

        $fact_module->save();

        //confirmation nofification here

        return redirect()->back();
    }



    public function destroyQuestion($module_id, $section_id, $question_id)
    {
        $fact_module = FactFindModule::find($module_id);

        $factfinds = json_decode($fact_module->factfinds, true);

        $section_question = $factfinds[$section_id]['module'];

        // array_splice($section_question, $question_id, 1);
        unset($section_question[$question_id]);

        $factfinds[$section_id]['module'] = $section_question;

        $r_facfinds = json_encode($factfinds);

        $fact_module->factfinds = $r_facfinds;

        $fact_module->save();

        //confirmation nofification here

        return redirect()->back();
    }

    public function destroySection($module_id, $section_id)
    {
        $fact_module = FactFindModule::find($module_id);

        $factfinds = json_decode($fact_module->factfinds, true);

        // array_splice($factfinds, $section_id, 1);
        unset($factfinds[$section_id]);

        $r_facfinds = json_encode($factfinds);

        $fact_module->factfinds = $r_facfinds;

        $fact_module->save();

        //confirmation nofification here

        return redirect()->back();
    }

    public function editSection(Request $request)
    {
        $this->validate($request, array(
            'fact_find_section_name' => 'required | string'
        ));
        $fact_module = FactFindModule::find($request->module_id);

        $factfinds = json_decode($fact_module->factfinds, true);
        $factfinds[$request->section_id]['name'] = $request->fact_find_section_name;

        $r_facfinds = json_encode($factfinds);

        $fact_module->factfinds = $r_facfinds;

        $fact_module->save();

        //confirmation nofification here

        return redirect()->back();
    }


    public function update(Request $request)
    {
        $this->validate($request, array(
            'fact_find_name' => 'required | string'
        ));

        $fact_module = FactFindModule::find($request->module_id);

        $fact_module->factfind_name = $request->fact_find_name;

        $fact_module->save();

        //confirmation nofification here

        return redirect()->back();

    }

    public function destroy($id)
    {
        $fact_module = FactFindModule::find($id);
        $fact_module->delete();

        //confirmation nofification here

        return redirect()->back();
    }
}
