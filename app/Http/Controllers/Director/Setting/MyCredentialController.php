<?php

namespace App\Http\Controllers\Director\Setting;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class MyCredentialController extends Controller
{
    // Contstructor
    public function __construct(){

    }

    /**
     * ===================================================
     * ===============< My Credential >=================
     * ===================================================
     */
    // My Credential Index
    public function index(){
        return view("director.setting.my_credential.index");
    }

}
