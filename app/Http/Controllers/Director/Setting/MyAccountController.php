<?php

namespace App\Http\Controllers\Director\Setting;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;


class MyAccountController extends Controller
{
    // Contstructor
    public function __construct(){

    }

    /**
     * ===================================================
     * ===============< My Account >=================
     * ===================================================
     */
    // My Account Index
    public function index(){
        $profile = User::find(Auth::user()->id);

        // dd($profile);

        return view("director.setting.my_account.index", compact(['profile']));
    }


    public function update(Request $request)
    {
        if($request->forname){
            $this->validate($request, [
                'forname'          =>  'required | string',
            ]);
        }
        if($request->middle_name){
            $this->validate($request, [
                'middle_name'          =>  'string',
            ]);
        }
        if($request->surname){
            $this->validate($request, [
                'surname'          =>  'required | string',
            ]);
        }
        if($request->email_address){
            $this->validate($request, [
                'email_address'          =>  'required | string | email',
            ]);
        }
        if($request->mobile_number){
            $this->validate($request, [
                'mobile_number'          =>  'required | regex:/^([0-9\s\-\+\(\)]*)$/',
            ]);
        }

        $profile = User::find(Auth::user()->id);
        $profile->forname = $request->forname;
        $profile->middle_name = $request->middle_name;
        $profile->surname = $request->surname;
        $profile->email_address = $request->email_address;
        $profile->mobile_number = $request->mobile_number;
        $profile->address = $request->address;
        // dd($request, $profile);

        $profile->save();

        return redirect()->back()->withSuccess('Your Profile has been Updated Succesfully.');
    }


    //Change Password
    public function changePassword(Request $request){
        // dd($request);
        $this->validate($request, [
            'old_password'          => 'required | string | min:8',
            'new_password'          => 'required | string | min:8 | same:confirm_password',
        ]);
        // dd($request);

        if( !Hash::check($request->old_password, Auth::user()->password) )
            return redirect()->back()->withDanger('Your Old Password Does not Match.');
        elseif($request->old_password == $request->new_password)
            return redirect()->back()->withInfo('Old Password and New Password must be Different.');
        else{
            $super = Auth::user();

            $super->password = Hash::make($request->new_password);
            // dd($super);

            $super->save();

            return redirect()->back()->withSuccess('Your Password has been Updated Succesfully.');
        }
    }

}
