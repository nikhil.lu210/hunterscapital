<?php

namespace App\Http\Controllers\Director\TermAndCondition;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Controllers\General;
use App\Models\Director\TermAndCondition\CPD\CPD;
use App\Models\SuperAdmin\Author\Author;
use App\User;

class CPDRegisterController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $users = User::select('id', 'saluation', 'forname', 'middle_name', 'surname')->where('role_id', 3)->get();
        $authors = Author::select('id', 'authors_name')->get();
        $cpds = CPD::with([
                        'userDetails' => function($query){
                            $query->select('id', 'saluation', 'forname', 'middle_name', 'surname');
                        },
                        'devSetByUser' => function($query){
                            $query->select('id', 'saluation', 'forname', 'middle_name', 'surname');
                        },
                        'authorDetails' => function($query){
                            $query->select('id', 'authors_name');
                        },
                    ])->get();
        // dd($users);

        return view("director.term_and_condition.cpd_register.index", compact('users','authors','cpds'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'user'                      => 'required',
            'cpd_topic'                 => 'required | string',
            'cpd_activity_type'         => 'required | string',
            'development_needed'        => 'required | string',
            'planned_completion_date'   => 'required',
            'cpd_activity_completed'    => 'required | string',
            'note_reflective_statement' => 'required | string',
            'structured_hour'           => 'required',
            'unstructured_hour'         => 'required',
        ]);

        $cpd = new CPD();

        if($request->user)
            $cpd->user = $request->user;

        if($request->dev_set_by){
            $a = $request->dev_set_by;
            $dev_by = explode("-",$a);

            if($dev_by[0] == 'user')
                $cpd->dev_set_by_user = $dev_by[1];
            else
                $cpd->dev_set_by_author = $dev_by[1];
        }

        if($request->cpd_topic)
            $cpd->cpd_topic = $request->cpd_topic;

        if($request->cpd_activity_type)
            $cpd->cpd_activity_type = $request->cpd_activity_type;

        if($request->development_needed)
            $cpd->development_needed = $request->development_needed;

        if($request->planned_completion_date)
            $cpd->planned_completion_date = General::dateFormatSlashToDash($request->planned_completion_date);

        if($request->cpd_activity_completed)
            $cpd->cpd_activity_completed = $request->cpd_activity_completed;

        if($request->completed_date)
            $cpd->completed_date = General::dateFormatSlashToDash($request->completed_date);

        if($request->note_reflective_statement)
            $cpd->note_reflective_statement = $request->note_reflective_statement;

        if($request->structured_hour)
            $cpd->structured_hour = $request->structured_hour;

        if($request->unstructured_hour)
            $cpd->unstructured_hour = $request->unstructured_hour;


        $cpd->save();

        return redirect()->back();

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        // dd($request->all());

        $this->validate($request, [
            'user'                      => 'required',
            'cpd_topic'                 => 'required | string',
            'cpd_activity_type'         => 'required | string',
            'development_needed'        => 'required | string',
            'planned_completion_date'   => 'required',
            'cpd_activity_completed'    => 'required | string',
            'note_reflective_statement' => 'required | string',
            'structured_hour'           => 'required',
            'unstructured_hour'         => 'required',
        ]);

        $cpd = CPD::find($request->hiddenID);

        if($request->user)
            $cpd->user = $request->user;

        if($request->dev_set_by){
            $a = $request->dev_set_by;
            $dev_by = explode("-",$a);

            if($dev_by[0] == 'user'){
                $cpd->dev_set_by_user = $dev_by[1];
                $cpd->dev_set_by_author = NULL;
            }
            else{
                $cpd->dev_set_by_user = NULL;
                $cpd->dev_set_by_author = $dev_by[1];
            }
        }

        if($request->cpd_topic)
            $cpd->cpd_topic = $request->cpd_topic;

        if($request->cpd_activity_type)
            $cpd->cpd_activity_type = $request->cpd_activity_type;

        if($request->development_needed)
            $cpd->development_needed = $request->development_needed;

        if($request->planned_completion_date)
            $cpd->planned_completion_date = General::dateFormatSlashToDash($request->planned_completion_date);

        if($request->cpd_activity_completed)
            $cpd->cpd_activity_completed = $request->cpd_activity_completed;

        if($request->completed_date)
            $cpd->completed_date = General::dateFormatSlashToDash($request->completed_date);

        if($request->note_reflective_statement)
            $cpd->note_reflective_statement = $request->note_reflective_statement;

        if($request->structured_hour)
            $cpd->structured_hour = $request->structured_hour;

        if($request->unstructured_hour)
            $cpd->unstructured_hour = $request->unstructured_hour;


        $cpd->save();

        return redirect()->back();
    }


    public function upload(Request $request){
        // dd($request->all());

        if($request->hasFile('document')){
            $this->validate($request,array(
                'document'    =>  'max:50024', //50 MB Maximum
            ));
        }

        $cpd_file = CPD::find($request->id);

        if($request->hasFile('document')){
            $document = $request->file('document')->store('cpdDocuments', 'public'); //Local Path Select & Store

            $filename = $request->files->get('document')->getClientOriginalName(); // File Name

            $temp = json_decode($cpd_file->files, true);
            $temp[] = array(
                        'name'      => $filename,
                        'document'  => $document,
                        'date'      => date('d M Y')
                    );

            $r_temp = json_encode($temp);
            $cpd_file->files = $r_temp;
        }

        $cpd_file->save();

        return redirect()->back();
    }


    public function download($id, $cpd_id){
        $cpd_file = CPD::find($cpd_id);

        $documents = json_decode($cpd_file->files, true);
        $dowloadable_file = $documents[$id];

        $path = storage_path('app/public/'.$dowloadable_file['document']);
        $headers = array(
                'Content-Type: application',
                );
        return \Response::download($path, $dowloadable_file['name'], $headers);
    }

    public function destroyFile($id, $cpd_id)
    {
        $cpd_file = CPD::find($cpd_id);

        $documents = json_decode($cpd_file->files, true);

        $deletable_file = $documents[$id];
        unlink(storage_path('app/public/'.$deletable_file['document']));

        array_splice($documents, $id, 1);

        $reverse_documents = json_encode($documents);

        $cpd_file->files = $reverse_documents;

        $cpd_file->save();

        return redirect()->back();

    }



    public function destroy($id){
        $cpd = CPD::find($id);

        $documents = json_decode($cpd->files, true);

        for($i =0; $i<sizeof($documents); $i++){
            unlink(storage_path('app/public/'.$documents[$i]['document']));
        }

        $cpd->files = NULL;
        $cpd->save();

        $cpd->delete();

        return redirect()->back();
    }
}
