<?php

namespace App\Http\Controllers\Director\TermAndCondition;

use App\User;
use Illuminate\Http\Request;

use App\Http\Controllers\General;
use App\Http\Controllers\Controller;
use App\Models\Director\TermAndCondition\SPS\SPS;

class SPSController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $users = User::select('id', 'saluation', 'forname', 'middle_name', 'surname')->get();
        $spss = SPS::with([
            'userDetails' => function($query){
                $query->select('id', 'saluation', 'forname', 'middle_name', 'surname');
            },
        ])->get();
        return view("director.term_and_condition.sps.index", compact(['users', 'spss']));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // dd($request->all());
        $this->validate($request, [
            'user'         => 'required',
            'issued_by'    => 'required | string',
            'date_issued'  => 'required | string',
            'expire_date'  => 'required | string',
        ]);

        $sps = new SPS();

        if($request->user)
            $sps->user          =   $request->user;

        if($request->issued_by)
            $sps->issued_by     =   $request->issued_by;

        if($request->date_issued)
            $sps->date_issued   =   General::dateFormatSlashToDash($request->date_issued);

        if($request->expire_date)
            $sps->expire_date   =   General::dateFormatSlashToDash($request->expire_date);

        $sps->save();

        return redirect()->back();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        // dd($request->all());
        $this->validate($request, [
            'user'         => 'required',
            'issued_by'    => 'required | string',
            'date_issued'  => 'required | string',
            'expire_date'  => 'required | string',
        ]);

        $sps = SPS::find($request->hiddenID);

        if($request->user)
            $sps->user          =   $request->user;

        if($request->issued_by)
            $sps->issued_by     =   $request->issued_by;

        if($request->date_issued)
            $sps->date_issued   =   General::dateFormatSlashToDash($request->date_issued);

        if($request->expire_date)
            $sps->expire_date   =   General::dateFormatSlashToDash($request->expire_date);

        $sps->save();

        return redirect()->back();
    }




    public function upload(Request $request){
        // dd($request->all());

        if($request->hasFile('document')){
            $this->validate($request,array(
                'document'    =>  'max:50024', //50 MB Maximum
            ));
        }

        $sps_file = SPS::find($request->id);

        if($request->hasFile('document')){
            $document = $request->file('document')->store('spsDocuments', 'public'); //Local Path Select & Store

            $filename = $request->files->get('document')->getClientOriginalName(); // File Name

            $temp = json_decode($sps_file->files, true);
            $temp[] = array(
                        'name'      => $filename,
                        'document'  => $document,
                        'date'      => date('d M Y')
                    );

            $r_temp = json_encode($temp);
            $sps_file->files = $r_temp;
        }

        $sps_file->save();

        return redirect()->back();
    }


    public function download($id, $sps_id){
        $sps_file = SPS::find($sps_id);

        $documents = json_decode($sps_file->files, true);
        $dowloadable_file = $documents[$id];

        $path = storage_path('app/public/'.$dowloadable_file['document']);
        $headers = array(
                'Content-Type: application',
                );
        return \Response::download($path, $dowloadable_file['name'], $headers);
    }

    public function destroyFile($id, $sps_id)
    {
        $sps_file = SPS::find($sps_id);

        $documents = json_decode($sps_file->files, true);

        $deletable_file = $documents[$id];
        unlink(storage_path('app/public/'.$deletable_file['document']));

        array_splice($documents, $id, 1);

        $reverse_documents = json_encode($documents);

        $sps_file->files = $reverse_documents;

        $sps_file->save();

        return redirect()->back();

    }



    public function destroy($id){
        $sps = SPS::find($id);

        $documents = json_decode($sps->files, true);

        for($i =0; $i<sizeof($documents); $i++){
            unlink(storage_path('app/public/'.$documents[$i]['document']));
        }

        $sps->files = NULL;
        $sps->save();

        $sps->delete();

        return redirect()->back();
    }
}
