<?php

namespace App\Http\Controllers\Director\Business\AddNewPolicy;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Controllers\General;
use App\Models\Director\Business\Policy\Policy;
use App\Models\Director\Clients\Client\Client;
use App\Models\Director\Clients\RiskList\RiskList;
use App\Models\SuperAdmin\Fund\Fund;
use App\Models\SuperAdmin\Provider\Provider;
use App\Models\SuperAdmin\Trust\Trust;
use App\User;

use DateTime;

class MortgageController extends Controller
{
    // Contstructor
    public function __construct(){

    }

    /**
     * ===================================================
     * ===============< mortgage >=================
     * ===================================================
     */
    // mortgage Index
    public function index(){
        $clients = Client::where('client_status', 1)->get();
        $advisors = User::where('role_id', 3)->where('user_status', 1)->get();
        $providers = Provider::whereNotNull('approved_at')->get();
        $funds = Fund::whereNotNull('approved_at')->get();
        $trusts = Trust::whereNotNull('approved_at')->get();
        // $risk_lists = RiskList::where('status', 1)->get();
        return view("director.business.add_new_policy.mortgage.index", compact(['clients', 'advisors', 'providers', 'funds', 'trusts']));   //, 'risk_lists'
    }


    public function store(Request $request)
    {
        // dd($request->all());

        $this->validate($request,[
            'client'                    => 'required',
            'advisor'                   => 'required',
            'specific_policy_type'      => 'required | string',
            'new_business_type'         => 'required | string',
            'provider_id'               => 'required',
            'policy_status'             => 'required | string',
            'risk_attitude'             => 'required | string',
        ]);

        $mortgage = new Policy();

        $mortgage->policy_name = 'Mortgage';

        if($request->client)
            $mortgage->client = $request->client;

        if($request->advisor)
            $mortgage->advisor = $request->advisor;

        if($request->specific_policy_type)
            $mortgage->specific_policy_type = $request->specific_policy_type;

        if($request->new_business_type)
            $mortgage->new_business_type = $request->new_business_type;

        if($request->adviser_reference)
            $mortgage->adviser_reference = $request->adviser_reference;

        if($request->policy_valuation_amount)
            $mortgage->policy_valuation_amount = $request->policy_valuation_amount;

        if($request->policy_request_date)
            $mortgage->policy_request_date = General::dateFormatSlashToDash($request->policy_request_date);

        if($request->policy_start_date)
            $mortgage->policy_start_date = General::dateFormatSlashToDash($request->policy_start_date);

        if($request->policy_end_date)
            $mortgage->policy_end_date = General::dateFormatSlashToDash($request->policy_end_date);

        if($request->policy_cancellation_date)
            $mortgage->policy_cancellation_date = General::dateFormatSlashToDash($request->policy_cancellation_date);

        if($request->policy_review_date)
            $mortgage->policy_review_date = General::dateFormatSlashToDash($request->policy_review_date);

        if($request->policy_valuation_date)
            $mortgage->policy_valuation_date = General::dateFormatSlashToDash($request->policy_valuation_date);

        if($request->provider_id)
            $mortgage->provider = $request->provider_id;

        if($request->trust)
            $mortgage->trust = $request->trust;

        if($request->beneficiaries)
            $mortgage->beneficiaries = $request->beneficiaries;

        if($request->funds) //JSON_ENCODE
            $mortgage->funds = json_encode($request->funds);
        else $mortgage->funds = null;

        if($request->policy_status)
            $mortgage->policy_status = $request->policy_status;

        if($request->policy_number)
            $mortgage->policy_number = $request->policy_number;

        if($request->risk_attitude)
            $mortgage->risk_attitude = $request->risk_attitude;


        //upside common for all policies

        if($request->commissions) //JSON_ENCODE
            $mortgage->commission_fees = json_encode($request->commissions);
        else $mortgage->commission_fees = null;

        if($request->payments) //JSON_ENCODE
            $mortgage->payments = json_encode($request->payments);
        else $mortgage->payments = null;

        if($request->mortgage_amount)
            $mortgage->mortgage_amount = $request->mortgage_amount;

        if($request->mortgage_monthly_amount)
            $mortgage->mortgage_monthly_amount = $request->mortgage_monthly_amount;

        if($request->mortgage_term)
            $mortgage->mortgage_term = $request->mortgage_term;

        if($request->mortgage_fixed_until)
            $mortgage->mortgage_fixed_until = General::dateFormatSlashToDash($request->mortgage_fixed_until);

        if(isset($request->mortgage_protected))
            $mortgage->mortgage_protected = $request->mortgage_protected;

        if($request->county)
            $mortgage->county = $request->county;

        if($request->postal_code)
            $mortgage->postal_code = $request->postal_code;

        if($request->address)
            $mortgage->address = $request->address;

        if($request->interest)
            $mortgage->interest = $request->interest;

        if($request->loan_to_value)
            $mortgage->loan_to_value = $request->loan_to_value;

        if($request->property_value)
            $mortgage->property_value = $request->property_value;

        if($request->deposit_amount)
            $mortgage->deposit_amount = $request->deposit_amount;

        if($request->valuation_date)
            $mortgage->valuation_date = General::dateFormatSlashToDash($request->valuation_date);

        if($request->mortgage_offer_date)
            $mortgage->mortgage_offer_date = General::dateFormatSlashToDash($request->mortgage_offer_date);

        if(isset($request->willing_to_pay))
            $mortgage->willing_to_pay = $request->willing_to_pay;

        if(isset($request->repayment_on_track))
            $mortgage->repayment_on_track = $request->repayment_on_track;

        if(isset($request->mortgage_portable))
            $mortgage->mortgage_portable = $request->mortgage_portable;



        //common factor
        if(isset($request->other_reason) && $request->other_reason == 'on'){
            $mortgage->other_reason = true;
            $mortgage->note = $request->note;
        } else $mortgage->other_reason = false;

        if(isset($request->ucis_products) && $request->ucis_products == 'on')
            $mortgage->ucis_product = true;
        else $mortgage->ucis_product = false;

        if(isset($request->direct_stock) && $request->direct_stock == 'on')
            $mortgage->direct_stock_share = true;
        else $mortgage->direct_stock_share = false;

        // dd($investment);

        $mortgage->save();

        return redirect()->route('director.business.all_business.index');

    }


    //update mortgage
    public function update(Request $request, $id){

        // dd($request->all());

        $this->validate($request,[
            'client'                    => 'required',
            'advisor'                   => 'required',
            'specific_policy_type'      => 'required | string',
            'new_business_type'         => 'required | string',
            'provider_id'               => 'required',
            'policy_status'             => 'required | string',
            'risk_attitude'             => 'required | string',
        ]);

        $mortgage = Policy::find($id);

        $mortgage->policy_name = 'Mortgage';

        if($request->client)
            $mortgage->client = $request->client;

        if($request->advisor)
            $mortgage->advisor = $request->advisor;

        if($request->specific_policy_type)
            $mortgage->specific_policy_type = $request->specific_policy_type;

        if($request->new_business_type)
            $mortgage->new_business_type = $request->new_business_type;

        if($request->adviser_reference)
            $mortgage->adviser_reference = $request->adviser_reference;

        if($request->policy_valuation_amount)
            $mortgage->policy_valuation_amount = $request->policy_valuation_amount;

        if($request->policy_request_date)
            $mortgage->policy_request_date = General::dateFormatSlashToDash($request->policy_request_date);

        if($request->policy_start_date)
            $mortgage->policy_start_date = General::dateFormatSlashToDash($request->policy_start_date);

        if($request->policy_end_date)
            $mortgage->policy_end_date = General::dateFormatSlashToDash($request->policy_end_date);

        if($request->policy_cancellation_date)
            $mortgage->policy_cancellation_date = General::dateFormatSlashToDash($request->policy_cancellation_date);

        $prev_review = new DateTime($mortgage->policy_review_date);
        if($request->policy_review_date){
            $mortgage->policy_review_date = General::dateFormatSlashToDash($request->policy_review_date);
            $current_date = new DateTime($mortgage->policy_review_date);
            if($prev_review != $current_date){
                $mortgage->is_send_mail = 'No';
            }
        }

        if($request->policy_valuation_date)
            $mortgage->policy_valuation_date = General::dateFormatSlashToDash($request->policy_valuation_date);

        if($request->provider_id)
            $mortgage->provider = $request->provider_id;

        if($request->trust)
            $mortgage->trust = $request->trust;

        if($request->beneficiaries)
            $mortgage->beneficiaries = $request->beneficiaries;

        if($request->funds) //JSON_ENCODE
            $mortgage->funds = json_encode($request->funds);
        else $mortgage->funds = null;

        if($request->policy_status)
            $mortgage->policy_status = $request->policy_status;

        if($request->policy_number)
            $mortgage->policy_number = $request->policy_number;

        if($request->risk_attitude)
            $mortgage->risk_attitude = $request->risk_attitude;


        //upside common for all policies

        if($request->commissions) //JSON_ENCODE
            $mortgage->commission_fees = json_encode($request->commissions);
        else $mortgage->commission_fees = null;

        if($request->payments) //JSON_ENCODE
            $mortgage->payments = json_encode($request->payments);
        else $mortgage->payments = null;

        if($request->mortgage_amount)
            $mortgage->mortgage_amount = $request->mortgage_amount;

        if($request->mortgage_monthly_amount)
            $mortgage->mortgage_monthly_amount = $request->mortgage_monthly_amount;

        if($request->mortgage_term)
            $mortgage->mortgage_term = $request->mortgage_term;

        if($request->mortgage_fixed_until)
            $mortgage->mortgage_fixed_until = General::dateFormatSlashToDash($request->mortgage_fixed_until);

        if(isset($request->mortgage_protected))
            $mortgage->mortgage_protected = $request->mortgage_protected;

        if($request->county)
            $mortgage->county = $request->county;

        if($request->postal_code)
            $mortgage->postal_code = $request->postal_code;

        if($request->address)
            $mortgage->address = $request->address;

        if($request->interest)
            $mortgage->interest = $request->interest;

        if($request->loan_to_value)
            $mortgage->loan_to_value = $request->loan_to_value;

        if($request->property_value)
            $mortgage->property_value = $request->property_value;

        if($request->deposit_amount)
            $mortgage->deposit_amount = $request->deposit_amount;

        if($request->valuation_date)
            $mortgage->valuation_date = General::dateFormatSlashToDash($request->valuation_date);

        if($request->mortgage_offer_date)
            $mortgage->mortgage_offer_date = General::dateFormatSlashToDash($request->mortgage_offer_date);

        if(isset($request->willing_to_pay))
            $mortgage->willing_to_pay = $request->willing_to_pay;

        if(isset($request->repayment_on_track))
            $mortgage->repayment_on_track = $request->repayment_on_track;

        if(isset($request->mortgage_portable))
            $mortgage->mortgage_portable = $request->mortgage_portable;



        //common factor
        if(isset($request->other_reason) && $request->other_reason == 'on'){
            $mortgage->other_reason = true;
            $mortgage->note = $request->note;
        } else $mortgage->other_reason = false;

        if(isset($request->ucis_products) && $request->ucis_products == 'on')
            $mortgage->ucis_product = true;
        else $mortgage->ucis_product = false;

        if(isset($request->direct_stock) && $request->direct_stock == 'on')
            $mortgage->direct_stock_share = true;
        else $mortgage->direct_stock_share = false;

        // dd($mortgage);

        $mortgage->save();

        return redirect()->route('director.business.all_business.index');

    }
}
