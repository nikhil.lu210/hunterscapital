<?php

namespace App\Http\Controllers\Director\Business\AddNewPolicy;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Controllers\General;
use App\Models\Director\Business\Policy\Policy;
use App\Models\Director\Clients\Client\Client;
use App\Models\Director\Clients\RiskList\RiskList;
use App\Models\SuperAdmin\Fund\Fund;
use App\Models\SuperAdmin\Provider\Provider;
use App\Models\SuperAdmin\Trust\Trust;
use App\User;

class PensionController extends Controller
{
    // Contstructor
    public function __construct(){

    }

    /**
     * ===================================================
     * ===============< pension >=================
     * ===================================================
     */
    // pension Index
    public function index(){

        $clients = Client::where('client_status', 1)->get();
        $advisors = User::where('role_id', 3)->where('user_status', 1)->get();
        $providers = Provider::whereNotNull('approved_at')->get();
        $funds = Fund::whereNotNull('approved_at')->get();
        $trusts = Trust::whereNotNull('approved_at')->get();
        $risk_lists = RiskList::where('status', 1)->get();
        return view("director.business.add_new_policy.pension.index", compact(['clients', 'advisors', 'providers', 'funds', 'trusts', 'risk_lists']));
    }

    public function store(Request $request)
    {
        // dd($request->all());

        $this->validate($request,[
            'client'                    => 'required',
            'advisor'                   => 'required',
            'specific_policy_type'      => 'required | string',
            'new_business_type'         => 'required | string',
            'provider_id'               => 'required',
            'policy_status'             => 'required | string',
            'risk_attitude'             => 'required | string',
        ]);

        $pension = new Policy();

        $pension->policy_name = 'Pension';

        if($request->client)
            $pension->client = $request->client;

        if($request->advisor)
            $pension->advisor = $request->advisor;

        if($request->specific_policy_type)
            $pension->specific_policy_type = $request->specific_policy_type;

        if($request->new_business_type)
            $pension->new_business_type = $request->new_business_type;

        if($request->adviser_reference)
            $pension->adviser_reference = $request->adviser_reference;

        if($request->policy_valuation_amount)
            $pension->policy_valuation_amount = $request->policy_valuation_amount;

        if($request->policy_request_date)
            $pension->policy_request_date = General::dateFormatSlashToDash($request->policy_request_date);

        if($request->policy_start_date)
            $pension->policy_start_date = General::dateFormatSlashToDash($request->policy_start_date);

        if($request->policy_end_date)
            $pension->policy_end_date = General::dateFormatSlashToDash($request->policy_end_date);

        if($request->policy_cancellation_date)
            $pension->policy_cancellation_date = General::dateFormatSlashToDash($request->policy_cancellation_date);

        if($request->policy_review_date)
            $pension->policy_review_date = General::dateFormatSlashToDash($request->policy_review_date);

        if($request->policy_valuation_date)
            $pension->policy_valuation_date = General::dateFormatSlashToDash($request->policy_valuation_date);

        if($request->provider_id)
            $pension->provider = $request->provider_id;

        if($request->trust)
            $pension->trust = $request->trust;

        if($request->beneficiaries)
            $pension->beneficiaries = $request->beneficiaries;

        if($request->funds) //JSON_ENCODE
            $pension->funds = json_encode($request->funds);

        if($request->policy_status)
            $pension->policy_status = $request->policy_status;

        if($request->policy_number)
            $pension->policy_number = $request->policy_number;

        if($request->risk_attitude)
            $pension->risk_attitude = $request->risk_attitude;


        //upside common for all policies

        if($request->commissions) //JSON_ENCODE
            $pension->commission_fees = json_encode($request->commissions);

        if($request->payments) //JSON_ENCODE
            $pension->payments = json_encode($request->payments);

            if($request->indexed_criteria)
            $pension->indexed_criteria = $request->indexed_criteria;

        if($request->widow_pension)
            $pension->widow_pension = $request->widow_pension;

        if($request->waiver_included)
            $pension->waiver_included = $request->waiver_included;

        if($request->contracted_out)
            $pension->contracted_out = $request->contracted_out;





        //common factors
        if($request->other_reason && $request->other_reason == 'on')
            $pension->other_reason = true;

        if($request->ucis_products && $request->ucis_products == 'on')
            $pension->ucis_product = true;

        if($request->direct_stock && $request->direct_stock == 'on')
            $pension->direct_stock_share = true;

        // dd($investment);

        $pension->save();

        return redirect()->route('director.business.all_business.index');

    }


    public function update(Request $request, $id)
    {
        // dd($request->all());

        $this->validate($request,[
            'client'                    => 'required',
            'advisor'                   => 'required',
            'specific_policy_type'      => 'required | string',
            'new_business_type'         => 'required | string',
            'provider_id'               => 'required',
            'policy_status'             => 'required | string',
            'risk_attitude'             => 'required | string',
        ]);

        $pension = Policy::find($id);

        $pension->policy_name = 'Pension';

        if($request->client)
            $pension->client = $request->client;

        if($request->advisor)
            $pension->advisor = $request->advisor;

        if($request->specific_policy_type)
            $pension->specific_policy_type = $request->specific_policy_type;

        if($request->new_business_type)
            $pension->new_business_type = $request->new_business_type;

        if($request->adviser_reference)
            $pension->adviser_reference = $request->adviser_reference;

        if($request->policy_valuation_amount)
            $pension->policy_valuation_amount = $request->policy_valuation_amount;

        if($request->policy_request_date)
            $pension->policy_request_date = General::dateFormatSlashToDash($request->policy_request_date);

        if($request->policy_start_date)
            $pension->policy_start_date = General::dateFormatSlashToDash($request->policy_start_date);

        if($request->policy_end_date)
            $pension->policy_end_date = General::dateFormatSlashToDash($request->policy_end_date);

        if($request->policy_cancellation_date)
            $pension->policy_cancellation_date = General::dateFormatSlashToDash($request->policy_cancellation_date);

        if($request->policy_review_date)
            $pension->policy_review_date = General::dateFormatSlashToDash($request->policy_review_date);

        if($request->policy_valuation_date)
            $pension->policy_valuation_date = General::dateFormatSlashToDash($request->policy_valuation_date);

        if($request->provider_id)
            $pension->provider = $request->provider_id;

        if($request->trust)
            $pension->trust = $request->trust;

        if($request->beneficiaries)
            $pension->beneficiaries = $request->beneficiaries;

        if($request->funds) //JSON_ENCODE
            $pension->funds = json_encode($request->funds);
        else $pension->funds = null;

        if($request->policy_status)
            $pension->policy_status = $request->policy_status;

        if($request->policy_number)
            $pension->policy_number = $request->policy_number;

        if($request->risk_attitude)
            $pension->risk_attitude = $request->risk_attitude;


        //upside common for all policies

        if($request->commissions) //JSON_ENCODE
            $pension->commission_fees = json_encode($request->commissions);
        else $pension->commission_fees = null;

        if($request->payments) //JSON_ENCODE
            $pension->payments = json_encode($request->payments);
        else $pension->payments = null;

        if($request->indexed_criteria)
            $pension->indexed_criteria = $request->indexed_criteria;

        if($request->widow_pension)
            $pension->widow_pension = $request->widow_pension;

        if(isset($request->waiver_included))
            $pension->waiver_included = $request->waiver_included;

        if(isset($request->contracted_out))
            $pension->contracted_out = $request->contracted_out;





        //common factors
        if(isset($request->other_reason) && $request->other_reason == 'on')
            $pension->other_reason = true;
        else $pension->other_reason = false;

        if(isset($request->ucis_products) && $request->ucis_products == 'on')
            $pension->ucis_product = true;
        else $pension->ucis_product = false;

        if(isset($request->direct_stock) && $request->direct_stock == 'on')
            $pension->direct_stock_share = true;
        else $pension->direct_stock_share = false;

        // dd($investment);

        $pension->save();

        return redirect()->route('director.business.all_business.index');

    }
}

