<?php

namespace App\Http\Controllers\Director\Business\AddNewPolicy;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Controllers\General;
use App\Models\Director\Business\Policy\Policy;
use App\Models\Director\Clients\Client\Client;
use App\Models\Director\Clients\RiskList\RiskList;
use App\Models\SuperAdmin\Fund\Fund;
use App\Models\SuperAdmin\Provider\Provider;
use App\Models\SuperAdmin\Trust\Trust;
use App\User;

class InvestmentController extends Controller
{
    // Contstructor
    public function __construct(){

    }

    /**
     * ===================================================
     * ===============< Investment >=================
     * ===================================================
     */
    // Investment Index
    public function index(){
        $clients = Client::where('client_status', 1)->get();
        $advisors = User::where('role_id', 3)->where('user_status', 1)->get();
        $providers = Provider::whereNotNull('approved_at')->get();
        $funds = Fund::whereNotNull('approved_at')->get();
        $trusts = Trust::whereNotNull('approved_at')->get();
        $risk_lists = RiskList::where('status', 1)->get();
        return view("director.business.add_new_policy.investment.index", compact(['clients', 'advisors', 'providers', 'funds', 'trusts', 'risk_lists']));
    }

    //request for provider
    public function requestProvider(Request $request)
    {
        $this->validate($request,array(
            "provider_name" => 'required | string',
            "email" => 'required | string | email',
            "county" => 'required | string',
            "postal_code" => 'required | string',
            "address" => 'required | string'
        ));

        $provider = new Provider();

        $provider->provider_name = $request->provider_name;
        $provider->email_address = $request->email;
        $provider->county = $request->county;
        $provider->postcode = $request->postal_code;
        $provider->address = $request->address;

        if($request->note) $provider->note = $request->note;

        $provider->save();

        //confirm messege
        //email to supera admin

        return redirect()->back();
    }


    //request for fund
    public function requestFund(Request $request)
    {
        // dd($request->all());
        $this->validate($request,array(
            "fund_name" => 'required | string',
            "mex_code" => 'required | string',
            "isin_code" => 'required | string',
            "sedol_code" => 'required | string',
            "citi_code" => 'required | string',
            "bb_ticker_code" => 'required | string',
        ));

        $fund = new Fund();

        $fund->fund_name = $request->fund_name;
        $fund->mex_code = $request->mex_code;
        $fund->isin_code = $request->isin_code;
        $fund->sedol_code = $request->sedol_code;
        $fund->citi_code = $request->citi_code;
        $fund->bb_ticker_code = $request->bb_ticker_code;

        if($request->note) $fund->note = $request->note;

        $fund->save();

        //confirm messege
        //email to supera admin

        return redirect()->back();
    }

    //request for provider
    public function requestTrust(Request $request)
    {
        // dd($request->all());
        $this->validate($request,array(
            "trust_name" => 'required | string',
            "forename" => 'required | string',
            "surname" => 'required | string',
            "contact_number" => 'required | string',
            "county" => 'required | string',
            "postcode" => 'required | string',
            "address" => 'required | string',
        ));

        $trust = new Trust();

        $trust->trust_name = $request->trust_name;
        $trust->forename = $request->forename;
        $trust->surname = $request->surname;
        $trust->contact_number = $request->contact_number;
        $trust->county = $request->county;
        $trust->postcode = $request->postcode;
        $trust->address = $request->address;

        if($request->note) $trust->note = $request->note;

        $trust->save();

        //confirm messege
        //email to supera admin

        return redirect()->back();
    }


    public function store(Request $request)
    {
        // dd($request->all());

        $this->validate($request,[
            'client'                    => 'required',
            'advisor'                   => 'required',
            'specific_policy_type'      => 'required | string',
            'new_business_type'         => 'required | string',
            'provider_id'               => 'required',
            'policy_status'             => 'required | string',
            'risk_attitude'             => 'required | string',
        ]);

        $investment = new Policy();

        $investment->policy_name = 'Investment';

        if($request->client)
            $investment->client = $request->client;

        if($request->advisor)
            $investment->advisor = $request->advisor;

        if($request->specific_policy_type)
            $investment->specific_policy_type = $request->specific_policy_type;

        if($request->new_business_type)
            $investment->new_business_type = $request->new_business_type;

        if($request->adviser_reference)
            $investment->adviser_reference = $request->adviser_reference;

        if($request->policy_valuation_amount)
            $investment->policy_valuation_amount = $request->policy_valuation_amount;

        if($request->policy_request_date)
            $investment->policy_request_date = General::dateFormatSlashToDash($request->policy_request_date);

        if($request->policy_start_date)
            $investment->policy_start_date = General::dateFormatSlashToDash($request->policy_start_date);

        if($request->policy_end_date)
            $investment->policy_end_date = General::dateFormatSlashToDash($request->policy_end_date);

        if($request->policy_cancellation_date)
            $investment->policy_cancellation_date = General::dateFormatSlashToDash($request->policy_cancellation_date);

        if($request->policy_review_date)
            $investment->policy_review_date = General::dateFormatSlashToDash($request->policy_review_date);

        if($request->policy_valuation_date)
            $investment->policy_valuation_date = General::dateFormatSlashToDash($request->policy_valuation_date);

        if($request->provider_id)
            $investment->provider = $request->provider_id;

        if($request->trust)
            $investment->trust = $request->trust;

        if($request->beneficiaries)
            $investment->beneficiaries = $request->beneficiaries;

        if($request->funds) //JSON_ENCODE
            $investment->funds = json_encode($request->funds);

        if($request->policy_status)
            $investment->policy_status = $request->policy_status;

        if($request->policy_number)
            $investment->policy_number = $request->policy_number;

        if($request->risk_attitude)
            $investment->risk_attitude = $request->risk_attitude;

        if($request->commissions) //JSON_ENCODE
            $investment->commission_fees = json_encode($request->commissions);

        if($request->payments) //JSON_ENCODE
            $investment->payments = json_encode($request->payments);

        if($request->withdrawals) //JSON_ENCODE
            $investment->withdrawals = json_encode($request->withdrawals);

        if($request->other_reason && $request->other_reason == 'on')
            $investment->other_reason = true;

        if($request->ucis_products && $request->ucis_products == 'on')
            $investment->ucis_product = true;

        if($request->direct_stock && $request->direct_stock == 'on')
            $investment->direct_stock_share = true;

        // dd($investment);

        $investment->save();

        //confirmation here

        return redirect()->route('director.business.all_business.index');

    }


    //update investment

    public function update(Request $request, $id){
        // dd($request->all());
        $this->validate($request,[
            'client'                    => 'required',
            'advisor'                   => 'required',
            'specific_policy_type'      => 'required | string',
            'new_business_type'         => 'required | string',
            'provider_id'               => 'required',
            'policy_status'             => 'required | string',
            'risk_attitude'             => 'required | string',
        ]);

        $investment = Policy::find($id);

        $investment->policy_name = 'Investment';

        if($request->client)
            $investment->client = $request->client;

        if($request->advisor)
            $investment->advisor = $request->advisor;

        if($request->specific_policy_type)
            $investment->specific_policy_type = $request->specific_policy_type;

        if($request->new_business_type)
            $investment->new_business_type = $request->new_business_type;

        if($request->adviser_reference)
            $investment->adviser_reference = $request->adviser_reference;

        if($request->policy_valuation_amount)
            $investment->policy_valuation_amount = $request->policy_valuation_amount;

        if($request->policy_request_date)
            $investment->policy_request_date = General::dateFormatSlashToDash($request->policy_request_date);

        if($request->policy_start_date)
            $investment->policy_start_date = General::dateFormatSlashToDash($request->policy_start_date);

        if($request->policy_end_date)
            $investment->policy_end_date = General::dateFormatSlashToDash($request->policy_end_date);

        if($request->policy_cancellation_date)
            $investment->policy_cancellation_date = General::dateFormatSlashToDash($request->policy_cancellation_date);

        if($request->policy_review_date)
            $investment->policy_review_date = General::dateFormatSlashToDash($request->policy_review_date);

        if($request->policy_valuation_date)
            $investment->policy_valuation_date = General::dateFormatSlashToDash($request->policy_valuation_date);

        if($request->provider_id)
            $investment->provider = $request->provider_id;

        if($request->trust)
            $investment->trust = $request->trust;

        if($request->beneficiaries)
            $investment->beneficiaries = $request->beneficiaries;

        if($request->funds) //JSON_ENCODE
            $investment->funds = json_encode($request->funds);
        else $investment->funds = null;

        if($request->policy_status)
            $investment->policy_status = $request->policy_status;

        if($request->policy_number)
            $investment->policy_number = $request->policy_number;

        if($request->risk_attitude)
            $investment->risk_attitude = $request->risk_attitude;

        //uper side is common for all factor

        if($request->commissions) //JSON_ENCODE
            $investment->commission_fees = json_encode($request->commissions);
        else $investment->commission_fees = jnull;

        if($request->payments) //JSON_ENCODE
            $investment->payments = json_encode($request->payments);
        else $investment->payments = null;

        if($request->withdrawals) //JSON_ENCODE
            $investment->withdrawals = json_encode($request->withdrawals);
        else $investment->withdrawals = null;



        //lower side also commom for all policy factor
        if(isset($request->other_reason) && $request->other_reason == 'on')
            $investment->other_reason = true;
        else $investment->other_reason = false;

        if(isset($request->ucis_products) && $request->ucis_products == 'on')
            $investment->ucis_product = true;
        else $investment->ucis_product = false;

        if(isset($request->direct_stock) && $request->direct_stock == 'on')
            $investment->direct_stock_share = true;
        else $investment->direct_stock_share = false;

        // dd($investment);

        // dd($request->withdrawals);

        $investment->save();

        //confirmatiion messenge here

        return redirect()->route('director.business.all_business.index');
    }
}

