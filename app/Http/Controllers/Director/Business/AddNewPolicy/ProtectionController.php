<?php

namespace App\Http\Controllers\Director\Business\AddNewPolicy;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Controllers\General;
use App\Models\Director\Business\Policy\Policy;
use App\Models\Director\Clients\Client\Client;
use App\Models\Director\Clients\RiskList\RiskList;
use App\Models\SuperAdmin\Fund\Fund;
use App\Models\SuperAdmin\Provider\Provider;
use App\Models\SuperAdmin\Trust\Trust;
use App\User;

use DateTime;

class ProtectionController extends Controller
{
    // Contstructor
    public function __construct(){

    }

    /**
     * ===================================================
     * ===============< protection >=================
     * ===================================================
     */
    // protection Index
    public function index(){
        $clients = Client::where('client_status', 1)->get();
        $advisors = User::where('role_id', 3)->where('user_status', 1)->get();
        $providers = Provider::whereNotNull('approved_at')->get();
        $funds = Fund::whereNotNull('approved_at')->get();
        $trusts = Trust::whereNotNull('approved_at')->get();
        // $risk_lists = RiskList::where('status', 1)->get();
        return view("director.business.add_new_policy.protection.index", compact(['clients', 'advisors', 'providers', 'funds', 'trusts'])); // , 'risk_lists'
    }


    public function store(Request $request)
    {
        // dd($request->all());

        $this->validate($request,[
            'client'                    => 'required',
            'advisor'                   => 'required',
            'specific_policy_type'      => 'required | string',
            'new_business_type'         => 'required | string',
            'provider_id'               => 'required',
            'policy_status'             => 'required | string',
            'risk_attitude'             => 'required | string',
        ]);

        $protection = new Policy();

        $protection->policy_name = 'Protection';

        if($request->client)
            $protection->client = $request->client;

        if($request->advisor)
            $protection->advisor = $request->advisor;

        if($request->specific_policy_type)
            $protection->specific_policy_type = $request->specific_policy_type;

        if($request->new_business_type)
            $protection->new_business_type = $request->new_business_type;

        if($request->adviser_reference)
            $protection->adviser_reference = $request->adviser_reference;

        if($request->policy_valuation_amount)
            $protection->policy_valuation_amount = $request->policy_valuation_amount;

        if($request->policy_request_date)
            $protection->policy_request_date = General::dateFormatSlashToDash($request->policy_request_date);

        if($request->policy_start_date)
            $protection->policy_start_date = General::dateFormatSlashToDash($request->policy_start_date);

        if($request->policy_end_date)
            $protection->policy_end_date = General::dateFormatSlashToDash($request->policy_end_date);

        if($request->policy_cancellation_date)
            $protection->policy_cancellation_date = General::dateFormatSlashToDash($request->policy_cancellation_date);

        if($request->policy_review_date)
            $protection->policy_review_date = General::dateFormatSlashToDash($request->policy_review_date);

        if($request->policy_valuation_date)
            $protection->policy_valuation_date = General::dateFormatSlashToDash($request->policy_valuation_date);

        if($request->provider_id)
            $protection->provider = $request->provider_id;

        if($request->trust)
            $protection->trust = $request->trust;

        if($request->beneficiaries)
            $protection->beneficiaries = $request->beneficiaries;

        if($request->policy_status)
            $protection->policy_status = $request->policy_status;

        if($request->policy_number)
            $protection->policy_number = $request->policy_number;

        if($request->risk_attitude)
            $protection->risk_attitude = $request->risk_attitude;


        //upside common for all policies

        if($request->commissions) //JSON_ENCODE
            $protection->commission_fees = json_encode($request->commissions);

        if($request->payments) //JSON_ENCODE
            $protection->payments = json_encode($request->payments);

        if($request->indexed_criteria)
            $protection->indexed_criteria = $request->indexed_criteria;

        if($request->premium_period)
            $protection->premium_period = $request->premium_period;

        if($request->waiver_included)
            $protection->waiver_included = $request->waiver_included;

        if($request->total_protection_sum)
            $protection->total_protection_sum = $request->total_protection_sum;

        if($request->protection_excess)
            $protection->protection_excess = $request->protection_excess;

        if($request->plan_purpose)
            $protection->plan_purpose = $request->plan_purpose;

        if($request->product_type)
            $protection->product_type = $request->product_type;

        if($request->protection_basis)
            $protection->protection_basis = $request->protection_basis;

        if($request->renewal_date)
            $protection->renewal_date = General::dateFormatSlashToDash($request->renewal_date);



        //common factors
        if(isset($request->other_reason) && $request->other_reason == 'on'){
            $protection->other_reason = true;
            $protection->note = $request->note;
        } else $protection->other_reason = false;

        if($request->ucis_products && $request->ucis_products == 'on')
            $protection->ucis_product = true;

        if($request->direct_stock && $request->direct_stock == 'on')
            $protection->direct_stock_share = true;

        // dd($protection);

        $protection->save();

        return redirect()->route('director.business.all_business.index');

    }


    public function update(Request $request, $id)
    {
        // dd($request->all());

        $this->validate($request,[
            'client'                    => 'required',
            'advisor'                   => 'required',
            'specific_policy_type'      => 'required | string',
            'new_business_type'         => 'required | string',
            'provider_id'               => 'required',
            'policy_status'             => 'required | string',
            'risk_attitude'             => 'required | string',
        ]);

        $protection = Policy::find($id);

        $protection->policy_name = 'Protection';

        if($request->client)
            $protection->client = $request->client;

        if($request->advisor)
            $protection->advisor = $request->advisor;

        if($request->specific_policy_type)
            $protection->specific_policy_type = $request->specific_policy_type;

        if($request->new_business_type)
            $protection->new_business_type = $request->new_business_type;

        if($request->adviser_reference)
            $protection->adviser_reference = $request->adviser_reference;

        if($request->policy_valuation_amount)
            $protection->policy_valuation_amount = $request->policy_valuation_amount;

        if($request->policy_request_date)
            $protection->policy_request_date = General::dateFormatSlashToDash($request->policy_request_date);

        if($request->policy_start_date)
            $protection->policy_start_date = General::dateFormatSlashToDash($request->policy_start_date);

        if($request->policy_end_date)
            $protection->policy_end_date = General::dateFormatSlashToDash($request->policy_end_date);

        if($request->policy_cancellation_date)
            $protection->policy_cancellation_date = General::dateFormatSlashToDash($request->policy_cancellation_date);

        $prev_review = new DateTime($protection->policy_review_date);
        if($request->policy_review_date){
            $protection->policy_review_date = General::dateFormatSlashToDash($request->policy_review_date);
            $current_date = new DateTime($protection->policy_review_date);
            if($prev_review != $current_date){
                $protection->is_send_mail = 'No';
            }
        }

        if($request->policy_valuation_date)
            $protection->policy_valuation_date = General::dateFormatSlashToDash($request->policy_valuation_date);

        if($request->provider_id)
            $protection->provider = $request->provider_id;

        if($request->trust)
            $protection->trust = $request->trust;

        if($request->beneficiaries)
            $protection->beneficiaries = $request->beneficiaries;

        if($request->policy_status)
            $protection->policy_status = $request->policy_status;

        if($request->policy_number)
            $protection->policy_number = $request->policy_number;

        if($request->risk_attitude)
            $protection->risk_attitude = $request->risk_attitude;


        //upside common for all policies

        if($request->commissions) //JSON_ENCODE
            $protection->commission_fees = json_encode($request->commissions);
        else $protection->commission_fees = null;

        if($request->payments) //JSON_ENCODE
            $protection->payments = json_encode($request->payments);
        else $protection->payments = null;

        if($request->indexed_criteria)
            $protection->indexed_criteria = $request->indexed_criteria;

        if($request->premium_period)
            $protection->premium_period = $request->premium_period;

        if(isset($request->waiver_included))
            $protection->waiver_included = $request->waiver_included;

        if($request->total_protection_sum)
            $protection->total_protection_sum = $request->total_protection_sum;

        if($request->protection_excess)
            $protection->protection_excess = $request->protection_excess;

        if($request->plan_purpose)
            $protection->plan_purpose = $request->plan_purpose;

        if($request->product_type)
            $protection->product_type = $request->product_type;

        if($request->protection_basis)
            $protection->protection_basis = $request->protection_basis;

        if($request->renewal_date)
            $protection->renewal_date = General::dateFormatSlashToDash($request->renewal_date);



        //common factors
        if(isset($request->other_reason) && $request->other_reason == 'on'){
            $protection->other_reason = true;
            $protection->note = $request->note;
        } else $protection->other_reason = false;

        if(isset($request->ucis_products) && $request->ucis_products == 'on')
            $protection->ucis_product = true;
        else $protection->ucis_product = false;

        if(isset($request->direct_stock) && $request->direct_stock == 'on')
            $protection->direct_stock_share = true;
        else $protection->direct_stock_share = false;

        // dd($investment);

        $protection->save();

        return redirect()->route('director.business.all_business.index');

    }
}
