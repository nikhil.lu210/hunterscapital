<?php

namespace App\Http\Controllers\Director\Business;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Controllers\General;
use App\Models\Director\Business\Policy\Policy;
use App\Models\Director\Business\Turnover\Turnover;
use App\Models\Director\Clients\Client\Client;
use App\User;

class TurnoverController extends Controller
{
    // Contstructor
    public function __construct(){

    }

    /**
     * ===================================================
     * ===============< Turnover >=================
     * ===================================================
     */
    // Turnover Index
    public function index(){
        $turnovers = Turnover::with([
                        'clientDetails' => function($query){
                            $query->select('id', 'client_title', 'client_forname', 'client_middle_name', 'client_surname');
                        },
                        'policyDetails' => function($query){
                            $query->select('id', 'policy_name');
                        },
                        'advisorDetails' => function($query){
                            $query->select('id', 'saluation', 'forname', 'middle_name', 'surname');
                        }
                    ])->get();

        // dd($turnovers->all());

        return view("director.business.turnover.index", compact(['turnovers']));
    }


    // Turnover create
    public function create(){
        $clients = Client::select('id', 'client_title', 'client_forname', 'client_middle_name', 'client_surname')->where('client_status', 1)->get();

        $policies = Policy::select('id', 'policy_name')->get();

        $advisors = User::select('id', 'saluation', 'forname', 'middle_name', 'surname')->where('role_id', 3)->where('user_status', 1)->get();

        return view("director.business.turnover.create", compact(['clients', 'policies', 'advisors']));
    }

    // Turnover Store
    public function store(Request $request){

        // dd($request->all());

        $turnover = new Turnover();

        if($request->client && $request->client != 'Unrelated Client')
            $turnover->client = $request->client;
        else $turnover->client = null;

        if($request->client_policy && $request->client_policy != 'Unrelated Policy')
            $turnover->client_policy = $request->client_policy;
        else $turnover->client_policy = null;

        if($request->advisor)
            $turnover->advisor = $request->advisor;

        if($request->turnover_date)
            $turnover->turnover_date = General::dateFormatSlashToDash($request->turnover_date);

        if($request->regulation_type)
            $turnover->regulation_type = $request->regulation_type;

        if($request->regulation_type_spec)
            $turnover->regulation_type_spec = $request->regulation_type_spec;

        if($request->turnover_type)
            $turnover->turnover_type = $request->turnover_type;

        if($request->turnover_frequency)
            $turnover->turnover_frequency = $request->turnover_frequency;

        if($request->adviser_reference)
            $turnover->adviser_reference = $request->adviser_reference;

        if($request->rdr_status)
            $turnover->rdr_status = $request->rdr_status;

        if($request->revenue_source)
            $turnover->revenue_source = $request->revenue_source;

        if($request->payment_frequency)
            $turnover->payment_frequency = $request->payment_frequency;

        if(isset($request->vat_applied))
            $turnover->vat_applied = $request->vat_applied;

        if($request->amount)
            $turnover->amount = $request->amount;

        if($request->revenue_source_name)
            $turnover->revenue_source_name = $request->revenue_source_name;

        if($request->note)
            $turnover->note = $request->note;


        // dd($turnover);

        $turnover->save();

        return redirect()->route('director.business.turnover.index');
    }




    // Turnover show
    public function show($id){
        $clients = Client::select('id', 'client_title', 'client_forname', 'client_middle_name', 'client_surname')->where('client_status', 1)->get();

        $policies = Policy::select('id', 'policy_name')->get();

        $advisors = User::select('id', 'saluation', 'forname', 'middle_name', 'surname')->where('role_id', 3)->where('user_status', 1)->get();

        $turnover = Turnover::with([
                                'clientDetails' => function($query){
                                    $query->select('id', 'client_title', 'client_forname', 'client_middle_name', 'client_surname');
                                },
                                'policyDetails' => function($query){
                                    $query->select('id', 'policy_name', 'specific_policy_type');
                                },
                                'advisorDetails' => function($query){
                                    $query->select('id', 'saluation', 'forname', 'middle_name', 'surname');
                                }
                            ])->where('id', $id)->first();

        return view("director.business.turnover.show", compact(['clients', 'policies', 'advisors','turnover']));
    }


    // Turnover update
    public function update(Request $request, $id){

        // dd($request->all());

        $turnover = Turnover::find($id);

        if($request->client && $request->client != 'Unrelated Client')
            $turnover->client = $request->client;
        else $turnover->client = null;

        if($request->client_policy && $request->client_policy != 'Unrelated Policy')
            $turnover->client_policy = $request->client_policy;
        else $turnover->client_policy = null;

        if($request->advisor)
            $turnover->advisor = $request->advisor;

        if($request->turnover_date)
            $turnover->turnover_date = General::dateFormatSlashToDash($request->turnover_date);

        if($request->regulation_type)
            $turnover->regulation_type = $request->regulation_type;

        if($request->regulation_type_spec)
            $turnover->regulation_type_spec = $request->regulation_type_spec;

        if($request->turnover_type)
            $turnover->turnover_type = $request->turnover_type;

        if($request->turnover_frequency)
            $turnover->turnover_frequency = $request->turnover_frequency;

        if($request->adviser_reference)
            $turnover->adviser_reference = $request->adviser_reference;

        if($request->rdr_status)
            $turnover->rdr_status = $request->rdr_status;

        if($request->revenue_source)
            $turnover->revenue_source = $request->revenue_source;

        if($request->payment_frequency)
            $turnover->payment_frequency = $request->payment_frequency;

        if(isset($request->vat_applied))
            $turnover->vat_applied = $request->vat_applied;

        if($request->amount)
            $turnover->amount = $request->amount;

        if($request->revenue_source_name)
            $turnover->revenue_source_name = $request->revenue_source_name;

        if($request->note)
            $turnover->note = $request->note;


        // dd($turnover);

        $turnover->save();

        return redirect()->route('director.business.turnover.index');
    }


}
