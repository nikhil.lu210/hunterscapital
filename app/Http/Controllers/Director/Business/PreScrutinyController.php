<?php

namespace App\Http\Controllers\Director\Business;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class PreScrutinyController extends Controller
{
    // Contstructor
    public function __construct(){

    }

    /**
     * ===================================================
     * ===============< Pre Scrutiny >=================
     * ===================================================
     */
    // Pre Scrutiny Index
    public function index(){
        return view("director.business.pre_scrutiny.index");
    }
}
