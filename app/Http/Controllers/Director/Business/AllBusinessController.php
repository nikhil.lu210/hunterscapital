<?php

namespace App\Http\Controllers\Director\Business;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Controllers\General;
use App\Models\Director\Business\Policy\Policy;
use App\Models\Director\Clients\Client\Client;
use App\Models\Director\Clients\RiskList\RiskList;
use App\Models\SuperAdmin\Fund\Fund;
use App\Models\SuperAdmin\Provider\Provider;
use App\Models\SuperAdmin\Trust\Trust;
use App\User;

class AllBusinessController extends Controller
{
    // Contstructor
    public function __construct(){

    }

    /**
     * ===================================================
     * ===============< All Business >=================
     * ===================================================
     */
    // All Business Index
    public function index(){
        $businesses = Policy::with([
                'clientDetails' => function($query){
                    $query->select('id', 'client_title', 'client_forname', 'client_middle_name', 'client_surname');
                },
                'advisorDetails' => function($query){
                    $query->select('id', 'saluation', 'forname', 'middle_name', 'surname');
                }
            ])->get();

        // dd($businesses);
        return view("director.business.all_business.index", compact(['businesses']));
    }

    // All Business showInvestment
    public function showInvestment($id){
        $clients = Client::where('client_status', 1)->get();
        $advisors = User::where('role_id', 3)->where('user_status', 1)->get();
        $providers = Provider::whereNotNull('approved_at')->get();
        $funds = Fund::whereNotNull('approved_at')->get();
        $trusts = Trust::whereNotNull('approved_at')->get();
        $risk_lists = RiskList::where('status', 1)->get();


        $business = Policy::with([
            'clientDetails' => function($query){
                $query->select('id', 'client_title', 'client_forname', 'client_middle_name', 'client_surname');
            },
            'advisorDetails' => function($query){
                $query->select('id', 'saluation', 'forname', 'middle_name', 'surname');
            }, 'providerDetails', 'trustDetails'
        ])->where('id', $id)->first();

        // dd($business);

        return view("director.business.add_new_policy.investment.show", compact(['business', 'clients', 'advisors', 'providers', 'funds', 'trusts', 'risk_lists']));
    }

    // All Business showMortgage
    public function showMortgage($id){
        $clients = Client::where('client_status', 1)->get();
        $advisors = User::where('role_id', 3)->where('user_status', 1)->get();
        $providers = Provider::whereNotNull('approved_at')->get();
        $funds = Fund::whereNotNull('approved_at')->get();
        $trusts = Trust::whereNotNull('approved_at')->get();
        // $risk_lists = RiskList::where('status', 1)->get();


        $business = Policy::with([
            'clientDetails' => function($query){
                $query->select('id', 'client_title', 'client_forname', 'client_middle_name', 'client_surname');
            },
            'advisorDetails' => function($query){
                $query->select('id', 'saluation', 'forname', 'middle_name', 'surname');
            }, 'providerDetails', 'trustDetails'
        ])->where('id', $id)->first();

        return view("director.business.add_new_policy.mortgage.show", compact(['business', 'clients', 'advisors', 'providers', 'funds', 'trusts']));  // 'risk_lists'
    }

    // All Business showPension
    public function showPension($id){
        $clients = Client::where('client_status', 1)->get();
        $advisors = User::where('role_id', 3)->where('user_status', 1)->get();
        $providers = Provider::whereNotNull('approved_at')->get();
        $funds = Fund::whereNotNull('approved_at')->get();
        $trusts = Trust::whereNotNull('approved_at')->get();
        $risk_lists = RiskList::where('status', 1)->get();


        $business = Policy::with([
            'clientDetails' => function($query){
                $query->select('id', 'client_title', 'client_forname', 'client_middle_name', 'client_surname');
            },
            'advisorDetails' => function($query){
                $query->select('id', 'saluation', 'forname', 'middle_name', 'surname');
            }, 'providerDetails', 'trustDetails'
        ])->where('id', $id)->first();

        return view("director.business.add_new_policy.pension.show", compact(['business', 'clients', 'advisors', 'providers', 'funds', 'trusts', 'risk_lists']));
    }

    // All Business showProtection
    public function showProtection($id){
        $clients = Client::where('client_status', 1)->get();
        $advisors = User::where('role_id', 3)->where('user_status', 1)->get();
        $providers = Provider::whereNotNull('approved_at')->get();
        $funds = Fund::whereNotNull('approved_at')->get();
        $trusts = Trust::whereNotNull('approved_at')->get();
        // $risk_lists = RiskList::where('status', 1)->get();


        $business = Policy::with([
            'clientDetails' => function($query){
                $query->select('id', 'client_title', 'client_forname', 'client_middle_name', 'client_surname');
            },
            'advisorDetails' => function($query){
                $query->select('id', 'saluation', 'forname', 'middle_name', 'surname');
            }, 'providerDetails', 'trustDetails'
        ])->where('id', $id)->first();

        return view("director.business.add_new_policy.protection.show", compact(['business', 'clients', 'advisors', 'providers', 'funds', 'trusts']));  // , 'risk_lists'
    }
}
