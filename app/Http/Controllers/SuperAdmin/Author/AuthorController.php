<?php

namespace App\Http\Controllers\SuperAdmin\Author;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use Illuminate\Support\Facades\Validator;
use App\Models\SuperAdmin\Author\Author;

class AuthorController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $authors = Author::all();
        return view('superadmin.author.index', compact('authors'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('superadmin.author.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validator($request->all());

        $author = new Author();

        $author->authors_name   =     $request->authors_name;
        $author->contact_number =     $request->contact_number;
        $author->county         =     $request->county;
        $author->postcode       =     $request->postcode;
        $author->address        =     $request->address;
        $author->note           =     $request->note;

        $author->save();

        return redirect()->route('superadmin.author.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $author = Author::find($id);
        return view('superadmin.author.show', compact('author'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validator($request->all());

        $author = Author::find($id);

        $author->authors_name    =     $request->authors_name;
        $author->contact_number =     $request->contact_number;
        $author->county         =     $request->county;
        $author->postcode       =     $request->postcode;
        $author->address        =     $request->address;
        $author->note           =     $request->note;

        $author->save();

        return redirect()->route('superadmin.author.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }






    protected function validator(array $data)
    {
        return Validator::make($data, [
            'authors_name'      => ['required', 'string'],
            'county'            => ['string'],
            'postcode'          => ['string'],
            'address'           => ['string'],
            'note'              => ['string'],
            'contact_number'    => ['string']
        ]);
    }
}
