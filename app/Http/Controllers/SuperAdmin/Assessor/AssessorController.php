<?php

namespace App\Http\Controllers\SuperAdmin\Assessor;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use Illuminate\Support\Facades\Validator;
use App\Models\SuperAdmin\Assessor\Assessor;

class AssessorController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $assessors = Assessor::all();
        return view('superadmin.assessor.index', compact('assessors'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('superadmin.assessor.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validator($request->all());

        $assessor = new Assessor();

        $assessor->assessor_name   =     $request->assessor_name;
        $assessor->contact_number =     $request->contact_number;
        $assessor->county         =     $request->county;
        $assessor->postcode       =     $request->postcode;
        $assessor->address        =     $request->address;
        $assessor->note           =     $request->note;

        $assessor->save();

        return redirect()->route('superadmin.assessor.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $assessor = Assessor::find($id);
        return view('superadmin.assessor.show', compact('assessor'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validator($request->all());

        $assessor = Assessor::find($id);

        $assessor->assessor_name    =     $request->assessor_name;
        $assessor->contact_number =     $request->contact_number;
        $assessor->county         =     $request->county;
        $assessor->postcode       =     $request->postcode;
        $assessor->address        =     $request->address;
        $assessor->note           =     $request->note;

        $assessor->save();

        return redirect()->route('superadmin.assessor.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }






    protected function validator(array $data)
    {
        return Validator::make($data, [
            'authors_name'      => ['required', 'string'],
            'county'            => ['string'],
            'postcode'          => ['string'],
            'address'           => ['string'],
            'note'              => ['string'],
            'contact_number'    => ['string']
        ]);
    }
}
