<?php

namespace App\Http\Controllers\SuperAdmin\Provider;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use Illuminate\Support\Facades\Validator;
use App\Models\SuperAdmin\Provider\Provider;

class ProviderController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $providers = Provider::whereNotNull('approved_at')->get();
        return view('superadmin.provider.index', compact('providers'));
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('superadmin.provider.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validator($request->all());

        $provider = new Provider();

        $provider->provider_name  =     $request->provider_name;
        $provider->email_address  =     $request->email_address;
        $provider->county         =     $request->county;
        $provider->postcode       =     $request->postcode;
        $provider->address        =     $request->address;
        $provider->note           =     $request->note;
        $provider->approved_at    =     now();

        $provider->save();

        return redirect()->route('superadmin.provider.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $provider = Provider::find($id);
        return view('superadmin.provider.show', compact('provider'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validator($request->all());

        $provider = Provider::find($id);

        $provider->provider_name  =     $request->provider_name;
        $provider->email_address  =     $request->email_address;
        $provider->county         =     $request->county;
        $provider->postcode       =     $request->postcode;
        $provider->address        =     $request->address;
        $provider->note           =     $request->note;

        $provider->save();

        return redirect()->route('superadmin.provider.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    protected function validator(array $data)
    {
        return Validator::make($data, [
            'provider_name' => ['required', 'string'],
            'county'        => ['string'],
            'postcode'      => ['string'],
            'address'       => ['string'],
            'note'          => ['string'],
            'email_address' => ['string', 'email']
        ]);
    }



    public function nonApprovedProvider()
    {
        $providers = Provider::whereNull('approved_at')->get();
        // dd($providers);
        return view('superadmin.provider.approve', compact('providers'));
    }

    public function approveProvider($id)
    {
        $approve = Provider::find($id);

        $approve->approved_at = now();
        $approve->save();

        return redirect()->back();
    }
    public function refuseProvider($id)
    {
        $refuse = Provider::find($id);

        $refuse->delete();

        return redirect()->back();
    }
}
