<?php

namespace App\Http\Controllers\SuperAdmin\Trust;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use Illuminate\Support\Facades\Validator;
use App\Models\SuperAdmin\Trust\Trust;

class TrustController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $trusts = Trust::whereNotNull('approved_at')->get();
        return view('superadmin.trust.index', compact('trusts'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('superadmin.trust.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validator($request->all());

        $trust = new Trust();

        $trust->trust_name      =     $request->trust_name;
        $trust->forename        =     $request->forename;
        $trust->surname         =     $request->surname;
        $trust->contact_number  =     $request->contact_number;
        $trust->county          =     $request->county;
        $trust->postcode        =     $request->postcode;
        $trust->address         =     $request->address;
        $trust->note            =     $request->note;
        $trust->approved_at     =     now();

        $trust->save();

        return redirect()->route('superadmin.trust.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $trust = Trust::find($id);
        return view('superadmin.trust.show', compact('trust'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validator($request->all());

        $trust = Trust::find($id);

        $trust->trust_name      =     $request->trust_name;
        $trust->forename        =     $request->forename;
        $trust->surname         =     $request->surname;
        $trust->contact_number  =     $request->contact_number;
        $trust->county          =     $request->county;
        $trust->postcode        =     $request->postcode;
        $trust->address         =     $request->address;
        $trust->note            =     $request->note;

        $trust->save();

        return redirect()->route('superadmin.trust.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }






    protected function validator(array $data)
    {
        return Validator::make($data, [
            'trust_name'        =>  ['required', 'string'],
            'forename'          =>  ['required', 'string'],
            'surname'           =>  ['required', 'string'],
            'contact_number'    =>  ['string'],
            'county'            =>  ['required', 'string'],
            'postcode'          =>  ['required', 'string'],
            'address'           =>  ['string'],
            'note'              =>  ['string'],
        ]);
    }





    public function nonApprovedTrust()
    {
        $trusts = Trust::whereNull('approved_at')->get();
        // dd($trusts);
        return view('superadmin.trust.approve', compact('trusts'));
    }

    public function approveTrust($id)
    {
        $approve = Trust::find($id);

        $approve->approved_at = now();
        $approve->save();

        return redirect()->back();
    }
    public function refuseTrust($id)
    {
        $refuse = Trust::find($id);

        $refuse->delete();

        return redirect()->back();
    }
}
