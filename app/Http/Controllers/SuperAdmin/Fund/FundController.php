<?php

namespace App\Http\Controllers\SuperAdmin\Fund;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use Illuminate\Support\Facades\Validator;
use App\Models\SuperAdmin\Fund\Fund;

class FundController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $funds = Fund::whereNotNull('approved_at')->get();
        return view('superadmin.fund.index', compact('funds'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('superadmin.fund.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validator($request->all());

        $fund = new Fund();

        $fund->fund_name        =     $request->fund_name;
        $fund->mex_code         =     $request->mex_code;
        $fund->isin_code        =     $request->isin_code;
        $fund->sedol_code       =     $request->sedol_code;
        $fund->citi_code        =     $request->citi_code;
        $fund->bb_ticker_code   =     $request->bb_ticker_code;
        $fund->note             =     $request->note;
        $fund->approved_at      =     now();

        $fund->save();

        return redirect()->route('superadmin.fund.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $fund = Fund::find($id);
        return view('superadmin.fund.show', compact('fund'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validator($request->all());

        $fund = Fund::find($id);

        $fund->fund_name        =     $request->fund_name;
        $fund->mex_code         =     $request->mex_code;
        $fund->isin_code        =     $request->isin_code;
        $fund->sedol_code       =     $request->sedol_code;
        $fund->citi_code        =     $request->citi_code;
        $fund->bb_ticker_code   =     $request->bb_ticker_code;
        $fund->note             =     $request->note;

        $fund->save();

        return redirect()->route('superadmin.fund.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }


    protected function validator(array $data)
    {
        return Validator::make($data, [
            'fund_name'         => ['required', 'string'],
            'mex_code'          => ['string'],
            'isin_code'         => ['string'],
            'sedol_code'        => ['string'],
            'citi_code'         => ['string'],
            'bb_ticker_code'    => ['string'],
            'note'              => ['string'],
        ]);
    }


    public function nonApprovedFund()
    {
        $funds = Fund::whereNull('approved_at')->get();
        // dd($funds);
        return view('superadmin.fund.approve', compact('funds'));
    }

    public function approveFund($id)
    {
        $approve = Fund::find($id);

        $approve->approved_at = now();
        $approve->save();

        return redirect()->back();
    }
    public function refuseFund($id)
    {
        $refuse = Fund::find($id);

        $refuse->delete();

        return redirect()->back();
    }
}
