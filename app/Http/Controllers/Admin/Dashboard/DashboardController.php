<?php

namespace App\Http\Controllers\Admin\Dashboard;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class DashboardController extends Controller
{
    // Contstructor
    public function __construct(){

    }

    /**
     * ===================================================
     * ===============< Dashboard >=================
     * ===================================================
     */
    // Dashboard Index
    public function index(){
        return view("admin.dashboard.index");
    }
}
