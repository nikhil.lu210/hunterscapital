<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'forname','surname','email_address','mobile_number', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];


    public function role(){
        return $this->belongsTo('App\Role');
    }


    public function tasks(){
        return $this->hasMany('App\Models\Director\Company\Task\Task','assigned_to');
    }


    public function client(){
        return $this->hasOne('App\Models\Director\Clients\Client\Client','advisor');
    }

    public function promostions(){
        return $this->hasMany('App\Models\Director\Company\Compliance\Promotion\Promotion', 'requested_by');
    }

    public function complaints(){
        return $this->hasMany('App\Models\Director\Company\Compliance\Complaint\Complaint', 'advisor');
    }


    public function policies(){
        return $this->hasMany('App\Models\Director\Business\Policy\Policy', 'advisor');
    }


    public function preScrutinies(){
        return $this->hasMany('App\Models\Director\Business\PreScrutiny\PreScrutiny', 'advisor');
    }


    public function userCpds(){
        return $this->hasMany('App\Models\Director\TermAndCondition\CPD\CPD', 'user');
    }

    public function devSetByUserCpds(){
        return $this->hasMany('App\Models\Director\TermAndCondition\CPD\CPD', 'dev_set_by_user');
    }

    public function turnovers(){
        return $this->hasMany('App\Models\Director\Business\Turnover\Turnover', 'advisor');
    }


    public function spses(){
        return $this->hasMany('App\Models\Director\TermAndCondition\SPS\SPS', 'user');
    }

}
