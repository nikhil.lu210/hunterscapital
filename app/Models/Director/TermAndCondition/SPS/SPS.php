<?php

namespace App\Models\Director\TermAndCondition\SPS;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class SPS extends Model
{
    use SoftDeletes;

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['deleted_at'];


    public function userDetails()
    {
        return $this->belongsTo('App\User', 'user');
    }
}
