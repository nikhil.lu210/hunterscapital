<?php

namespace App\Models\Director\TermAndCondition\CPD;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class CPD extends Model
{
    use SoftDeletes;

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['deleted_at'];


    public function userDetails()
    {
        return $this->belongsTo('App\User', 'user');
    }
    public function devSetByUser()
    {
        return $this->belongsTo('App\User', 'dev_set_by_user');
    }
    public function authorDetails()
    {
        return $this->belongsTo('App\Models\SuperAdmin\Author\Author', 'dev_set_by_author');
    }
}
