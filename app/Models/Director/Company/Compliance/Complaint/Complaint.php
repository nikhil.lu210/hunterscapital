<?php

namespace App\Models\Director\Company\Compliance\Complaint;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Complaint extends Model
{
    use SoftDeletes;

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['deleted_at'];

    public function advisorDetails(){
        return $this->belongsTo('App\User', 'advisor');
    }

    public function clientDetails(){
        return $this->belongsTo('App\Models\Director\Clients\Client\Client', 'client');
    }

    public function assessorDetails(){
        return $this->belongsTo('App\Models\SuperAdmin\Assessor\Assessor', 'assessor');
    }

}
