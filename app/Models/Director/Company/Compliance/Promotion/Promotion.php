<?php

namespace App\Models\Director\Company\Compliance\Promotion;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Promotion extends Model
{
    use SoftDeletes;

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['deleted_at'];


    public function requestedBy(){
        return $this->belongsTo('App\User', 'requested_by');
    }

    public function closedBy(){
        return $this->belongsTo('App\Models\SuperAdmin\Author\Author', 'closed_by');
    }
    public function ifacAdvisor(){
        return $this->belongsTo('App\Models\SuperAdmin\Author\Author', 'ifac_advisor');
    }
    // public function jurisdiction(){
    //     return $this->belongsTo('App\Models\SuperAdmin\Author\Author', 'jurisdiction');
    // }
    public function reviewedBy(){
        return $this->belongsTo('App\Models\SuperAdmin\Author\Author', 'reviewer');
    }
    public function signedOfBy(){
        return $this->belongsTo('App\Models\SuperAdmin\Author\Author', 'signed_of_by');
    }
}
