<?php

namespace App\Models\Director\Company\Compliance\Visit;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Visit extends Model
{
    use SoftDeletes;

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['deleted_at'];


    public function signedOfBy(){
        return $this->belongsTo('App\Models\SuperAdmin\Author\Author', 'signed_off_by');
    }


    public function visitActions(){
        return $this->hasMany('App\Models\Director\Company\Compliance\Visit\VisitAction\VisitAction', 'visit_id');
    }
    public function visitFiles(){
        return $this->hasMany('App\Models\Director\Company\Compliance\Visit\VisitFile\VisitFile', 'visit_id');
    }



}
