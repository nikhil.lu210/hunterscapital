<?php

namespace App\Models\Director\Company\Compliance\Visit\VisitAction;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class VisitAction extends Model
{
    use SoftDeletes;

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['deleted_at'];

    public function visit(){
        return $this->belongsTo('App\Models\Director\Company\Compliance\Visit\Visit', 'visit_id');
    }
}
