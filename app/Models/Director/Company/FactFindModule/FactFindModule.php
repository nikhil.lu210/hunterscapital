<?php

namespace App\Models\Director\Company\FactFindModule;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class FactFindModule extends Model
{
    use SoftDeletes;

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['deleted_at'];

    public function dataFactfinds(){
        return $this->hasMany('AApp\Models\Director\Clients\FactFindData\FactFindData', 'factfind_id');
    }
}
