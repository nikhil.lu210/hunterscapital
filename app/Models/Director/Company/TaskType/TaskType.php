<?php

namespace App\Models\Director\Company\TaskType;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class TaskType extends Model
{
    use SoftDeletes;

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['deleted_at'];


    public function tasks(){
        return $this->hasMany('App\Models\Director\Company\Task\Task', 'task_type');
    }
}
