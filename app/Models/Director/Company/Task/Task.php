<?php

namespace App\Models\Director\Company\Task;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Task extends Model
{
    use SoftDeletes;

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['deleted_at'];

    public function director(){
        return $this->belongsTo('App\User','assigned_to');
    }

    public function taskClient(){
        return $this->belongsTo('App\Models\Director\Clients\Client\Client','client');
    }

    public function taskType(){
        return $this->belongsTo('App\Models\Director\Company\TaskType\TaskType', 'task_type');
    }


}
