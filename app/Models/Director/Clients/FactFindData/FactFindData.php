<?php

namespace App\Models\Director\Clients\FactFindData;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class FactFindData extends Model
{
    use SoftDeletes;

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['deleted_at'];


    public function moduleFactfind(){
        return $this->belongsTo('App\Models\Director\Company\FactFindModule\FactFindModule', 'factfind_id');
    }

    public function client(){
        return $this->belongsTo('App\Models\Director\Clients\Client\Client', 'client_id');
    }
}
