<?php

namespace App\Models\Director\Clients\LetterBuilder;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class LetterBuilder extends Model
{
    use SoftDeletes;

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['deleted_at'];


    public function client(){
        return $this->belongsTo('App\Models\Director\Clients\Client\Client', 'client');
    }
}
