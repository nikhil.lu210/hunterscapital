<?php

namespace App\Models\Director\Clients\ClientGrading;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ClientGrading extends Model
{
    use SoftDeletes;

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['deleted_at'];


    public function clients(){
        return $this->hasMany('App\Models\Director\Clients\Client\Client', 'grading');
    }
}
