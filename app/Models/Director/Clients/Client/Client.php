<?php

namespace App\Models\Director\Clients\Client;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Client extends Model
{
    use SoftDeletes;

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['deleted_at'];

    public function user(){
        return $this->hasOne('App\User','advisor');
    }

    public function tasks(){
        return $this->hasMany('App\Models\Director\Company\Task\Task', 'client');
    }


    public function clientGrading(){
        return $this->belongsTo('App\Models\Director\Clients\ClientGrading\ClientGrading', 'grading');
    }


    public function complaints(){
        return $this->hasMany('App\Models\Director\Company\Compliance\Complaint\Complaint', 'client');
    }


    public function policies(){
        return $this->hasMany('App\Models\Director\Business\Policy\Policy', 'client');
    }


    public function preScrutinies(){
        return $this->hasMany('App\Models\Director\Business\PreScrutiny\PreScrutiny', 'client');
    }


    public function turnovers(){
        return $this->hasMany('App\Models\Director\Business\Turnover\Turnover', 'client');
    }

    public function letterbuilders(){
        return $this->hasMany('App\Models\Director\Clients\LetterBuilder\LetterBuilder', 'client');
    }


    public function centerDocuments(){
        return $this->hasMany('App\Models\Director\Clients\Document\Document', 'client');
    }

    public function factfinds(){
        return $this->hasMany('App\Models\Director\Clients\FactFindData\FactFindData', 'client_id');
    }

    public function checklists(){
        return $this->hasMany('App\Models\Director\Clients\Checklist', 'client_id');
    }



}
