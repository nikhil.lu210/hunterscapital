<?php

namespace App\Models\Director\Clients\Document;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Document extends Model
{
    use SoftDeletes;

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['deleted_at'];



    public function documentClient(){
        return $this->belongsTo('App\Models\Director\Clients\Client\Client', 'client');
    }


    public function policy(){
        return $this->belongsTo('App\Models\Director\Business\Policy\Policy', 'client_policy');
    }

}
