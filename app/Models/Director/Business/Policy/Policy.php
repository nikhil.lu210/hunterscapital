<?php

namespace App\Models\Director\Business\Policy;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Policy extends Model
{
    use SoftDeletes;

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['deleted_at'];


    public function clientDetails()
    {
        return $this->belongsTo('App\Models\Director\Clients\Client\Client', 'client');
    }


    public function advisorDetails()
    {
        return $this->belongsTo('App\User', 'advisor');
    }

    public function providerDetails()
    {
        return $this->belongsTo('App\Models\SuperAdmin\Provider\Provider', 'provider');
    }


    public function trustDetails()
    {
        return $this->belongsTo('App\Models\SuperAdmin\Trust\Trust', 'trust');
    }


    public function turnovers(){
        return $this->hasMany('App\Models\Director\Business\Turnover\Turnover', 'client_policy');
    }


    public function documents(){
        return $this->hasMany('App\Models\Director\Clients\Document\Document', 'client_policy');
    }


}
