<?php

namespace App\Models\Director\Business\Turnover;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Turnover extends Model
{
    use SoftDeletes;

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['deleted_at'];


    public function clientDetails()
    {
        return $this->belongsTo('App\Models\Director\Clients\Client\Client', 'client');
    }

    public function policyDetails(){
        return $this->belongsTo('App\Models\Director\Business\Policy\Policy', 'client_policy');
    }

    public function advisorDetails()
    {
        return $this->belongsTo('App\User', 'advisor');
    }

}
