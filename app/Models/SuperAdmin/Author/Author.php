<?php

namespace App\Models\SuperAdmin\Author;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Author extends Model
{
    use SoftDeletes;

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['deleted_at'];


    public function closedByPromotion(){
        return $this->hasMany('App\Models\Director\Company\Compliance\Promotion\Promotion', 'closed_by');
    }
    public function ifacAdvisorPromotion(){
        return $this->hasMany('App\Models\Director\Company\Compliance\Promotion\Promotion', 'ifac_advisor');
    }
    // public function jurisdictionPromotion(){
    //     return $this->hasMany('App\Models\Director\Company\Compliance\Promotion\Promotion', 'jurisdiction');
    // }
    public function reviewerPromotion(){
        return $this->hasMany('App\Models\Director\Company\Compliance\Promotion\Promotion', 'reviewer');
    }
    public function signedOfByPromotion(){
        return $this->hasMany('App\Models\Director\Company\Compliance\Promotion\Promotion', 'signed_of_by');
    }


    public function visits(){
        return $this->hasMany('App\Models\Director\Company\Compliance\Visit\Visit', 'signed_of_by');
    }

    public function cpdsDetails(){
        return $this->hasMany('App\Models\Director\TermAndCondition\CPD\CPD', 'dev_set_by_author');
    }
}
