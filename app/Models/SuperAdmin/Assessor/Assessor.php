<?php

namespace App\Models\SuperAdmin\Assessor;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Assessor extends Model
{
    use SoftDeletes;

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['deleted_at'];

    public function complaints(){
        return $this->hasMany('App\Models\Director\Company\Compliance\Complaint\Complaint', 'assessor');
    }
}
