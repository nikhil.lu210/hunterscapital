<?php

namespace App\Models\SuperAdmin\Provider;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Provider extends Model
{
    use SoftDeletes;

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['deleted_at'];


    public function policies(){
        return $this->hasMany('App\Models\Director\Business\Policy\Policy', 'provider');
    }
}
