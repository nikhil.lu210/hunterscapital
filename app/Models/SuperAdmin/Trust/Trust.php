<?php

namespace App\Models\SuperAdmin\Trust;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;


class Trust extends Model
{
    use SoftDeletes;

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['deleted_at'];


    public function policies(){
        return $this->hasMany('App\Models\Director\Business\Policy\Policy', 'trust');
    }

}
