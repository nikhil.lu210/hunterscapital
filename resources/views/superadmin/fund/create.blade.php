@extends('layouts.superadmin.app')

@section('page_title', '| FUNDS')

@section('stylesheet_links')
    {{--  External CSS  --}}
    <link rel="stylesheet" href="{{ asset('custom/assets/vendor/select2/select2.css') }}" />

@endsection

@section('stylesheet')
    {{--  External CSS  --}}

@endsection

@section('content')

<header class="page-header">
    <h2>Funds</h2>

    <div class="right-wrapper pull-right">
        <ol class="breadcrumbs">
            <li>
                <a href="{{ Route('superadmin.dashboard.index') }}">
                    <i class="fa fa-home"></i>
                </a>
            </li>
            <li><span class="active">Funds</span></li>
            <li><span>Assign New Fund</span></li>
        </ol>
    </div>
</header>

<!-- start: page -->
<section class="detials-page">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-5 col-lg-5 col-xl-5 col-centered">
                {{-- Profile Details  --}}
                <section class="panel panel-danger">
                    <form action="{{ route('superadmin.fund.store') }}" method="post">
                    @csrf
                        <header class="panel-heading">
                            <div class="panel-actions">
                                <a href="#" class="fa fa-caret-down"></a>
                            </div>

                            <h2 class="panel-title">Assign New Fund</h2>
                        </header>

                        <div class="panel-body">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label class="control-label">Fund Name <span class="required">*</span></label>
                                        <input type="text" name="fund_name" class="form-control @error('fund_name') is-invalid @enderror" placeholder="Eg.: Fund Name" required>

                                        {{-- Validation Alert Messages --}}
                                        @error('fund_name')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                        @enderror
                                    </div>
                                </div>

                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label class="control-label">MEX Code</label>
                                        <input type="text" name="mex_code" class="form-control @error('mex_code') is-invalid @enderror" placeholder="Eg.: MEX-001">

                                        {{-- Validation Alert Messages --}}
                                        @error('mex_code')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                        @enderror
                                    </div>
                                </div>

                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label class="control-label">ISIN Code</label>
                                        <input type="text" name="isin_code" class="form-control @error('isin_code') is-invalid @enderror" placeholder="Eg.: ISIN-001">

                                        {{-- Validation Alert Messages --}}
                                        @error('isin_code')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                        @enderror
                                    </div>
                                </div>

                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label class="control-label">SEDOL Code</label>
                                        <input type="text" name="sedol_code" class="form-control @error('sedol_code') is-invalid @enderror" placeholder="Eg.: SEDOL-001">

                                        {{-- Validation Alert Messages --}}
                                        @error('sedol_code')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                        @enderror
                                    </div>
                                </div>

                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label class="control-label">CITI Code</label>
                                        <input type="text" name="citi_code" class="form-control @error('citi_code') is-invalid @enderror" placeholder="Eg.: CITI-001">

                                        {{-- Validation Alert Messages --}}
                                        @error('citi_code')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                        @enderror
                                    </div>
                                </div>

                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label class="control-label">BB Ticker Code</label>
                                        <input type="text" name="bb_ticker_code" class="form-control @error('bb_ticker_code') is-invalid @enderror" placeholder="Eg.: BB-TICKER-001">

                                        {{-- Validation Alert Messages --}}
                                        @error('bb_ticker_code')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                        @enderror
                                    </div>
                                </div>

                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label class="control-label">Note</label>
                                        <textarea name="note" class="form-control @error('note') is-invalid @enderror" placeholder="Eg.: Simple Note Here"></textarea>

                                        {{-- Validation Alert Messages --}}
                                        @error('note')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                        @enderror
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="panel-footer text-right">
                            <button type="submit" class="btn btn-dark btn-custom">Assign Now</button>
                        </div>
                    </form>
                </section>
            </div>
        </div>
    </div>
</section>
<!-- end: page -->

@endsection


@section('script_links')
    {{--  External Javascript  --}}
    <script src="{{ asset('custom/assets/vendor/select2/select2.js') }}"></script>

@endsection

@section('scripts')
    {{--  External Javascript  --}}

@endsection
