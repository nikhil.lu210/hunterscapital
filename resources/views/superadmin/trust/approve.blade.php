@extends('layouts.superadmin.app')

@section('page_title', '| TRUSTS | Non-Approved')

@section('stylesheet_links')
    {{--  External CSS  --}}
    <link rel="stylesheet" href="{{ asset('custom/assets/vendor/select2/select2.css') }}" />
    <link rel="stylesheet" href="{{ asset('custom/assets/vendor/jquery-datatables-bs3/assets/css/datatables.css') }}" />

@endsection

@section('stylesheet')
    {{--  External CSS  --}}
    <style>
        .dataTables_wrapper .DTTT.btn-group {
            display: inline-block;
            position: absolute;
            top: -74px;
            right: 0px;
        }
    </style>
@endsection

@section('content')

<header class="page-header">
    <h2>Non-Approved Trusts</h2>

    <div class="right-wrapper pull-right">
        <ol class="breadcrumbs">
            <li>
                <a href="{{ Route('superadmin.dashboard.index') }}">
                    <i class="fa fa-home"></i>
                </a>
            </li>
            <li><span class="active">Trusts</span></li>
            <li><span>Non-Approved Trusts</span></li>
        </ol>
    </div>
</header>

<!-- start: page -->
<section class="panel">
    <header class="panel-heading">
        <h2 class="panel-title">Non-Approved Trusts</h2>
    </header>
    <div class="panel-body">
        <table class="table table-bordered table-striped mb-none" id="datatable-tabletools" data-swf-path="assets/vendor/jquery-datatables/extras/TableTools/swf/copy_csv_xls_pdf.swf">
            <thead>
                <tr class="table-header-row">
                    <th>Sl.</th>
                    <th>Trust Name</th>
                    <th>Trustee Full Name</th>
                    <th>Contact No</th>
                    <th>County</th>
                    <th>Post Code</th>
                    <th>Address</th>
                    <th>Note</th>
                    <th>Requested At</th>
                    <th class="text-center">Actions</th>
                </tr>
            </thead>
            <tbody>
                @foreach ($trusts as $sl=>$data)
                    <tr class="table-body-row">
                        <th>{{ $sl+1 }}</th>
                        <td>{{ $data->trust_name }}</td>
                        <td>{{ $data->forename.' '.$data->surname }}</td>
                        <td>{{ $data->contact_number }}</td>
                        <td>{{ $data->county }}</td>
                        <td>{{ $data->postcode }}</td>
                        <td>{{ $data->address }}</td>
                        <td>{{ $data->note }}</td>
                        <td>{{ $data->created_at->format('d M, Y') }}</td>
                        <td class="action-td text-center">
                            <a href="{{ route('superadmin.trust.non_approved_trust.refuse', ['id' => $data->id]) }}" class="btn btn-danger btn-sm btn-sm-custom btn-custom-danger btn-delete" onclick="return confirm('Are You Sure Want to Refuse this trust request and Delete it...?');">
                                <i class="fa fa-trash"></i>
                            </a>
                            <a href="{{ route('superadmin.trust.non_approved_trust.approve', ['id' => $data->id]) }}" class="btn btn-dark btn-sm btn-sm-custom btn-custom-info" onclick="return confirm('Are You Sure Want to Approve this trust Request...?');">
                                <i class="fa fa-check"></i>
                            </a>
                        </td>
                    </tr>
                @endforeach
            </tbody>
        </table>
    </div>
</section>
<!-- end: page -->

@endsection


@section('script_links')
    {{--  External Javascript  --}}
    <script src="{{ asset('custom/assets/vendor/select2/select2.js') }}"></script>
    <script src="{{ asset('custom/assets/vendor/jquery-datatables/media/js/jquery.dataTables.js') }}"></script>
    <script src="{{ asset('custom/assets/vendor/jquery-datatables/extras/TableTools/js/dataTables.tableTools.min.js') }}"></script>
    <script src="{{ asset('custom/assets/vendor/jquery-datatables-bs3/assets/js/datatables.js') }}"></script>

    <script src="{{ asset('custom/assets/javascripts/tables/examples.datatables.default.js') }}"></script>
    <script src="{{ asset('custom/assets/javascripts/tables/examples.datatables.row.with.details.js') }}"></script>
    <script src="{{ asset('custom/assets/javascripts/tables/examples.datatables.tabletools.js') }}"></script>

@endsection

@section('scripts')
    {{--  External Javascript  --}}

@endsection
