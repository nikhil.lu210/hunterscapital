@extends('layouts.director.app')

@section('page_title', '| Settings | My Profile')

@section('stylesheet_links')
    {{--  External CSS  --}}

@endsection

@section('stylesheet')
    {{--  External CSS  --}}
    <style>
    .modal-header .close {
        outline: none !important;
        border: 1px solid #8e8e8e;
        padding: 0px 5px;
        border-radius: 0;
        opacity: 1;
        color: #8e8e8e;
        transition: 0.3s all ease-in-out;
    }
    .modal-header .close:hover {
        outline: none !important;
        border: 1px solid #0f1b25;
        background-color: #0f1b25;
        opacity: 1;
        color: #ffffff;
        transition: 0.3s all ease-in-out;
    }
    .modal-title{
        color: #0f1b25;
    }
    .modal-content {
        border: 1px solid #e5e5e5;
        border-radius: 0;
        box-shadow: 0 3px 9px rgba(0, 0, 0, 0.1);
    }
    .btn.btn-dark.btn-custom.btn-block {
        margin: 12px 0px;
        border-radius: 0px;
    }
    @media (min-width: 768px){
        .modal-dialog {
            width: 500px;
            margin: 11% auto;
        }
    }
    </style>
@endsection

@section('content')

<header class="page-header">
    <h2>{{ $profile->saluation.' '.$profile->forname.' '.$profile->middle_name.' '.$profile->surname }}</h2>

    <div class="right-wrapper pull-right">
        <ol class="breadcrumbs">
            <li>
                <a href="{{ route('director.dashboard.index') }}">
                    <i class="fa fa-home"></i>
                </a>
            </li>
            <li><span class="active">Settings</span></li>
            <li><span>My Account</span></li>
        </ol>
    </div>
</header>

<!-- start: page -->

<div class="row">
    <div class="col-md-12">
        @if (session('danger'))
            <div class="alert alert-danger" role="alert">
                {{ session('danger') }}
            </div>
        @elseif (session('success'))
            <div class="alert alert-success" role="alert">
                {{ session('success') }}
            </div>
        @elseif (session('info'))
            <div class="alert alert-info" role="alert">
                {{ session('info') }}
            </div>
        @elseif (session('primary'))
            <div class="alert alert-primary" role="alert">
                {{ session('primary') }}
            </div>
        @elseif (session('dark'))
            <div class="alert alert-dark" role="alert">
                {{ session('dark') }}
            </div>
        @endif
    </div>

    <div class="col-md-4 col-lg-4">

        <section class="panel">
            <div class="panel-body">
                <div class="thumb-info mb-md">
                    <img src="{{ asset('custom/assets/images/!logged-user.jpg') }}" class="rounded img-responsive" alt="Image Not Found">
                    <div class="thumb-info-title">
                        <span class="thumb-info-inner">
                            {{ $profile->saluation.' '.$profile->forname.' '.$profile->middle_name.' '.$profile->surname }}
                        </span>
                        <span class="thumb-info-type">Director</span>
                    </div>
                </div>

                <div class="widget-toggle-expand mb-md">
                    <div class="widget-content-expanded">
                        <ul class="simple-todo-list">
                            <li><a class="text-muted" href="#" data-toggle="modal" data-target="#update_profile">Update Profile</a></li>
                            <li><a class="text-muted" href="#" data-toggle="modal" data-target="#update_password">Update Password</a></li>
                        </ul>
                    </div>
                </div>

            </div>
        </section>

    </div>

    <div class="col-md-8 col-lg-8">
        <section class="panel">
            <header class="panel-heading">
                <h2 class="panel-title">Profile Details</h2>
            </header>

            <div class="panel-body">
                <table class="table table-bordered table-striped mb-none">
                    <tr>
                        <th>Forename</th>
                        <td>{{ $profile->forname }}</td>
                    </tr>

                    <tr>
                        <th>Middlename</th>
                        <td>{{ $profile->middle_name }}</td>
                    </tr>

                    <tr>
                        <th>Surname</th>
                        <td>{{ $profile->surname }}</td>
                    </tr>

                    <tr>
                        <th>Contact Number</th>
                        <td>{{ $profile->mobile_number }}</td>
                    </tr>

                    <tr>
                        <th>Email Address</th>
                        <td>{{ $profile->email_address }}</td>
                    </tr>

                    <tr>
                        <th>Address</th>
                        <td>
                            <ul>
                                <li>County: {{ $profile->county_old }}</li>
                                <li>City: {{ $profile->town_city }}</li>
                                <li>Post Code: {{ $profile->post_code }}</li>
                                <li>Address: {{ $profile->address }}</li>
                            </ul>
                        </td>
                    </tr>

                    <tr>
                        <th>Joined Date in Network</th>
                        <td>{{ $profile->created_at->format('d M Y') }}</td>
                    </tr>

                    <tr>
                        <th>Status</th>
                        <td>
                            @if ($profile->user_status == 1)
                                Active
                            @else
                                Inactive
                            @endif
                        </td>
                    </tr>
                </table>
            </div>
        </section>
    </div>

</div>
<!-- end: page -->




{{-- ========================< Profile Update Modal Starts >======================= --}}
<div class="modal fade" id="update_profile" tabindex="-1" role="dialog" aria-labelledby="gridSystemModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="gridSystemModalLabel"><b>Update Profile</b></h4>
            </div>
            <form action="{{ route('director.setting.my_account.update') }}" method="POST">
                @csrf
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-12">
                            <label class="control-label">Forname <span class="required">*</span></label>
                            <input type="text" name="forname" class="form-control  @error('forname') is-invalid @enderror" placeholder="eg.: Jhon" value="{{ $profile->forname }}" autocomplete="off" required>

                            {{-- Validation Alert Messages --}}
                            @error('forname')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>

                        <div class="col-md-12">
                            <label class="control-label">Middle Name</label>
                            <input type="text" name="middle_name" class="form-control  @error('middle_name') is-invalid @enderror" placeholder="eg.: Joe" value="{{ $profile->middle_name }}" autocomplete="off">

                            {{-- Validation Alert Messages --}}
                            @error('middle_name')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>

                        <div class="col-md-12">
                            <label class="control-label">Surname <span class="required">*</span></label>
                            <input type="text" name="surname" class="form-control  @error('surname') is-invalid @enderror" placeholder="eg.: Doe" value="{{ $profile->surname }}" autocomplete="off" required>

                            {{-- Validation Alert Messages --}}
                            @error('surname')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>

                        <div class="col-md-12">
                            <label class="control-label">Email Address <span class="required">*</span></label>
                            <input type="email" name="email_address" class="form-control  @error('email_address') is-invalid @enderror" placeholder="eg.: emailaddress@mail.com" value="{{ $profile->email_address }}" autocomplete="off" required>

                            {{-- Validation Alert Messages --}}
                            @error('email_address')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>

                        <div class="col-md-12">
                            <label class="control-label">Mobile Number</label>
                            <input type="text" name="mobile_number" class="form-control  @error('mobile_number') is-invalid @enderror" placeholder="eg.: +880 1234 456789" value="{{ $profile->mobile_number }}" autocomplete="off">

                            {{-- Validation Alert Messages --}}
                            @error('mobile_number')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>

                        <div class="col-md-12">
                            <label class="control-label">Address</label>
                            <textarea rows="3" name="address" class="form-control  @error('address') is-invalid @enderror" placeholder="eg.: Address">{{ $profile->address }}</textarea>

                            {{-- Validation Alert Messages --}}
                            @error('address')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                    </div>
                </div>

                <div class="modal-footer text-right">
                    <button type="submit" class="btn btn-dark btn-custom btn-sm">Update Your Profile</button>
                </div>
            </form>
        </div>
    </div>
</div>
{{-- =========================< Profile Update Modal Ends >======================== --}}




{{-- ========================< Password Update Modal Starts >======================= --}}
<div class="modal fade" id="update_password" tabindex="-1" role="dialog" aria-labelledby="gridSystemModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="gridSystemModalLabel"><b>Update Password</b></h4>
            </div>
            <form action="{{ route('director.setting.my_account.password.change') }}" method="POST">
                @csrf
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label for="old_password">Old Password <span class="required">*</span></label>
                                <input class="form-control {{ $errors->has('old_password') ? ' is-invalid' : '' }}" placeholder="Old Password" value="{{ old('old_password') }}" required name="old_password" type="password">

                                @if ($errors->has('old_password'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('old_password') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="col-md-12">
                            <div class="form-group">
                                <label for="new_password">New Password <span class="required">*</span></label>
                                <input class="form-control {{ $errors->has('new_password') ? ' is-invalid' : '' }}" placeholder="New Password" value="{{ old('new_password') }}" required name="new_password" type="password">

                                @if ($errors->has('new_password'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('new_password') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="col-md-12">
                            <div class="form-group">
                                <label for="confirm_password">Confirm New Password <span class="required">*</span></label>
                                <input class="form-control {{ $errors->has('confirm_password') ? ' is-invalid' : '' }}" placeholder="Confirm New Password" value="{{ old('confirm_password') }}" required name="confirm_password" type="password">

                                @if ($errors->has('confirm_password'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('confirm_password') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                    </div>
                </div>

                <div class="modal-footer text-right">
                    <button type="submit" class="btn btn-dark btn-custom btn-sm">Update Your Password</button>
                </div>
            </form>
        </div>
    </div>
</div>
{{-- =========================< Password Update Modal Ends >======================== --}}
@endsection


@section('script_links')
{{--  External Javascript  --}}

@endsection

@section('scripts')
    {{--  External Javascript  --}}

@endsection
