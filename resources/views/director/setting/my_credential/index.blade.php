@extends('layouts.director.app')

@section('page_title', '| Settings | My Credentials')

@section('stylesheet_links')
    {{--  External CSS  --}}
    
@endsection

@section('stylesheet')
    {{--  External CSS  --}}
    <style>

    </style>
@endsection

@section('content')

<header class="page-header">
    <h2>My Credentials</h2>

    <div class="right-wrapper pull-right">
        <ol class="breadcrumbs">
            <li>
                <a href="{{ route('director.dashboard.index') }}">
                    <i class="fa fa-home"></i>
                </a>
            </li>
            <li><span class="active">Settings</span></li>
            <li><span class="active">My Account</span></li>
            <li><span>My Credentials</span></li>
        </ol>
    </div>
</header>

<!-- start: page -->

<!-- end: page -->
@endsection


@section('script_links')
{{--  External Javascript  --}}

@endsection

@section('scripts')
    {{--  External Javascript  --}}

@endsection