@extends('layouts.director.app')

@section('page_title', '| Company | Fact Find')

@section('stylesheet_links')
    {{--  External CSS  --}}
    <link rel="stylesheet" href="{{ asset('custom/assets/vendor/select2/select2.css') }}" />
    <link rel="stylesheet" href="{{ asset('custom/assets/vendor/bootstrap-multiselect/bootstrap-multiselect.css') }}" />
    <link rel="stylesheet" href="{{ asset('custom/assets/vendor/select2/select2.css') }}" />
    <link rel="stylesheet" href="{{ asset('custom/assets/vendor/bootstrap-tagsinput/bootstrap-tagsinput.css') }}" />

    <link rel="stylesheet" href="{{ asset('custom/assets/vendor/bootstrap-datepicker/css/datepicker3.css') }}" />

@endsection

@section('stylesheet')
    {{--  External CSS  --}}
    <style>
        .panel .panel .panel-heading {
            padding: 10px 15px;
        }
        .panel {
            border-radius: 0px;
            border: 1px solid #555555;
            margin-bottom: 0px;
        }
        .tab-content .panel-footer {
            margin: 0px;
        }
        .tab-content{
            padding: 0px;
        }
        .nav-tabs li a,
        .nav-tabs li a:hover{
            background: #f6f6f6;
            border-radius: 0px;
            border-bottom: none;
            border-left: 1px solid #555555;
            border-right: 1px solid #555555;
            border-top: 3px solid #555555;
            border-color: #555;
            color: #555555;
            font-weight: bold;
        }
        .nav-tabs li.active a,
        .nav-tabs li.active a:hover,
        .nav-tabs li.active a:focus,
        .nav-tabs li.active a:active {
            background: #555;
            color: #fff;
            font-weight: bold;
            border-color: #555555;
            border-left-color: #555;
            border-radius: 0px;
        }
        .tabs-vertical .nav-tabs li a,
        .tabs-vertical .nav-tabs li a:hover{
            background: #f6f6f6;
            border-radius: 0px;
            border-bottom: none;
            border-left: 1px solid #555555;
            border-right: 1px solid #555555;
            border-top: 3px solid #555555;
            border-color: #555;
            color: #555555;
            font-weight: bold;
        }
        .tabs-vertical .nav-tabs li.active a,
        .tabs-vertical .nav-tabs li.active a:hover,
        .tabs-vertical .nav-tabs li.active a:focus,
        .tabs-vertical .nav-tabs li.active a:active {
            background: #555;
            color: #fff;
            font-weight: bold;
            border-color: #555555;
            border-left-color: #555;
            border-radius: 0px;
        }
        .tabs-vertical .nav-tabs > li:last-child a
        .tabs-vertical .nav-tabs > li:last-child a:hover {
            border-radius: 0px !important;
            border-bottom: 1px solid #555 !important;
        }
        .modal-header .close {
            outline: none !important;
            border: 1px solid #8e8e8e;
            padding: 0px 5px;
            border-radius: 0;
            opacity: 1;
            color: #8e8e8e;
            transition: 0.3s all ease-in-out;
        }
        .modal-header .close:hover {
            outline: none !important;
            border: 1px solid #0f1b25;
            background-color: #0f1b25;
            opacity: 1;
            color: #ffffff;
            transition: 0.3s all ease-in-out;
        }
        .modal-title{
            color: #0f1b25;
        }
        .modal-content {
            border: 1px solid #e5e5e5;
            border-radius: 0;
            box-shadow: 0 3px 9px rgba(0, 0, 0, 0.1);
        }
        .btn.btn-dark.btn-custom.btn-block {
            margin: 12px 0px;
            border-radius: 0px;
        }
        .d-none{
            display: none !important;
        }
        @media (min-width: 768px){
            .modal-dialog {
                width: 500px;
                margin: 11% auto;
            }
        }
    </style>
@endsection

@section('content')

<header class="page-header">
    <h2>Fact Find</h2>

    <div class="right-wrapper pull-right">
        <ol class="breadcrumbs">
            <li>
                <a href="{{ Route('director.dashboard.index') }}">
                    <i class="fa fa-home"></i>
                </a>
            </li>
            <li><span class="active">Company</span></li>
            <li><span>Fact Find</span></li>
        </ol>
    </div>
</header>

<!-- start: page -->
<section class="panel">
    <header class="panel-heading">
        <h2 class="panel-title"><b>All Fact Finds</b></h2>
        <button type="button" data-toggle="modal" data-target="#add_new_fact_find_modal" class="btn btn-primary btn-custom btn-sm header-btn"> <i class="fa fa-plus"></i> Fact Find</button>
    </header>

    <div class="panel-body">
        <div class="row">
            <div class="col-md-12">
                <div class="tabs">
                    <ul class="nav nav-tabs">
                        @foreach($fact_modules as $key => $fact_module)
                        <li class="@if($key==0)active @endif">
                            <a href="#fact_find_id_{{ $fact_module->id }}" data-toggle="tab">{{ $fact_module->factfind_name }}</a>
                        </li>
                        @endforeach

                    </ul>

                    <div class="tab-content">
                        @foreach($fact_modules as $key => $fact_module)
                            <section id="fact_find_id_{{ $fact_module->id }}" class="panel tab-pane @if($key==0)active @endif">
                                <header class="panel-heading">
                                    <h2 class="panel-title">{{ $fact_module->factfind_name }}</h2>

                                    <button type="button" data-target="#add_fact_find_section" data-todo="{{ $fact_module }}" data-toggle="modal" class="btn btn-primary btn-custom btn-sm header-btn" style="margin-right: 10px;"><i class="fa fa-plus"></i> Section</button>


                                    <button type="button" data-target="#edit_fact_find_module" data-todo="{{ $fact_module }}" data-toggle="modal" class="btn btn-info btn-custom btn-sm header-btn" style="margin-right: 10px;"><i class="fa fa-pen"></i></button>
                                    @if($fact_module->isDefault == 0)
                                    <a href="{{ route('director.company.fact_find.destroy', ['id' => $fact_module->id]) }}" class="btn btn-danger btn-custom btn-sm header-btn" style="margin-right: 10px;" onclick="return confirm('Are you sure want to delete {{ $fact_module->factfind_name }} ?');"><i class="fa fa-trash"></i></a>
                                    @endif
                                </header>
                                <div class="panel-body">
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="tabs tabs-vertical tabs-left">
                                                @php
                                                    $factfinds = array();
                                                    $factfinds = (array)json_decode($fact_module->factfinds, true);

                                                @endphp
                                                <ul class="nav nav-tabs col-md-3">
                                                    @foreach($factfinds as $list => $factfind )
                                                        <li @if($list == 0)class="active"@endif>
                                                            <a href="#fact_find_section_id_{{ $list."section" }}" data-toggle="tab" aria-expanded="true">{{ $factfind['name'] }}</a>
                                                        </li>
                                                    @endforeach
                                                </ul>

                                                <div class="tab-content col-md-12">

                                                    @foreach($factfinds as $list => $factfind )
                                                        <section id="fact_find_section_id_{{ $list."section" }}" class="panel tab-pane @if($list == 0)active @endif">
                                                            <header class="panel-heading">
                                                                <h2 class="panel-title">{{ $factfind['name'] }}</h2>
                                                                {{-- New Question --}}
                                                                <a href="#new_question_modal" data-todo="{{ json_encode($factfind) }}" data-section_id="{{ $list }}" data-fact_module_id="{{ $fact_module->id }}" data-toggle="modal" class="btn btn-primary btn-custom btn-sm header-btn"><i class="fa fa-plus"></i> Question</a>

                                                                {{-- New Title --}}
                                                                {{-- <a href="#" class="btn btn-dark btn-custom btn-sm header-btn" style="margin-right: 10px;">New Title</a> --}}

                                                                {{-- Edit Section --}}
                                                                <button type="button" data-target="#edit_fact_find_section" data-module_id="{{ $fact_module->id }}" data-section_id="{{ $list }}" data-todo="{{ json_encode($factfind) }}" data-toggle="modal" class="btn btn-info btn-custom btn-sm header-btn" style="margin-right: 10px;"><i class="fa fa-pen"></i></button>

                                                                {{-- delete Section --}}
                                                                <a href="{{ route('director.company.fact_find.destroy_section', ['module_id' => $fact_module->id, 'section_id' => $list]) }}" class="btn btn-danger btn-custom btn-sm header-btn" onclick="return confirm('Are you sure want to delete {{ $factfind['name'] }} ?');" style="margin-right: 10px;"><i class="fa fa-trash"></i></a>
                                                            </header>

                                                            <div class="panel-body">
                                                                <div class="row">
                                                                    {{-- {{dd($factfind['module'])}} --}}
                                                                    @if($factfind['module'] != null)
                                                                        @foreach($factfind['module'] as $item => $data)

                                                                            <div class="col-md-6">
                                                                                <div class="panel">
                                                                                    <header class="panel-heading">
                                                                                        <h4 class="panel-title">{{ $data['question_name'] }}</h4>
                                                                                        <a href="{{ route('director.company.fact_find.destroy_question', ['module_id' => $fact_module->id, 'section_id' => $list, 'question_id' => $item]) }}" class="btn btn-danger btn-sm header-btn" onclick="return confirm('Are you sure want to delete {{ $data['question_name'] }} ?');"><i class="fa fa-trash"></i></a>
                                                                                    </header>
                                                                                    <form action="{{ route('director.company.fact_find.update_question', ['module_id' => $fact_module->id, 'section_id' => $list, 'question_id' => $item]) }}" method="post">
                                                                                        @csrf
                                                                                        <div class="panel-body">
                                                                                            <div class="row">
                                                                                                <div class="col-md-12">
                                                                                                    <label class="control-label">Question Name</label>
                                                                                                    <input type="text" autocomplete="off" name="question_name" class="form-control form-control-sm @error('question_name') is-invalid @enderror" placeholder="eg.: Question Name" value="{{ $data['question_name'] }}" autocomplete="off">

                                                                                                    {{-- Validation Alert Messages --}}
                                                                                                    @error('question_name')
                                                                                                        <span class="invalid-feedback" role="alert">
                                                                                                            <strong>{{ $message }}</strong>
                                                                                                        </span>
                                                                                                    @enderror
                                                                                                </div>

                                                                                                <div class="col-md-12">
                                                                                                    <label class="control-label">Field Type</label>
                                                                                                    <select name="field_type" id="field_type_{{ $fact_module->id.'_'.$list.'_'.$item }}" data-plugin-selectTwo class="form-control populate field-type @error('field_type') is-invalid @enderror">
                                                                                                        <option disabled>Select Field Type</option>
                                                                                                        <option @if($data['field_type'] == "text")selected @endif value="text">Text</option>
                                                                                                        <option @if($data['field_type'] == "textarea")selected @endif value="textarea">Multi-Line Text Area</option>
                                                                                                        <option @if($data['field_type'] == "date")selected @endif value="date">Date</option>
                                                                                                        <option @if($data['field_type'] == "checkbox")selected @endif value="checkbox">Check box</option>
                                                                                                        <option @if($data['field_type'] == "select")selected @endif value="select">Select Box</option>
                                                                                                        <option @if($data['field_type'] == "number")selected @endif value="number">Numeric</option>
                                                                                                    </select>

                                                                                                    {{-- Validation Alert Messages --}}
                                                                                                    @error('field_type')
                                                                                                        <span class="invalid-feedback" role="alert">
                                                                                                            <strong>{{ $message }}</strong>
                                                                                                        </span>
                                                                                                    @enderror
                                                                                                </div>



                                                                                                {{-- This Will Apeend If Select Box --}}
                                                                                                <div class="col-md-12 option-show @if($data['field_type'] != 'select')d-none @endif" id="option_show_{{ $fact_module->id.'_'.$list.'_'.$item }}">
                                                                                                    <label class="control-label">Options</label>
                                                                                                    <small>Please type all the needed options and press <strong style="color:red">Enter</strong></small>
                                                                                                    <select name="options[]" id="tags-input-multiple" multiple data-role="tagsinput" data-tag-class="label label-primary">
                                                                                                        {{-- Options --}}
                                                                                                        @if($data['field_type'] == 'select')
                                                                                                            @foreach($data['options'] as $option)
                                                                                                                <option value="{{ $option }}" selected>{{ $option }}</option>
                                                                                                            @endforeach
                                                                                                        @endif
                                                                                                    </select>

                                                                                                </div>


                                                                                                <div class="col-md-6">
                                                                                                    <label class="control-label">Print By Default?</label>
                                                                                                    <div class="checkbox-custom checkbox-default">
                                                                                                        <input type="checkbox" class="form-control  @error('print_by_default') is-invalid @enderror" @if($data['print_by_default'] == 'on') checked="" @endif name="print_by_default">
                                                                                                        <label><strong>Yes</strong></label>
                                                                                                    </div>

                                                                                                    {{-- Validation Alert Messages --}}
                                                                                                    @error('print_by_default')
                                                                                                        <span class="invalid-feedback" role="alert">
                                                                                                            <strong>{{ $message }}</strong>
                                                                                                        </span>
                                                                                                    @enderror
                                                                                                </div>

                                                                                                <div class="col-md-6">
                                                                                                    <label class="control-label">Mendatory?</label>
                                                                                                    <div class="checkbox-custom checkbox-default">
                                                                                                        <input type="checkbox" class="form-control  @error('mendatory') is-invalid @enderror" @if($data['mendatory'] == 'on')checked="" @endif name="mendatory">
                                                                                                        <label><strong>Yes</strong></label>
                                                                                                    </div>

                                                                                                    {{-- Validation Alert Messages --}}
                                                                                                    @error('mendatory')
                                                                                                        <span class="invalid-feedback" role="alert">
                                                                                                            <strong>{{ $message }}</strong>
                                                                                                        </span>
                                                                                                    @enderror
                                                                                                </div>

                                                                                                <div class="col-md-6">
                                                                                                    <label class="control-label">Hidden?</label>
                                                                                                    <div class="checkbox-custom checkbox-default">
                                                                                                        <input type="checkbox" class="form-control  @error('hidden') is-invalid @enderror" @if($data['hidden'] == 'on') checked="" @endif name="hidden">
                                                                                                        <label><strong>Yes</strong></label>
                                                                                                    </div>

                                                                                                    {{-- Validation Alert Messages --}}
                                                                                                    @error('hidden')
                                                                                                        <span class="invalid-feedback" role="alert">
                                                                                                            <strong>{{ $message }}</strong>
                                                                                                        </span>
                                                                                                    @enderror
                                                                                                </div>

                                                                                                <div class="col-md-6">
                                                                                                    <label class="control-label">Allow Client Edit?</label>
                                                                                                    <div class="checkbox-custom checkbox-default">
                                                                                                        <input type="checkbox" id="allow_client_edit_{{ $fact_module->id.'_'.$list.'_'.$item }}" class="form-control  @error('allow_client_edit') is-invalid @enderror" @if($data['allow_client_edit'] == 'on')checked="" @endif @if($data['field_type'] == 'select')disabled @endif name="allow_client_edit" >
                                                                                                        <label><strong>Yes</strong></label>
                                                                                                    </div>

                                                                                                    {{-- Validation Alert Messages --}}
                                                                                                    @error('allow_client_edit')
                                                                                                        <span class="invalid-feedback" role="alert">
                                                                                                            <strong>{{ $message }}</strong>
                                                                                                        </span>
                                                                                                    @enderror
                                                                                                </div>
                                                                                            </div>
                                                                                        </div>

                                                                                        <footer class="panel-footer text-right">
                                                                                            <button class="btn-dark btn-xs" type="submit">Update</button>
                                                                                        </footer>
                                                                                    </form>
                                                                                </div>
                                                                            </div>
                                                                        @endforeach
                                                                    @endif
                                                                </div>
                                                            </div>
                                                        </section>
                                                    @endforeach
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </section>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- end: page -->




{{-- ========================< Add New Fact Find section Modal Starts >======================= --}}
<div class="modal fade" id="add_fact_find_section" tabindex="-1" role="dialog" aria-labelledby="gridSystemModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="gridSystemModalLabel"><b>New Fact Find Section</b></h4>
            </div>
            <form action="{{ route('director.company.fact_find.store_new_section') }}" method="POST">
                @csrf
                <div class="modal-body">
                    <div class="row">
                        <input type="hidden" name="fact_id" id="fact_id">
                        <div class="col-md-12">
                            <label class="control-label">Fact Find Section Name</label>
                            <input type="text" autocomplete="off" name="fact_find_section_name" class="form-control form-control-sm @error('fact_find_section_name') is-invalid @enderror" placeholder="eg.: Fact Find Section Name" autocomplete="off">

                            {{-- Validation Alert Messages --}}
                            @error('fact_find_section_name')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                    </div>
                </div>

                <div class="modal-footer text-right">
                    <button type="submit" class="btn btn-dark btn-custom btn-sm">Assign Now</button>
                </div>
            </form>
        </div>
    </div>
</div>
{{-- =========================< Add New Fact Find sectoin Modal Ends >======================== --}}



{{-- ========================< Edit Fact Find section Modal Starts >======================= --}}
<div class="modal fade" id="edit_fact_find_section" tabindex="-1" role="dialog" aria-labelledby="gridSystemModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="gridSystemModalLabel"><b>Edit Fact Find Section</b></h4>
            </div>
            <form action="{{ route('director.company.fact_find.edit_section') }}" method="POST">
                @csrf
                <div class="modal-body">
                    <div class="row">
                        <input type="hidden" name="module_id" id="edit_module_id">
                        <input type="hidden" name="section_id" id="edit_section_id">
                        <div class="col-md-12">
                            <label class="control-label">Fact Find Section Name</label>
                            <input type="text" id="edit_section_name" autocomplete="off" name="fact_find_section_name" class="form-control form-control-sm @error('fact_find_section_name') is-invalid @enderror" placeholder="eg.: Fact Find Section Name" autocomplete="off">

                            {{-- Validation Alert Messages --}}
                            @error('fact_find_section_name')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                    </div>
                </div>

                <div class="modal-footer text-right">
                    <button type="submit" class="btn btn-dark btn-custom btn-sm">Update</button>
                </div>
            </form>
        </div>
    </div>
</div>
{{-- =========================< Edit Fact Find sectoin Modal Ends >======================== --}}


{{-- ========================< Add New Fact Find Starts >======================= --}}
<div class="modal fade" id="add_new_fact_find_modal" tabindex="-1" role="dialog" aria-labelledby="gridSystemModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="gridSystemModalLabel"><b>New Fact Find</b></h4>
            </div>
            <form action="{{ route('director.company.fact_find.store') }}" method="POST">
                @csrf
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-12">
                            <label class="control-label">Fact Find Name</label>
                            <input type="text" autocomplete="off" name="fact_find_name" class="form-control form-control-sm @error('fact_find_name') is-invalid @enderror" placeholder="eg.: Fact Find Name" autocomplete="off">

                            {{-- Validation Alert Messages --}}
                            @error('fact_find_name')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                    </div>
                </div>

                <div class="modal-footer text-right">
                    <button type="submit" class="btn btn-dark btn-custom btn-sm">Assign Now</button>
                </div>
            </form>
        </div>
    </div>
</div>
{{-- =========================< Add New Fact Find Ends >======================== --}}


{{-- ========================< Add New Fact Find Starts >======================= --}}
<div class="modal fade" id="edit_fact_find_module" tabindex="-1" role="dialog" aria-labelledby="gridSystemModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="gridSystemModalLabel"><b>Edit Fact Find</b></h4>
            </div>
            <form action="{{ route('director.company.fact_find.update') }}" method="POST">
                @csrf
                <div class="modal-body">
                    <div class="row">
                        <input type="hidden" name="module_id" id="edit_module_id">
                        <div class="col-md-12">
                            <label class="control-label">Fact Find Name</label>
                            <input type="text" autocomplete="off" name="fact_find_name" id="edit_fact_find_name" class="form-control form-control-sm @error('fact_find_name') is-invalid @enderror" placeholder="eg.: Fact Find Name" autocomplete="off">

                            {{-- Validation Alert Messages --}}
                            @error('fact_find_name')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                    </div>
                </div>

                <div class="modal-footer text-right">
                    <button type="submit" class="btn btn-dark btn-custom btn-sm">Assign Now</button>
                </div>
            </form>
        </div>
    </div>
</div>
{{-- =========================< Add New Fact Find Ends >======================== --}}



{{-- ========================< new question set modal Starts >======================= --}}
<div class="modal fade" id="new_question_modal" tabindex="-1" role="dialog" aria-labelledby="gridSystemModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="gridSystemModalLabel"><b>Add New Question</b></h4>
            </div>
            <form action="{{ route('director.company.fact_find.add_new_question') }}" method="POST">
                @csrf
                <div class="modal-body">
                    <input type="hidden" id="section_id" name="section_id">
                    <input type="hidden" id="module_id" name="module_id">
                    <div class="row">
                        <div class="col-md-12">
                            <label class="control-label">Question Name</label>
                            <input type="text" autocomplete="off" name="question_name" class="form-control form-control-sm @error('question_name') is-invalid @enderror" placeholder="eg.: Question Name" autocomplete="off">

                            {{-- Validation Alert Messages --}}
                            @error('question_name')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>

                        <div class="col-md-12">
                            <label class="control-label">Field Type</label>
                            <select name="field_type" id="option_new" data-plugin-selectTwo class="form-control populate  @error('field_type') is-invalid @enderror">
                                <option disabled selected>Select Field Type</option>
                                <option value="text">Text</option>
                                <option value="textarea">Multi-Line Text Area</option>
                                <option value="date">Date</option>
                                <option value="checkbox">Check box</option>
                                <option value="select">Select Box</option>
                                <option value="number">Numeric</option>
                            </select>

                            {{-- Validation Alert Messages --}}
                            @error('field_type')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>

                        {{-- This Will Apeend If Select Box --}}
                        <div class="col-md-12 option-show d-none" id="option_show_new">
                            <label class="control-label">Options</label>
                            <small>Please type all the needed options and press <strong style="color:red">Enter</strong></small>
                            <select name="options[]" id="tags-input-multiple" multiple data-role="tagsinput" data-tag-class="label label-primary">
                                {{-- Options --}}
                            </select>

                        </div>

                        <div class="col-md-6">
                            <label class="control-label">Print By Default?</label>
                            <div class="checkbox-custom checkbox-default">
                                <input type="checkbox" class="form-control  @error('print_by_default') is-invalid @enderror" checked="" name="print_by_default">
                                <label><strong>Yes</strong></label>
                            </div>

                            {{-- Validation Alert Messages --}}
                            @error('print_by_default')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>

                        <div class="col-md-6">
                            <label class="control-label">Mendatory?</label>
                            <div class="checkbox-custom checkbox-default">
                                <input type="checkbox" class="form-control  @error('mendatory') is-invalid @enderror" checked="" name="mendatory">
                                <label><strong>Yes</strong></label>
                            </div>

                            {{-- Validation Alert Messages --}}
                            @error('mendatory')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>

                        <div class="col-md-6">
                            <label class="control-label">Hidden?</label>
                            <div class="checkbox-custom checkbox-default">
                                <input type="checkbox" class="form-control  @error('hidden') is-invalid @enderror" checked="" name="hidden">
                                <label><strong>Yes</strong></label>
                            </div>

                            {{-- Validation Alert Messages --}}
                            @error('hidden')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>

                        <div class="col-md-6">
                            <label class="control-label">Allow Client Edit?</label>
                            <div class="checkbox-custom checkbox-default">
                                <input type="checkbox" id="allow_client_edit_modal" class="form-control @error('allow_client_edit') is-invalid @enderror" checked="" name="allow_client_edit">
                                <label><strong>Yes</strong></label>
                            </div>

                            {{-- Validation Alert Messages --}}
                            @error('allow_client_edit')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                    </div>
                </div>

                <div class="modal-footer text-right">
                    <button type="submit" class="btn btn-dark btn-custom btn-sm">Assign Now</button>
                </div>
            </form>
        </div>
    </div>
</div>
{{-- =========================< new question set modal Ends >======================== --}}

@endsection


@section('script_links')
    {{--  External Javascript  --}}
    <script src="{{ asset('custom/assets/vendor/select2/select2.js') }}"></script>
    <script src="{{ asset('custom/assets/vendor/bootstrap-multiselect/bootstrap-multiselect.js') }}"></script>
    <script src="{{ asset('custom/assets/vendor/bootstrap-datepicker/js/bootstrap-datepicker.js') }}"></script>

    <script src="{{ asset('custom/assets/vendor/jquery-validation/jquery.validate.js') }}"></script>
    <script src="{{ asset('custom/assets/vendor/bootstrap-wizard/jquery.bootstrap.wizard.js') }}"></script>
    <script src="{{ asset('custom/assets/javascripts/forms/examples.wizard.js') }}"></script>

    <script src="{{ asset('custom/assets/javascripts/forms/examples.advanced.form.js') }}"></script>

    <script src="{{ asset('custom/assets/vendor/bootstrap-tagsinput/bootstrap-tagsinput.js') }}"></script>

@endsection

@section('scripts')
    {{--  External Javascript  --}}
    <script>
        $('#edit_fact_find_module').on('show.bs.modal', function (event) {
            var button = $(event.relatedTarget); // Button that triggered the modal
            var data = button.data('todo');
            var modal = $(this);

            modal.find('.modal-body #edit_module_id').val(data.id);
            modal.find('.modal-body #edit_fact_find_name').val(data.factfind_name);
        });
    </script>



    <script>
        $('#add_fact_find_section').on('show.bs.modal', function (event) {
            var button = $(event.relatedTarget); // Button that triggered the modal
            var data = button.data('todo');
            var modal = $(this);

            modal.find('.modal-body #fact_id').val(data.id);
        });
    </script>

    <script>
        $('#edit_fact_find_section').on('show.bs.modal', function (event) {
            var button = $(event.relatedTarget); // Button that triggered the modal
            var data = button.data('todo');
            var module_id = button.data('module_id');
            var section_id = button.data('section_id');
            var modal = $(this);

            modal.find('.modal-body #edit_module_id').val(module_id);
            modal.find('.modal-body #edit_section_id').val(section_id);
            modal.find('.modal-body #edit_section_name').val(data.name);
        });
    </script>

    <script>
        $('#new_question_modal').on('show.bs.modal', function (event) {
            var button = $(event.relatedTarget); // Button that triggered the modal
            var data = button.data('todo');
            var section_id = button.data('section_id');
            var module_id = button.data('fact_module_id');

            var modal = $(this);

            modal.find('.modal-body #section_id').val(section_id);
            modal.find('.modal-body #module_id').val(module_id);
        });
    </script>

    <script>
        $('.field-type').change(function(event){
            var get_id = '#' + $(this).attr('id');
            var id = get_id.substring(11, get_id.length);

            get_id = $(get_id);

            var show_option = "#option_show"+id;
            show_option = $(show_option);

            var client_edit = '#allow_client_edit'+id;

            if(get_id.val() == 'select'){
                show_option.removeClass('d-none');
                $(client_edit).prop( "disabled", true );
                $(client_edit).prop( "checked", false );
            } else{
                show_option.addClass('d-none');
                $(client_edit).prop( "disabled", false );
                $(client_edit).prop( "checked", true );
            }
        });

        $('#option_new').change(function(event){

            var show_option = "#option_show_new";
            show_option = $(show_option);

            if($(this).val() == 'select'){
                show_option.removeClass('d-none');
                $('#allow_client_edit_modal').prop( "disabled", true );
            } else{
                show_option.addClass('d-none');
                $('#allow_client_edit_modal').prop( "disabled", false );
            }
        });
    </script>

@endsection
