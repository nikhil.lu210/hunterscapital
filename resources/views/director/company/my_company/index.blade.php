@extends('layouts.director.app')

@section('page_title', '| Company | My Company')

@section('stylesheet_links')
    {{--  External CSS  --}}

@endsection

@section('stylesheet')
    {{--  External CSS  --}}

@endsection

@section('content')

<header class="page-header">
    <h2>My Company</h2>

    <div class="right-wrapper pull-right">
        <ol class="breadcrumbs">
            <li>
                <a href="{{ Route('director.dashboard.index') }}">
                    <i class="fa fa-home"></i>
                </a>
            </li>
            <li><span class="active">Company</span></li>
            <li><span>My Company</span></li>
        </ol>
    </div>
</header>

<!-- start: page -->
<section class="panel">
    <header class="panel-heading">
        <h2 class="panel-title">My Company Details</h2>
        <a href="{{ Route('director.company.my_company.edit') }}" class="btn btn-dark btn-custom btn-sm header-btn">Edit Details</a>
    </header>
    <div class="panel-body">
        <div class="col-md-12 col-xl-6">
            <table class="table table-bordered table-striped mb-none">
                <tbody>
                    <tr>
                        <th>Company Logo</th>
                        <td>
                            @isset($company->company_logo)
                            <img src="data:image/png;base64,{{ $company->company_logo }}" height="30" />
                            @endisset

                            @empty($company->company_logo)
                            <img src="{{ asset('custom/images/logo.png') }}" height="30" />
                            @endempty
                        </td>
                    </tr>
                    <tr>
                        <th>Company Name</th>
                        <td>{{ $company->company_name }}</td>
                    </tr>
                    <tr>
                        <th>Company Address</th>
                        <td>
                            <ul>
                                <li>Country: {{ $company->county }}</li>
                                <li>City: {{ $company->town_city }}</li>
                                <li>Postcode: {{ $company->postal_code }}</li>
                                <li>Location: {{ $company->area_location }}</li>
                                <li>House No: {{ $company->house_plot_no }}</li>
                            </ul>
                        </td>
                    </tr>
                    <tr>
                        <th>Website</th>
                        <td>{{ $company->company_website }}</td>
                    </tr>
                    <tr>
                        <th>Principal Email</th>
                        <td>{{ $company->principal_email }}</td>
                    </tr>
                    <tr>
                        <th>File Checking Email</th>
                        <td>{{ $company->filechecking_email }}</td>
                    </tr>
                    <tr>
                        <th>Secondary File Checking Email</th>
                        <td>{{ $company->secondary_filechecking_email }}</td>
                    </tr>
                    <tr>
                        <th>Office Email</th>
                        <td>{{ $company->office_email }}</td>
                    </tr>
                    <tr>
                        <th>Telephone No.</th>
                        <td>{{ $company->phone_number }}</td>
                    </tr>
                    <tr>
                        <th>Mobile No.</th>
                        <td>{{ $company->mobile_number }}</td>
                    </tr>
                </tbody>
            </table>
        </div>

        <div class="col-md-12 col-xl-6">
            <table class="table table-bordered table-striped mb-none">
                <tbody>
                    <tr class="table-body-row">
                        <th>VAT Status</th>
                        <td>
                            @if($company->vat_status == 0)
                                Not Registered
                            @else
                                Registered
                            @endif
                        </td>
                    </tr>
                    <tr>
                        <th>FCA Number</th>
                        <td>{{ $company->fca_number }}</td>
                    </tr>
                    <tr>
                        <th>Financial Year End</th>
                        <td>{{ $company->financial_year_end }}</td>
                    </tr>
                    <tr>
                        <th>PII Renewal Date</th>
                        <td>{{ $company->pii_renewal_date }}</td>
                    </tr>
                    <tr>
                        <th>Business</th>
                        <td>
                            @isset($company->business)
                                <?php
                                    $business = json_decode($company->business, true);
                                    // dd($business);
                                ?>
                                <ul>
                                    @isset($business["insurance"])<li>Insurance</li>@endisset
                                    @isset($business["invOrPen"])<li>Investment / Pension</li>@endisset
                                    @isset($business["mortgage"])<li>Mortgage</li>@endisset
                                    @isset($business["ccl"])<li>CCL</li>@endisset
                                </ul>
                            @endisset
                        </td>
                    </tr>
                    <tr>
                        <th>Date Joined</th>
                        <?php
                            $joindate = new DateTime($company->created_at);
                        ?>
                        <td>{{ $joindate->format('d M Y') }}</td>
                    </tr>
                    {{-- <tr>
                        <th>Expire Date</th>
                        <td>expire_date</td>
                    </tr> --}}
                </tbody>
            </table>
        </div>
    </div>
</section>
<!-- end: page -->

@endsection


@section('script_links')
    {{--  External Javascript  --}}

@endsection

@section('scripts')
    {{--  External Javascript  --}}

@endsection
