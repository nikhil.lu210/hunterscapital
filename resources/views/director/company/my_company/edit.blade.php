@extends('layouts.director.app')

@section('page_title', '| Company | My Company | Edit Details')

@section('stylesheet_links')
    {{--  External CSS  --}}
    <link rel="stylesheet" href="{{ asset('custom/assets/vendor/select2/select2.css') }}" />
    <link rel="stylesheet" href="{{ asset('custom/assets/vendor/bootstrap-datepicker/css/datepicker3.css') }}" />
    <link rel="stylesheet" href="{{ asset('custom/assets/vendor/bootstrap-fileupload/bootstrap-fileupload.min.css') }}" />
@endsection

@section('stylesheet')
    {{--  External CSS  --}}
    <style>
        html .wizard-progress .wizard-steps li.active a span,
        html.dark .wizard-progress .wizard-steps li.active a span {
            color: #dd3333;
            border-color: #dd3333;
        }
        html .wizard-progress .wizard-steps li.completed a span,
        html.dark .wizard-progress .wizard-steps li.completed a span,
        html .wizard-progress .steps-progress .progress-indicator,
        html.dark .wizard-progress .steps-progress .progress-indicator{
            border-color: #dd3333;
            background: #dd3333;
        }
        .pager{
            margin: 0;
        }
        .pager li > a,
        .pager li > span,
        .pager li > a:hover,
        .pager li > span:hover{
            background-color: #02070a !important;
            border: 1px solid #02070a !important;
        }
        .pager .disabled > a,
        .pager .disabled > a:hover,
        .pager .disabled > a:focus,
        .pager .disabled > span{
            background-color: #ffffff !important;
            color: #02070a;
        }
        .btn-custom{
            border-radius: 0px !important;
        }
        .panel{
            border: 1px solid #eee;
        }
        .panel .panel .panel-heading{
            padding: 10px 15px;
        }
        .panel .panel .panel-heading .panel-title{
            color: #ffffff;
        }
        .fileupload .uneditable-input .fa {
            top: 39px;
        }

        .modal-header .close {
            outline: none !important;
            border: 1px solid #8e8e8e;
            padding: 0px 5px;
            border-radius: 0;
            opacity: 1;
            color: #8e8e8e;
            transition: 0.3s all ease-in-out;
        }
        .modal-header .close:hover {
            outline: none !important;
            border: 1px solid #0f1b25;
            background-color: #0f1b25;
            opacity: 1;
            color: #ffffff;
            transition: 0.3s all ease-in-out;
        }
        .modal-title{
            color: #0f1b25;
        }
        .modal-content {
            border: 1px solid #e5e5e5;
            border-radius: 0;
            box-shadow: 0 3px 9px rgba(0, 0, 0, 0.1);
        }
        .btn.btn-dark.btn-custom.btn-block {
            margin: 12px 0px;
            border-radius: 0px;
        }
        @media (min-width: 768px){
            .modal-dialog {
                width: 500px;
                margin: 11% auto;
            }
        }

        .modal-footer {
            padding: 5px 15px;
        }
        .add-fund{
            margin-right: 5px;
        }
        .fileupload .uneditable-input .fa {
            top: 40px;
            left: 25px;
        }
    </style>
@endsection

@section('content')

<header class="page-header">
    <h2>Update My Company Details</h2>

    <div class="right-wrapper pull-right">
        <ol class="breadcrumbs">
            <li>
                <a href="{{ Route('director.dashboard.index') }}">
                    <i class="fa fa-home"></i>
                </a>
            </li>
            <li><span class="active">Company</span></li>
            <li>
                <a href="{{ Route('director.company.my_company.index') }}">
                    <span class="active">My Company</span>
                </a>
            </li>
            <li><span>Edit Details</span></li>
        </ol>
    </div>
</header>

<!-- start: page -->
<section class="panel form-wizard" id="w3">
    <header class="panel-heading">
        <div class="panel-actions">
            <a href="#" class="fa fa-caret-down"></a>
        </div>

        <h2 class="panel-title">Company Details Update</h2>
    </header>

    <form class="form-horizontal" novalidate="novalidate" action="{{ route('director.company.my_company.update') }}" method="POST" enctype="multipart/form-data">
        @csrf

        <div class="panel-body">
            <div class="wizard-progress">
                <div class="steps-progress">
                    <div class="progress-indicator" style="width: 0%;"></div>
                </div>
                <ul class="wizard-steps">
                    <li class="active">
                        <a href="#company_details" data-toggle="tab"><span>A</span>Company <br> Details</a>
                    </li>
                    <li>
                        <a href="#email_address" data-toggle="tab"><span>B</span>Email <br> Addresses</a>
                    </li>
                    <li>
                        <a href="#address" data-toggle="tab"><span>C</span>Company <br> Address</a>
                    </li>
                    <li>
                        <a href="#business_details" data-toggle="tab"><span>D</span>Business <br> Details</a>
                    </li>
                    <li>
                        <a href="#logo_and_footer" data-toggle="tab"><span>E</span>Logo & <br> Footer</a>
                    </li>
                </ul>
            </div>
            <div class="tab-content">
                {{-- company_details --}}
                <div id="company_details" class="tab-pane active">
                    <div class="form-group">

                        <div class="col-md-6">
                            <label class="control-label">Company Name <span class="required">*</span></label>
                            <input type="text" autocomplete="off" name="company_name" class="form-control  @error('company_name') is-invalid @enderror" placeholder="eg.: Company Name" value="{{ $company->company_name}}" required/>

                            {{-- Validation Alert Messages --}}
                            @error('company_name')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>

                        <div class="col-md-6">
                            <label class="control-label">Company Website</label>
                            <input type="url" autocomplete="off" name="company_website" class="form-control  @error('company_website') is-invalid @enderror" placeholder="eg.: www.company.com" value="{{$company->company_website}}" />

                            {{-- Validation Alert Messages --}}
                            @error('company_website')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>

                        <div class="col-md-4">
                            <label class="control-label">Phone Number</label>
                            <input type="text" autocomplete="off" name="phone_number" class="form-control  @error('phone_number') is-invalid @enderror" placeholder="eg.: 125 2254 12" value="{{$company->phone_number}}"/>

                            {{-- Validation Alert Messages --}}
                            @error('phone_number')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>

                        <div class="col-md-5">
                            <label class="control-label">Mobile Number <span class="required">*</span></label>
                            <input type="text" autocomplete="off" name="mobile_number" class="form-control  @error('mobile_number') is-invalid @enderror" placeholder="eg.: +88012 55 225412" value="{{$company->mobile_number}}" required/>

                            {{-- Validation Alert Messages --}}
                            @error('mobile_number')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>

                        <div class="col-md-3">
                            <label class="control-label">Vat Status <span class="required">*</span></label>
                            <select name="vat_status" data-plugin-selectTwo class="form-control populate  @error('vat_status') is-invalid @enderror" required>
                                <option disabled>Select Status</option>
                                <option @if($company->vat_status == 1)selected @endif value="1">Vat Registered</option>
                                <option @if($company->vat_status == 0)selected @endif value="0">Vat Not Registered</option>
                            </select>

                            {{-- Validation Alert Messages --}}
                            @error('client_id')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>

                    </div>
                </div>

                {{-- email_address --}}
                <div id="email_address" class="tab-pane">
                    <div class="form-group">
                        <div class="col-md-6">
                            <label class="control-label">Principal Email <span class="required">*</span></label>
                            <input type="email" autocomplete="off" name="principal_email" class="form-control  @error('principal_email') is-invalid @enderror" placeholder="eg.: yourmail@mail.com" value="{{$company->principal_email}}" required/>

                            {{-- Validation Alert Messages --}}
                            @error('principal_email')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>

                        <div class="col-md-6">
                            <label class="control-label">Filechecking Email <span class="required">*</span></label>
                            <input type="email" autocomplete="off" name="filechecking_email" class="form-control  @error('filechecking_email') is-invalid @enderror" placeholder="eg.: yourmail@mail.com" value="{{$company->filechecking_email}}" required/>

                            {{-- Validation Alert Messages --}}
                            @error('filechecking_email')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>

                        <div class="col-md-6">
                            <label class="control-label">Office Email <span class="required">*</span></label>
                            <input type="email" autocomplete="off" name="office_email" class="form-control  @error('office_email') is-invalid @enderror" placeholder="eg.: yourmail@mail.com" value="{{$company->office_email}}" required/>

                            {{-- Validation Alert Messages --}}
                            @error('office_email')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>

                        <div class="col-md-6">
                            <label class="control-label">Secondary Filechecking Email</label>
                            <input type="email" autocomplete="off" name="secondary_filechecking_email" class="form-control  @error('secondary_filechecking_email') is-invalid @enderror" placeholder="eg.: yourmail@mail.com" value="{{$company->secondary_filechecking_email}}" />

                            {{-- Validation Alert Messages --}}
                            @error('secondary_filechecking_email')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                    </div>
                </div>

                {{-- address --}}
                <div id="address" class="tab-pane">
                    <div class="form-group">
                        <div class="col-md-6">
                            <label class="control-label"><b>County</b> <span class="required">*</span></label>
                            <select name="county" data-plugin-selectTwo class="form-control populate  @error('county') is-invalid @enderror" required>
                                <option>Please Select</option>
                                <option value="{{$company->county}}" selected>{{$company->county}}</option>
                                <optgroup label="England">
                                    <option value="bath-and-north-east-somerset">Bath and North East Somerset</option>
                                    <option value="bedford">Bedford</option>
                                    <option value="berkshire">Berkshire</option>
                                    <option value="blackburn-with-darwen">Blackburn with Darwen</option>
                                    <option value="blackpool">Blackpool</option>
                                    <option value="bournemouth">Bournemouth</option>
                                    <option value="brighton-&amp;-hove">Brighton &amp; Hove</option>
                                    <option value="bristol">Bristol</option>
                                    <option value="buckinghamshire">Buckinghamshire</option>
                                    <option value="cambridgeshire">Cambridgeshire</option>
                                    <option value="central-bedfordshire">Central Bedfordshire</option>
                                    <option value="cheshire-east">Cheshire East</option>
                                    <option value="cheshire-west-and-chester">Cheshire West and Chester</option>
                                    <option value="cornwall">Cornwall</option>
                                    <option value="cumbria">Cumbria</option>
                                    <option value="darlington">Darlington</option>
                                    <option value="derby">Derby</option>
                                    <option value="derbyshire">Derbyshire</option>
                                    <option value="devon">Devon</option>
                                    <option value="dorset">Dorset</option>
                                    <option value="durham">Durham</option>
                                    <option value="east-riding-of-yorkshire">East Riding of Yorkshire</option>
                                    <option value="east-sussex">East Sussex</option>
                                    <option value="essex">Essex</option>
                                    <option value="gloucestershire">Gloucestershire</option>
                                    <option value="greater-london">Greater London</option>
                                    <option value="greater-manchester">Greater Manchester</option>
                                    <option value="halton">Halton</option>
                                    <option value="hampshire">Hampshire</option>
                                    <option value="hartlepool">Hartlepool</option>
                                    <option value="herefordshire">Herefordshire</option>
                                    <option value="hertfordshire">Hertfordshire</option>
                                    <option value="isle-of-wight">Isle of Wight</option>
                                    <option value="kent">Kent</option>
                                    <option value="kingston-upon-hull">Kingston upon Hull</option>
                                    <option value="lancashire">Lancashire</option>
                                    <option value="leicester">Leicester</option>
                                    <option value="leicestershire">Leicestershire</option>
                                    <option value="lincolnshire">Lincolnshire</option>
                                    <option value="luton">Luton</option>
                                    <option value="medway">Medway</option>
                                    <option value="merseyside">Merseyside</option>
                                    <option value="middlesbrough">Middlesbrough</option>
                                    <option value="milton-keynes">Milton Keynes</option>
                                    <option value="norfolk">Norfolk</option>
                                    <option value="north-east-lincolnshire">North East Lincolnshire</option>
                                    <option value="north-lincolnshire">North Lincolnshire</option>
                                    <option value="north-somerset">North Somerset</option>
                                    <option value="north-yorkshire">North Yorkshire</option>
                                    <option value="northamptonshire">Northamptonshire</option>
                                    <option value="northumberland">Northumberland</option>
                                    <option value="nottingham">Nottingham</option>
                                    <option value="nottinghamshire">Nottinghamshire</option>
                                    <option value="oxfordshire">Oxfordshire</option>
                                    <option value="peterborough">Peterborough</option>
                                    <option value="plymouth">Plymouth</option>
                                    <option value="poole">Poole</option>
                                    <option value="portsmouth">Portsmouth</option>
                                    <option value="redcar-and-cleveland">Redcar and Cleveland</option>
                                    <option value="rutland">Rutland</option>
                                    <option value="shropshire">Shropshire</option>
                                    <option value="somerset">Somerset</option>
                                    <option value="south-gloucestershire">South Gloucestershire</option>
                                    <option value="south-yorkshire">South Yorkshire</option>
                                    <option value="southampton">Southampton</option>
                                    <option value="southend-on-sea">Southend-on-Sea</option>
                                    <option value="staffordshire">Staffordshire</option>
                                    <option value="stockton-on-tees">Stockton-on-Tees</option>
                                    <option value="stoke-on-trent">Stoke-on-Trent</option>
                                    <option value="suffolk">Suffolk</option>
                                    <option value="surrey">Surrey</option>
                                    <option value="swindon">Swindon</option>
                                    <option value="telford-and-wrekin">Telford and Wrekin</option>
                                    <option value="thurrock">Thurrock</option>
                                    <option value="torbay">Torbay</option>
                                    <option value="tyne-and-wear">Tyne and Wear</option>
                                    <option value="warrington">Warrington</option>
                                    <option value="warwickshire">Warwickshire</option>
                                    <option value="west-midlands">West Midlands</option>
                                    <option value="west-sussex">West Sussex</option>
                                    <option value="west-yorkshire">West Yorkshire</option>
                                    <option value="wiltshire">Wiltshire</option>
                                    <option value="worcestershire">Worcestershire</option>
                                    <option value="york">York</option>
                                </optgroup>
                                <optgroup label="Scotland">
                                    <option value="aberdeen">Aberdeen</option>
                                    <option value="aberdeenshire">Aberdeenshire</option>
                                    <option value="angus">Angus</option>
                                    <option value="argyll-and-bute">Argyll and Bute</option>
                                    <option value="ayrshire-and-arran">Ayrshire and Arran</option>
                                    <option value="banffshire">Banffshire</option>
                                    <option value="berwickshire">Berwickshire</option>
                                    <option value="caithness">Caithness</option>
                                    <option value="clackmannanshire">Clackmannanshire</option>
                                    <option value="dumfries">Dumfries</option>
                                    <option value="dunbartonshire">Dunbartonshire</option>
                                    <option value="dundee">Dundee</option>
                                    <option value="east-lothian">East Lothian</option>
                                    <option value="edinburgh">Edinburgh</option>
                                    <option value="fife">Fife</option>
                                    <option value="glasgow">Glasgow</option>
                                    <option value="inverness">Inverness</option>
                                    <option value="kincardineshire">Kincardineshire</option>
                                    <option value="lanarkshire">Lanarkshire</option>
                                    <option value="midlothian">Midlothian</option>
                                    <option value="moray">Moray</option>
                                    <option value="nairn">Nairn</option>
                                    <option value="orkney-islands">Orkney Islands</option>
                                    <option value="perth-and-kinross">Perth and Kinross</option>
                                    <option value="renfrewshire">Renfrewshire</option>
                                    <option value="ross-and-cromarty">Ross and Cromarty</option>
                                    <option value="roxburgh,-ettrick-and-lauderdale">Roxburgh, Ettrick and Lauderdale</option>
                                    <option value="shetland-islands">Shetland Islands</option>
                                    <option value="stirling-and-falkirk">Stirling and Falkirk</option>
                                    <option value="sutherland">Sutherland</option>
                                    <option value="the-stewartry-of-kirkcudbright">The Stewartry of Kirkcudbright</option>
                                    <option value="tweeddale">Tweeddale</option>
                                    <option value="west-lothian">West Lothian</option>
                                    <option value="western-isles">Western Isles</option>
                                    <option value="wigtown">Wigtown</option>
                                </optgroup>
                                <optgroup label="Wales">
                                    <option value="blaenau-gwent">Blaenau Gwent</option>
                                    <option value="bridgend">Bridgend</option>
                                    <option value="caerphilly">Caerphilly</option>
                                    <option value="cardiff">Cardiff</option>
                                    <option value="carmarthenshire">Carmarthenshire</option>
                                    <option value="ceredigion">Ceredigion</option>
                                    <option value="conwy">Conwy</option>
                                    <option value="denbighshire">Denbighshire</option>
                                    <option value="flintshire">Flintshire</option>
                                    <option value="gwynedd">gwynedd</option>
                                    <option value="isle-of-anglesey">Isle of Anglesey</option>
                                    <option value="merthyr-tydfil">Merthyr Tydfil</option>
                                    <option value="monmouthshire">Monmouthshire</option>
                                    <option value="neath-port-talbot">Neath Port Talbot</option>
                                    <option value="newport">Newport</option>
                                    <option value="pembrokeshire">Pembrokeshire</option>
                                    <option value="powys">Powys</option>
                                    <option value="rhondda-cynon-taf">Rhondda Cynon Taf</option>
                                    <option value="swansea">Swansea</option>
                                    <option value="torfaen">Torfaen</option>
                                    <option value="vale-of-glamorgan">Vale of Glamorgan</option>
                                    <option value="wrexham">Wrexham</option>
                                </optgroup>
                                <optgroup label="Northern Ireland">
                                    <option value="antrim">Antrim</option>
                                    <option value="ards">Ards</option>
                                    <option value="armagh">Armagh</option>
                                    <option value="ballymena">Ballymena</option>
                                    <option value="ballymoney">Ballymoney</option>
                                    <option value="banbridge">Banbridge</option>
                                    <option value="belfast">Belfast</option>
                                    <option value="carrickfergus">Carrickfergus</option>
                                    <option value="castlereagh">Castlereagh</option>
                                    <option value="coleraine">Coleraine</option>
                                    <option value="cookstown">Cookstown</option>
                                    <option value="craigavon">Craigavon</option>
                                    <option value="derry">Derry</option>
                                    <option value="down">Down</option>
                                    <option value="dungannon-and-south-tyrone">Dungannon and South Tyrone</option>
                                    <option value="fermanagh">Fermanagh</option>
                                    <option value="larne">Larne</option>
                                    <option value="limavady">Limavady</option>
                                    <option value="lisburn">Lisburn</option>
                                    <option value="magherafelt">Magherafelt</option>
                                    <option value="moyle">Moyle</option>
                                    <option value="newry-and-mourne">Newry and Mourne</option>
                                    <option value="newtownabbey">Newtownabbey</option>
                                    <option value="north-down">North Down</option>
                                    <option value="omagh">Omagh</option>
                                    <option value="strabane">Strabane</option>
                                </optgroup>
                                <optgroup label="Ireland">
                                    <option value="carlow">Carlow</option>
                                    <option value="cavan">Cavan</option>
                                    <option value="clare">Clare</option>
                                    <option value="cork">Cork</option>
                                    <option value="donegal">Donegal</option>
                                    <option value="dublin">Dublin</option>
                                    <option value="dun-laoghaire-rathdown">Dún Laoghaire-Rathdown</option>
                                    <option value="fingal">Fingal</option>
                                    <option value="galway">Galway</option>
                                    <option value="kerry">Kerry</option>
                                    <option value="kildare">Kildare</option>
                                    <option value="kilkenny">Kilkenny</option>
                                    <option value="laois">Laois</option>
                                    <option value="leitrim">Leitrim</option>
                                    <option value="limerick">Limerick</option>
                                    <option value="longford">Longford</option>
                                    <option value="louth">Louth</option>
                                    <option value="mayo">Mayo</option>
                                    <option value="meath">Meath</option>
                                    <option value="monaghan">Monaghan</option>
                                    <option value="offaly">Offaly</option>
                                    <option value="roscommon">Roscommon</option>
                                    <option value="sligo">Sligo</option>
                                    <option value="south-dublin">South Dublin</option>
                                    <option value="tipperary">Tipperary</option>
                                    <option value="waterford">Waterford</option>
                                    <option value="westmeath">Westmeath</option>
                                    <option value="wexford">Wexford</option>
                                    <option value="wicklow">Wicklow</option>
                                </optgroup>
                            </select>

                            {{-- Validation Alert Messages --}}
                            @error('county')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>

                        <div class="col-md-6">
                            <label class="control-label">Town / City <span class="required">*</span></label>
                            <input type="text" autocomplete="off" name="town_city" class="form-control  @error('town_city') is-invalid @enderror" placeholder="eg.: City / Town Name" value="{{$company->town_city}}" required/>

                            {{-- Validation Alert Messages --}}
                            @error('town_city')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>

                        <div class="col-md-4">
                            <label class="control-label"><b>Postal Code</b></label>
                            <input type="text" autocomplete="off" name="postal_code" class="form-control  @error('postal_code') is-invalid @enderror" placeholder="eg.: 112233" value="{{$company->postal_code}}"/>

                            {{-- Validation Alert Messages --}}
                            @error('postal_code')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>

                        <div class="col-md-4">
                            <label class="control-label"><b>Area / Location</b></label>
                            <input type="text" autocomplete="off" name="area_location" class="form-control  @error('area_location') is-invalid @enderror" placeholder="eg.: area_location" value="{{$company->area_location}}"/>

                            {{-- Validation Alert Messages --}}
                            @error('area_location')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>

                        <div class="col-md-4">
                            <label class="control-label"><b>House / Plot No.</b></label>
                            <input type="text" autocomplete="off" name="house_plot_no" class="form-control  @error('house_plot_no') is-invalid @enderror" placeholder="eg.: house_plot_no" value="{{$company->house_plot_no}}"/>

                            {{-- Validation Alert Messages --}}
                            @error('house_plot_no')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>

                        <div class="col-md-12">
                            <label class="control-label"><b>Address In Details</b></label>
                            <textarea name="address" rows="3" class="form-control  @error('address') is-invalid @enderror" placeholder="eg.: Jhon Doe Address" >{{$company->address}}</textarea>

                            {{-- Validation Alert Messages --}}
                            @error('address')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                    </div>
                </div>

                {{-- business_details --}}
                <div id="business_details" class="tab-pane">
                    <div class="form-group">
                        <div class="col-md-4">
                            <label class="control-label">FCA Number</label>
                            <input type="text" autocomplete="off" name="fca_number" class="form-control  @error('fca_number') is-invalid @enderror" placeholder="eg.: FCA Number" value="{{$company->fca_number}}">

                            {{-- Validation Alert Messages --}}
                            @error('fca_number')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>

                        <div class="col-md-4">
                            <label class="control-label">Financial Year End</label>
                            <input type="text" autocomplete="off" data-plugin-datepicker="" name="financial_year_end" class="form-control  @error('financial_year_end') is-invalid @enderror" placeholder="eg.: dd/mm/yyyy" value="@if($company->financial_year_end != null){{App\Http\Controllers\Director\Company\MyCompanyController::reverseDateFormat($company->financial_year_end)}} @endif">

                            {{-- Validation Alert Messages --}}
                            @error('financial_year_end')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>

                        <div class="col-md-4">
                            <label class="control-label">PII Renewal Date</label>
                            <input type="text" autocomplete="off" data-plugin-datepicker="" name="pii_renewal_date" class="form-control  @error('pii_renewal_date') is-invalid @enderror" placeholder="eg.: dd/mm/yyyy" value="@if($company->pii_renewal_date != null){{App\Http\Controllers\Director\Company\MyCompanyController::reverseDateFormat($company->pii_renewal_date)}} @endif">

                            {{-- Validation Alert Messages --}}
                            @error('pii_renewal_date')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>

                        <?php
                            $business = json_decode($company->business, true);
                        ?>

                        <div class="col-md-12">
                            <label class="control-label">Business</label>
                            <div class="row">
                                <div class="col-md-3">
                                    <div class="checkbox-custom checkbox-default">
                                        <input type="checkbox" name="business[insurance]" @if(isset($business['insurance']) && $business['insurance'] == 'on')checked @endif id="insurance" class=" @error('insurance') is-invalid @enderror">
                                        <label for="insurance">Insurance</label>
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="checkbox-custom checkbox-default">
                                        <input type="checkbox" name="business[invOrPen]" @if(isset($business['invOrPen']) && $business['invOrPen'] == 'on')checked @endif id="investment_pension" class=" @error('investment_pension') is-invalid @enderror">
                                        <label for="investment_pension">Investment / Pension</label>
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="checkbox-custom checkbox-default">
                                        <input type="checkbox" name="business[mortgage]" @if(isset($business['mortgage']) && $business['mortgage'] == 'on')checked @endif id="mortgage" class=" @error('mortgage') is-invalid @enderror">
                                        <label for="mortgage">Mortgage</label>
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="checkbox-custom checkbox-default">
                                        <input type="checkbox" name="business[ccl]" @if(isset($business['ccl']) && $business['ccl'] == 'on')checked @endif id="ccl" class=" @error('ccl') is-invalid @enderror">
                                        <label for="ccl">CCL</label>
                                    </div>
                                </div>
                            </div>

                            {{-- Validation Alert Messages --}}
                            @error('insurance')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror

                            {{-- Validation Alert Messages --}}
                            @error('investment_pension')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror

                            {{-- Validation Alert Messages --}}
                            @error('mortgage')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror

                            {{-- Validation Alert Messages --}}
                            @error('ccl')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                    </div>
                </div>

                {{-- logo_and_footer --}}
                <div id="logo_and_footer" class="tab-pane">
                    <div class="form-group">
                        <div class="col-md-6">
                            <label class="control-label">Company Logo</label>
                            <div class="fileupload fileupload-exists" data-provides="fileupload">
                                <div class="input-append">
                                    <div class="uneditable-input">
                                        <i class="fa fa-file fileupload-exists" aria-hidden="true"></i>
                                        <span class="fileupload-preview"></span>
                                    </div>
                                    <span class="btn btn-default btn-file">
                                        <span class="fileupload-exists">Change</span>
                                        <span class="fileupload-new">Select file</span>
                                        <input type="file" name="company_logo" accept=".png, .jpg, .jpeg" >
                                    </span>
                                    <a href="#" class="btn btn-default fileupload-exists remove-disable" data-dismiss="fileupload">Remove</a>
                                </div>
                            </div>
                        </div>

                        <div class="col-md-12">
                            <label class="control-label">Company Footer</label>
                            <textarea class="form-control @error('company_footer') is-invalid @enderror" rows="3" id="textareaDefault" placeholder="Company Footer" name="company_footer" >{{$company->company_footer}}</textarea>

                            {{-- Validation Alert Messages --}}
                            @error('company_footer')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="panel-footer">
            <ul class="pager">
                <li class="previous disabled pull-left">
                    <a href="#" class="btn btn-dark btn-custom btn-sm">
                        <i class="fa fa-angle-left"></i> Previous
                    </a>
                </li>
                <li class="finish hidden pull-right">
                    <button type="submit" class="btn btn-dark btn-custom btn-sm submit-btn">
                        Update Now <i class="fa fa-thumbs-o-up"></i>
                    </button>
                </li>
                <li class="next">
                    <a href="#" class="btn btn-dark btn-custom btn-sm">
                        Next <i class="fa fa-angle-right"></i>
                    </a>
                </li>
            </ul>
        </div>

    </form>
</section>
<!-- end: page -->

@endsection


@section('script_links')
    {{--  External Javascript  --}}
    <script src="{{ asset('custom/assets/vendor/select2/select2.js') }}"></script>
    <script src="{{ asset('custom/assets/vendor/bootstrap-datepicker/js/bootstrap-datepicker.js') }}"></script>

    <script src="{{ asset('custom/assets/vendor/jquery-validation/jquery.validate.js') }}"></script>
    <script src="{{ asset('custom/assets/vendor/bootstrap-wizard/jquery.bootstrap.wizard.js') }}"></script>
    <script src="{{ asset('custom/assets/javascripts/forms/examples.wizard.js') }}"></script>

    <script src="{{ asset('custom/assets/vendor/bootstrap-fileupload/bootstrap-fileupload.min.js') }}"></script>
@endsection

@section('scripts')
    {{--  External Javascript  --}}

@endsection
