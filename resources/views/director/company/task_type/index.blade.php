@extends('layouts.director.app')

@section('page_title', '| Company | Task Types')

@section('stylesheet_links')
    {{--  External CSS  --}}
    <link rel="stylesheet" href="{{ asset('custom/assets/vendor/select2/select2.css') }}" />
    <link rel="stylesheet" href="{{ asset('custom/assets/vendor/jquery-datatables-bs3/assets/css/datatables.css') }}" />

@endsection

@section('stylesheet')
    {{--  External CSS  --}}
    <style>
        .dataTables_wrapper .DTTT.btn-group {
            display: none !important;
            position: absolute;
            top: -74px;
            right: 0px;
        }

        .member-active{
            background-color: green;
            color: #ffffff;
            padding: 2px 7px;
            border-radius: 3px;
            font-weight: 600;
        }
        .member-inactive{
            background-color: red;
            color: #ffffff;
            padding: 2px 7px;
            border-radius: 3px;
            font-weight: 600;
        }

        .silver{
            background-color: silver;
            color: #000000;
            padding: 2px 7px;
            border-radius: 3px;
            font-weight: 600;
        }
        .bronze{
            background-color: #ab4700;
            color: #ffffff;
            padding: 2px 7px;
            border-radius: 3px;
            font-weight: 600;
        }
        .gold{
            background-color: #ff7c1f;
            color: #ffffff;
            padding: 2px 7px;
            border-radius: 3px;
            font-weight: 600;
        }
        .platinum{
            background-color: #a0bfb4;
            color: #000000;
            padding: 2px 7px;
            border-radius: 3px;
            font-weight: 600;
        }

        .access-granted{
            background-color: green;
            color: #ffffff;
            padding: 2px 7px;
            border-radius: 3px;
            font-weight: 600;
        }
        .access-not-granted{
            background-color: red;
            color: #ffffff;
            padding: 2px 7px;
            border-radius: 3px;
            font-weight: 600;
        }
        .modal-header .close {
            outline: none !important;
            border: 1px solid #8e8e8e;
            padding: 0px 5px;
            border-radius: 0;
            opacity: 1;
            color: #8e8e8e;
            transition: 0.3s all ease-in-out;
        }
        .modal-header .close:hover {
            outline: none !important;
            border: 1px solid #0f1b25;
            background-color: #0f1b25;
            opacity: 1;
            color: #ffffff;
            transition: 0.3s all ease-in-out;
        }
        .modal-title{
            color: #0f1b25;
        }
        .modal-content {
            border: 1px solid #e5e5e5;
            border-radius: 0;
            box-shadow: 0 3px 9px rgba(0, 0, 0, 0.1);
        }
        .btn.btn-dark.btn-custom.btn-block {
            margin: 12px 0px;
            border-radius: 0px;
        }
        @media (min-width: 768px){
            .modal-dialog {
                width: 500px;
                margin: 11% auto;
            }
        }
    </style>
@endsection

@section('content')

<header class="page-header">
    <h2>Task Types</h2>

    <div class="right-wrapper pull-right">
        <ol class="breadcrumbs">
            <li>
                <a href="{{ Route('director.dashboard.index') }}">
                    <i class="fa fa-home"></i>
                </a>
            </li>
            <li><span class="active">Company</span></li>
            <li><span>Task Types</span></li>
        </ol>
    </div>
</header>

<!-- start: page -->
<section class="panel">
    <header class="panel-heading">
        <h2 class="panel-title">All Task Types</h2>
        <button class="btn btn-dark btn-custom btn-sm header-btn" data-toggle="modal" data-target="#newTaskType">Add New Task Type</button>
    </header>
    <div class="panel-body">
        <table class="table table-bordered table-striped mb-none" id="datatable-tabletools" data-swf-path="assets/vendor/jquery-datatables/extras/TableTools/swf/copy_csv_xls_pdf.swf">
            <thead>
                <tr class="table-header-row">
                    <th class="text-center">Sl.</th>
                    <th class="text-center">Task Type</th>
                    <th class="text-center">Task Status</th>
                    <th class="text-center">Actions</th>
                </tr>
            </thead>
            <tbody>
                @foreach ($task_types as $key=>$data)
                <tr class="table-body-row">
                    <th class="text-center">{{ $key+1 }}</th>
                    <th class="text-center">{{ $data->title }}</th>
                    @if ( $data->status == 1 )
                        <td class="text-center"><span class="member-active"><i class="fa fa-check"></i></span></td>
                    @else
                        <td class="text-center"><span class="member-inactive"><i class="fa fa-times"></i></span></td>
                    @endif
                    <td class="action-td text-center">
                        <button class="btn btn-dark btn-sm btn-sm-custom btn-custom-info" data-toggle="modal" data-target="#editTaskType" data-json="{{ $data }}">
                            <i class="fa fa-pen"></i>
                        </button>
                    </td>
                </tr>
                @endforeach
            </tbody>
        </table>
    </div>
</section>
<!-- end: page -->




{{-- ========================< Task Type Create Modal Starts >======================= --}}
<div class="modal fade" id="newTaskType" tabindex="-1" role="dialog" aria-labelledby="gridSystemModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="gridSystemModalLabel"><b>Add New Task Type</b></h4>
            </div>
            <form action="{{ route('director.company.task_type.store') }}" method="POST">
                @csrf
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-12">
                            <label class="control-label"><b>Task Type Title</b> <span class="required">*</span></label>
                            <input type="text" autocomplete="off" name="title" class="form-control  @error('title') is-invalid @enderror" placeholder="eg.: Urgent Task" required/>

                            {{-- Validation Alert Messages --}}
                            @error('title')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                        <div class="col-md-12">
                            <div class="form-group">
                                <label class="control-label"><b>Work Type Description</b></label>
                                <textarea name="description" rows="2" class="form-control  @error('description') is-invalid @enderror" placeholder="eg.: Mortgage"></textarea>

                                {{-- Validation Alert Messages --}}
                                @error('description')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="col-md-12">
                            <label class="control-label"><b>Disable This Task type? </b></label>
                            <div class="checkbox-custom checkbox-default">
                                <input type="checkbox" name="status" class=" @error('status') is-invalid @enderror">
                                <label for="status">Yes</label>
                            </div>

                            {{-- Validation Alert Messages --}}
                            @error('status')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                    </div>
                </div>

                <div class="modal-footer text-right">
                    <button type="submit" class="btn btn-dark btn-custom btn-sm">Assign As New task Type</button>
                </div>
            </form>
        </div>
    </div>
</div>
{{-- =========================< Task Type Create Modal Ends >======================== --}}




{{-- ========================< Task Type Edit Modal Starts >======================= --}}
<div class="modal fade" id="editTaskType" tabindex="-1" role="dialog" aria-labelledby="gridSystemModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="gridSystemModalLabel"><b>Update This Task Type</b></h4>
            </div>
            <form action="{{ route('director.company.task_type.update') }}" method="POST">
                @csrf
                <div class="modal-body">
                    <input type="hidden" id="hiddenID" name="id">
                    <div class="row">
                        <div class="col-md-12">
                            <label class="control-label"><b>Task Title</b> <span class="required">*</span></label>
                            <input type="text" autocomplete="off" id="taskTitle" name="title" class="form-control  @error('title') is-invalid @enderror" placeholder="eg.: Urgent Task" required/>

                            {{-- Validation Alert Messages --}}
                            @error('title')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>

                        <div class="col-md-12">
                            <div class="form-group">
                                <label class="control-label"><b>Work Type Description</b></label>
                                <textarea name="description" id="taskDescription" rows="2" class="form-control  @error('description') is-invalid @enderror" placeholder="eg.: Desciption Here"></textarea>

                                {{-- Validation Alert Messages --}}
                                @error('description')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="col-md-12">
                            <label class="control-label"><b>Disable This Task type? </b></label>
                            <div class="checkbox-custom checkbox-default">
                                <input type="checkbox" id="taskStatus" name="status" class=" @error('status') is-invalid @enderror" checked>
                                <label for="status">Yes</label>
                            </div>

                            {{-- Validation Alert Messages --}}
                            @error('status')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                    </div>
                </div>

                <div class="modal-footer text-right">
                    <button type="submit" class="btn btn-dark btn-custom btn-sm">Update This task Type</button>
                </div>
            </form>
        </div>
    </div>
</div>
{{-- =========================< Task Type Edit Modal Ends >======================== --}}
@endsection


@section('script_links')
    {{--  External Javascript  --}}
    <script src="{{ asset('custom/assets/vendor/select2/select2.js') }}"></script>
    <script src="{{ asset('custom/assets/vendor/jquery-datatables/media/js/jquery.dataTables.js') }}"></script>
    <script src="{{ asset('custom/assets/vendor/jquery-datatables/extras/TableTools/js/dataTables.tableTools.min.js') }}"></script>
    <script src="{{ asset('custom/assets/vendor/jquery-datatables-bs3/assets/js/datatables.js') }}"></script>

    <script src="{{ asset('custom/assets/javascripts/tables/examples.datatables.default.js') }}"></script>
    <script src="{{ asset('custom/assets/javascripts/tables/examples.datatables.row.with.details.js') }}"></script>
    <script src="{{ asset('custom/assets/javascripts/tables/examples.datatables.tabletools.js') }}"></script>
@endsection

@section('scripts')
    {{--  External Javascript  --}}
    <script>

            $('#editTaskType').on('show.bs.modal', function (event) {
                var button = $(event.relatedTarget); // Button that triggered the modal
                var data = button.data('json');
                // console.log(data);
                var modal = $(this);
                modal.find('.modal-body #hiddenID').val(data.id);
                modal.find('.modal-body #taskTitle').val(data.title);
                modal.find('.modal-body #taskDescription').html(data.description);
                if(data.status == 0)
                    modal.find('.modal-body #taskStatus').prop("checked", true);
                else
                    modal.find('.modal-body #taskStatus').prop("checked", false);
            });
        </script>

@endsection
