@extends('layouts.director.app')

@section('page_title', '| Company | Task Manager')

@section('stylesheet_links')
    {{--  External CSS  --}}
    <link rel="stylesheet" href="{{ asset('custom/assets/vendor/select2/select2.css') }}" />
    <link rel="stylesheet" href="{{ asset('custom/assets/vendor/jquery-datatables-bs3/assets/css/datatables.css') }}" />

    <link rel="stylesheet" href="{{ asset('custom/assets/vendor/bootstrap-datepicker/css/datepicker3.css') }}" />
@endsection

@section('stylesheet')
    {{--  External CSS  --}}
    <style>
        .dataTables_wrapper .DTTT.btn-group {
            display: none !important;
            position: absolute;
            top: -74px;
            right: 0px;
        }

        .member-active{
            background-color: green;
            color: #ffffff;
            padding: 2px 7px;
            border-radius: 3px;
            font-weight: 600;
        }
        .member-inactive{
            background-color: red;
            color: #ffffff;
            padding: 2px 7px;
            border-radius: 3px;
            font-weight: 600;
        }

        .silver{
            background-color: silver;
            color: #000000;
            padding: 2px 7px;
            border-radius: 3px;
            font-weight: 600;
        }
        .bronze{
            background-color: #ab4700;
            color: #ffffff;
            padding: 2px 7px;
            border-radius: 3px;
            font-weight: 600;
        }
        .gold{
            background-color: #ff7c1f;
            color: #ffffff;
            padding: 2px 7px;
            border-radius: 3px;
            font-weight: 600;
        }
        .platinum{
            background-color: #a0bfb4;
            color: #000000;
            padding: 2px 7px;
            border-radius: 3px;
            font-weight: 600;
        }

        .access-granted{
            background-color: green;
            color: #ffffff;
            padding: 2px 7px;
            border-radius: 3px;
            font-weight: 600;
        }
        .access-not-granted{
            background-color: red;
            color: #ffffff;
            padding: 2px 7px;
            border-radius: 3px;
            font-weight: 600;
        }
        .modal-header .close {
            outline: none !important;
            border: 1px solid #8e8e8e;
            padding: 0px 5px;
            border-radius: 0;
            opacity: 1;
            color: #8e8e8e;
            transition: 0.3s all ease-in-out;
        }
        .modal-header .close:hover {
            outline: none !important;
            border: 1px solid #0f1b25;
            background-color: #0f1b25;
            opacity: 1;
            color: #ffffff;
            transition: 0.3s all ease-in-out;
        }
        .modal-title{
            color: #0f1b25;
        }
        .modal-content {
            border: 1px solid #e5e5e5;
            border-radius: 0;
            box-shadow: 0 3px 9px rgba(0, 0, 0, 0.1);
        }
        .btn.btn-dark.btn-custom.btn-block {
            margin: 12px 0px;
            border-radius: 0px;
        }
        @media (min-width: 768px){
            .modal-dialog {
                width: 500px;
                margin: 11% auto;
            }
        }
    </style>
@endsection

@section('content')

<header class="page-header">
    <h2>Task Manager</h2>

    <div class="right-wrapper pull-right">
        <ol class="breadcrumbs">
            <li>
                <a href="{{ Route('director.dashboard.index') }}">
                    <i class="fa fa-home"></i>
                </a>
            </li>
            <li><span class="active">Company</span></li>
            <li><span>Task Manager</span></li>
        </ol>
    </div>
</header>

<!-- start: page -->
<section class="panel">
    <header class="panel-heading">
        <h2 class="panel-title">All Task</h2>
        <button class="btn btn-dark btn-custom btn-sm header-btn" data-toggle="modal" data-target="#newTask">Add New Task</button>
    </header>
    <div class="panel-body">
        <table class="table table-bordered table-striped mb-none" id="datatable-tabletools" data-swf-path="assets/vendor/jquery-datatables/extras/TableTools/swf/copy_csv_xls_pdf.swf">
            <thead>
                <tr class="table-header-row">
                    <th class="text-center">Sl.</th>
                    <th class="text-center">Task</th>
                    <th class="text-center">Task Type</th>
                    <th class="text-center">Assigned To</th>
                    <th class="text-center">Client</th>
                    <th class="text-center">DeadLine</th>
                    <th class="text-center">Status</th>
                    <th class="text-center">Actions</th>
                </tr>
            </thead>
            <tbody>
                @foreach ($tasks as $key=>$task)
                {{-- {{ dd($task) }} --}}
                <tr class="table-body-row">
                    <th class="text-center">{{ $key+1 }}</th>
                    <th class="text-center">{{ $task->task_title}}</th>
                    <th class="text-center">{{ $task->taskType->title }}</th>
                    <td class="text-center">{{ $task->director->saluation." ".$task->director->surname." ".$task->director->forname }}</td>
                    <td class="text-center">{{ $task->taskClient->client_title." ".$task->taskClient->client_forname." ".$task->taskClient->client_surname }}</td>
                    <td class="text-center">{{ $task->end_date }}</td>
                    <td class="text-center">{{ $task->task_status }}</td>
                    <td class="action-td text-center">
                        <button class="btn btn-dark btn-sm btn-sm-custom btn-custom-info" data-toggle="modal" data-target="#editTask" data-json="{{ $task }}">
                            <i class="fa fa-info"></i>
                        </button>
                    </td>
                </tr>
                @endforeach
            </tbody>
        </table>
    </div>
</section>
<!-- end: page -->




{{-- ========================< Task Create Modal Starts >======================= --}}
<div class="modal fade" id="newTask" tabindex="-1" role="dialog" aria-labelledby="gridSystemModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="gridSystemModalLabel"><b>Add New Task</b></h4>
            </div>
            <form action="{{ route('director.company.task_manager.store') }}" method="POST">
                @csrf
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-12">
                            <label class="control-label">Task Title <span class="required">*</span></label>
                            <input type="text" autocomplete="off" name="task_title" class="form-control  @error('task_title') is-invalid @enderror" placeholder="eg.: Task Name" required>

                            {{-- Validation Alert Messages --}}
                            @error('task_title')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>

                        <div class="col-md-12">
                            <label class="control-label">Task Type <span class="required">*</span></label>
                            <select name="task_type" data-plugin-selectTwo class="form-control populate  @error('task_type') is-invalid @enderror" required>
                                <option disabled selected>Select Task Type</option>
                                @foreach ($task_types as $type)
                                    <option value="{{ $type->id }}">{{ $type->title }}</option>
                                @endforeach
                            </select>

                            {{-- Validation Alert Messages --}}
                            @error('task_type')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>

                        <div class="col-md-12">
                            <label class="control-label">Assigned To <span class="required">*</span></label>
                            <select name="assigned_to" data-plugin-selectTwo class="form-control populate  @error('assigned_to') is-invalid @enderror" required>
                                <option disabled selected>Select Assigned To</option>
                                @foreach ($users as $user)
                                    <option value="{{ $user->id }}">{{ $user->saluation." ".$user->forname." ".$user->surname }}</option>
                                @endforeach
                            </select>

                            {{-- Validation Alert Messages --}}
                            @error('assigned_to')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>

                        <div class="col-md-12">
                            <label class="control-label">Client <span class="required">*</span></label>
                            <select name="client" data-plugin-selectTwo class="form-control populate  @error('client') is-invalid @enderror" required>
                                <option disabled selected>Select Client</option>
                                @foreach ($clients as $client)
                                    <option value="{{ $client->id }}">{{ $client->client_forname." ".$client->client_middle_name." ".$client->client_surname }}</option>
                                @endforeach
                            </select>

                            {{-- Validation Alert Messages --}}
                            @error('client')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>

                        <div class="col-md-12">
                            <label class="control-label">Task Status <span class="required">*</span></label>
                            <select name="task_status" data-plugin-selectTwo class="form-control populate  @error('task_status') is-invalid @enderror" required>
                                <option disabled selected>Select Task Status</option>
                                <option value="Pending">Pending</option>
                                <option value="Processing">Processing</option>
                                <option value="On Review">On Review</option>
                                <option value="Completed">Completed</option>
                            </select>

                            {{-- Validation Alert Messages --}}
                            @error('task_status')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>

                        <div class="col-md-12">
                            <label class="control-label">Task Start Date <span class="required">*</span></label>
                            <input type="text" autocomplete="off" data-plugin-datepicker="" name="start_date" class="form-control  @error('start_date') is-invalid @enderror" placeholder="eg.: dd/mm/yyyy" autocomplete="off" required>

                            {{-- Validation Alert Messages --}}
                            @error('start_date')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>

                        <div class="col-md-12">
                            <label class="control-label">Task End Date</label>
                            <input type="text" autocomplete="off" data-plugin-datepicker="" name="end_date" class="form-control  @error('end_date') is-invalid @enderror" placeholder="eg.: dd/mm/yyyy" autocomplete="off">

                            {{-- Validation Alert Messages --}}
                            @error('end_date')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>

                        <div class="col-md-12">
                            {{-- <label class="control-label"><b>Disable This Task type? </b></label> --}}
                            <div class="checkbox-custom checkbox-default">
                                <input type="checkbox" name="disable_end_date" class=" @error('disable_end_date') is-invalid @enderror">
                                <label for="disable_end_date">Disable Duration Date?</label>
                            </div>

                            {{-- Validation Alert Messages --}}
                            @error('disable_end_date')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>

                        <div class="col-md-12">
                            <div class="form-group">
                                <label class="control-label"><b>Task Notes</b></label>
                                <textarea name="note" rows="2" class="form-control  @error('note') is-invalid @enderror" placeholder="eg.: Task Details Here"></textarea>

                                {{-- Validation Alert Messages --}}
                                @error('note')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>
                    </div>
                </div>

                <div class="modal-footer text-right">
                    <button type="submit" class="btn btn-dark btn-custom btn-sm">Assign New Task</button>
                </div>
            </form>
        </div>
    </div>
</div>
{{-- =========================< Task Create Modal Ends >======================== --}}




{{-- ========================< Task Edit Modal Starts >======================= --}}
<div class="modal fade" id="editTask" tabindex="-1" role="dialog" aria-labelledby="gridSystemModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="gridSystemModalLabel"><b>Add New Task</b></h4>
            </div>
            <form action="{{ route('director.company.task_manager.update') }}" method="POST">
                @csrf
                <div class="modal-body">
                    <input type="hidden" name="hiddenID" id="hiddenID">
                    <div class="row">
                        <div class="col-md-12">
                            <label class="control-label">Task Title <span class="required">*</span></label>
                            <input type="text" autocomplete="off" name="task_title" id="task_title" class="form-control  @error('task_title') is-invalid @enderror" placeholder="eg.: Task Name" required>

                            {{-- Validation Alert Messages --}}
                            @error('task_title')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>

                        <div class="col-md-12">
                            <label class="control-label">Task Type <span class="required">*</span></label>
                            <select name="task_type" id="task_type" data-plugin-selectTwo class="form-control populate  @error('task_type') is-invalid @enderror" required>
                                <option disabled>Select Task Type</option>
                                @foreach ($task_types as $type)
                                    <option value="{{ $type->id }}">{{ $type->title }}</option>
                                @endforeach
                            </select>

                            {{-- Validation Alert Messages --}}
                            @error('task_type')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>

                        <div class="col-md-12">
                            <label class="control-label">Assigned To <span class="required">*</span></label>
                            <select name="assigned_to" id="assigned_to" data-plugin-selectTwo class="form-control populate  @error('assigned_to') is-invalid @enderror" required>
                                <option disabled>Select Assigned To</option>
                                @foreach ($users as $user)
                                    <option value="{{ $user->id }}">{{ $user->saluation." ".$user->forname." ".$user->surname }}</option>
                                @endforeach
                            </select>

                            {{-- Validation Alert Messages --}}
                            @error('assigned_to')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>

                        <div class="col-md-12">
                            <label class="control-label">Client <span class="required">*</span></label>
                            <select name="client" id="client" data-plugin-selectTwo class="form-control populate  @error('client') is-invalid @enderror" required>
                                <option disabled>Select Client</option>
                                @foreach ($clients as $client)
                                    <option value="{{ $client->id }}">{{ $client->client_forname." ".$client->client_middle_name." ".$client->client_surname }}</option>
                                @endforeach
                            </select>

                            {{-- Validation Alert Messages --}}
                            @error('client')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>

                        <div class="col-md-12">
                            <label class="control-label">Task Status <span class="required">*</span></label>
                            <select name="task_status" id="task_status" data-plugin-selectTwo class="form-control populate  @error('task_status') is-invalid @enderror" required>
                                <option disabled>Select Task Status</option>
                                <option value="Pending">Pending</option>
                                <option value="Processing">Processing</option>
                                <option value="On Review">On Review</option>
                                <option value="Completed">Completed</option>
                            </select>

                            {{-- Validation Alert Messages --}}
                            @error('task_status')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>

                        <div class="col-md-12">
                            <label class="control-label">Task Start Date <span class="required">*</span></label>
                            <input type="text" autocomplete="off" data-plugin-datepicker="" name="start_date" id="start_date" class="form-control  @error('start_date') is-invalid @enderror" placeholder="eg.: dd/mm/yyyy" autocomplete="off" required>

                            {{-- Validation Alert Messages --}}
                            @error('start_date')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>

                        <div class="col-md-12">
                            <label class="control-label">Task End Date</label>
                            <input type="text" autocomplete="off" data-plugin-datepicker="" name="end_date" id="end_date" class="form-control  @error('end_date') is-invalid @enderror" placeholder="eg.: dd/mm/yyyy" autocomplete="off">

                            {{-- Validation Alert Messages --}}
                            @error('end_date')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>

                        <div class="col-md-12">
                            {{-- <label class="control-label"><b>Disable This Task type? </b></label> --}}
                            <div class="checkbox-custom checkbox-default">
                                <input type="checkbox" name="disable_end_date" id="disable_end_date" class=" @error('disable_end_date') is-invalid @enderror">
                                <label for="disable_end_date">Disable Duration Date?</label>
                            </div>

                            {{-- Validation Alert Messages --}}
                            @error('disable_end_date')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>

                        <div class="col-md-12">
                            <div class="form-group">
                                <label class="control-label"><b>Task Notes</b></label>
                                <textarea name="note" id="note" rows="2" class="form-control  @error('note') is-invalid @enderror" placeholder="eg.: Task Details Here"></textarea>

                                {{-- Validation Alert Messages --}}
                                @error('note')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>
                    </div>
                </div>

                <div class="modal-footer text-right">
                    <button type="submit" class="btn btn-dark btn-custom btn-sm">Assign New Task</button>
                </div>
            </form>
        </div>
    </div>
</div>
{{-- =========================< Task Edit Modal Ends >======================== --}}

@endsection


@section('script_links')
    {{--  External Javascript  --}}
    <script src="{{ asset('custom/assets/vendor/select2/select2.js') }}"></script>
    <script src="{{ asset('custom/assets/vendor/jquery-datatables/media/js/jquery.dataTables.js') }}"></script>
    <script src="{{ asset('custom/assets/vendor/jquery-datatables/extras/TableTools/js/dataTables.tableTools.min.js') }}"></script>
    <script src="{{ asset('custom/assets/vendor/jquery-datatables-bs3/assets/js/datatables.js') }}"></script>

    <script src="{{ asset('custom/assets/javascripts/tables/examples.datatables.default.js') }}"></script>
    <script src="{{ asset('custom/assets/javascripts/tables/examples.datatables.row.with.details.js') }}"></script>
    <script src="{{ asset('custom/assets/javascripts/tables/examples.datatables.tabletools.js') }}"></script>
    <script src="{{ asset('custom/assets/vendor/bootstrap-datepicker/js/bootstrap-datepicker.js') }}"></script>
@endsection

@section('scripts')
    {{--  External Javascript  --}}

    <script>
    $('#editTask').on('show.bs.modal', function (event) {
        var button = $(event.relatedTarget); // Button that triggered the modal
        var data = button.data('json');
        // console.log(data);
        var modal = $(this);
        modal.find('.modal-body #hiddenID').val(data.id);
        modal.find('.modal-body #task_title').val(data.task_title);

        // Task Type
        modal.find('.modal-body #task_type option[value=' + data.task_type.id + ']').attr('selected', 'selected');
        var name = modal.find('.modal-body #task_type option[value=' + data.task_type.id + ']').html();
        modal.find('.modal-body #s2id_task_type .select2-chosen').html(name);

        // Assigned To
        modal.find('.modal-body #assigned_to option[value=' + data.director.id + ']').attr('selected', 'selected');
        var name = modal.find('.modal-body #assigned_to option[value=' + data.director.id + ']').html();
        modal.find('.modal-body #s2id_assigned_to .select2-chosen').html(name);

        // Client
        modal.find('.modal-body #client option[value=' + data.task_client.id + ']').attr('selected', 'selected');
        var name = modal.find('.modal-body #client option[value=' + data.task_client.id + ']').html();
        modal.find('.modal-body #s2id_client .select2-chosen').html(name);

        // Task Status
        modal.find('.modal-body #task_status option[value=' + data.task_status + ']').attr('selected', 'selected');
        var name = modal.find('.modal-body #task_status option[value=' + data.task_status + ']').html();
        modal.find('.modal-body #s2id_task_status .select2-chosen').html(name);

        // Start Date
        var start_date_format = GetFormattedDate(data.start_date);
        modal.find('.modal-body #start_date').val(start_date_format);

        // End Date
        modal.find('.modal-body #end_date').val(null);
        if(data.end_date != null){
            var end_date_format = GetFormattedDate(data.end_date);
            modal.find('.modal-body #end_date').val(end_date_format);

            modal.find('.modal-body #disable_end_date').prop("checked", false);
        }else{
            modal.find('.modal-body #disable_end_date').prop("checked", true);
        }

        // Task Notes
        modal.find('.modal-body #note').html(data.note);


        let disable_end_date = $("#disable_end_date");
        let end_date = $('#end_date');

        if(disable_end_date.is(':checked')) end_date.attr('disabled', 'disabled');
        else end_date.removeAttr('disabled', 'disabled');

        disable_end_date.click(function(){
            if($(this).is(':checked')) end_date.attr('disabled', 'disabled');
            else end_date.removeAttr('disabled', 'disabled');
        });

    });

    function GetFormattedDate(date_format) {
        var format = date_format.split("-");
        // console.log(format);
        return format[1] + "/" + format[2] + "/" + format[0];
    }
    </script>

@endsection
