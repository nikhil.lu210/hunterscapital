@extends('layouts.director.app')

@section('page_title', '| Company | Compliance | In & Out | Leaving')

@section('stylesheet_links')
    {{--  External CSS  --}}
    <link rel="stylesheet" href="{{ asset('custom/assets/vendor/select2/select2.css') }}" />
    <link rel="stylesheet" href="{{ asset('custom/assets/vendor/bootstrap-datepicker/css/datepicker3.css') }}" />

@endsection

@section('stylesheet')
    {{--  External CSS  --}}
    <style>
        /* Row CSS */
        .tgl{
            padding: 4px 0 !important;
        }
    </style>
@endsection

@section('content')

<header class="page-header">
    <h2>Leaving</h2>

    <div class="right-wrapper pull-right">
        <ol class="breadcrumbs">
            <li>
                <a href="{{ Route('director.dashboard.index') }}">
                    <i class="fa fa-home"></i>
                </a>
            </li>
            <li><span class="active">Company</span></li>
            <li><span class="active">Compliance</span></li>
            <li><span class="active">In & Out</span></li>
            <li><span>Leaving</span></li>
        </ol>
    </div>
</header>

<!-- start: page -->
<section class="panel">
    <header class="panel-heading">
        <div class="row">
            <div class="col-md-8"><h2 class="panel-title">Leaving</h2></div>
            <div class="col-md-4">
                <form action="" method="get">
                    @csrf
                    <div class="row">
                        <div class="col-md-8">
                            <select name="site" data-plugin-selectTwo class="form-control populate" required>
                                <option selected disabled>Select User</option>
                                <option value="user_01">user_01</option>
                                <option value="user_02">user_02</option>
                                <option value="user_03">user_03</option>
                            </select>
                        </div>
                        <div class="col-md-4">
                            <button class="btn btn-dark btn-custom" type="submit">Filter By user</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </header>

    <form action="" method="post">
        @csrf
        <div class="panel-body">
            <table class="table table-bordered table-striped mb-none">
                <thead>
                    <tr class="table-header-row">
                        <th class="text-center">Checklist</th>
                        <th class="text-center">Completed</th>
                        <th class="text-center">Requested Date</th>
                        <th class="text-center">Received Date</th>
                        <th class="text-center">Notes</th>
                    </tr>
                </thead>
                <tbody>
                    <tr class="table-body-row">
                        <th class="text-center">Section A: AR Agreement</th>
                        <td class="text-center tgl">
                            <div class="switch switch-primary">
                                <input type="checkbox" name="switch" data-plugin-ios-switch checked="checked" />
                            </div>
                        </td>
                        <td>
                            <input type="text" autocomplete="off" data-plugin-datepicker="" name="requested_date" class="form-control" placeholder="eg.: dd/mm/yyyy" autocomplete="off">
                        </td>
                        <td>
                            <input type="text" autocomplete="off" data-plugin-datepicker="" name="received_date" class="form-control" placeholder="eg.: dd/mm/yyyy" autocomplete="off">
                        </td>
                        <td>
                            <input type="text" autocomplete="off" name="noote" class="form-control" placeholder="eg.: Simple Notes" autocomplete="off"/>
                        </td>
                    </tr>
                </tbody>
            </table>
        </div>

        <div class="panel-footer text-right">
            <button type="submit" class="btn btn-dark btn-custom btn-md">Save Now</button>
        </div>
    </form>
</section>
<!-- end: page -->
@endsection


@section('script_links')
    {{--  External Javascript  --}}
    <script src="{{ asset('custom/assets/vendor/select2/select2.js') }}"></script>
    <script src="{{ asset('custom/assets/vendor/bootstrap-datepicker/js/bootstrap-datepicker.js') }}"></script>
    <script src="{{ asset('custom/assets/vendor/ios7-switch/ios7-switch.js') }}"></script>
    <script src="{{ asset('custom/assets/javascripts/forms/examples.advanced.form.js') }}"></script>
@endsection

@section('scripts')
    {{--  External Javascript  --}}
    <script>
        //Row Scripts Here
    </script>
@endsection
