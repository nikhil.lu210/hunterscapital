@extends('layouts.director.app')

@section('page_title', '| Company | Compliance | Online Account')

@section('stylesheet_links')
    {{--  External CSS  --}}
    <link rel="stylesheet" href="{{ asset('custom/assets/vendor/select2/select2.css') }}" />
    <link rel="stylesheet" href="{{ asset('custom/assets/vendor/jquery-datatables-bs3/assets/css/datatables.css') }}" />

@endsection

@section('stylesheet')
    {{--  External CSS  --}}
    <style>
        .dataTables_wrapper .DTTT.btn-group {
            display: none !important;
            position: absolute;
            top: -74px;
            right: 0px;
        }

        .member-active{
            background-color: green;
            color: #ffffff;
            padding: 2px 7px;
            border-radius: 3px;
            font-weight: 600;
        }
        .member-inactive{
            background-color: red;
            color: #ffffff;
            padding: 2px 7px;
            border-radius: 3px;
            font-weight: 600;
        }

        .silver{
            background-color: silver;
            color: #000000;
            padding: 2px 7px;
            border-radius: 3px;
            font-weight: 600;
        }
        .bronze{
            background-color: #ab4700;
            color: #ffffff;
            padding: 2px 7px;
            border-radius: 3px;
            font-weight: 600;
        }
        .gold{
            background-color: #ff7c1f;
            color: #ffffff;
            padding: 2px 7px;
            border-radius: 3px;
            font-weight: 600;
        }
        .platinum{
            background-color: #a0bfb4;
            color: #000000;
            padding: 2px 7px;
            border-radius: 3px;
            font-weight: 600;
        }

        .approved{
            background-color: green;
            color: #ffffff;
            padding: 2px 7px;
            border-radius: 3px;
            font-weight: 600;
        }
        .unapproved{
            background-color: red;
            color: #ffffff;
            padding: 2px 7px;
            border-radius: 3px;
            font-weight: 600;
        }
        .pending{
            background-color: yellow;
            color: #000000;
            padding: 2px 7px;
            border-radius: 3px;
            font-weight: 600;
        }
        .modal-header .close {
            outline: none !important;
            border: 1px solid #8e8e8e;
            padding: 0px 5px;
            border-radius: 0;
            opacity: 1;
            color: #8e8e8e;
            transition: 0.3s all ease-in-out;
        }
        .modal-header .close:hover {
            outline: none !important;
            border: 1px solid #0f1b25;
            background-color: #0f1b25;
            opacity: 1;
            color: #ffffff;
            transition: 0.3s all ease-in-out;
        }
        .modal-title{
            color: #0f1b25;
        }
        .modal-content {
            border: 1px solid #e5e5e5;
            border-radius: 0;
            box-shadow: 0 3px 9px rgba(0, 0, 0, 0.1);
        }
        .btn.btn-dark.btn-custom.btn-block {
            margin: 12px 0px;
            border-radius: 0px;
        }
        @media (min-width: 768px){
            .modal-dialog {
                width: 500px;
                margin: 11% auto;
            }
        }
    </style>
@endsection

@section('content')

<header class="page-header">
    <h2>Online Accounts</h2>

    <div class="right-wrapper pull-right">
        <ol class="breadcrumbs">
            <li>
                <a href="{{ Route('director.dashboard.index') }}">
                    <i class="fa fa-home"></i>
                </a>
            </li>
            <li><span class="active">Company</span></li>
            <li><span class="active">Compliance</span></li>
            <li><span>Online Accounts</span></li>
        </ol>
    </div>
</header>

<!-- start: page -->
<section class="panel">
    <header class="panel-heading">
        <h2 class="panel-title">All Online Accounts</h2>
        <button class="btn btn-dark btn-custom btn-sm header-btn" data-toggle="modal" data-target="#newOnlineAccount">Add New Online Accounts</button>
    </header>
    <div class="panel-body">
        <table class="table table-bordered table-striped mb-none" id="datatable-tabletools" data-swf-path="assets/vendor/jquery-datatables/extras/TableTools/swf/copy_csv_xls_pdf.swf">
            <thead>
                <tr class="table-header-row">
                    <th class="text-center">Sl.</th>
                    <th class="text-center">Site</th>
                    <th class="text-center">Username</th>
                    <th class="text-center">Password</th>
                    <th class="text-center">Application Date</th>
                    <th class="text-center">Decision</th>
                    <th class="text-center">Decision Date</th>
                    <th class="text-center">Site Checked</th>
                    <th class="text-center">Actions</th>
                </tr>
            </thead>
            <tbody>
                @foreach($online_accounts as $key => $online_account)
                    <tr class="table-body-row">
                        <th class="text-center">{{$key+1}}</th>
                        <th class="text-center">{{$online_account->site}}</th>
                        <th class="text-center">{{$online_account->email}}</th>
                        <th class="text-center">{{$online_account->password}}</th>
                        <?php $date = new DateTime($online_account->created_at); ?>
                        <th class="text-center">{{ $date->format('d M Y')}}</th>

                        <th class="text-center"><span class="{{ strtolower($online_account->decision) }}">{{$online_account->decision}}</span></th>
                        {{-- <th class="text-center"><span class="unapproved">Unapproved</span></th> --}}
                        {{-- <th class="text-center"><span class="pending">Pending</span></th> --}}

                        <?php $dateU = new DateTime($online_account->updated_at); ?>
                        <th class="text-center">{{ $dateU->format('d M Y')}}</th>

                        @if($online_account->site_checked == 1)
                            <td class="text-center"><span class="member-active"><i class="fa fa-check"></i></span></td>
                        @else
                            <td class="text-center"><span class="member-inactive"><i class="fa fa-times"></i></span></td>
                        @endif

                        <td class="action-td text-center">
                            <a href="{{ route('director.company.compliance.online_account.destroy', ['id' => $online_account->id]) }}" class="btn btn-danger btn-sm btn-sm-custom btn-custom-danger btn-delete" onclick="return confirm('Are You Sure Want To Delete This Account...?')">
                                <i class="fa fa-trash" aria-hidden="true"></i>
                            </a>
                            <a href="#" class="btn btn-dark btn-sm btn-sm-custom btn-custom-info" data-toggle="modal" data-target="#editOnlineAccount" data-todo="{{ $online_account }}"><i class="fa fa-pencil"></i></a>
                            <a href="#" class="btn btn-dark btn-sm btn-sm-custom btn-custom-info" data-toggle="modal" data-target="#editDecision" data-todo="{{ $online_account }}"><i class="fa fa-legal"></i></a>
                        </td>
                    </tr>
                @endforeach
            </tbody>
        </table>
    </div>
</section>
<!-- end: page -->




{{-- ========================< New Online Account Create Modal Starts >======================= --}}
<div class="modal fade" id="newOnlineAccount" tabindex="-1" role="dialog" aria-labelledby="gridSystemModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="gridSystemModalLabel"><b>Add New Online Account</b></h4>
            </div>
            <form action="{{ route('director.company.compliance.online_account.store') }}" method="POST">
                @csrf
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-12">
                            <label class="control-label">Site <span class="required">*</span></label>
                            <select name="site" data-plugin-selectTwo class="form-control populate  @error('site') is-invalid @enderror" required>
                                <option value="ONA">ONA</option>
                                <option value="Connect">Connect</option>
                                <option value="Chrysalis">Chrysalis</option>
                                <option value="Gabriel">Gabriel</option>
                            </select>

                            {{-- Validation Alert Messages --}}
                            @error('site')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>

                        <div class="col-md-12">
                            <label class="control-label"><b>Email (Username)</b> <span class="required">*</span></label>
                            <input type="email" name="email" class="form-control  @error('email') is-invalid @enderror" placeholder="eg.: user@mail.com" required autocomplete="off"/>

                            {{-- Validation Alert Messages --}}
                            @error('email')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>

                        <div class="col-md-12">
                            <label class="control-label"><b>Password</b> <span class="required">*</span></label>
                            <div class="input-group">
                                <input type="text" autocomplete="off" name="password" class="form-control @error('password') is-invalid @enderror" id="password_generator" placeholder="Eg.: 12@#$adss" required minlength="8" autocomplete="off">
                                <span class="input-group-btn">
                                    <button class="btn btn-dark btn-custom" type="button" id="passGen">Generate</button>
                                </span>
                            </div>
                            <small>Password Must Be Minimum 8 Character</small>

                            {{-- Validation Alert Messages --}}
                            @error('password')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                    </div>
                </div>

                <div class="modal-footer text-right">
                    <button type="submit" class="btn btn-dark btn-custom btn-sm">Assign As New Account</button>
                </div>
            </form>
        </div>
    </div>
</div>
{{-- =========================< New Online Account Create Modal Ends >======================== --}}




{{-- ========================< Online Account Update Modal Starts >======================= --}}
<div class="modal fade" id="editOnlineAccount" tabindex="-1" role="dialog" aria-labelledby="gridSystemModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="gridSystemModalLabel"><b>Update This Account</b></h4>
            </div>
            <form action="{{ route('director.company.compliance.online_account.update') }}" method="POST">
                @csrf
                <div class="modal-body">
                    <div class="row">
                        <input type="hidden" name="id" id="update_id">
                        <div class="col-md-12">
                            <label class="control-label">Site <span class="required">*</span></label>
                            <select name="site" id="update_site" data-plugin-selectTwo class="form-control populate  @error('site') is-invalid @enderror" required>
                                <option value="ONA">ONA</option>
                                <option value="Connect" selected>Connect</option>
                                <option value="Chrysalis">Chrysalis</option>
                                <option value="Gabriel">Gabriel</option>
                            </select>

                            {{-- Validation Alert Messages --}}
                            @error('site')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>

                        <div class="col-md-12">
                            <label class="control-label"><b>Email (Username)</b> <span class="required">*</span></label>
                            <input type="email" id="update_email" name="email" class="form-control  @error('email') is-invalid @enderror" placeholder="eg.: user@mail.com" value="user@mail.com" required autocomplete="off"/>

                            {{-- Validation Alert Messages --}}
                            @error('email')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>

                        <div class="col-md-12">
                            <label class="control-label"><b>Password</b> <span class="required">*</span></label>
                            <div class="input-group" id="update_password">
                                <input type="text" autocomplete="off" name="password" class="form-control @error('password') is-invalid @enderror" id="up_password_generator" placeholder="Eg.: 12@#$adss" value="12@#$adss" required minlength="8" autocomplete="off">
                                <span class="input-group-btn">
                                    <button class="btn btn-dark btn-custom" type="button" id="updatePassGen">Generate</button>
                                </span>
                            </div>
                            <small>Password Must Be Minimum 8 Character</small>

                            {{-- Validation Alert Messages --}}
                            @error('password')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                    </div>
                </div>

                <div class="modal-footer text-right">
                    <button type="submit" class="btn btn-dark btn-custom btn-sm">Update Now</button>
                </div>
            </form>
        </div>
    </div>
</div>
{{-- =========================< Online Account Update Modal Ends >======================== --}}




{{-- ========================< Decision Update Modal Starts >======================= --}}
<div class="modal fade" id="editDecision" tabindex="-1" role="dialog" aria-labelledby="gridSystemModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="gridSystemModalLabel"><b>Account Decision</b></h4>
            </div>
            <form action="{{ route('director.company.compliance.online_account.decision') }}" method="POST">
                @csrf
                <div class="modal-body">
                    <div class="row">
                        <input type="hidden" name="id" id="decision_id">
                        <div class="col-md-12">
                            <label class="control-label"><b>Application Decision</b> <span class="required">*</span></label>
                            <select name="decision" id="decision_decision" data-plugin-selectTwo class="form-control populate  @error('decision') is-invalid @enderror" required>
                                <option value="Approved">Approved</option>
                                <option value="Pending">Pending</option>
                                <option value="Unapproved">Unapproved</option>
                            </select>

                            {{-- Validation Alert Messages --}}
                            @error('decision')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>

                        <div class="col-md-12">
                            <div class="form-group">
                                <label class="control-label"><b>Note</b></label>
                                <textarea name="note" id="decision_note" rows="2" class="form-control  @error('note') is-invalid @enderror" placeholder="eg.: Simple Notes"></textarea>

                                {{-- Validation Alert Messages --}}
                                @error('note')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="col-md-12">
                            <label class="control-label"><b>Site Checked? </b></label>
                            <div class="checkbox-custom checkbox-default">
                                <input type="checkbox" name="site_checked" id="site_checked" class=" @error('site_checked') is-invalid @enderror">
                                <label for="site_checked">Yes</label>
                            </div>

                            {{-- Validation Alert Messages --}}
                            @error('site_checked')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                    </div>
                </div>

                <div class="modal-footer text-right">
                    <button type="submit" class="btn btn-dark btn-custom btn-sm">Update This task Type</button>
                </div>
            </form>
        </div>
    </div>
</div>
{{-- =========================< Decision Update Modal Ends >======================== --}}
@endsection


@section('script_links')
    {{--  External Javascript  --}}
    <script src="{{ asset('custom/assets/vendor/select2/select2.js') }}"></script>
    <script src="{{ asset('custom/assets/vendor/jquery-datatables/media/js/jquery.dataTables.js') }}"></script>
    <script src="{{ asset('custom/assets/vendor/jquery-datatables/extras/TableTools/js/dataTables.tableTools.min.js') }}"></script>
    <script src="{{ asset('custom/assets/vendor/jquery-datatables-bs3/assets/js/datatables.js') }}"></script>

    <script src="{{ asset('custom/assets/javascripts/tables/examples.datatables.default.js') }}"></script>
    <script src="{{ asset('custom/assets/javascripts/tables/examples.datatables.row.with.details.js') }}"></script>
    <script src="{{ asset('custom/assets/javascripts/tables/examples.datatables.tabletools.js') }}"></script>
@endsection

@section('scripts')
    {{--  External Javascript  --}}
    <script>
        $( document ).ready(function() {
            $( '#passGen' ).on( "click", function(){
                passwordGeneratoin('password_generator');
            });
            $( '#updatePassGen' ).on( "click", function(){
                passwordGeneratoin('up_password_generator');
            });

            function passwordGeneratoin(aa){
                var text = "";
                var possible = "abcdefghijklmnopqrstuvwxyz!@#$%^&*()-+<>ABCDEFGHIJKLMNOP1234567890";

                for (var i = 0; i <= 8; i++)
                    text += possible.charAt(Math.floor(Math.random() * possible.length));

                var sendpass = document.getElementById(aa);
                sendpass.value = text;
            }


            // $( '#copyPass' ).on( "click", function(){
            //     var copyPass = document.getElementById("password_generator");
            //     copyPass.select();
            //     document.execCommand("copy");
            // });
        });
    </script>

    <script>
        $('#editOnlineAccount').on('show.bs.modal', function (event) {
            var button = $(event.relatedTarget); // Button that triggered the modal
            var data = button.data('todo');

            var modal = $(this);

            modal.find('.modal-body #update_id').val(data.id);

            modal.find('.modal-body #update_site option[value=' + data.site + ']').attr('selected', 'selected');
            var name = modal.find('.modal-body #update_site option[value=' + data.site + ']').html();
            modal.find('.modal-body #s2id_update_site .select2-chosen').html(name);

            modal.find('.modal-body #update_email').val(data.email);
            modal.find('.modal-body #update_password input').val(data.password);
        });

    </script>

    <script>
        $('#editDecision').on('show.bs.modal', function (event) {
            var button = $(event.relatedTarget); // Button that triggered the modal
            var data = button.data('todo');

            var modal = $(this);

            modal.find('.modal-body #decision_id').val(data.id);

            modal.find('.modal-body #decision_decision option[value=' + data.decision + ']').attr('selected', 'selected');
            var name = modal.find('.modal-body #decision_decision option[value=' + data.decision + ']').html();
            modal.find('.modal-body #s2id_decision_decision .select2-chosen').html(name);

            modal.find('.modal-body #decision_note').html(data.decision_note);
            if(data.site_checked == 1)
                // console.log("okay");
                modal.find('.modal-body #site_checked').prop('checked', true);
            else
                // console.log("notokay");
                modal.find('.modal-body #site_checked').prop('checked', false);
        });

    </script>
@endsection
