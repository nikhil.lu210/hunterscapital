@extends('layouts.director.app')

@section('page_title', '| Company | Compliance | Complaints | Create')

@section('stylesheet_links')
    {{--  External CSS  --}}
    <link rel="stylesheet" href="{{ asset('custom/assets/vendor/select2/select2.css') }}" />
    <link rel="stylesheet" href="{{ asset('custom/assets/vendor/bootstrap-multiselect/bootstrap-multiselect.css') }}" />
    <link rel="stylesheet" href="{{ asset('custom/assets/vendor/select2/select2.css') }}" />
    <link rel="stylesheet" href="{{ asset('custom/assets/vendor/bootstrap-datepicker/css/datepicker3.css') }}" />

@endsection

@section('stylesheet')
    {{--  External CSS  --}}
    <style>
        html .wizard-progress .wizard-steps li.active a span,
        html.dark .wizard-progress .wizard-steps li.active a span {
            color: #dd3333;
            border-color: #dd3333;
        }
        html .wizard-progress .wizard-steps li.completed a span,
        html.dark .wizard-progress .wizard-steps li.completed a span,
        html .wizard-progress .steps-progress .progress-indicator,
        html.dark .wizard-progress .steps-progress .progress-indicator{
            border-color: #dd3333;
            background: #dd3333;
        }
        .pager{
            margin: 0;
        }
        .pager li > a,
        .pager li > span,
        .pager li > a:hover,
        .pager li > span:hover{
            background-color: #02070a !important;
            border: 1px solid #02070a !important;
        }
        .pager .disabled > a,
        .pager .disabled > a:hover,
        .pager .disabled > a:focus,
        .pager .disabled > span{
            background-color: #ffffff !important;
            color: #02070a;
        }
        .btn.btn-dark.btn-custom.btn-block {
            margin: 12px 0px;
            border-radius: 0px;
        }
        .btn-group, .btn-group-vertical{
            display: block;
        }
        button.multiselect.dropdown-toggle.btn.btn-default {
            width: 100%;
            text-align: left;
        }
        .btn .caret{
            float: right;
            margin-top: 7px;
        }
        .open > .dropdown-menu {
            top: 35px;
            width: 100%;
            padding: 5px 0px 10px;
        }
        html .wizard-progress .wizard-steps li a, html.dark .wizard-progress .wizard-steps li a {
            padding: 25px 0px 0;
        }
    </style>
@endsection

@section('content')

<header class="page-header">
    <h2>New Complaint</h2>

    <div class="right-wrapper pull-right">
        <ol class="breadcrumbs">
            <li>
                <a href="{{ Route('director.dashboard.index') }}">
                    <i class="fa fa-home"></i>
                </a>
            </li>
            <li><span class="active">Company</span></li>
            <li><span class="active">Compliance</span></li>
            <li>
                <a href="{{ route('director.company.compliance.complaint.index') }}">
                    <span class="active">Complaints</span>
                </a>
            </li>
            <li><span>New Complaint</span></li>
        </ol>
    </div>
</header>

<!-- start: page -->
<section class="panel form-wizard" id="w3">
    <header class="panel-heading">
        <div class="panel-actions">
            <a href="#" class="fa fa-caret-down"></a>
        </div>

        <h2 class="panel-title">Add New Complaint</h2>
    </header>

    <form class="form-horizontal" novalidate="novalidate" action="{{ route('director.company.compliance.complaint.update', ['id'=>$complaint->id]) }}" method="POST">
        @csrf
        <div class="panel-body">
            <div class="wizard-progress">
                <div class="steps-progress">
                    <div class="progress-indicator" style="width: 0%;"></div>
                </div>
                <ul class="wizard-steps">
                    <li class="active">
                        <a href="#complaint_details" data-toggle="tab"><span>1</span>Complaint <br>Details</a>
                    </li>
                    <li>
                        <a href="#response_date" data-toggle="tab"><span>2</span>Response <br>Dates</a>
                    </li>
                    <li>
                        <a href="#assessor_decision" data-toggle="tab"><span>3</span>Assessor <br>Decision</a>
                    </li>
                    <li>
                        <a href="#fos_adjudicator_decision" data-toggle="tab"><span>4</span>FOS Adjudicator <br>Decision</a>
                    </li>
                    <li>
                        <a href="#ombudsman_decision" data-toggle="tab"><span>5</span>Ombudsman <br>Decision</a>
                    </li>
                </ul>
            </div>
            <div class="tab-content">
                {{-- complaint_details Tab --}}
                <div id="complaint_details" class="tab-pane active">
                    <div class="form-group">
                        <div class="col-md-4">
                            <label class="control-label">Adviser <span class="required">*</span></label>
                            <select name="advisor_id" data-plugin-selectTwo class="form-control populate  @error('advisor_id') is-invalid @enderror" required>
                                <option disabled>Select Adviser</option>
                                <option value="{{ $complaint->advisorDetails->id }}" selected>{{ $complaint->advisorDetails->saluation." ".$complaint->advisorDetails->forname." ".$complaint->advisorDetails->middle_name." ".$complaint->advisorDetails->surname }}</option>
                                @foreach($advisors as $advisor)
                                    @php
                                        if($advisor->id == $complaint->advisor) continue;
                                    @endphp
                                    <option value="{{ $advisor->id }}">{{ $advisor->saluation." ".$advisor->forname." ".$advisor->surname }}</option>
                                @endforeach
                            </select>

                            {{-- Validation Alert Messages --}}
                            @error('advisor_id')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>

                        <div class="col-md-4">
                            <label class="control-label">Business Type <span class="required">*</span></label>
                            <select name="business_type" data-plugin-selectTwo class="form-control populate  @error('business_type') is-invalid @enderror" required>
                                <option disabled>Select Business Type</option>
                                <option value="{{ $complaint->business_type }}" selected>{{ $complaint->business_type }}</option>
                                <optgroup label="Investment">
                                    <option value="Bond Guaranteed">Bond Guaranteed</option>
                                    <option value="Bond With Profit">Bond With Profit</option>
                                    <option value="Endowment & TEP">Endowment & TEP</option>
                                    <option value="Friendly Society">Friendly Society</option>
                                    <option value="Fund Management">Fund Management</option>
                                    <option value="Investment Trust">Investment Trust</option>
                                    <option value="Fund Switch (Investments)">Fund Switch (Investments)</option>
                                </optgroup>
                            </select>

                            {{-- Validation Alert Messages --}}
                            @error('business_type')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>

                        <div class="col-md-4">
                            <label class="control-label">Complaint Type <span class="required">*</span></label>
                            <select name="complaint_type" data-plugin-selectTwo class="form-control populate  @error('complaint_type') is-invalid @enderror" required>
                                <option disabled>Select Complaint Type</option>
                                <option value="{{ $complaint->complaint_type }}" selected>{{ $complaint->complaint_type }}</option>
                                <option value="Overcharging">Overcharging</option>
                                <option value="Delays">Delays</option>
                                <option value="Other Admin">Other Admin</option>
                                <option value="Misleading Advice">Misleading Advice</option>
                                <option value="Failure to carry out instructions">Failure to carry out instructions</option>
                                <option value="Poor customer service">Poor customer service</option>
                                <option value="Misleading advertising">Misleading advertising</option>
                                <option value="Disputes over sums/amounts">Disputes over sums/amounts</option>
                                <option value="Switching/churning">Switching/churning</option>
                                <option value="Breach of contract">Breach of contract</option>
                                <option value="Arrears handling">Arrears handling</option>
                                <option value="Other">Other</option>
                            </select>

                            {{-- Validation Alert Messages --}}
                            @error('complaint_type')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>

                        <div class="col-md-4">
                            <label class="control-label">Client Advice Date</label>
                            <input type="text" autocomplete="off" data-plugin-datepicker="" name="client_advice_date" class="form-control  @error('client_advice_date') is-invalid @enderror" placeholder="eg.: dd/mm/yyyy" value="{{ App\Http\Controllers\General::reverseDateFormatDashToSlash($complaint->client_advice_date) }}" autocomplete="off">

                            {{-- Validation Alert Messages --}}
                            @error('client_advice_date')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>

                        <div class="col-md-4">
                            <label class="control-label">Client <span class="required">*</span></label>
                            <select name="client" data-plugin-selectTwo class="form-control populate  @error('client') is-invalid @enderror" required>
                                <option disabled>Select Client</option>
                                <option value="{{ $complaint->clientDetails->id }}" selected>{{ $complaint->clientDetails->client_title." ".$complaint->clientDetails->client_forname." ".$complaint->clientDetails->client_middle_name." ".$complaint->clientDetails->client_surname }}</option>
                                @foreach($clients as $client)
                                    @php
                                        if($client->id == $complaint->client) continue;
                                    @endphp
                                    <option value="{{$client->id}}">{{$client->client_title." ".$client->client_forname." ".$client->client_surname}}</option>
                                @endforeach
                            </select>

                            {{-- Validation Alert Messages --}}
                            @error('client_id')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>

                        <div class="col-md-4">
                            <label class="control-label">Assessor</label>
                            <select name="assessor" data-plugin-selectTwo class="form-control populate  @error('assessor') is-invalid @enderror">
                                {{-- <option disabled>Select Assessor</option> --}}
                                @if ($complaint->assessor)
                                    <option value="{{ $complaint->assessorDetails->id }}" selected>{{ $complaint->assessorDetails->assessor_name }}</option>
                                @else
                                    <option disabled selected>Select Assessor</option>
                                @endif
                                @foreach($assessors as $assessor)
                                    @php
                                        if($assessor->id == $complaint->assessor) continue;
                                    @endphp
                                    <option value="{{$assessor->id}}">{{$assessor->assessor_name}}</option>
                                @endforeach
                            </select>

                            {{-- Validation Alert Messages --}}
                            @error('assessor')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>

                        <div class="col-md-4">
                            <label class="control-label">Client Received <span class="required">*</span></label>
                            <input type="text" autocomplete="off" data-plugin-datepicker="" name="client_received" class="form-control  @error('client_received') is-invalid @enderror" placeholder="eg.: dd/mm/yyyy" value="{{ App\Http\Controllers\General::reverseDateFormatDashToSlash($complaint->client_received) }}" autocomplete="off" required>

                            {{-- Validation Alert Messages --}}
                            @error('client_received')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>

                        <div class="col-md-4">
                            <label class="control-label">Internally Received <span class="required">*</span></label>
                            <input type="text" autocomplete="off" data-plugin-datepicker="" name="internally_received" class="form-control  @error('internally_received') is-invalid @enderror" placeholder="eg.: dd/mm/yyyy" value="{{ App\Http\Controllers\General::reverseDateFormatDashToSlash($complaint->internally_received) }}" autocomplete="off" required>

                            {{-- Validation Alert Messages --}}
                            @error('internally_received')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>

                        <div class="col-md-4">
                            <label class="control-label">Confirmation Sent</label>
                            <input type="text" autocomplete="off" data-plugin-datepicker="" name="confirmation_sent" class="form-control  @error('confirmation_sent') is-invalid @enderror" placeholder="eg.: dd/mm/yyyy" value="{{ App\Http\Controllers\General::reverseDateFormatDashToSlash($complaint->confirmation_sent) }}" autocomplete="off">

                            {{-- Validation Alert Messages --}}
                            @error('confirmation_sent')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>

                        <div class="col-md-3">
                            <label class="control-label">Verbal Complaint?</label>
                            <div class="checkbox-custom checkbox-default">
                                <input type="checkbox" class="form-control  @error('verbar_complaint') is-invalid @enderror" @if($complaint->verbar_complaint == 1) checked=""@endif name="verbar_complaint">
                                <label><strong>Yes</strong></label>
                            </div>

                            {{-- Validation Alert Messages --}}
                            @error('verbar_complaint')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>

                        <div class="col-md-12">
                            <label class="control-label">Details</label>
                            <textarea rows="3" name="details" class="form-control  @error('details') is-invalid @enderror" placeholder="eg.: Details">{{ $complaint->details }}</textarea>

                            {{-- Validation Alert Messages --}}
                            @error('details')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                    </div>
                </div>

                {{-- response_date Tab --}}
                <div id="response_date" class="tab-pane">
                    <div class="form-group">
                        <div class="col-md-4">
                            <label class="control-label">4 Week Letter</label>
                            <input type="text" autocomplete="off" data-plugin-datepicker="" name="four_week_later" class="form-control  @error('four_week_later') is-invalid @enderror" placeholder="eg.: dd/mm/yyyy" value="{{ App\Http\Controllers\General::reverseDateFormatDashToSlash($complaint->four_week_later) }}" autocomplete="off">

                            {{-- Validation Alert Messages --}}
                            @error('four_week_later')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>

                        <div class="col-md-4">
                            <label class="control-label">8 Week Letter</label>
                            <input type="text" autocomplete="off" data-plugin-datepicker="" name="eight_week_later" class="form-control  @error('eight_week_later') is-invalid @enderror" placeholder="eg.: dd/mm/yyyy" value="{{ App\Http\Controllers\General::reverseDateFormatDashToSlash($complaint->eight_week_later) }}" autocomplete="off">

                            {{-- Validation Alert Messages --}}
                            @error('eight_week_later')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>

                        <div class="col-md-4">
                            <label class="control-label">Final Response</label>
                            <input type="text" autocomplete="off" data-plugin-datepicker="" name="final_response" class="form-control  @error('final_response') is-invalid @enderror" placeholder="eg.: dd/mm/yyyy" value="{{ App\Http\Controllers\General::reverseDateFormatDashToSlash($complaint->final_response) }}" autocomplete="off">

                            {{-- Validation Alert Messages --}}
                            @error('final_response')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                    </div>
                </div>

                {{-- assessor_decision Tab --}}
                <div id="assessor_decision" class="tab-pane">
                    <div class="form-group">
                        <div class="col-md-4">
                            <label class="control-label">To Assessor</label>
                            <input type="text" autocomplete="off" data-plugin-datepicker="" name="to_assessor" class="form-control  @error('to_assessor') is-invalid @enderror" placeholder="eg.: dd/mm/yyyy" value="{{ App\Http\Controllers\General::reverseDateFormatDashToSlash($complaint->to_assessor) }}" autocomplete="off">

                            {{-- Validation Alert Messages --}}
                            @error('to_assessor')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>

                        <div class="col-md-4">
                            <label class="control-label">From Assessor</label>
                            <input type="text" autocomplete="off" data-plugin-datepicker="" name="from_assessor" class="form-control  @error('from_assessor') is-invalid @enderror" placeholder="eg.: dd/mm/yyyy" value="{{ App\Http\Controllers\General::reverseDateFormatDashToSlash($complaint->from_assessor) }}" autocomplete="off">

                            {{-- Validation Alert Messages --}}
                            @error('from_assessor')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>

                        <div class="col-md-4">
                            <label class="control-label">To Client</label>
                            <input type="text" autocomplete="off" data-plugin-datepicker="" name="to_client_assessor" class="form-control  @error('to_client_assessor') is-invalid @enderror" placeholder="eg.: dd/mm/yyyy" value="{{ App\Http\Controllers\General::reverseDateFormatDashToSlash($complaint->to_client_assessor) }}" autocomplete="off">

                            {{-- Validation Alert Messages --}}
                            @error('to_client_assessor')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>

                        <div class="col-md-6">
                            <label class="control-label">Redress Amount</label>
                            <input type="number" name="redress_amount_assessor" class="form-control  @error('redress_amount_assessor') is-invalid @enderror" placeholder="eg.: 10" autocomplete="off" min="0" value="{{ $complaint->redress_amount_assessor }}">

                            {{-- Validation Alert Messages --}}
                            @error('redress_amount_assessor')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>

                        <div class="col-md-6">
                            <label class="control-label">Assessor Decision</label>
                            <select name="assessor_decision" data-plugin-selectTwo class="form-control populate  @error('assessor_decision') is-invalid @enderror">
                                @if ($complaint->assessor_decision)
                                    <option value="{{ $complaint->assessor_decision }}" selected>{{ $complaint->assessor_decision }}</option>
                                @else
                                    <option disabled selected>Select Assessor Decision</option>
                                @endif
                                <option value="Upheld">Upheld</option>
                                <option value="Rejected">Rejected</option>
                                <option value="Pending">Pending</option>
                                <option value="Withdrawn">Withdrawn</option>
                                <option value="Reffered">Reffered</option>
                                <option value="Early Resolution-Non Reportable">Early Resolution-Non Reportable</option>
                            </select>

                            {{-- Validation Alert Messages --}}
                            @error('assessor_decision')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>

                        <div class="col-md-12">
                            <label class="control-label">Notes</label>
                            <textarea rows="3" name="assessor_note" value="{{ $complaint->assessor_note }}" class="form-control  @error('assessor_note') is-invalid @enderror" placeholder="eg.: Notes"></textarea>

                            {{-- Validation Alert Messages --}}
                            @error('assessor_note')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                    </div>
                </div>

                {{-- fos_adjudicator_decision Tab --}}
                <div id="fos_adjudicator_decision" class="tab-pane">
                    <div class="form-group">
                        <div class="col-md-12">
                            <label class="control-label">Has the complaint been submitted to the FOS?</label>
                            <div class="checkbox-custom checkbox-default">
                                <input type="checkbox" class="form-control" checked="" id="submitted_to_fos">
                                <label><strong>Yes</strong></label>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-md-4">
                            <label class="control-label">To Adjudicator</label>
                            <input type="text" autocomplete="off" data-plugin-datepicker="" name="to_adjudicator" class="form-control  @error('to_adjudicator') is-invalid @enderror" placeholder="eg.: dd/mm/yyyy" value="{{ App\Http\Controllers\General::reverseDateFormatDashToSlash($complaint->to_adjudicator) }}" autocomplete="off">

                            {{-- Validation Alert Messages --}}
                            @error('to_adjudicator')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>

                        <div class="col-md-4">
                            <label class="control-label">From Adjudicator</label>
                            <input type="text" autocomplete="off" data-plugin-datepicker="" name="from_adjudicator" class="form-control  @error('from_adjudicator') is-invalid @enderror" placeholder="eg.: dd/mm/yyyy" value="{{ App\Http\Controllers\General::reverseDateFormatDashToSlash($complaint->from_adjudicator) }}" autocomplete="off">

                            {{-- Validation Alert Messages --}}
                            @error('from_adjudicator')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>

                        <div class="col-md-4">
                            <label class="control-label">To Client</label>
                            <input type="text" autocomplete="off" data-plugin-datepicker="" name="to_client_adjudicator" class="form-control  @error('to_client_adjudicator') is-invalid @enderror" placeholder="eg.: dd/mm/yyyy" value="{{ App\Http\Controllers\General::reverseDateFormatDashToSlash($complaint->to_client_adjudicator) }}" autocomplete="off">

                            {{-- Validation Alert Messages --}}
                            @error('to_client_adjudicator')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>

                        <div class="col-md-6">
                            <label class="control-label">Redress Amount</label>
                            <input type="number" name="redress_amount_adjudicator" class="form-control  @error('redress_amount_adjudicator') is-invalid @enderror" placeholder="eg.: 10" autocomplete="off" min="0" value="{{ $complaint->redress_amount_adjudicator }}">

                            {{-- Validation Alert Messages --}}
                            @error('redress_amount_adjudicator')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>

                        <div class="col-md-6">
                            <label class="control-label">Adjudicator Decision</label>
                            <select name="adjudicator_decision" data-plugin-selectTwo class="form-control populate  @error('adjudicator_decision') is-invalid @enderror">
                                @if ($complaint->adjudicator_decision)
                                    <option value="{{ $complaint->adjudicator_decision }}" selected>{{ $complaint->adjudicator_decision }}</option>
                                @else
                                    <option disabled selected>Select Assessor Decision</option>
                                @endif
                                <option value="Upheld">Upheld</option>
                                <option value="Rejected">Rejected</option>
                                <option value="Pending">Pending</option>
                                <option value="Withdrawn">Withdrawn</option>
                                <option value="Reffered">Reffered</option>
                                <option value="Early Resolution-Non Reportable">Early Resolution-Non Reportable</option>
                            </select>

                            {{-- Validation Alert Messages --}}
                            @error('adjudicator_decision')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>

                        <div class="col-md-12">
                            <label class="control-label">Notes</label>
                            <textarea rows="3" name="adjudicator_note" class="form-control  @error('adjudicator_note') is-invalid @enderror" placeholder="eg.: Notes">{{ $complaint->adjudicator_note }}</textarea>

                            {{-- Validation Alert Messages --}}
                            @error('adjudicator_note')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                    </div>
                </div>

                {{-- ombudsman_decision Tab --}}
                <div id="ombudsman_decision" class="tab-pane">
                    <div class="form-group">
                        <div class="col-md-12">
                            <label class="control-label">Has the complaint been submitted to the Full Ombudsman?</label>
                            <div class="checkbox-custom checkbox-default">
                                <input type="checkbox" class="form-control" checked="" id="submitted_to_ombudsman">
                                <label><strong>Yes</strong></label>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-md-4">
                            <label class="control-label">To the Ombudsman</label>
                            <input type="text" autocomplete="off" data-plugin-datepicker="" name="to_ombudsman" class="form-control  @error('to_ombudsman') is-invalid @enderror" placeholder="eg.: dd/mm/yyyy" value="{{ App\Http\Controllers\General::reverseDateFormatDashToSlash($complaint->to_ombudsman) }}" autocomplete="off">

                            {{-- Validation Alert Messages --}}
                            @error('to_ombudsman')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>

                        <div class="col-md-4">
                            <label class="control-label">From Ombudsman</label>
                            <input type="text" autocomplete="off" data-plugin-datepicker="" name="from_ombudsman" class="form-control  @error('from_ombudsman') is-invalid @enderror" placeholder="eg.: dd/mm/yyyy" value="{{ App\Http\Controllers\General::reverseDateFormatDashToSlash($complaint->from_ombudsman) }}" autocomplete="off">

                            {{-- Validation Alert Messages --}}
                            @error('from_ombudsman')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>

                        <div class="col-md-4">
                            <label class="control-label">To Client</label>
                            <input type="text" autocomplete="off" data-plugin-datepicker="" name="to_client_ombudsman" class="form-control  @error('to_client_ombudsman') is-invalid @enderror" placeholder="eg.: dd/mm/yyyy" value="{{ App\Http\Controllers\General::reverseDateFormatDashToSlash($complaint->to_client_ombudsman) }}" autocomplete="off">

                            {{-- Validation Alert Messages --}}
                            @error('to_client_ombudsman')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>

                        <div class="col-md-6">
                            <label class="control-label">Redress Amount</label>
                            <input type="number" name="redress_amount_ombudsman" class="form-control  @error('redress_amount_ombudsman') is-invalid @enderror" placeholder="eg.: 10" autocomplete="off" min="0" value="{{ $complaint->redress_amount_ombudsman }}">

                            {{-- Validation Alert Messages --}}
                            @error('redress_amount_ombudsman')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>

                        <div class="col-md-6">
                            <label class="control-label">Final Decision</label>
                            <select name="ombudsman_decision" data-plugin-selectTwo class="form-control populate  @error('ombudsman_decision') is-invalid @enderror">
                                @if ($complaint->adjudicator_decision)
                                    <option value="{{ $complaint->adjudicator_decision }}" selected>{{ $complaint->adjudicator_decision }}</option>
                                @else
                                    <option disabled selected>Select Assessor Decision</option>
                                @endif
                                <option value="Upheld">Upheld</option>
                                <option value="Rejected">Rejected</option>
                                <option value="Pending">Pending</option>
                                <option value="Withdrawn">Withdrawn</option>
                                <option value="Reffered">Reffered</option>
                                <option value="Early Resolution-Non Reportable">Early Resolution-Non Reportable</option>
                            </select>

                            {{-- Validation Alert Messages --}}
                            @error('ombudsman_decision')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>

                        <div class="col-md-12">
                            <label class="control-label">Notes</label>
                            <textarea rows="3" name="ombudsman_note" class="form-control  @error('ombudsman_note') is-invalid @enderror" placeholder="eg.: Notes">{{ $complaint->ombudsman_note }}</textarea>

                            {{-- Validation Alert Messages --}}
                            @error('ombudsman_note')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="panel-footer">
            <ul class="pager">
                <li class="previous disabled pull-left">
                    <a href="#" class="btn btn-dark btn-custom btn-sm">
                        <i class="fa fa-angle-left"></i> Previous
                    </a>
                </li>
                <li class="finish hidden pull-right">
                    <button type="submit" class="btn btn-dark btn-custom btn-sm submit-btn">
                        Update Complaint
                    </button>
                </li>
                <li class="next">
                    <a href="#" class="btn btn-dark btn-custom btn-sm">
                        Next <i class="fa fa-angle-right"></i>
                    </a>
                </li>
            </ul>
        </div>

    </form>
</section>
<!-- end: page -->

@endsection


@section('script_links')
    {{--  External Javascript  --}}
    <script src="{{ asset('custom/assets/vendor/select2/select2.js') }}"></script>
    <script src="{{ asset('custom/assets/vendor/bootstrap-multiselect/bootstrap-multiselect.js') }}"></script>
    <script src="{{ asset('custom/assets/vendor/bootstrap-datepicker/js/bootstrap-datepicker.js') }}"></script>

    <script src="{{ asset('custom/assets/vendor/jquery-validation/jquery.validate.js') }}"></script>
    <script src="{{ asset('custom/assets/vendor/bootstrap-wizard/jquery.bootstrap.wizard.js') }}"></script>
    <script src="{{ asset('custom/assets/javascripts/forms/examples.wizard.js') }}"></script>

    <script src="{{ asset('custom/assets/javascripts/forms/examples.advanced.form.js') }}"></script>
@endsection

@section('scripts')
    {{--  External Javascript  --}}
    <script>
        $( document ).ready(function() {
            //Code From Here
        });
    </script>
@endsection
