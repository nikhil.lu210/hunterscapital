@extends('layouts.director.app')

@section('page_title', '| Company | Compliance | Complaints')

@section('stylesheet_links')
    {{--  External CSS  --}}
    <link rel="stylesheet" href="{{ asset('custom/assets/vendor/select2/select2.css') }}" />
    <link rel="stylesheet" href="{{ asset('custom/assets/vendor/jquery-datatables-bs3/assets/css/datatables.css') }}" />

    <link rel="stylesheet" href="{{ asset('custom/assets/vendor/bootstrap-fileupload/bootstrap-fileupload.min.css') }}" />
@endsection

@section('stylesheet')
    {{--  External CSS  --}}
    <style>
        .dataTables_wrapper .DTTT.btn-group {
            display: none !important;
            position: absolute;
            top: -74px;
            right: 0px;
        }

        .member-active{
            background-color: green;
            color: #ffffff;
            padding: 2px 7px;
            border-radius: 3px;
            font-weight: 600;
        }
        .member-inactive{
            background-color: red;
            color: #ffffff;
            padding: 2px 7px;
            border-radius: 3px;
            font-weight: 600;
        }

        .silver{
            background-color: silver;
            color: #000000;
            padding: 2px 7px;
            border-radius: 3px;
            font-weight: 600;
        }
        .bronze{
            background-color: #ab4700;
            color: #ffffff;
            padding: 2px 7px;
            border-radius: 3px;
            font-weight: 600;
        }
        .gold{
            background-color: #ff7c1f;
            color: #ffffff;
            padding: 2px 7px;
            border-radius: 3px;
            font-weight: 600;
        }
        .platinum{
            background-color: #a0bfb4;
            color: #000000;
            padding: 2px 7px;
            border-radius: 3px;
            font-weight: 600;
        }

        .approved{
            background-color: green;
            color: #ffffff;
            padding: 2px 7px;
            border-radius: 3px;
            font-weight: 600;
        }
        .unapproved{
            background-color: red;
            color: #ffffff;
            padding: 2px 7px;
            border-radius: 3px;
            font-weight: 600;
        }
        .pending{
            background-color: yellow;
            color: #000000;
            padding: 2px 7px;
            border-radius: 3px;
            font-weight: 600;
        }
        .modal-header .close {
            outline: none !important;
            border: 1px solid #8e8e8e;
            padding: 0px 5px;
            border-radius: 0;
            opacity: 1;
            color: #8e8e8e;
            transition: 0.3s all ease-in-out;
        }
        .modal-header .close:hover {
            outline: none !important;
            border: 1px solid #0f1b25;
            background-color: #0f1b25;
            opacity: 1;
            color: #ffffff;
            transition: 0.3s all ease-in-out;
        }
        .modal-title{
            color: #0f1b25;
        }
        .modal-content {
            border: 1px solid #e5e5e5;
            border-radius: 0;
            box-shadow: 0 3px 9px rgba(0, 0, 0, 0.1);
        }
        .btn.btn-dark.btn-custom.btn-block {
            margin: 12px 0px;
            border-radius: 0px;
        }
        @media (min-width: 768px){
            .modal-dialog {
                width: 500px;
                margin: 11% auto;
            }
        }
        .fileupload .uneditable-input .fa {
            top: 38px;
            left: 25px;
        }
    </style>
@endsection

@section('content')

<header class="page-header">
    <h2>Complaints</h2>

    <div class="right-wrapper pull-right">
        <ol class="breadcrumbs">
            <li>
                <a href="{{ Route('director.dashboard.index') }}">
                    <i class="fa fa-home"></i>
                </a>
            </li>
            <li><span class="active">Company</span></li>
            <li><span class="active">Compliance</span></li>
            <li><span>Complaints</span></li>
        </ol>
    </div>
</header>

<!-- start: page -->
<section class="panel">
    <header class="panel-heading">
        <h2 class="panel-title">All Complaints</h2>
        <a href="{{ route('director.company.compliance.complaint.create') }}" class="btn btn-dark btn-custom btn-sm header-btn">Add New Complaints</a>
    </header>
    <div class="panel-body">
        <table class="table table-bordered table-striped mb-none" id="datatable-tabletools" data-swf-path="assets/vendor/jquery-datatables/extras/TableTools/swf/copy_csv_xls_pdf.swf">
            <thead>
                <tr class="table-header-row">
                    <th class="text-center">Sl.</th>
                    <th class="text-center">Adviser</th>
                    <th class="text-center">Business Type</th>
                    <th class="text-center">Complaint Type</th>
                    <th class="text-center">Assessor</th>
                    <th class="text-center">Client</th>
                    <th class="text-center">Information</th>
                    <th class="text-center">Client Received Date</th>
                    <th class="text-center">Document Download</th>
                    <th class="text-center">Document Upload</th>
                    <th class="text-center">Actions</th>
                </tr>
            </thead>
            <tbody>
                @foreach($complaints as $key => $complaint)
                <tr class="table-body-row">
                    <th class="text-center">
                        @if ($key+1 < 10)
                            0{{ $key+1 }}
                        @else
                            {{ $key+1 }}
                        @endif
                    </th>
                    <th class="text-center">{{$complaint->advisorDetails->saluation." ".$complaint->advisorDetails->forname." ".$complaint->advisorDetails->surname}}</th>
                    <th class="text-center">{{$complaint->business_type}}</th>
                    <th class="text-center">{{$complaint->complaint_type}}</th>
                    <th class="text-center">
                        @if($complaint->assessorDetails){{ $complaint->assessorDetails->assessor_name}}
                        @else Not Assigned
                        @endif
                    </th>

                    <th class="text-center">{{$complaint->clientDetails->client_title." ".$complaint->clientDetails->client_forname." ".$complaint->clientDetails->client_surname}}</th>

                    <th class="text-center">
                        <button type="button" class="btn btn-dark btn-sm btn-sm-custom btn-custom-info" data-toggle="modal" data-target="#complaint_details_modal" data-complaint="{{ $complaint }}"><i class="fa fa-info"></i></button>
                    </th>

                    @php $r_date = new DateTime($complaint->client_received); @endphp
                    <th class="text-center">{{$r_date->format('d M Y')}}</th>

                    <th class="text-center">
                        <button type="button" class="btn btn-dark btn-sm btn-sm-custom btn-custom-info" data-toggle="modal" data-target="#document_download_modal" data-todo="{{ $complaint->documents }}" data-complaint_id="{{ $complaint->id }}"><i class="fa fa-cloud-download"></i></button>
                    </th>

                    <th class="text-center">
                        <button type="button" class="btn btn-dark btn-sm btn-sm-custom btn-custom-info" data-toggle="modal" data-target="#document_upload_modal" data-complaint_id="{{ $complaint->id }}"><i class="fa fa-cloud-upload"></i></button>
                    </th>

                    <td class="action-td text-center">
                        <a href="{{ route('director.company.compliance.complaint.destroy', ['id' => $complaint->id]) }}" class="btn btn-danger btn-sm btn-sm-custom btn-custom-danger btn-delete" onclick="return confirm('Are You Sure Want To Delete This Complaint...?')">
                            <i class="fa fa-trash" aria-hidden="true"></i>
                        </a>
                        <a href="{{ route('director.company.compliance.complaint.show', ['id' => $complaint->id]) }}" class="btn btn-dark btn-sm btn-sm-custom btn-custom-info"><i class="fa fa-info"></i></a>
                    </td>
                </tr>
                @endforeach
            </tbody>
        </table>
    </div>
</section>
<!-- end: page -->





{{-- ========================< Complaint Details Modal Starts >======================= --}}
<div class="modal fade" id="complaint_details_modal" tabindex="-1" role="dialog" aria-labelledby="gridSystemModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="gridSystemModalLabel"><b>Complaint Information</b></h4>
            </div>
            <div class="modal-body">
                <table class="table table-bordered table-striped mb-none">
                    <tr>
                        <th>Client Received</th>
                        <td id="client_received">07_08_2019</td>
                    </tr>
                    <tr>
                        <th>Internally Received</th>
                        <td id="internally_received">07_08_2019</td>
                    </tr>
                    <tr>
                        <th>Final Response</th>
                        <td id="final_response">Final_Response</td>
                    </tr>
                    <tr>
                        <th>Assessor Decision</th>
                        <td id="assessor_decision">Assessor _Decision</td>
                    </tr>
                    <tr>
                        <th>Adjudicator Decision</th>
                        <td id="adjudictor_decision">Adjudicator_Decision</td>
                    </tr>
                    <tr>
                        <th>Ombudsman Decision</th>
                        <td id="ombudsman_decision">Ombudsman_Decision</td>
                    </tr>
                </table><br>

                <h5><strong><u>Complaint Details:</u></strong></h5>
                <p id="complaint_details">Lorem ipsum dolor sit amet consectetur adipisicing elit. Illo ipsum ex distinctio ratione maiores dolorem quisquam quod molestiae quis esse.</p><br>

            </div>
        </div>
    </div>
</div>
{{-- =========================< Complaint Details Modal Ends >======================== --}}




{{-- ========================< Document Download Modal Starts >======================= --}}
<div class="modal fade" id="document_download_modal" tabindex="-1" role="dialog" aria-labelledby="gridSystemModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="gridSystemModalLabel"><b>Complaint Documents</b></h4>
            </div>
            <div class="modal-body">
                <table class="table table-bordered table-striped mb-none">
                    <thead>
                        <tr class="table-header-row">
                            <th class="text-center">Sl.</th>
                            <th class="text-center">Document Name</th>
                            <th class="text-center">Upload Date</th>
                            <th class="text-center">Actions</th>
                        </tr>
                    </thead>
                    <tbody id="documents_download">
                        <tr class="table-body-row">
                            <th class="text-center" id="document_sl">01</th>
                            <th class="text-center" id="document_name"><b>document_name</b></th>
                            <th class="text-center" id="document_date">01_02_2019</th>

                            <td class="action-td text-center" id="document_action">
                                <a href="#" class="btn btn-danger btn-sm btn-sm-custom btn-custom-danger btn-delete" onclick="return confirm('Are You Sure Want To Delete This Document...?')">
                                    <i class="fa fa-trash" aria-hidden="true"></i>
                                </a>
                                <a href="#" class="btn btn-dark btn-sm btn-sm-custom btn-custom-info"><i class="fa fa-cloud-download"></i></a>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
{{-- =========================< Document Download Modal Ends >======================== --}}




{{-- ========================< Document Upload Create Modal Starts >======================= --}}
<div class="modal fade" id="document_upload_modal" tabindex="-1" role="dialog" aria-labelledby="gridSystemModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="gridSystemModalLabel"><b>Add New Document</b></h4>
            </div>
            <form action="{{ route('director.company.compliance.complaint.uploadFile') }}" method="POST" enctype="multipart/form-data">
                @csrf
                <div class="modal-body">
                    <div class="row">
                        <input type="hidden" name="id" id="up_complaint_id">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label class="control-label">Document <span class="required">*</span></label>
                                <div class="fileupload fileupload-new" data-provides="fileupload">
                                    <div class="input-append">
                                        <div class="uneditable-input">
                                            <i class="fa fa-file fileupload-exists"></i>
                                            <span class="fileupload-preview"></span>
                                        </div>
                                        <span class="btn btn-default btn-file">
                                            <span class="fileupload-exists">Change</span>
                                            <span class="fileupload-new">Select file</span>
                                            <input type="file" name="document" class="@error('document') is-invalid @enderror" required/>
                                        </span>
                                        <a href="#" class="btn btn-default fileupload-exists" data-dismiss="fileupload">Remove</a>
                                    </div>
                                </div>

                                {{-- Validation Alert Messages --}}
                                @error('document')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="col-md-12">
                            <label class="control-label"><b>Short Description</b></label>
                            <textarea rows="3" name="note" class="form-control @error('note') is-invalid @enderror" placeholder="eg.: Simple Notes."></textarea>

                            {{-- Validation Alert Messages --}}
                            @error('note')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                    </div>
                </div>

                <div class="modal-footer text-right">
                    <button type="submit" class="btn btn-dark btn-custom btn-sm">Upload Now</button>
                </div>
            </form>
        </div>
    </div>
</div>
{{-- =========================< Document Upload Create Modal Ends >======================== --}}
@endsection


@section('script_links')
    {{--  External Javascript  --}}
    <script src="{{ asset('custom/assets/vendor/select2/select2.js') }}"></script>
    <script src="{{ asset('custom/assets/vendor/jquery-datatables/media/js/jquery.dataTables.js') }}"></script>
    <script src="{{ asset('custom/assets/vendor/jquery-datatables/extras/TableTools/js/dataTables.tableTools.min.js') }}"></script>
    <script src="{{ asset('custom/assets/vendor/jquery-datatables-bs3/assets/js/datatables.js') }}"></script>

    <script src="{{ asset('custom/assets/javascripts/tables/examples.datatables.default.js') }}"></script>
    <script src="{{ asset('custom/assets/javascripts/tables/examples.datatables.row.with.details.js') }}"></script>
    <script src="{{ asset('custom/assets/javascripts/tables/examples.datatables.tabletools.js') }}"></script>

    <script src="{{ asset('custom/assets/vendor/bootstrap-fileupload/bootstrap-fileupload.min.js') }}"></script>
@endsection

@section('scripts')
    {{--  External Javascript  --}}
    <script>
        $('#complaint_details_modal').on('show.bs.modal', function (event) {
            var button = $(event.relatedTarget); // Button that triggered the modal
            var data = button.data('complaint');

            var modal = $(this);

            modal.find('.modal-body #client_received').html(data.client_advice_date);
            modal.find('.modal-body #internally_received').html(data.internally_received);
            modal.find('.modal-body #final_response').html(data.final_response);
            modal.find('.modal-body #assessor_decision').html(data.assessor_decision);
            modal.find('.modal-body #adjudictor_decision').html(data.adjudicator_decision);
            modal.find('.modal-body #ombudsman_decision').html(data.ombudsman_decision);
            modal.find('.modal-body #complaint_details').html(data.details);
        });
    </script>


    <script>
        $('#document_upload_modal').on('show.bs.modal', function (event) {
            var button = $(event.relatedTarget); // Button that triggered the modal
            var complaint_id = button.data('complaint_id');

            var modal = $(this);

            modal.find('.modal-body #up_complaint_id').val(complaint_id);
        });
    </script>

    <script>
        $('#document_download_modal').on('show.bs.modal', function (event) {
            var button = $(event.relatedTarget); // Button that triggered the modal
            var documents = button.data('todo');
            var complaint_id = button.data('complaint_id');

            var modal = $(this);

            modal.find('.modal-body #documents_download').empty();

            for(var i = documents.length-1, sl=1; i>=0; i--,sl++){

                var urldes = window.location.origin+"/director/company/compliance/complaint/destoryFile/"+ i +"/"+complaint_id;
                var urldown = window.location.origin+"/director/company/compliance/complaint/downloadFile/"+ i+"/"+complaint_id;

                var tr = "<tr class='table-body-row'><th class='text-center'>"+ sl +"</th><th class='text-center'><b>"+ documents[i].name +"</b></th><th class='text-center'>"+ documents[i].date +"</th><td class='action-td text-center'><a href='"+urldes+"' onclick='return confirm(\"Are You Sure Want To Delete This Complaint...?\")' class='btn btn-danger btn-sm btn-sm-custom btn-custom-danger btn-delete'><i class='fa fa-trash' aria-hidden='true'></i></a><a href='"+urldown+"' class='btn btn-dark btn-sm btn-sm-custom btn-custom-info'><i class='fa fa-cloud-download'></i></a></td></tr>";

                modal.find('.modal-body #documents_download').append(tr);
            }


        });
    </script>
@endsection



