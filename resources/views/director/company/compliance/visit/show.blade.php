@extends('layouts.director.app')

@section('page_title', '| Company | Compliance | Visits | Show')

@section('stylesheet_links')
    {{--  External CSS  --}}
    <link rel="stylesheet" href="{{ asset('custom/assets/vendor/select2/select2.css') }}" />
    <link rel="stylesheet" href="{{ asset('custom/assets/vendor/bootstrap-multiselect/bootstrap-multiselect.css') }}" />
    <link rel="stylesheet" href="{{ asset('custom/assets/vendor/select2/select2.css') }}" />
    <link rel="stylesheet" href="{{ asset('custom/assets/vendor/bootstrap-datepicker/css/datepicker3.css') }}" />

@endsection

@section('stylesheet')
    {{--  External CSS  --}}
    <style>
        /* Custom CSS */
    </style>
@endsection

@section('content')

<header class="page-header">
    <h2>Visits</h2>

    <div class="right-wrapper pull-right">
        <ol class="breadcrumbs">
            <li>
                <a href="{{ Route('director.dashboard.index') }}">
                    <i class="fa fa-home"></i>
                </a>
            </li>
            <li><span class="active">Company</span></li>
            <li><span class="active">Compliance</span></li>
            <li>
                <a href="{{ route('director.company.compliance.visit.index') }}">
                    <span class="active">Visits</span>
                </a>
            </li>
            <li><span>Update Visit</span></li>
        </ol>
    </div>
</header>

<!-- start: page -->
<section class="detials-page">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                {{-- Profile Details  --}}
                <section class="panel panel-danger">
                    <form action="{{ route('director.company.compliance.visit.update', ['id'=> $visit->id]) }}" method="post">
                    @csrf
                        <header class="panel-heading">
                            <div class="panel-actions">
                                <a href="#" class="fa fa-caret-down"></a>
                            </div>

                            <h2 class="panel-title">Update Visit</h2>
                        </header>

                        <div class="panel-body">
                            <div class="panel">
                                <div class="panel-body">
                                    <div class="row">
                                        <div class="col-md-6">
                                            <label class="control-label">Date <span class="required">*</span></label>
                                            <input type="text" autocomplete="off" data-plugin-datepicker="" name="visit_date" class="form-control  @error('visit_date') is-invalid @enderror" placeholder="eg.: dd/mm/yyyy" value="{{ App\Http\Controllers\General::reverseDateFormatDashToSlash($visit->visit_date) }}" autocomplete="off" required>

                                            {{-- Validation Alert Messages --}}
                                            @error('visit_date')
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                            @enderror
                                        </div>

                                        <div class="col-md-6">
                                            <label class="control-label">Signed Off On</label>
                                            <input type="text" autocomplete="off" data-plugin-datepicker="" name="signed_off_on" class="form-control  @error('signed_off_on') is-invalid @enderror" placeholder="eg.: dd/mm/yyyy" value="{{ App\Http\Controllers\General::reverseDateFormatDashToSlash($visit->signed_off_on) }}">

                                            {{-- Validation Alert Messages --}}
                                            @error('signed_off_on')
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                            @enderror
                                        </div>

                                        <div class="col-md-4">
                                            <label class="control-label">Nature of Visit</label>
                                            <select name="nature_of_visit" data-plugin-selectTwo class="form-control populate  @error('nature_of_visit') is-invalid @enderror">
                                                <option disabled>Select Nature of Visit</option>
                                                <option selected value="{{ $visit->nature_of_visit }}">{{ $visit->nature_of_visit }}</option>
                                                <option value="Initial Visit">Initial Visit</option>
                                                <option value="Annual Compliance Visit">Annual Compliance Visit</option>
                                                <option value="Compliance">Compliance</option>
                                                <option value="Pre Joining Visit">Pre Joining Visit</option>
                                            </select>

                                            {{-- Validation Alert Messages --}}
                                            @error('nature_of_visit')
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                            @enderror
                                        </div>

                                        <div class="col-md-4">
                                            <label class="control-label">Signed Off By</label>
                                            <select name="signed_off_by" data-plugin-selectTwo class="form-control populate  @error('signed_off_by') is-invalid @enderror">
                                                <option disabled>Select Signed Off By</option>
                                                <option selected value="{{ $visit->signedOfBy->id }}">{{ $visit->signedOfBy->authors_name }}</option>
                                                @foreach ($authors as $author)
                                                    @php
                                                        if($author->id == $visit->signed_off_by) continue;
                                                    @endphp
                                                    <option value="{{ $author->id }}">{{ $author->authors_name }}</option>
                                                @endforeach
                                            </select>

                                            {{-- Validation Alert Messages --}}
                                            @error('signed_off_by')
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                            @enderror
                                        </div>

                                        <div class="col-md-4">
                                            <label class="control-label">Grading</label>
                                            <select name="grading" data-plugin-selectTwo class="form-control populate  @error('grading') is-invalid @enderror">
                                                <option disabled>Select Grading</option>
                                                <option @if ($visit->grading == 'Low Risk') selected @endif value="Low Risk">Low Risk</option>
                                                <option @if ($visit->grading == 'Medium Risk') selected @endif value="Medium Risk">Medium Risk</option>
                                                <option @if ($visit->grading == 'High Risk') selected @endif value="High Risk">High Risk</option>
                                            </select>

                                            {{-- Validation Alert Messages --}}
                                            @error('grading')
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                            @enderror
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="panel-footer text-right">
                            <button type="submit" class="btn btn-dark btn-custom">Update Now</button>
                        </div>
                    </form>
                </section>
            </div>
        </div>
    </div>
</section>
<!-- end: page -->

@endsection


@section('script_links')
    {{--  External Javascript  --}}
    <script src="{{ asset('custom/assets/vendor/select2/select2.js') }}"></script>
    <script src="{{ asset('custom/assets/vendor/bootstrap-multiselect/bootstrap-multiselect.js') }}"></script>
    <script src="{{ asset('custom/assets/vendor/bootstrap-datepicker/js/bootstrap-datepicker.js') }}"></script>

    <script src="{{ asset('custom/assets/vendor/jquery-validation/jquery.validate.js') }}"></script>
    <script src="{{ asset('custom/assets/vendor/bootstrap-wizard/jquery.bootstrap.wizard.js') }}"></script>
    <script src="{{ asset('custom/assets/javascripts/forms/examples.wizard.js') }}"></script>

    <script src="{{ asset('custom/assets/javascripts/forms/examples.advanced.form.js') }}"></script>
@endsection

@section('scripts')
    {{--  External Javascript  --}}
    <script>
        $( document ).ready(function() {
            //Code From Here
        });
    </script>
@endsection
