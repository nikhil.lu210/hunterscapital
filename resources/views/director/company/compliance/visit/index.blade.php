@extends('layouts.director.app')

@section('page_title', '| Company | Compliance | Visits')

@section('stylesheet_links')
    {{--  External CSS  --}}
    <link rel="stylesheet" href="{{ asset('custom/assets/vendor/select2/select2.css') }}" />
    <link rel="stylesheet" href="{{ asset('custom/assets/vendor/jquery-datatables-bs3/assets/css/datatables.css') }}" />

    <link rel="stylesheet" href="{{ asset('custom/assets/vendor/bootstrap-fileupload/bootstrap-fileupload.min.css') }}" />
@endsection

@section('stylesheet')
    {{--  External CSS  --}}
    <style>
        .dataTables_wrapper .DTTT.btn-group {
            display: none !important;
            position: absolute;
            top: -74px;
            right: 0px;
        }

        .member-active{
            background-color: green;
            color: #ffffff;
            padding: 2px 7px;
            border-radius: 3px;
            font-weight: 600;
        }
        .member-inactive{
            background-color: red;
            color: #ffffff;
            padding: 2px 7px;
            border-radius: 3px;
            font-weight: 600;
        }

        .silver{
            background-color: silver;
            color: #000000;
            padding: 2px 7px;
            border-radius: 3px;
            font-weight: 600;
        }
        .bronze{
            background-color: #ab4700;
            color: #ffffff;
            padding: 2px 7px;
            border-radius: 3px;
            font-weight: 600;
        }
        .gold{
            background-color: #ff7c1f;
            color: #ffffff;
            padding: 2px 7px;
            border-radius: 3px;
            font-weight: 600;
        }
        .platinum{
            background-color: #a0bfb4;
            color: #000000;
            padding: 2px 7px;
            border-radius: 3px;
            font-weight: 600;
        }

        .approved{
            background-color: green;
            color: #ffffff;
            padding: 2px 7px;
            border-radius: 3px;
            font-weight: 600;
        }
        .unapproved{
            background-color: red;
            color: #ffffff;
            padding: 2px 7px;
            border-radius: 3px;
            font-weight: 600;
        }
        .pending{
            background-color: yellow;
            color: #000000;
            padding: 2px 7px;
            border-radius: 3px;
            font-weight: 600;
        }
        .modal-header .close {
            outline: none !important;
            border: 1px solid #8e8e8e;
            padding: 0px 5px;
            border-radius: 0;
            opacity: 1;
            color: #8e8e8e;
            transition: 0.3s all ease-in-out;
        }
        .modal-header .close:hover {
            outline: none !important;
            border: 1px solid #0f1b25;
            background-color: #0f1b25;
            opacity: 1;
            color: #ffffff;
            transition: 0.3s all ease-in-out;
        }
        .modal-title{
            color: #0f1b25;
        }
        .modal-content {
            border: 1px solid #e5e5e5;
            border-radius: 0;
            box-shadow: 0 3px 9px rgba(0, 0, 0, 0.1);
        }
        .btn.btn-dark.btn-custom.btn-block {
            margin: 12px 0px;
            border-radius: 0px;
        }
        @media (min-width: 768px){
            .modal-dialog {
                width: 500px;
                margin: 11% auto;
            }
            #visit_action_modal .modal-dialog {
                width: 70%;
                margin: 11% auto;
            }
        }
        .fileupload .uneditable-input .fa {
            top: 38px;
            left: 25px;
        }
    </style>
@endsection

@section('content')

<header class="page-header">
    <h2>Visits</h2>

    <div class="right-wrapper pull-right">
        <ol class="breadcrumbs">
            <li>
                <a href="{{ Route('director.dashboard.index') }}">
                    <i class="fa fa-home"></i>
                </a>
            </li>
            <li><span class="active">Company</span></li>
            <li><span class="active">Compliance</span></li>
            <li><span>Visits</span></li>
        </ol>
    </div>
</header>

<!-- start: page -->
<section class="panel">
    <header class="panel-heading">
        <h2 class="panel-title">All Visits</h2>
        <a href="{{ route('director.company.compliance.visit.create') }}" class="btn btn-dark btn-custom btn-sm header-btn">Add New Visit</a>
    </header>
    <div class="panel-body">
        <table class="table table-bordered table-striped mb-none" id="datatable-tabletools" data-swf-path="assets/vendor/jquery-datatables/extras/TableTools/swf/copy_csv_xls_pdf.swf">
            <thead>
                <tr class="table-header-row">
                    <th class="text-center">Sl.</th>
                    <th class="text-center">Date</th>
                    <th class="text-center">Nature of Visit</th>
                    <th class="text-center">Signed Off By</th>
                    <th class="text-center">Signed Off Date</th>
                    <th class="text-center">Grading</th>
                    <th class="text-center">Files</th>
                    <th class="text-center">Visit Actions</th>
                    <th class="text-center">Actions</th>
                </tr>
            </thead>
            <tbody>
                @foreach ($visits as $sl=>$data)
                    <tr class="table-body-row">
                        <th class="text-center">
                            @if ($sl+1 < 10)
                                0{{ $sl+1 }}
                            @else
                                {{ $sl+1 }}
                            @endif
                        </th>
                        @php
                            $a = new DateTime($data->visit_date);
                            $v_date = $a->format('d M Y');
                            $b = new DateTime($data->signed_off_date);
                            $s_off_date = $b->format('d M Y');
                        @endphp
                        <th class="text-center">{{ $v_date }}</th>
                        <th class="text-center">{{ $data->nature_of_visit }}</th>
                        <th class="text-center">{{ $data->signedOfBy->authors_name }}</th>
                        <th class="text-center">{{ $s_off_date }}</th>
                        <th class="text-center">{{ $data->grading }}</th>

                        <th class="text-center">
                            <button type="button" class="btn btn-dark btn-sm btn-sm-custom btn-custom-info" data-toggle="modal" data-todo="{{ $data->visitFiles }}" data-target="#document_download_modal"><i class="fa fa-cloud-download"></i></button>

                            <button type="button" class="btn btn-dark btn-sm btn-sm-custom btn-custom-info" data-toggle="modal" data-target="#document_upload_modal" data-visit_id="{{ $data->id }}"><i class="fa fa-cloud-upload"></i></button>
                        </th>

                        <td class="action-td text-center">
                            <a href="{{ route('director.company.compliance.visit.action.create', ['id' => $data->id]) }}" class="btn btn-primary btn-sm btn-sm-custom btn-custom-info"><i class="fa fa-plus"></i></a>

                            <button type="button" class="btn btn-dark btn-sm btn-sm-custom btn-custom-info" data-toggle="modal" data-todo="{{ $data->visitActions }}" data-target="#visit_action_modal"><i class="fa fa-eye"></i></button>
                        </td>

                        <td class="action-td text-center">
                            <a href="{{ route('director.company.compliance.visit.destroy', ['id' => $data->id]) }}" class="btn btn-danger btn-sm btn-sm-custom btn-custom-danger btn-delete" onclick="return confirm('Are You Sure Want To Delete This Visit...?')">
                                <i class="fa fa-trash" aria-hidden="true"></i>
                            </a>
                            <a href="{{ route('director.company.compliance.visit.show', ['id' => $data->id]) }}" class="btn btn-info btn-sm btn-sm-custom btn-custom-info"><i class="fa fa-edit"></i></a>
                        </td>
                    </tr>
                @endforeach
            </tbody>
        </table>
    </div>
</section>
<!-- end: page -->






{{-- ========================< Document Download Modal Starts >======================= --}}
<div class="modal fade" id="document_download_modal" tabindex="-1" role="dialog" aria-labelledby="gridSystemModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="gridSystemModalLabel"><b>Visit Documents</b></h4>
            </div>
            <div class="modal-body">
                <table class="table table-bordered table-striped mb-none">
                    <thead>
                        <tr class="table-header-row">
                            <th class="text-center">Sl.</th>
                            <th class="text-center">Document Name</th>
                            <th class="text-center">Upload Date</th>
                            <th class="text-center">Actions</th>
                        </tr>
                    </thead>
                    <tbody id="documents_download">
                        <tr class="table-body-row">
                            <th class="text-center" id="document_sl">01</th>
                            <th class="text-center" id="document_name"><b>document_name</b></th>
                            <th class="text-center" id="document_date">01_02_2019</th>

                            <td class="action-td text-center" id="document_action">
                                <a href="#" class="btn btn-danger btn-sm btn-sm-custom btn-custom-danger btn-delete" onclick="return confirm('Are You Sure Want To Delete This Document...?')">
                                    <i class="fa fa-trash" aria-hidden="true"></i>
                                </a>
                                <a href="#" class="btn btn-dark btn-sm btn-sm-custom btn-custom-info"><i class="fa fa-cloud-download"></i></a>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
{{-- =========================< Document Download Modal Ends >======================== --}}




{{-- ========================< Document Upload Create Modal Starts >======================= --}}
<div class="modal fade" id="document_upload_modal" tabindex="-1" role="dialog" aria-labelledby="gridSystemModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="gridSystemModalLabel"><b>Add New Document</b></h4>
            </div>
            <form action="{{ route('director.company.compliance.visit.document_upload') }}" method="POST" enctype="multipart/form-data">
                @csrf
                <div class="modal-body">
                    <div class="row">
                        <input type="hidden" name="visit_id" id="hidden_visit_id">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label class="control-label">Document <span class="required">*</span></label>
                                <div class="fileupload fileupload-new" data-provides="fileupload">
                                    <div class="input-append">
                                        <div class="uneditable-input">
                                            <i class="fa fa-file fileupload-exists"></i>
                                            <span class="fileupload-preview"></span>
                                        </div>
                                        <span class="btn btn-default btn-file">
                                            <span class="fileupload-exists">Change</span>
                                            <span class="fileupload-new">Select file</span>
                                            <input type="file" name="document" class="@error('document') is-invalid @enderror" required/>
                                        </span>
                                        <a href="#" class="btn btn-default fileupload-exists" data-dismiss="fileupload">Remove</a>
                                    </div>
                                </div>

                                {{-- Validation Alert Messages --}}
                                @error('document')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="col-md-12">
                            <label class="control-label"><b>Short Description</b></label>
                            <textarea rows="3" name="note" class="form-control @error('note') is-invalid @enderror" placeholder="eg.: Simple Notes."></textarea>

                            {{-- Validation Alert Messages --}}
                            @error('note')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                    </div>
                </div>

                <div class="modal-footer text-right">
                    <button type="submit" class="btn btn-dark btn-custom btn-sm">Upload Now</button>
                </div>
            </form>
        </div>
    </div>
</div>
{{-- =========================< Document Upload Create Modal Ends >======================== --}}





{{-- ========================< Visit Actions Modal Starts >======================= --}}
<div class="modal fade" id="visit_action_modal" tabindex="-1" role="dialog" aria-labelledby="gridSystemModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="gridSystemModalLabel"><b>Visit Actions</b></h4>
            </div>
            <div class="modal-body">
                <table class="table table-bordered table-striped mb-none">
                    <thead>
                        <tr class="table-header-row">
                            <th class="text-center">Sl.</th>
                            <th class="text-center">Agenda</th>
                            <th class="text-center">Outcome</th>
                            <th class="text-center">Deadline</th>
                            <th class="text-center">Completed On</th>
                            <th class="text-center">Related Action</th>
                            <th class="text-center">Note</th>
                            <th class="text-center">Actions</th>
                        </tr>
                    </thead>
                    <tbody id="visit_action">
                        <tr class="table-body-row">
                            <th class="text-center">01</th>
                            <th class="text-center">agenda</th>
                            <th class="text-center">outcome</th>
                            <th class="text-center">deadline</th>
                            <th class="text-center">completed_on</th>
                            <th class="text-center">issue</th>
                            <th class="text-center">note</th>

                            <td class="action-td text-center">
                                <a href="#" class="btn btn-danger btn-sm btn-sm-custom btn-custom-danger btn-delete" onclick="return confirm('Are You Sure Want To Delete This Action...?')">
                                    <i class="fa fa-trash" aria-hidden="true"></i>
                                </a>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
{{-- =========================< Visit Actions Modal Ends >======================== --}}
@endsection


@section('script_links')
    {{--  External Javascript  --}}
    <script src="{{ asset('custom/assets/vendor/select2/select2.js') }}"></script>
    <script src="{{ asset('custom/assets/vendor/jquery-datatables/media/js/jquery.dataTables.js') }}"></script>
    <script src="{{ asset('custom/assets/vendor/jquery-datatables/extras/TableTools/js/dataTables.tableTools.min.js') }}"></script>
    <script src="{{ asset('custom/assets/vendor/jquery-datatables-bs3/assets/js/datatables.js') }}"></script>

    <script src="{{ asset('custom/assets/javascripts/tables/examples.datatables.default.js') }}"></script>
    <script src="{{ asset('custom/assets/javascripts/tables/examples.datatables.row.with.details.js') }}"></script>
    <script src="{{ asset('custom/assets/javascripts/tables/examples.datatables.tabletools.js') }}"></script>

    <script src="{{ asset('custom/assets/vendor/bootstrap-fileupload/bootstrap-fileupload.min.js') }}"></script>
@endsection

@section('scripts')
    {{--  External Javascript  --}}
    <script>
        $(document).ready(function(){
            $('#document_upload_modal').on('show.bs.modal', function (event) {
                var button = $(event.relatedTarget); // Button that triggered the modal
                var visit_id = button.data('visit_id');
                // console.log(visit_id);

                var modal = $(this);

                modal.find('.modal-body #hidden_visit_id').val(visit_id);
            });
        });
    </script>

    <script>
        $(document).ready(function(){
            $('#document_download_modal').on('show.bs.modal', function (event) {
                var button = $(event.relatedTarget); // Button that triggered the modal
                var documents = button.data('todo');

                // console.log(documents);

                var modal = $(this);

                modal.find('.modal-body #documents_download').empty();

                for(var i = documents.length-1, sl=1; i>=0; i--,sl++){

                    var url_destroy = window.location.origin+"/director/company/compliance/visit/document_destroy/"+ documents[i].id;
                    var url_download = window.location.origin+"/director/company/compliance/visit/document_download/"+ documents[i].id;

                    var fileName = documents[i].file_type;
                    var uploadDate = documents[i].updated_at;

                    var tr = "<tr class='table-body-row'><th class='text-center'>"+ sl +"</th><th class='text-center'><b>"+ fileName +"</b></th><th class='text-center'>"+ uploadDate +"</th><td class='action-td text-center'><a href='"+url_destroy+"' onclick='return confirm(\"Are You Sure Want To Delete This Complaint...?\")' class='btn btn-danger btn-sm btn-sm-custom btn-custom-danger btn-delete'><i class='fa fa-trash' aria-hidden='true'></i></a><a href='"+url_download+"' class='btn btn-dark btn-sm btn-sm-custom btn-custom-info'><i class='fa fa-cloud-download'></i></a></td></tr>";

                    modal.find('.modal-body #documents_download').append(tr);
                }
            });
        });
    </script>

    <script>
        $(document).ready(function(){
            $('#visit_action_modal').on('show.bs.modal', function (event) {
                var button = $(event.relatedTarget); // Button that triggered the modal
                var documents = button.data('todo');

                var modal = $(this);

                modal.find('.modal-body #visit_action').empty();

                for(var i = documents.length-1, sl=1; i>=0; i--,sl++){

                    var action_destroy = window.location.origin+"/director/company/compliance/visit/action/destroy/"+ documents[i].id;

                    var table_row = '<tr class="table-body-row"><th class="text-center">'+sl+'</th><th class="text-center">'+documents[i].agenda+'</th><th class="text-center">'+documents[i].outcome+'</th><th class="text-center">'+documents[i].deadline+'</th><th class="text-center">'+documents[i].completed_on+'</th><th class="text-center">'+documents[i].issue+'</th><th class="text-center">'+documents[i].note+'</th><td class="action-td text-center"><a href="'+action_destroy+'" class="btn btn-danger btn-sm btn-sm-custom btn-custom-danger btn-delete" onclick="return confirm(\'Are You Sure Want To Delete This Action...?\')"><i class="fa fa-trash" aria-hidden="true"></i></a></td></tr>';

                    modal.find('.modal-body #visit_action').append(table_row);
                }
            });
        });
    </script>
@endsection
