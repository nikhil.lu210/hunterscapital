@extends('layouts.director.app')

@section('page_title', '| Company | Compliance | Visits | Actions')

@section('stylesheet_links')
    {{--  External CSS  --}}
    <link rel="stylesheet" href="{{ asset('custom/assets/vendor/select2/select2.css') }}" />
    <link rel="stylesheet" href="{{ asset('custom/assets/vendor/bootstrap-multiselect/bootstrap-multiselect.css') }}" />
    <link rel="stylesheet" href="{{ asset('custom/assets/vendor/select2/select2.css') }}" />
    <link rel="stylesheet" href="{{ asset('custom/assets/vendor/bootstrap-datepicker/css/datepicker3.css') }}" />

@endsection

@section('stylesheet')
    {{--  External CSS  --}}
    <style>
        /* Custom CSS */
    </style>
@endsection

@section('content')

<header class="page-header">
    <h2>Visit Action</h2>

    <div class="right-wrapper pull-right">
        <ol class="breadcrumbs">
            <li>
                <a href="{{ Route('director.dashboard.index') }}">
                    <i class="fa fa-home"></i>
                </a>
            </li>
            <li><span class="active">Company</span></li>
            <li><span class="active">Compliance</span></li>
            <li>
                <a href="{{ route('director.company.compliance.visit.index') }}">
                    <span class="active">Visits</span>
                </a>
            </li>
            <li><span>New Visit Action</span></li>
        </ol>
    </div>
</header>

<!-- start: page -->
<section class="detials-page">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                {{-- Profile Details  --}}
                <section class="panel panel-danger">
                    <form action="{{ route('director.company.compliance.visit.action.store', ['id'=> $visit->id]) }}" method="post">
                    @csrf
                        <header class="panel-heading">
                            <div class="panel-actions">
                                <a href="#" class="fa fa-caret-down"></a>
                            </div>

                            <h2 class="panel-title">Create New Action</h2>
                        </header>

                        <div class="panel-body">
                            <div class="panel">
                                <div class="panel-body">
                                    <div class="row">
                                        <div class="col-md-3">
                                            <label class="control-label">Agenda <span class="required">*</span></label>
                                            <select name="agenda" data-plugin-selectTwo class="form-control populate  @error('agenda') is-invalid @enderror" required>
                                                <option selected disabled>Select Agenda</option>
                                                <option value="Overview & Stability">Overview & Stability</option>
                                                <option value="Compliance">Compliance</option>
                                                <option value="Training & Competence">Training & Competence</option>
                                                <option value="Advice Procedures">Advice Procedures</option>
                                                <option value="Training & Competence">Training & Competence</option>
                                            </select>

                                            {{-- Validation Alert Messages --}}
                                            @error('agenda')
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                            @enderror
                                        </div>

                                        <div class="col-md-3">
                                            <label class="control-label">Outcomes <span class="required">*</span></label>
                                            <select name="outcome" data-plugin-selectTwo class="form-control populate  @error('outcome') is-invalid @enderror" required>
                                                <option selected disabled>Select Outcomes</option>
                                                <optgroup label="Overview & Stability">
                                                    <option value="Structure">Structure</option>
                                                    <option value="Financial">Financial</option>
                                                    <option value="Active Business Areas">Active Business Areas</option>
                                                    <option value="Retail Distribution Review">Retail Distribution Review</option>
                                                </optgroup>
                                                <optgroup label="Compliance">
                                                    <option value="Financial Promotions">Financial Promotions</option>
                                                    <option value="Data Protection & Issues">Data Protection & Issues</option>
                                                    <option value="Advising & Setting Standards">Advising & Setting Standards</option>
                                                    <option value="Communication with Clients">Communication with Clients</option>
                                                    <option value="Client Money & Record Keeping">Client Money & Record Keeping</option>
                                                    <option value="Complaints">Complaints</option>
                                                    <option value="Notification & Breaches">Notification & Breaches</option>
                                                    <option value="Premises and Signage">Premises and Signage</option>
                                                    <option value="Public Interest Disclosure Act 1998">Public Interest Disclosure Act 1998</option>
                                                </optgroup>
                                                <optgroup label="Training & Competence">
                                                    <option value="Supervision of Advisers">Supervision of Advisers</option>
                                                    <option value="CPD">CPD</option>
                                                    <option value="File Checks">File Checks</option>
                                                    <option value="Advice Procedures">Advice Procedures</option>
                                                </optgroup>
                                                <optgroup label="Advice Procedures">
                                                    <option value="Sales Process">Sales Process</option>
                                                    <option value="Licence Procedures">Licence Procedures</option>
                                                    <option value="Generic Business Areas">Generic Business Areas</option>
                                                    <option value="Specific Business Types">Specific Business Types</option>
                                                    <option value="Specialist Advice Procedures">Specialist Advice Procedures</option>
                                                </optgroup>
                                                <optgroup label="Treating Customers Fairly">
                                                    <option value="TCF Systems">TCF Systems</option>
                                                    <option value="TCF Outcomes">TCF Outcomes</option>
                                                </optgroup>
                                            </select>

                                            {{-- Validation Alert Messages --}}
                                            @error('outcome')
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                            @enderror
                                        </div>

                                        <div class="col-md-3">
                                            <label class="control-label">Deadline <span class="required">*</span></label>
                                            <input type="text" autocomplete="off" data-plugin-datepicker="" name="deadline" class="form-control  @error('deadline') is-invalid @enderror" placeholder="eg.: dd/mm/yyyy" autocomplete="off" required>

                                            {{-- Validation Alert Messages --}}
                                            @error('deadline')
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                            @enderror
                                        </div>

                                        <div class="col-md-3">
                                            <label class="control-label">Completed On</label>
                                            <input type="text" autocomplete="off" data-plugin-datepicker="" name="completed_on" class="form-control  @error('completed_on') is-invalid @enderror" placeholder="eg.: dd/mm/yyyy" autocomplete="off">

                                            {{-- Validation Alert Messages --}}
                                            @error('completed_on')
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                            @enderror
                                        </div>

                                        <div class="col-md-4">
                                            <label class="control-label">Issue <span class="required">*</span></label>
                                            <textarea rows="3" name="issue" class="form-control @error('issue') is-invalid @enderror" placeholder="eg.: Issue Describe." required></textarea>

                                            {{-- Validation Alert Messages --}}
                                            @error('issue')
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                            @enderror
                                        </div>

                                        <div class="col-md-4">
                                            <label class="control-label">Related Actions <span class="required">*</span></label>
                                            <textarea rows="3" name="related_action" class="form-control @error('related_action') is-invalid @enderror" placeholder="eg.: Related Actions." required></textarea>

                                            {{-- Validation Alert Messages --}}
                                            @error('related_action')
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                            @enderror
                                        </div>

                                        <div class="col-md-4">
                                            <label class="control-label">Notes</label>
                                            <textarea rows="3" name="note" class="form-control @error('note') is-invalid @enderror" placeholder="eg.: Simple Note."></textarea>

                                            {{-- Validation Alert Messages --}}
                                            @error('note')
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                            @enderror
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="panel-footer text-right">
                            <button type="submit" class="btn btn-dark btn-custom">Create Now</button>
                        </div>
                    </form>
                </section>
            </div>
        </div>
    </div>
</section>
<!-- end: page -->

@endsection


@section('script_links')
    {{--  External Javascript  --}}
    <script src="{{ asset('custom/assets/vendor/select2/select2.js') }}"></script>
    <script src="{{ asset('custom/assets/vendor/bootstrap-multiselect/bootstrap-multiselect.js') }}"></script>
    <script src="{{ asset('custom/assets/vendor/bootstrap-datepicker/js/bootstrap-datepicker.js') }}"></script>

    <script src="{{ asset('custom/assets/vendor/jquery-validation/jquery.validate.js') }}"></script>
    <script src="{{ asset('custom/assets/vendor/bootstrap-wizard/jquery.bootstrap.wizard.js') }}"></script>
    <script src="{{ asset('custom/assets/javascripts/forms/examples.wizard.js') }}"></script>

    <script src="{{ asset('custom/assets/javascripts/forms/examples.advanced.form.js') }}"></script>
@endsection

@section('scripts')
    {{--  External Javascript  --}}
    <script>
        $( document ).ready(function() {
            //Code From Here
        });
    </script>
@endsection
