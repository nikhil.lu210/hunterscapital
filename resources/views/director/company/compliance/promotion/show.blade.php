@extends('layouts.director.app')

@section('page_title', '| Company | Compliance | Promotions | Create')

@section('stylesheet_links')
    {{--  External CSS  --}}
    <link rel="stylesheet" href="{{ asset('custom/assets/vendor/select2/select2.css') }}" />
    <link rel="stylesheet" href="{{ asset('custom/assets/vendor/bootstrap-multiselect/bootstrap-multiselect.css') }}" />
    <link rel="stylesheet" href="{{ asset('custom/assets/vendor/select2/select2.css') }}" />
    <link rel="stylesheet" href="{{ asset('custom/assets/vendor/bootstrap-datepicker/css/datepicker3.css') }}" />

@endsection

@section('stylesheet')
    {{--  External CSS  --}}
    <style>
        /* Custom CSS */
    </style>
@endsection

@section('content')

<header class="page-header">
    <h2>Update Promotion</h2>

    <div class="right-wrapper pull-right">
        <ol class="breadcrumbs">
            <li>
                <a href="{{ Route('director.dashboard.index') }}">
                    <i class="fa fa-home"></i>
                </a>
            </li>
            <li><span class="active">Company</span></li>
            <li><span class="active">Compliance</span></li>
            <li>
                <a href="{{ route('director.company.compliance.promotion.index') }}">
                    <span class="active">Promotions</span>
                </a>
            </li>
            <li><span>Update Promotion</span></li>
        </ol>
    </div>
</header>

<!-- start: page -->
<section class="detials-page">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                {{-- Profile Details  --}}
                <section class="panel panel-danger">
                    <form action="{{ route('director.company.compliance.promotion.update', ['id'=>$promotion->id]) }}" method="post">
                    @csrf
                        <header class="panel-heading">
                            <div class="panel-actions">
                                <a href="#" class="fa fa-caret-down"></a>
                            </div>

                            <h2 class="panel-title">Update Promotion</h2>
                        </header>

                        <div class="panel-body">
                            <div class="panel">
                                <div class="panel-body">
                                    <div class="row">
                                        <div class="col-md-4">
                                            <label class="control-label">Promotion Category <span class="required">*</span></label>
                                            <select name="promotion_category" data-plugin-selectTwo class="form-control populate  @error('promotion_category') is-invalid @enderror" required>
                                                <option disabled>Select Promotion Category</option>
                                                <option selected value="{{ $promotion->promotion_category }}">{{ $promotion->promotion_category }}</option>
                                                <option value="Financial Promotion">Financial Promotion</option>
                                                <option value="Stationary">Stationary</option>
                                                <option value="Disclosure Document">Disclosure Document</option>
                                            </select>

                                            {{-- Validation Alert Messages --}}
                                            @error('promotion_category')
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                            @enderror
                                        </div>

                                        <div class="col-md-4">
                                            <label class="control-label">Promotion Type <span class="required">*</span></label>
                                            <select name="promotion_type" data-plugin-selectTwo class="form-control populate  @error('promotion_type') is-invalid @enderror" required>
                                                <option disabled>Select Promotion Type</option>
                                                <option selected value="{{ $promotion->promotion_type }}">{{ $promotion->promotion_type }}</option>
                                                <option value="Directiory listings">Directiory listings</option>
                                                <option value="Computer Based Training">Computer Based Training</option>
                                                <option value="Invitation (Introductions / Meetings)">Invitation (Introductions / Meetings)</option>
                                                <option value="Website">Website</option>
                                                <option value="Company Stationery">Company Stationery</option>
                                                <option value="Journalism/Newsletters">Journalism/Newsletters</option>
                                                <option value="Image Adverts">Image Adverts</option>
                                                <option value="Guides on financial advice">Guides on financial advice</option>
                                                <option value="Direct Offers">Direct Offers</option>
                                                <option value="General service adverts">General service adverts</option>
                                                <option value="Poster">Poster</option>
                                                <option value="Flyer">Flyer</option>
                                                <option value="Client Agreement">Client Agreement</option>
                                                <option value="CIDD">CIDD</option>
                                                <option value="initaial Stationary">initaial Stationary</option>
                                            </select>

                                            {{-- Validation Alert Messages --}}
                                            @error('promotion_type')
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                            @enderror
                                        </div>

                                        <div class="col-md-4">
                                            <label class="control-label">Promotion Medium <span class="required">*</span></label>
                                            <select name="promotion_medium" data-plugin-selectTwo class="form-control populate  @error('promotion_medium') is-invalid @enderror" required>
                                                <option disabled>Select Promotion Medium</option>
                                                <option selected value="{{ $promotion->promotion_medium }}">{{ $promotion->promotion_medium }}</option>
                                                <option value="Email">Email</option>
                                                <option value="Internet">Internet</option>
                                                <option value="letter">letter</option>
                                                <option value="Radio">Radio</option>
                                                <option value="Seminar">Seminar</option>
                                                <option value="Poster">Poster</option>
                                                <option value="Newspaper">Newspaper</option>
                                                <option value="Magazine">Magazine</option>
                                                <option value="Other">Other</option>
                                            </select>

                                            {{-- Validation Alert Messages --}}
                                            @error('promotion_medium')
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                            @enderror
                                        </div>

                                        <div class="col-md-4">
                                            <label class="control-label">Promotion Title <span class="required">*</span></label>
                                            <input type="text" autocomplete="off" name="promotion_title" class="form-control @error('promotion_title') is-invalid @enderror" placeholder="Eg.: Promotion Title" value="{{ $promotion->promotion_title }}" required>

                                            {{-- Validation Alert Messages --}}
                                            @error('promotion_title')
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                            @enderror
                                        </div>

                                        <div class="col-md-4">
                                            <label class="control-label">Requested Date <span class="required">*</span></label>
                                            <input type="text" autocomplete="off" data-plugin-datepicker="" name="requested_date" class="form-control  @error('requested_date') is-invalid @enderror" placeholder="eg.: dd/mm/yyyy" autocomplete="off" required value="{{ App\Http\Controllers\General::reverseDateFormatDashToSlash($promotion->requested_date) }}">

                                            {{-- Validation Alert Messages --}}
                                            @error('requested_date')
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                            @enderror
                                        </div>

                                        <div class="col-md-4">
                                            <label class="control-label">Deadline</label>
                                            <input type="text" autocomplete="off" data-plugin-datepicker="" name="deadline" class="form-control  @error('deadline') is-invalid @enderror" placeholder="eg.: dd/mm/yyyy" autocomplete="off" value="{{ App\Http\Controllers\General::reverseDateFormatDashToSlash($promotion->deadline) }}">

                                            {{-- Validation Alert Messages --}}
                                            @error('deadline')
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                            @enderror
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="panel">
                                <div class="panel-body">
                                    <div class="row">
                                        <div class="col-md-2">
                                            <label class="control-label">Promotion Closed?</label>
                                            <select name="promotion_closed" data-plugin-selectTwo class="form-control populate  @error('promotion_closed') is-invalid @enderror">
                                                @if($promotion->promotion_closed == 0)
                                                    <option value="0" selected>No</option>
                                                    <option value="1">Yes</option>
                                                @elseif($promotion->promotion_closed == 1)
                                                    <option value="0">No</option>
                                                    <option value="1" selected>Yes</option>
                                                @else
                                                    <option disabled selected>Select Status</option>
                                                    <option value="0">No</option>
                                                    <option value="1">Yes</option>
                                                @endif
                                            </select>

                                            {{-- Validation Alert Messages --}}
                                            @error('promotion_closed')
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                            @enderror
                                        </div>

                                        <div class="col-md-5">
                                            <label class="control-label">Closed By</label>
                                            <select name="closed_by" data-plugin-selectTwo class="form-control populate  @error('closed_by') is-invalid @enderror">
                                                <option disabled>Select Closed By</option>
                                                <option selected value="{{ $promotion->closedBy->id }}">{{ $promotion->closedBy->authors_name }}</option>
                                                @foreach ($authors as $author)
                                                    @php
                                                        if($author->id == $promotion->closed_by) continue;
                                                    @endphp
                                                    <option value="{{ $author->id }}">{{ $author->authors_name }}</option>
                                                @endforeach
                                            </select>

                                            {{-- Validation Alert Messages --}}
                                            @error('closed_by')
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                            @enderror
                                        </div>

                                        <div class="col-md-5">
                                            <label class="control-label">Closed Date</label>
                                            <input type="text" autocomplete="off" data-plugin-datepicker="" name="closed_date" class="form-control  @error('closed_date') is-invalid @enderror" placeholder="eg.: dd/mm/yyyy" autocomplete="off" value="{{ App\Http\Controllers\General::reverseDateFormatDashToSlash($promotion->closed_date) }}">

                                            {{-- Validation Alert Messages --}}
                                            @error('closed_date')
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                            @enderror
                                        </div>

                                        <div class="col-md-12">
                                            <?php
                                                $checkbox = json_decode($promotion->overall_result, true);
                                                // dd($checkbox);
                                            ?>
                                            <label class="control-label">Overall result</label>
                                            <div class="row">
                                                <div class="col-md-3">
                                                    <div class="checkbox-custom checkbox-default">
                                                        <input type="checkbox" id="overall_result[one]" name="overall_result[one]" class=" @error('overall_result[one]') is-invalid @enderror" @if(isset($checkbox['one']) && $checkbox['one'] == 'on') checked="true" @endif>
                                                        <label for="overall_result[one]">Promotion appears to meet FCA requirements</label>
                                                    </div>
                                                </div>

                                                <div class="col-md-9">
                                                    <div class="checkbox-custom checkbox-default">
                                                        <input type="checkbox" id="overall_result[two]" name="overall_result[two]" class=" @error('overall_result[two]') is-invalid @enderror" @if(isset($checkbox['two']) && $checkbox['two'] == 'on') checked="true" @endif>
                                                        <label for="overall_result[two]">Promotion appears to meet FCA requirements; however there are some comments that should be taken as advisory comments.</label>
                                                    </div>
                                                </div>

                                                <div class="col-md-12">
                                                    <div class="checkbox-custom checkbox-default">
                                                        <input type="checkbox" id="overall_result[three]" name="overall_result[three]" class=" @error('overall_result[three]') is-invalid @enderror" @if(isset($checkbox['three']) && $checkbox['three'] == 'on') checked="true" @endif>
                                                        <label for="overall_result[three]">There are a number of key points that need to be addressed before approval can be given.</label>
                                                    </div>
                                                </div>
                                            </div>

                                            {{-- Validation Alert Messages --}}
                                            @error('overall_result[one]')
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                            @enderror

                                            {{-- Validation Alert Messages --}}
                                            @error('overall_result[two]')
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                            @enderror

                                            {{-- Validation Alert Messages --}}
                                            @error('overall_result[three]')
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                            @enderror
                                        </div>

                                        <div class="col-md-12">
                                            <label class="control-label">Notes</label>
                                            <textarea name="note" class="form-control @error('note') is-invalid @enderror" placeholder="Eg.: Simple Notes" rows="3">{{ $promotion->note }}</textarea>

                                            {{-- Validation Alert Messages --}}
                                            @error('note')
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                            @enderror
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="panel">
                                <div class="panel-body">
                                    <div class="row">
                                        <div class="col-md-4">
                                            <label class="control-label">Requested By <span class="required">*</span></label>
                                            <select name="requested_by" data-plugin-selectTwo class="form-control populate  @error('requested_by') is-invalid @enderror" required>
                                                <option disabled>Select Requested By</option>
                                                <option selected value="{{ $promotion->requestedBy->id }}">{{ $promotion->requestedBy->saluation.' '.$promotion->requestedBy->forname.' '.$promotion->requestedBy->middle_name.' '.$promotion->requestedBy->surname }}</option>
                                                @foreach ($advisors as $advisor)
                                                @php
                                                    if($advisor->id == $promotion->requested_by) continue;
                                                @endphp
                                                    <option value="{{ $advisor->id }}">{{ $advisor->saluation.' '.$advisor->forname.' '.$advisor->middle_name.' '.$advisor->surname }}</option>
                                                @endforeach
                                            </select>

                                            {{-- Validation Alert Messages --}}
                                            @error('requested_by')
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                            @enderror
                                        </div>

                                        <div class="col-md-4">
                                            <label class="control-label">IFAC Advisor</label>
                                            <select name="ifac_advisor" data-plugin-selectTwo class="form-control populate  @error('ifac_advisor') is-invalid @enderror" required>
                                                <option disabled>Select IFAC Advisor</option>
                                                <option selected value="{{ $promotion->ifacAdvisor->id }}">{{ $promotion->ifacAdvisor->authors_name }}</option>
                                                @foreach ($authors as $author)
                                                    @php
                                                        if($author->id == $promotion->ifac_advisor) continue;
                                                    @endphp
                                                    <option value="{{ $author->id }}">{{ $author->authors_name }}</option>
                                                @endforeach
                                            </select>

                                            {{-- Validation Alert Messages --}}
                                            @error('ifac_advisor')
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                            @enderror
                                        </div>

                                        <div class="col-md-4">
                                            <label class="control-label">Jurisdiction</label>
                                            <select name="jurisdiction" data-plugin-selectTwo class="form-control populate  @error('jurisdiction') is-invalid @enderror" required>
                                                @if ($promotion->jurisdiction == 'UK')
                                                    <option value="UK" selected>UK</option>
                                                    <option value="UK + EEA">UK + EEA</option>
                                                    <option value="Other">Other</option>
                                                @elseif($promotion->jurisdiction == 'UK + EEA')
                                                    <option value="UK">UK</option>
                                                    <option value="UK + EEA" selected>UK + EEA</option>
                                                    <option value="Other">Other</option>
                                                @elseif($promotion->jurisdiction == 'Other')
                                                    <option value="UK">UK</option>
                                                    <option value="UK + EEA">UK + EEA</option>
                                                    <option value="Other" selected>Other</option>
                                                @else
                                                    <option selected disabled>Select Jurisdiction</option>
                                                    <option value="UK">UK</option>
                                                    <option value="UK + EEA">UK + EEA</option>
                                                    <option value="Other">Other</option>
                                                @endif
                                            </select>

                                            {{-- Validation Alert Messages --}}
                                            @error('jurisdiction')
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                            @enderror
                                        </div>

                                        <div class="col-md-12">
                                            <label class="control-label">Product Area Covered</label>
                                            <textarea name="product_area_covered" class="form-control @error('product_area_covered') is-invalid @enderror" placeholder="Eg.: Product Area Covered" rows="3">{{ $promotion->product_area_covered }}</textarea>

                                            {{-- Validation Alert Messages --}}
                                            @error('product_area_covered')
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                            @enderror
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="panel">
                                <div class="panel-body">
                                    <div class="row">
                                        <div class="col-md-4">
                                            <label class="control-label">Reviewer</label>
                                            <select name="reviewer" data-plugin-selectTwo class="form-control populate  @error('reviewer') is-invalid @enderror">
                                                <option disabled>Select Reviewer</option>
                                                <option selected value="{{ $promotion->reviewedBy->id }}">{{ $promotion->reviewedBy->authors_name }}</option>
                                                @foreach ($authors as $author)
                                                    @php
                                                        if($author->id == $promotion->reviewer) continue;
                                                    @endphp
                                                    <option value="{{ $author->id }}">{{ $author->authors_name }}</option>
                                                @endforeach
                                            </select>

                                            {{-- Validation Alert Messages --}}
                                            @error('reviewer')
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                            @enderror
                                        </div>

                                        <div class="col-md-4">
                                            <label class="control-label">Reviewed Date</label>
                                            <input type="text" autocomplete="off" data-plugin-datepicker="" name="reviewed_date" class="form-control  @error('reviewed_date') is-invalid @enderror" placeholder="eg.: dd/mm/yyyy" autocomplete="off" value="{{ App\Http\Controllers\General::reverseDateFormatDashToSlash($promotion->reviewed_date) }}">

                                            {{-- Validation Alert Messages --}}
                                            @error('reviewed_date')
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                            @enderror
                                        </div>

                                        <div class="col-md-4">
                                            <label class="control-label">Signed Off By</label>
                                            <select name="signed_of_by" data-plugin-selectTwo class="form-control populate  @error('signed_of_by') is-invalid @enderror">
                                                <option disabled>Select Signed Off By</option>
                                                <option selected value="{{ $promotion->signedOfBy->id }}">{{ $promotion->signedOfBy->authors_name }}</option>
                                                @foreach ($authors as $author)
                                                    @php
                                                        if($author->id == $promotion->reviewer) continue;
                                                    @endphp
                                                    <option value="{{ $author->id }}">{{ $author->authors_name }}</option>
                                                @endforeach
                                            </select>

                                            {{-- Validation Alert Messages --}}
                                            @error('signed_of_by')
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                            @enderror
                                        </div>

                                        <div class="col-md-4">
                                            <label class="control-label">Approval Date</label>
                                            <input type="text" autocomplete="off" data-plugin-datepicker="" name="approval_date" class="form-control  @error('approval_date') is-invalid @enderror" placeholder="eg.: dd/mm/yyyy" autocomplete="off" value="{{ App\Http\Controllers\General::reverseDateFormatDashToSlash($promotion->approval_date) }}">

                                            {{-- Validation Alert Messages --}}
                                            @error('approval_date')
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                            @enderror
                                        </div>

                                        <div class="col-md-4">
                                            <label class="control-label">Approval To</label>
                                            <input type="text" autocomplete="off" data-plugin-datepicker="" name="approval_to" class="form-control  @error('approval_to') is-invalid @enderror" placeholder="eg.: dd/mm/yyyy" autocomplete="off" value="{{ App\Http\Controllers\General::reverseDateFormatDashToSlash($promotion->approval_to) }}">

                                            {{-- Validation Alert Messages --}}
                                            @error('approval_to')
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                            @enderror
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="panel-footer text-right">
                            <button type="submit" class="btn btn-dark btn-custom">Update Promotion</button>
                        </div>
                    </form>
                </section>
            </div>
        </div>
    </div>
</section>
<!-- end: page -->

@endsection


@section('script_links')
    {{--  External Javascript  --}}
    <script src="{{ asset('custom/assets/vendor/select2/select2.js') }}"></script>
    <script src="{{ asset('custom/assets/vendor/bootstrap-multiselect/bootstrap-multiselect.js') }}"></script>
    <script src="{{ asset('custom/assets/vendor/bootstrap-datepicker/js/bootstrap-datepicker.js') }}"></script>

    <script src="{{ asset('custom/assets/vendor/jquery-validation/jquery.validate.js') }}"></script>
    <script src="{{ asset('custom/assets/vendor/bootstrap-wizard/jquery.bootstrap.wizard.js') }}"></script>
    <script src="{{ asset('custom/assets/javascripts/forms/examples.wizard.js') }}"></script>

    <script src="{{ asset('custom/assets/javascripts/forms/examples.advanced.form.js') }}"></script>
@endsection

@section('scripts')
    {{--  External Javascript  --}}
    <script>
        $( document ).ready(function() {
            //Code From Here
        });
    </script>
@endsection
