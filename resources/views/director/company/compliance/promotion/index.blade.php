@extends('layouts.director.app')

@section('page_title', '| Company | Compliance | Promotions')

@section('stylesheet_links')
    {{--  External CSS  --}}
    <link rel="stylesheet" href="{{ asset('custom/assets/vendor/select2/select2.css') }}" />
    <link rel="stylesheet" href="{{ asset('custom/assets/vendor/jquery-datatables-bs3/assets/css/datatables.css') }}" />

    <link rel="stylesheet" href="{{ asset('custom/assets/vendor/bootstrap-fileupload/bootstrap-fileupload.min.css') }}" />
@endsection

@section('stylesheet')
    {{--  External CSS  --}}
    <style>
        .dataTables_wrapper .DTTT.btn-group {
            display: none !important;
            position: absolute;
            top: -74px;
            right: 0px;
        }

        .member-active{
            background-color: green;
            color: #ffffff;
            padding: 2px 7px;
            border-radius: 3px;
            font-weight: 600;
        }
        .member-inactive{
            background-color: red;
            color: #ffffff;
            padding: 2px 7px;
            border-radius: 3px;
            font-weight: 600;
        }

        .silver{
            background-color: silver;
            color: #000000;
            padding: 2px 7px;
            border-radius: 3px;
            font-weight: 600;
        }
        .bronze{
            background-color: #ab4700;
            color: #ffffff;
            padding: 2px 7px;
            border-radius: 3px;
            font-weight: 600;
        }
        .gold{
            background-color: #ff7c1f;
            color: #ffffff;
            padding: 2px 7px;
            border-radius: 3px;
            font-weight: 600;
        }
        .platinum{
            background-color: #a0bfb4;
            color: #000000;
            padding: 2px 7px;
            border-radius: 3px;
            font-weight: 600;
        }

        .approved{
            background-color: green;
            color: #ffffff;
            padding: 2px 7px;
            border-radius: 3px;
            font-weight: 600;
        }
        .unapproved{
            background-color: red;
            color: #ffffff;
            padding: 2px 7px;
            border-radius: 3px;
            font-weight: 600;
        }
        .pending{
            background-color: yellow;
            color: #000000;
            padding: 2px 7px;
            border-radius: 3px;
            font-weight: 600;
        }
        .modal-header .close {
            outline: none !important;
            border: 1px solid #8e8e8e;
            padding: 0px 5px;
            border-radius: 0;
            opacity: 1;
            color: #8e8e8e;
            transition: 0.3s all ease-in-out;
        }
        .modal-header .close:hover {
            outline: none !important;
            border: 1px solid #0f1b25;
            background-color: #0f1b25;
            opacity: 1;
            color: #ffffff;
            transition: 0.3s all ease-in-out;
        }
        .modal-title{
            color: #0f1b25;
        }
        .modal-content {
            border: 1px solid #e5e5e5;
            border-radius: 0;
            box-shadow: 0 3px 9px rgba(0, 0, 0, 0.1);
        }
        .btn.btn-dark.btn-custom.btn-block {
            margin: 12px 0px;
            border-radius: 0px;
        }
        @media (min-width: 768px){
            .modal-dialog {
                width: 500px;
                margin: 11% auto;
            }
        }
        .fileupload .uneditable-input .fa {
            top: 38px;
            left: 25px;
        }
    </style>
@endsection

@section('content')

<header class="page-header">
    <h2>Promotions</h2>

    <div class="right-wrapper pull-right">
        <ol class="breadcrumbs">
            <li>
                <a href="{{ Route('director.dashboard.index') }}">
                    <i class="fa fa-home"></i>
                </a>
            </li>
            <li><span class="active">Company</span></li>
            <li><span class="active">Compliance</span></li>
            <li><span>Promotions</span></li>
        </ol>
    </div>
</header>

<!-- start: page -->
<section class="panel">
    <header class="panel-heading">
        <h2 class="panel-title">All Promotions</h2>
        <a href="{{ route('director.company.compliance.promotion.create') }}" class="btn btn-dark btn-custom btn-sm header-btn">Add New Promotion</a>
    </header>
    <div class="panel-body">
        <table class="table table-bordered table-striped mb-none" id="datatable-tabletools" data-swf-path="assets/vendor/jquery-datatables/extras/TableTools/swf/copy_csv_xls_pdf.swf">
            <thead>
                <tr class="table-header-row">
                    <th class="text-center">Sl.</th>
                    <th class="text-center">Type</th>
                    <th class="text-center">Medium</th>
                    <th class="text-center">Title</th>
                    <th class="text-center">Requested By</th>
                    <th class="text-center">Requested Date</th>
                    <th class="text-center">Approval</th>
                    <th class="text-center">Deadline</th>
                    <th class="text-center">Document Download</th>
                    <th class="text-center">Document Upload</th>
                    <th class="text-center">Actions</th>
                </tr>
            </thead>
            <tbody>
                @foreach ($promotions as $sl=>$data)
                    <tr class="table-body-row">
                        <th class="text-center">
                            @if ($sl+1 < 10)
                                0{{ $sl+1 }}
                            @else
                                {{ $sl+1 }}
                            @endif
                        </th>
                        <th class="text-center">{{ $data->promotion_type }}</th>
                        <th class="text-center">{{ $data->promotion_medium }}</th>
                        <th class="text-center">{{ $data->promotion_title }}</th>
                        <th class="text-center">
                            {{ $data->requestedBy->saluation.' '.$data->requestedBy->forname.' '.$data->requestedBy->middle_name.' '.$data->requestedBy->surname }}
                        </th>
                        @php
                            $a = new DateTime($data->requested_date);
                            $req_date = $a->format('d M Y');
                            $b = new DateTime($data->approval_date);
                            $app_date = $b->format('d M Y');
                            $c = new DateTime($data->deadline);
                            $ded_date = $c->format('d M Y');
                        @endphp
                        <th class="text-center">{{ $req_date }}</th>
                        <th class="text-center">{{ $app_date }}</th>
                        <th class="text-center">{{ $ded_date }}</th>

                        <th class="text-center">
                            <button type="button" class="btn btn-dark btn-sm btn-sm-custom btn-custom-info" data-toggle="modal" data-todo="{{ $data->documents }}" data-promotion_id="{{ $data->id }}" data-target="#document_download_modal"><i class="fa fa-cloud-download"></i></button>
                        </th>

                        <th class="text-center">
                            <button type="button" class="btn btn-dark btn-sm btn-sm-custom btn-custom-info" data-toggle="modal" data-target="#document_upload_modal" data-promotion_id="{{ $data->id }}"><i class="fa fa-cloud-upload"></i></button>
                        </th>

                        <td class="action-td text-center">
                            <a href="#" class="btn btn-danger btn-sm btn-sm-custom btn-custom-danger btn-delete" onclick="return confirm('Are You Sure Want To Delete This Promotion...?')">
                                <i class="fa fa-trash" aria-hidden="true"></i>
                            </a>
                            <a href="{{ route('director.company.compliance.promotion.show', ['id' => 1]) }}" class="btn btn-dark btn-sm btn-sm-custom btn-custom-info"><i class="fa fa-info"></i></a>
                        </td>
                    </tr>
                @endforeach
            </tbody>
        </table>
    </div>
</section>
<!-- end: page -->





{{-- ========================< Document Download Modal Starts >======================= --}}
<div class="modal fade" id="document_download_modal" tabindex="-1" role="dialog" aria-labelledby="gridSystemModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="gridSystemModalLabel"><b>Promotion Documents</b></h4>
            </div>
            <div class="modal-body">
                <table class="table table-bordered table-striped mb-none">
                    <thead>
                        <tr class="table-header-row">
                            <th class="text-center">Sl.</th>
                            <th class="text-center">Document Name</th>
                            <th class="text-center">Upload Date</th>
                            <th class="text-center">Actions</th>
                        </tr>
                    </thead>
                    <tbody id="documents_download">
                        <tr class="table-body-row">
                            <th class="text-center" id="document_sl">01</th>
                            <th class="text-center" id="document_name"><b>document_name</b></th>
                            <th class="text-center" id="document_date">01_02_2019</th>

                            <td class="action-td text-center" id="document_action">
                                <a href="#" class="btn btn-danger btn-sm btn-sm-custom btn-custom-danger btn-delete" onclick="return confirm('Are You Sure Want To Delete This Document...?')">
                                    <i class="fa fa-trash" aria-hidden="true"></i>
                                </a>
                                <a href="#" class="btn btn-dark btn-sm btn-sm-custom btn-custom-info"><i class="fa fa-cloud-download"></i></a>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
{{-- =========================< Document Download Modal Ends >======================== --}}




{{-- ========================< Document Upload Create Modal Starts >======================= --}}
<div class="modal fade" id="document_upload_modal" tabindex="-1" role="dialog" aria-labelledby="gridSystemModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="gridSystemModalLabel"><b>Add New Document</b></h4>
            </div>
            <form action="{{ route('director.company.compliance.promotion.document_upload') }}" method="POST" enctype="multipart/form-data">
                @csrf
                <div class="modal-body">
                    <div class="row">
                        <input type="hidden" name="hidden_promotion_id" id="hidden_promotion_id">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label class="control-label">Document <span class="required">*</span></label>
                                <div class="fileupload fileupload-new" data-provides="fileupload">
                                    <div class="input-append">
                                        <div class="uneditable-input">
                                            <i class="fa fa-file fileupload-exists"></i>
                                            <span class="fileupload-preview"></span>
                                        </div>
                                        <span class="btn btn-default btn-file">
                                            <span class="fileupload-exists">Change</span>
                                            <span class="fileupload-new">Select file</span>
                                            <input type="file" name="document" class="@error('document') is-invalid @enderror" required/>
                                        </span>
                                        <a href="#" class="btn btn-default fileupload-exists" data-dismiss="fileupload">Remove</a>
                                    </div>
                                </div>

                                {{-- Validation Alert Messages --}}
                                @error('document')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="col-md-12">
                            <label class="control-label"><b>Short Description</b></label>
                            <textarea rows="3" name="note" class="form-control @error('note') is-invalid @enderror" placeholder="eg.: Simple Notes."></textarea>

                            {{-- Validation Alert Messages --}}
                            @error('note')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                    </div>
                </div>

                <div class="modal-footer text-right">
                    <button type="submit" class="btn btn-dark btn-custom btn-sm">Upload Now</button>
                </div>
            </form>
        </div>
    </div>
</div>
{{-- =========================< Document Upload Create Modal Ends >======================== --}}
@endsection


@section('script_links')
    {{--  External Javascript  --}}
    <script src="{{ asset('custom/assets/vendor/select2/select2.js') }}"></script>
    <script src="{{ asset('custom/assets/vendor/jquery-datatables/media/js/jquery.dataTables.js') }}"></script>
    <script src="{{ asset('custom/assets/vendor/jquery-datatables/extras/TableTools/js/dataTables.tableTools.min.js') }}"></script>
    <script src="{{ asset('custom/assets/vendor/jquery-datatables-bs3/assets/js/datatables.js') }}"></script>

    <script src="{{ asset('custom/assets/javascripts/tables/examples.datatables.default.js') }}"></script>
    <script src="{{ asset('custom/assets/javascripts/tables/examples.datatables.row.with.details.js') }}"></script>
    <script src="{{ asset('custom/assets/javascripts/tables/examples.datatables.tabletools.js') }}"></script>

    <script src="{{ asset('custom/assets/vendor/bootstrap-fileupload/bootstrap-fileupload.min.js') }}"></script>
@endsection

@section('scripts')
    {{--  External Javascript  --}}
    <script>
        $(document).ready(function(){
            $('#document_upload_modal').on('show.bs.modal', function (event) {
                var button = $(event.relatedTarget); // Button that triggered the modal
                var promotion_id = button.data('promotion_id');
                // console.log(promotion_id);

                var modal = $(this);

                modal.find('.modal-body #hidden_promotion_id').val(promotion_id);
            });
        });
    </script>

    <script>
        $(document).ready(function(){
            $('#document_download_modal').on('show.bs.modal', function (event) {
                var button = $(event.relatedTarget); // Button that triggered the modal
                var promotion_id = button.data('promotion_id');
                var documents = button.data('todo');

                var modal = $(this);

                modal.find('.modal-body #documents_download').empty();

                for(var i = documents.length-1, sl=1; i>=0; i--,sl++){

                    var urldes = window.location.origin+"/director/company/compliance/promotion/destoryFile/"+ i +"/"+promotion_id;
                    var urldown = window.location.origin+"/director/company/compliance/promotion/downloadFile/"+ i+"/"+promotion_id;

                    var tr = "<tr class='table-body-row'><th class='text-center'>"+ sl +"</th><th class='text-center'><b>"+ documents[i].name +"</b></th><th class='text-center'>"+ documents[i].date +"</th><td class='action-td text-center'><a href='"+urldes+"' onclick='return confirm(\"Are You Sure Want To Delete This Complaint...?\")' class='btn btn-danger btn-sm btn-sm-custom btn-custom-danger btn-delete'><i class='fa fa-trash' aria-hidden='true'></i></a><a href='"+urldown+"' class='btn btn-dark btn-sm btn-sm-custom btn-custom-info'><i class='fa fa-cloud-download'></i></a></td></tr>";

                    modal.find('.modal-body #documents_download').append(tr);
                }
            });
        });
    </script>
@endsection
