@extends('layouts.director.app')

@section('page_title', '| Company | New User')

@section('stylesheet_links')
    {{--  External CSS  --}}
    <link rel="stylesheet" href="{{ asset('custom/assets/vendor/select2/select2.css') }}" />
    <link rel="stylesheet" href="{{ asset('custom/assets/vendor/bootstrap-multiselect/bootstrap-multiselect.css') }}" />
    <link rel="stylesheet" href="{{ asset('custom/assets/vendor/select2/select2.css') }}" />
    <link rel="stylesheet" href="{{ asset('custom/assets/vendor/bootstrap-datepicker/css/datepicker3.css') }}" />

@endsection

@section('stylesheet')
    {{--  External CSS  --}}
    <style>
        html .wizard-progress .wizard-steps li.active a span,
        html.dark .wizard-progress .wizard-steps li.active a span {
            color: #dd3333;
            border-color: #dd3333;
        }
        html .wizard-progress .wizard-steps li.completed a span,
        html.dark .wizard-progress .wizard-steps li.completed a span,
        html .wizard-progress .steps-progress .progress-indicator,
        html.dark .wizard-progress .steps-progress .progress-indicator{
            border-color: #dd3333;
            background: #dd3333;
        }
        .pager{
            margin: 0;
        }
        .pager li > a,
        .pager li > span,
        .pager li > a:hover,
        .pager li > span:hover{
            background-color: #02070a !important;
            border: 1px solid #02070a !important;
        }
        .pager .disabled > a,
        .pager .disabled > a:hover,
        .pager .disabled > a:focus,
        .pager .disabled > span{
            background-color: #ffffff !important;
            color: #02070a;
        }
        .btn.btn-dark.btn-custom.btn-block {
            margin: 12px 0px;
            border-radius: 0px;
        }
        .btn-group, .btn-group-vertical{
            display: block;
        }
        button.multiselect.dropdown-toggle.btn.btn-default {
            width: 100%;
            text-align: left;
        }
        .btn .caret{
            float: right;
            margin-top: 7px;
        }
        .open > .dropdown-menu {
            top: 35px;
            width: 100%;
            padding: 5px 0px 10px;
        }
    </style>
@endsection

@section('content')

<header class="page-header">
    <h2>New User</h2>

    <div class="right-wrapper pull-right">
        <ol class="breadcrumbs">
            <li>
                <a href="{{ Route('director.dashboard.index') }}">
                    <i class="fa fa-home"></i>
                </a>
            </li>
            <li><span class="active">Company</span></li>
            <li><span>New User</span></li>
        </ol>
    </div>
</header>

<!-- start: page -->
<section class="panel form-wizard" id="w3">
    <header class="panel-heading">
        <div class="panel-actions">
            <a href="#" class="fa fa-caret-down"></a>
        </div>

        <h2 class="panel-title">Assign New User</h2>
    </header>

<form class="form-horizontal" novalidate="novalidate" action="{{ route('director.company.user.store', ['id' => 'store']) }}" method="POST">
    @csrf
    <div class="panel-body">
        <div class="wizard-progress">
            <div class="steps-progress">
                <div class="progress-indicator" style="width: 0%;"></div>
            </div>
            <ul class="wizard-steps">
                <li class="active">
                    <a href="#personal_info" data-toggle="tab"><span>1</span>Personal <br>Information</a>
                </li>
                <li>
                    <a href="#qualification" data-toggle="tab"><span>2</span>Qualification <br>Details</a>
                </li>
                <li>
                    <a href="#professional_bodies" data-toggle="tab"><span>3</span>Professional <br>Bodies</a>
                </li>
                <li>
                    <a href="#authentication" data-toggle="tab"><span>4</span>Authentication <br>Access</a>
                </li>
            </ul>
        </div>
        <div class="tab-content">
            {{-- Personal Info Tab --}}
            <div id="personal_info" class="tab-pane active">
                <div class="form-group">
                    <div class="col-md-4">
                        <label class="control-label">Forename <span class="required">*</span></label>
                        <input type="text" autocomplete="off" name="forname" class="form-control  @error('forname') is-invalid @enderror" placeholder="eg.: Jhon" required/>

                        {{-- Validation Alert Messages --}}
                        @error('forname')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                    </div>

                    <div class="col-md-4">
                        <label class="control-label">Middle Name</label>
                        <input type="text" autocomplete="off" name="middle_name" class="form-control  @error('middle_name') is-invalid @enderror" placeholder="eg.: Doe"/>

                        {{-- Validation Alert Messages --}}
                        @error('middle_name')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                    </div>

                    <div class="col-md-4">
                        <label class="control-label">Surame <span class="required">*</span></label>
                        <input type="text" autocomplete="off" name="surname" class="form-control  @error('surname') is-invalid @enderror" placeholder="eg.: Junior"/>

                        {{-- Validation Alert Messages --}}
                        @error('surname')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                    </div>

                    <div class="col-md-4">
                        <label class="control-label">Salutation</label>
                        <input type="text" autocomplete="off" name="salutation" class="form-control  @error('salutation') is-invalid @enderror" placeholder="eg.: Salutation"/>

                        {{-- Validation Alert Messages --}}
                        @error('salutation')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                    </div>

                    <div class="col-md-4">
                        <label class="control-label">Date Of Birth</label>
                        <input type="text" autocomplete="off" data-plugin-datepicker="" name="birthdate" class="form-control  @error('birthdate') is-invalid @enderror" placeholder="eg.: dd/mm/yyyy">

                        {{-- Validation Alert Messages --}}
                        @error('birthdate')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                    </div>

                    <div class="col-md-4">
                        <label class="control-label">Email <span class="required">*</span></label>
                        <input type="email" name="email_address" class="form-control  @error('email_address') is-invalid @enderror" placeholder="eg.: user_mail@mail.com" required/>

                        {{-- Validation Alert Messages --}}
                        @error('email_address')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                    </div>

                    <div class="col-md-4">
                        <label class="control-label">Phone Number</label>
                        <input type="text" autocomplete="off" name="phone_number" class="form-control  @error('phone_number') is-invalid @enderror" placeholder="eg.: Phone Number"/>

                        {{-- Validation Alert Messages --}}
                        @error('phone_number')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                    </div>

                    <div class="col-md-4">
                        <label class="control-label">Mobile Number <span class="required">*</span></label>
                        <input type="text" autocomplete="off" name="mobile_number" class="form-control  @error('mobile_number') is-invalid @enderror" required placeholder="eg.: Mobile Number"/>

                        {{-- Validation Alert Messages --}}
                        @error('mobile_number')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                    </div>

                    <div class="col-md-4">
                        <label class="control-label">Fax Number</label>
                        <input type="text" autocomplete="off" name="fax_number" class="form-control  @error('fax_number') is-invalid @enderror" placeholder="eg.: Fax Number"/>

                        {{-- Validation Alert Messages --}}
                        @error('fax_number')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                    </div>

                    <div class="col-md-4">
                        <label class="control-label">NI Number</label>
                        <input type="text" autocomplete="off" name="ni_number" class="form-control  @error('ni_number') is-invalid @enderror" placeholder="eg.: NI Number"/>

                        {{-- Validation Alert Messages --}}
                        @error('ni_number')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                    </div>

                    <div class="col-md-4">
                        <label class="control-label">Pre-Sale Type</label>
                        <select name="pre_sale_type" data-plugin-selectTwo class="form-control populate  @error('pre_sale_type') is-invalid @enderror">
                            <option disabled selected>Select Pre-Sale Type</option>
                            <option value="First 3 Months">First 3 Months</option>
                            <option value="First 10 Cases">First 10 Cases</option>
                            <option value="3 of Each Type">3 of Each Type</option>
                        </select>

                        {{-- Validation Alert Messages --}}
                        @error('pre_sale_type')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                    </div>

                    <div class="col-md-4">
                        <label class="control-label">FCA Number</label>
                        <input type="text" autocomplete="off" name="fca_number" class="form-control  @error('fca_number') is-invalid @enderror" placeholder="eg.: FCA Number"/>

                        {{-- Validation Alert Messages --}}
                        @error('fca_number')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                    </div>

                    <div class="col-md-4">
                        <label class="control-label">County Old</label>
                        <input type="text" autocomplete="off" name="county_old" class="form-control  @error('county_old') is-invalid @enderror" placeholder="eg.: County Old"/>

                        {{-- Validation Alert Messages --}}
                        @error('county_old')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                    </div>

                    <div class="col-md-4">
                        <label class="control-label">Town / City</label>
                        <input type="text" autocomplete="off" name="town_city" class="form-control  @error('town_city') is-invalid @enderror" placeholder="eg.: Town / City"/>

                        {{-- Validation Alert Messages --}}
                        @error('town_city')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                    </div>

                    <div class="col-md-4">
                        <label class="control-label">Post Code</label>
                        <input type="text" autocomplete="off" name="post_code" class="form-control  @error('post_code') is-invalid @enderror" placeholder="eg.: Post Code"/>

                        {{-- Validation Alert Messages --}}
                        @error('post_code')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                    </div>

                    <div class="col-md-12">
                        <label class="control-label">Address</label>
                        <textarea rows="3" name="post_code" class="form-control  @error('post_code') is-invalid @enderror" placeholder="eg.: Address"></textarea>

                        {{-- Validation Alert Messages --}}
                        @error('post_code')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                    </div>

                </div>
            </div>

            {{-- Qualification Tab --}}
            <div id="qualification" class="tab-pane">
                <div class="form-group">
                    <div class="col-md-12">
                        <label class="control-label">Job Title</label>
                        <input type="text" autocomplete="off" name="job_title" class="form-control  @error('job_title') is-invalid @enderror" placeholder="eg.: Job Title"/>

                        {{-- Validation Alert Messages --}}
                        @error('job_title')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                    </div>

                    <div class="col-md-4">
                        <label class="control-label">Special</label>
                        <select class="form-control  @error('special') is-invalid @enderror" multiple="multiple" data-plugin-multiselect data-plugin-options='{ "includeSelectAllOption": false }' name="special[]">
                            <option value="Level 4 Attained">Level 4 Attained</option>
                            <option value="RDR Gap Filling">RDR Gap Filling</option>
                        </select>

                        {{-- Validation Alert Messages --}}
                        @error('special')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                    </div>

                    <div class="col-md-4">
                        <label class="control-label">Basic</label>
                        <select class="form-control  @error('basic') is-invalid @enderror" multiple="multiple" data-plugin-multiselect data-plugin-options='{ "includeSelectAllOption": false }' name="basic[]">
                            <option value="FPC">FPC</option>
                            <option value="CeFA 1-4">CeFA 1-4</option>
                            <option value="CF2">CF2</option>
                            <option value="CF 1-5">CF 1-5</option>
                        </select>

                        {{-- Validation Alert Messages --}}
                        @error('basic')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                    </div>

                    <div class="col-md-4">
                        <label class="control-label">Mortgage</label>
                        <select class="form-control  @error('mortgage') is-invalid @enderror" multiple="multiple" data-plugin-multiselect data-plugin-options='{ "includeSelectAllOption": false }' name="mortgage[]">
                            <option value="CeMAP 1-3">CeMAP 1-3</option>
                            <option value="MAQ">MAQ</option>
                            <option value="CeMAP Bridge">CeMAP Bridge</option>
                            <option value="IFS DipMap">IFS DipMap</option>
                            <option value="CF6">CF6</option>
                        </select>

                        {{-- Validation Alert Messages --}}
                        @error('mortgage')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                    </div>

                    <div class="col-md-4">
                        <label class="control-label">Lifetime</label>
                        <select class="form-control  @error('lifetime') is-invalid @enderror" multiple="multiple" data-plugin-multiselect data-plugin-options='{ "includeSelectAllOption": false }' name="lifetime[]">
                            <option value="CF7">CF7</option>
                            <option value="ER1">ER1</option>
                            <option value="HR1">HR1</option>
                            <option value="CeRER">CeRER</option>
                        </select>

                        {{-- Validation Alert Messages --}}
                        @error('lifetime')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                    </div>

                    <div class="col-md-4">
                        <label class="control-label">Other</label>
                        <select class="form-control  @error('other') is-invalid @enderror" multiple="multiple" data-plugin-multiselect data-plugin-options='{ "includeSelectAllOption": false }' name="other">
                            <option value="CF8">CF8</option>
                            <option value="CF9">CF9</option>
                            <option value="CeRCC">CeRCC</option>
                            <option value="SV1">SV1</option>
                            <option value="G80">G80</option>
                            <option value="IMC">IMC</option>
                        </select>

                        {{-- Validation Alert Messages --}}
                        @error('other')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                    </div>

                    <div class="col-md-4">
                        <label class="control-label">Heigher Level</label>
                        <select class="form-control  @error('heigher_level') is-invalid @enderror" multiple="multiple" data-plugin-multiselect data-plugin-options='{ "includeSelectAllOption": false }' name="heigher_level[]">
                            <option value="J01">J01</option>
                            <option value="J02">J02</option>
                            <option value="J03">J03</option>
                            <option value="J04">J04</option>
                            <option value="J05">J05</option>
                            <option value="J06">J06</option>
                            <option value="J07">J07</option>
                            <option value="J08">J08</option>
                            <option value="J09">J09</option>
                            <option value="J10">J10</option>
                        </select>

                        {{-- Validation Alert Messages --}}
                        @error('heigher_level')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                    </div>
                </div>
            </div>

            {{-- Professional Bodies Tab --}}
            <div id="professional_bodies" class="tab-pane">
                <div class="form-group">
                    <div class="col-md-3">
                        <label class="control-label">100% Pre-Sale</label>
                        <div class="checkbox-custom checkbox-default">
                            <input type="checkbox" class="form-control  @error('pre_sale_100_percent') is-invalid @enderror" name="pre_sale_100_percent">
                            <label><strong>Yes</strong></label>
                        </div>

                        {{-- Validation Alert Messages --}}
                        @error('pre_sale_100_percent')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                    </div>

                    <div class="col-md-3">
                        <label class="control-label">Exclude from Auto Filecheck</label>
                        <div class="checkbox-custom checkbox-default">
                            <input type="checkbox" class="form-control  @error('exclude_from_auto_filecheck') is-invalid @enderror" name="exclude_from_auto_filecheck">
                            <label><strong>Yes</strong></label>
                        </div>

                        {{-- Validation Alert Messages --}}
                        @error('exclude_from_auto_filecheck')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                    </div>

                    <div class="col-md-3">
                        <label class="control-label">Exclude from Commission Check</label>
                        <div class="checkbox-custom checkbox-default">
                            <input type="checkbox" class="form-control  @error('exclude_from_commision_check') is-invalid @enderror" name="exclude_from_commision_check">
                            <label><strong>Yes</strong></label>
                        </div>

                        {{-- Validation Alert Messages --}}
                        @error('exclude_from_commision_check')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                    </div>

                    <div class="col-md-3">
                        <label class="control-label">Exclude from the advisers list (Policies and Clients)</label>
                        <div class="checkbox-custom checkbox-default">
                            <input type="checkbox" class="form-control  @error('exclude_from_adviser_list') is-invalid @enderror" name="exclude_from_adviser_list">
                            <label><strong>Yes</strong></label>
                        </div>

                        {{-- Validation Alert Messages --}}
                        @error('exclude_from_adviser_list')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                    </div>
                    <div class="col-md-12">
                        <hr>
                    </div>
                    <div class="col-md-3">
                        <label class="control-label">PFS (CII) - Personal Finance Society</label>
                        <div class="checkbox-custom checkbox-default">
                            <input type="checkbox" class="form-control  @error('personal_finance') is-invalid @enderror" name="personal_finance">
                            <label><strong>Yes</strong></label>
                        </div>

                        {{-- Validation Alert Messages --}}
                        @error('personal_finance')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                    </div>

                    <div class="col-md-3">
                        <label class="control-label">IFP - Institute of Financial Planners / CISI</label>
                        <div class="checkbox-custom checkbox-default">
                            <input type="checkbox" class="form-control  @error('institute_of_financial_planners') is-invalid @enderror" name="institute_of_financial_planners">
                            <label><strong>Yes</strong></label>
                        </div>

                        {{-- Validation Alert Messages --}}
                        @error('institute_of_financial_planners')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                    </div>

                    <div class="col-md-2">
                        <label class="control-label">IFS School of Finance / LIBF</label>
                        <div class="checkbox-custom checkbox-default">
                            <input type="checkbox" class="form-control  @error('school_of_finance') is-invalid @enderror" name="school_of_finance">
                            <label><strong>Yes</strong></label>
                        </div>

                        {{-- Validation Alert Messages --}}
                        @error('school_of_finance')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                    </div>

                    <div class="col-md-2">
                        <label class="control-label">Institute of Actuaries</label>
                        <div class="checkbox-custom checkbox-default">
                            <input type="checkbox" class="form-control  @error('institute_of_actuaries') is-invalid @enderror" name="institute_of_actuaries">
                            <label><strong>Yes</strong></label>
                        </div>

                        {{-- Validation Alert Messages --}}
                        @error('institute_of_actuaries')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                    </div>

                    <div class="col-md-2">
                        <label class="control-label">Institute of Bankers</label>
                        <div class="checkbox-custom checkbox-default">
                            <input type="checkbox" class="form-control  @error('institute_of_bankers') is-invalid @enderror" name="institute_of_bankers">
                            <label><strong>Yes</strong></label>
                        </div>

                        {{-- Validation Alert Messages --}}
                        @error('institute_of_bankers')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                    </div>
                </div>
            </div>

            {{-- Authentication Tab --}}
            <div id="authentication" class="tab-pane">
                <div class="col-md-4 col-centered">
                    <div class="panel">
                        <div class="panel-body">
                            <div class="form-group">
                                <div class="col-md-12">
                                    <label class="control-label">Email (Username) <span class="required">*</span></label>
                                    <input type="email" name="email" class="form-control  @error('email') is-invalid @enderror" placeholder="eg.: login_mail@mail.com" required/>

                                    {{-- Validation Alert Messages --}}
                                    @error('email')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>

                                <div class="col-md-12">
                                    <label class="control-label">Passowrd <span class="required">*</span></label>
                                    <div class="input-group">
                                        <input type="text" autocomplete="off" name="password" class="form-control @error('password') is-invalid @enderror" id="password_generator" placeholder="Eg.: 12@#$adss" required>
                                        <span class="input-group-btn">
                                            <button class="btn btn-dark btn-custom" type="button" id="passGen">Generate</button>
                                        </span>
                                    </div>

                                    {{-- Validation Alert Messages --}}
                                    @error('password')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>

                                <div class="col-md-12">
                                    <label class="control-label">Job Role <span class="required">*</span></label>
                                    <select name="job_role" data-plugin-selectTwo class="form-control populate  @error('job_role') is-invalid @enderror" required>
                                        <option value="1">Super Admin</option>
                                        <option value="3">Director</option>
                                    </select>

                                    {{-- Validation Alert Messages --}}
                                    @error('job_role')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>

                                <div class="col-md-12">
                                    <label class="control-label">User Status <span class="required">*</span></label>
                                    <select name="user_status" data-plugin-selectTwo class="form-control populate  @error('user_status') is-invalid @enderror" required>
                                        <option value="1">Active</option>
                                        <option value="0">Inactive</option>
                                    </select>

                                    {{-- Validation Alert Messages --}}
                                    @error('user_status')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>

                                <div class="col-md-12">
                                    <br>
                                    <div class="checkbox-custom checkbox-default">
                                        <input type="checkbox" class="@error('access_status') is-invalid @enderror" name="send_access_info" id="sendMail">
                                        <label for="sendMail"><b>Send Mail To client_name's Email With All Access Information.</b></label>
                                    </div>

                                    {{-- Validation Alert Messages --}}
                                    @error('access_status')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="panel-footer">
        <ul class="pager">
            <li class="previous disabled pull-left">
                <a href="#" class="btn btn-dark btn-custom btn-sm">
                    <i class="fa fa-angle-left"></i> Previous
                </a>
            </li>
            <li class="finish hidden pull-right">
                <button type="submit" class="btn btn-dark btn-custom btn-sm submit-btn">
                    Add New user
                </button>
            </li>
            <li class="next">
                <a href="#" class="btn btn-dark btn-custom btn-sm">
                    Next <i class="fa fa-angle-right"></i>
                </a>
            </li>
        </ul>
    </div>

</form>
</section>
<!-- end: page -->

@endsection


@section('script_links')
    {{--  External Javascript  --}}
    <script src="{{ asset('custom/assets/vendor/select2/select2.js') }}"></script>
    <script src="{{ asset('custom/assets/vendor/bootstrap-multiselect/bootstrap-multiselect.js') }}"></script>
    <script src="{{ asset('custom/assets/vendor/bootstrap-datepicker/js/bootstrap-datepicker.js') }}"></script>

    <script src="{{ asset('custom/assets/vendor/jquery-validation/jquery.validate.js') }}"></script>
    <script src="{{ asset('custom/assets/vendor/bootstrap-wizard/jquery.bootstrap.wizard.js') }}"></script>
    <script src="{{ asset('custom/assets/javascripts/forms/examples.wizard.js') }}"></script>
    <script src="{{ asset('custom/assets/vendor/pnotify/pnotify.custom.js') }}"></script>

    <script src="{{ asset('custom/assets/javascripts/forms/examples.advanced.form.js') }}"></script>
@endsection

@section('scripts')
    {{--  External Javascript  --}}
    <script>
        $( document ).ready(function() {
            $( '#passGen' ).on( "click", function(){
                var text = "";
                var possible = "abcdefghijklmnopqrstuvwxyz!@#$%^&*()-+<>ABCDEFGHIJKLMNOP1234567890";

                for (var i = 0; i <= 12; i++)
                    text += possible.charAt(Math.floor(Math.random() * possible.length));

                var sendpass = document.getElementById("password_generator");
                sendpass.value = text;
            });


            $( '#copyPass' ).on( "click", function(){
                var copyPass = document.getElementById("password_generator");
                copyPass.select();
                document.execCommand("copy");
            });
        });
    </script>
@endsection
