@extends('layouts.director.app')

@section('page_title', '| Company | All Users | Details')

@section('stylesheet_links')
    {{--  External CSS  --}}
    <link rel="stylesheet" href="{{ asset('custom/assets/vendor/select2/select2.css') }}" />
    <link rel="stylesheet" href="{{ asset('custom/assets/vendor/bootstrap-multiselect/bootstrap-multiselect.css') }}" />
    <link rel="stylesheet" href="{{ asset('custom/assets/vendor/select2/select2.css') }}" />
    <link rel="stylesheet" href="{{ asset('custom/assets/vendor/bootstrap-datepicker/css/datepicker3.css') }}" />

@endsection

@section('stylesheet')
    {{--  External CSS  --}}
    <style>
        html .wizard-progress .wizard-steps li.active a span,
        html.dark .wizard-progress .wizard-steps li.active a span {
            color: #dd3333;
            border-color: #dd3333;
        }
        html .wizard-progress .wizard-steps li.completed a span,
        html.dark .wizard-progress .wizard-steps li.completed a span,
        html .wizard-progress .steps-progress .progress-indicator,
        html.dark .wizard-progress .steps-progress .progress-indicator{
            border-color: #dd3333;
            background: #dd3333;
        }
        .pager{
            margin: 0;
        }
        .pager li > a,
        .pager li > span,
        .pager li > a:hover,
        .pager li > span:hover{
            background-color: #02070a !important;
            border: 1px solid #02070a !important;
        }
        .pager .disabled > a,
        .pager .disabled > a:hover,
        .pager .disabled > a:focus,
        .pager .disabled > span{
            background-color: #ffffff !important;
            color: #02070a;
        }
        .btn.btn-dark.btn-custom.btn-block {
            margin: 12px 0px;
            border-radius: 0px;
        }
        .btn-group, .btn-group-vertical{
            display: block;
        }
        button.multiselect.dropdown-toggle.btn.btn-default {
            width: 100%;
            text-align: left;
        }
        .btn .caret{
            float: right;
            margin-top: 7px;
        }
        .open > .dropdown-menu {
            top: 35px;
            width: 100%;
            padding: 5px 0px 10px;
        }
    </style>
@endsection

@section('content')

<header class="page-header">
    <h2>User Details</h2>

    <div class="right-wrapper pull-right">
        <ol class="breadcrumbs">
            <li>
                <a href="{{ Route('director.dashboard.index') }}">
                    <i class="fa fa-home"></i>
                </a>
            </li>
            <li><span class="active">Company</span></li>
            <li>
                <a href="{{ Route('director.dashboard.index') }}">
                    <span class="active">ALl Users</span>
                </a>
            </li>
            <li><span>User Details</span></li>
        </ol>
    </div>
</header>

<!-- start: page -->
<section class="panel form-wizard" id="w3">
    <header class="panel-heading">
        <div class="panel-actions">
            <a href="#" class="fa fa-caret-down"></a>
        </div>

        <h2 class="panel-title">Update User Information</h2>
    </header>

<form class="form-horizontal" novalidate="novalidate" action="{{ route('director.company.user.store', ['id' => $user->id]) }}" method="POST">
    @csrf
    <div class="panel-body">
        <div class="wizard-progress">
            <div class="steps-progress">
                <div class="progress-indicator" style="width: 0%;"></div>
            </div>
            <ul class="wizard-steps">
                <li class="active">
                    <a href="#personal_info" data-toggle="tab"><span>1</span>Personal <br>Information</a>
                </li>
                <li>
                    <a href="#qualification" data-toggle="tab"><span>2</span>Qualification <br>Details</a>
                </li>
                <li>
                    <a href="#professional_bodies" data-toggle="tab"><span>3</span>Professional <br>Bodies</a>
                </li>
                <li>
                    <a href="#authentication" data-toggle="tab"><span>4</span>Authentication <br>Access</a>
                </li>
            </ul>
        </div>
        <div class="tab-content">
            {{-- Personal Info Tab --}}
            <div id="personal_info" class="tab-pane active">
                <div class="form-group">
                    <div class="col-md-4">
                        <label class="control-label">Forname <span class="required">*</span></label>
                        <input type="text" autocomplete="off" name="forname" class="form-control  @error('forname') is-invalid @enderror" placeholder="eg.: Jhon" value="{{ $user->forname }}" required/>

                        {{-- Validation Alert Messages --}}
                        @error('forname')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                    </div>

                    <div class="col-md-4">
                        <label class="control-label">Middle Name</label>
                        <input type="text" autocomplete="off" name="middle_name" class="form-control  @error('middle_name') is-invalid @enderror" placeholder="eg.: Doe" value="{{ $user->middle_name }}"/>

                        {{-- Validation Alert Messages --}}
                        @error('middle_name')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                    </div>

                    <div class="col-md-4">
                        <label class="control-label">Surame <span class="required">*</span></label>
                        <input type="text" autocomplete="off" name="surname" class="form-control  @error('surname') is-invalid @enderror" placeholder="eg.: Junior" value="{{ $user->surname }}"/>

                        {{-- Validation Alert Messages --}}
                        @error('surname')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                    </div>

                    <div class="col-md-4">
                        <label class="control-label">Salutation</label>
                        <input type="text" autocomplete="off" name="salutation" class="form-control  @error('salutation') is-invalid @enderror" placeholder="eg.: Salutation" value="{{ $user->saluation }}"/>

                        {{-- Validation Alert Messages --}}
                        @error('salutation')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                    </div>

                    <div class="col-md-4">
                        <label class="control-label">Date Of Birth</label>
                        <input type="text" autocomplete="off" data-plugin-datepicker="" name="birthdate" class="form-control  @error('birthdate') is-invalid @enderror" placeholder="eg.: dd/mm/yyyy" value="{{ App\Http\Controllers\General::reverseDateFormatDashToSlash($user->date_of_birth) }}">

                        {{-- Validation Alert Messages --}}
                        @error('birthdate')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                    </div>

                    <div class="col-md-4">
                        <label class="control-label">Email <span class="required">*</span></label>
                        <input type="email" name="email_address" class="form-control  @error('email_address') is-invalid @enderror" placeholder="eg.: user_mail@mail.com" value="{{ $user->email_address }}" required/>

                        {{-- Validation Alert Messages --}}
                        @error('email_address')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                    </div>

                    <div class="col-md-4">
                        <label class="control-label">Phone Number</label>
                        <input type="text" autocomplete="off" name="phone_number" class="form-control  @error('phone_number') is-invalid @enderror" placeholder="eg.: Phone Number" value="{{ $user->phone_number }}"/>

                        {{-- Validation Alert Messages --}}
                        @error('phone_number')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                    </div>

                    <div class="col-md-4">
                        <label class="control-label">Mobile Number <span class="required">*</span></label>
                        <input type="text" autocomplete="off" name="mobile_number" class="form-control  @error('mobile_number') is-invalid @enderror" required placeholder="eg.: Mobile Number" value="{{ $user->mobile_number }}"/>

                        {{-- Validation Alert Messages --}}
                        @error('mobile_number')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                    </div>

                    <div class="col-md-4">
                        <label class="control-label">Fax Number</label>
                        <input type="text" autocomplete="off" name="fax_number" class="form-control  @error('fax_number') is-invalid @enderror" placeholder="eg.: Fax Number" value="{{ $user->fax_number }}"/>

                        {{-- Validation Alert Messages --}}
                        @error('fax_number')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                    </div>

                    <div class="col-md-4">
                        <label class="control-label">NI Number</label>
                        <input type="text" autocomplete="off" name="ni_number" class="form-control  @error('ni_number') is-invalid @enderror" placeholder="eg.: NI Number" value="{{ $user->ni_number }}"/>

                        {{-- Validation Alert Messages --}}
                        @error('ni_number')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                    </div>

                    <div class="col-md-4">
                        <label class="control-label">Pre-Sale Type</label>
                        <select name="pre_sale_type" data-plugin-selectTwo class="form-control populate  @error('pre_sale_type') is-invalid @enderror">
                            <option disabled @if($user->saluation == NULL) selected @endif>Select Pre-Sale Type</option>
                            <option @if($user->saluation == 'First 3 Months') selected @endif value="First 3 Months">First 3 Months</option>
                            <option @if($user->saluation == 'First 10 Cases') selected @endif value="First 10 Cases">First 10 Cases</option>
                            <option @if($user->saluation == '3 of Each Type') selected @endif value="3 of Each Type">3 of Each Type</option>
                        </select>

                        {{-- Validation Alert Messages --}}
                        @error('pre_sale_type')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                    </div>

                    <div class="col-md-4">
                        <label class="control-label">FCA Number</label>
                        <input type="text" autocomplete="off" name="fca_number" class="form-control  @error('fca_number') is-invalid @enderror" placeholder="eg.: FCA Number" value="{{ $user->ni_number }}" value="{{ $user->fca_number }}"/>

                        {{-- Validation Alert Messages --}}
                        @error('fca_number')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                    </div>

                    <div class="col-md-4">
                        <label class="control-label">County Old</label>
                        <input type="text" autocomplete="off" name="county_old" class="form-control  @error('county_old') is-invalid @enderror" placeholder="eg.: County Old" value="{{ $user->county_old }}"/>

                        {{-- Validation Alert Messages --}}
                        @error('county_old')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                    </div>

                    <div class="col-md-4">
                        <label class="control-label">Town / City</label>
                        <input type="text" autocomplete="off" name="town_city" class="form-control  @error('town_city') is-invalid @enderror" placeholder="eg.: Town / City" value="{{ $user->town_city }}"/>

                        {{-- Validation Alert Messages --}}
                        @error('town_city')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                    </div>

                    <div class="col-md-4">
                        <label class="control-label">Post Code</label>
                        <input type="text" autocomplete="off" name="post_code" class="form-control  @error('post_code') is-invalid @enderror" placeholder="eg.: Post Code" value="{{ $user->post_code }}"/>

                        {{-- Validation Alert Messages --}}
                        @error('post_code')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                    </div>

                    <div class="col-md-12">
                        <label class="control-label">Address</label>
                        <textarea rows="3" name="address" class="form-control  @error('address') is-invalid @enderror" placeholder="eg.: Address">{{ $user->address }}</textarea>

                        {{-- Validation Alert Messages --}}
                        @error('address')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                    </div>

                </div>
            </div>

            {{-- Qualification Tab --}}
            <div id="qualification" class="tab-pane">
                <div class="form-group">
                    <div class="col-md-12">
                        <label class="control-label">Job Title</label>
                        <input type="text" autocomplete="off" name="job_title" class="form-control  @error('job_title') is-invalid @enderror" placeholder="eg.: Job Title" value="{{ $user->job_title }}"/>

                        {{-- Validation Alert Messages --}}
                        @error('job_title')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                    </div>

                    {{-- array search custom function --}}
                    @php
                        function customSearch($array, $value){
                            for($i =0; $i< sizeof($array); $i++){
                                if($array[$i] == $value) return true;
                            }
                            return false;
                        }
                    @endphp

                    <div class="col-md-4">
                        <label class="control-label">Special</label>
                        @php
                            $special = array();
                            $special = (array)json_decode($user->specials, true);
                        @endphp
                        <select class="form-control  @error('special') is-invalid @enderror" multiple="multiple" data-plugin-multiselect data-plugin-options='{ "includeSelectAllOption": false }' name="special[]">
                            <option value="Level 4 Attained" @if(customSearch($special,'Level 4 Attained')) selected @endif>Level 4 Attained</option>
                            <option value="RDR Gap Filling" @if(customSearch($special, 'RDR Gap Filling')) selected @endif>RDR Gap Filling</option>
                        </select>

                        {{-- Validation Alert Messages --}}
                        @error('special')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                    </div>

                    <div class="col-md-4">
                        @php
                            $basic = array();
                            $basic = (array)json_decode($user->basics, true);
                        @endphp
                        <label class="control-label">Basic</label>
                        <select class="form-control  @error('basic') is-invalid @enderror" multiple="multiple" data-plugin-multiselect data-plugin-options='{ "includeSelectAllOption": false }' name="basic[]">
                            <option @if(customSearch($basic, 'FPC')) selected @endif value="FPC">FPC</option>
                            <option @if(customSearch($basic, 'CeFA 1-4')) selected @endif value="CeFA 1-4">CeFA 1-4</option>
                            <option @if(customSearch($basic, 'CF2')) selected @endif value="CF2">CF2</option>
                            <option @if(customSearch($basic, 'CF 1-5')) selected @endif value="CF 1-5">CF 1-5</option>
                        </select>

                        {{-- Validation Alert Messages --}}
                        @error('basic')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                    </div>

                    <div class="col-md-4">
                        <label class="control-label">Mortgage</label>
                        @php
                            $mortgage = array();
                            $mortgage = (array)json_decode($user->mortgages, true);
                        @endphp
                        <select class="form-control  @error('mortgage') is-invalid @enderror" multiple="multiple" data-plugin-multiselect data-plugin-options='{ "includeSelectAllOption": false }' name="mortgage[]">
                            <option @if(customSearch($mortgage, 'CeMAP 1-3')) selected @endif value="CeMAP 1-3">CeMAP 1-3</option>
                            <option @if(customSearch($mortgage, 'MAQ')) selected @endif value="MAQ">MAQ</option>
                            <option @if(customSearch($mortgage, 'CeMAP Bridge')) selected @endif value="CeMAP Bridge">CeMAP Bridge</option>
                            <option @if(customSearch($mortgage, 'IFS DipMap')) selected @endif value="IFS DipMap">IFS DipMap</option>
                            <option @if(customSearch($mortgage, 'CF6')) selected @endif value="CF6">CF6</option>
                        </select>

                        {{-- Validation Alert Messages --}}
                        @error('mortgage')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                    </div>

                    <div class="col-md-4">
                        <label class="control-label">Lifetime</label>
                        @php
                            $lifetime = array();
                            $lifetime = (array)json_decode($user->lifetimes, true);
                        @endphp
                        <select class="form-control  @error('lifetime') is-invalid @enderror" multiple="multiple" data-plugin-multiselect data-plugin-options='{ "includeSelectAllOption": false }' name="lifetime[]">
                            <option @if(customSearch($lifetime, 'CF7')) selected @endif value="CF7">CF7</option>
                            <option @if(customSearch($lifetime, 'ER1')) selected @endif value="ER1">ER1</option>
                            <option @if(customSearch($lifetime, 'HR1')) selected @endif value="HR1">HR1</option>
                            <option @if(customSearch($lifetime, 'CeRER')) selected @endif value="CeRER">CeRER</option>
                        </select>

                        {{-- Validation Alert Messages --}}
                        @error('lifetime')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                    </div>

                    <div class="col-md-4">
                        <label class="control-label">Other</label>
                        @php
                            $other = array();
                            $other = (array)json_decode($user->others, true);
                        @endphp
                        <select class="form-control  @error('other') is-invalid @enderror" multiple="multiple" data-plugin-multiselect data-plugin-options='{ "includeSelectAllOption": false }' name="other[]">
                            <option @if(customSearch($other, 'CF8')) selected @endif value="CF8">CF8</option>
                            <option @if(customSearch($other, 'CF9')) selected @endif value="CF9">CF9</option>
                            <option @if(customSearch($other, 'CeRCC')) selected @endif value="CeRCC">CeRCC</option>
                            <option @if(customSearch($other, 'SV1')) selected @endif value="SV1">SV1</option>
                            <option @if(customSearch($other, 'G80')) selected @endif value="G80">G80</option>
                            <option @if(customSearch($other, 'IMC')) selected @endif value="IMC">IMC</option>
                        </select>

                        {{-- Validation Alert Messages --}}
                        @error('other')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                    </div>

                    <div class="col-md-4">
                        <label class="control-label">Heigher Level</label>
                        @php
                            $heigher_level = array();
                            $heigher_level = (array)json_decode($user->heigher_levels, true);
                        @endphp
                        <select class="form-control  @error('heigher_level') is-invalid @enderror" multiple="multiple" data-plugin-multiselect data-plugin-options='{ "includeSelectAllOption": false }' name="heigher_level[]">
                            <option @if(customSearch($heigher_level, 'J01')) selected @endif value="J01">J01</option>
                            <option @if(customSearch($heigher_level, 'J02')) selected @endif value="J02">J02</option>
                            <option @if(customSearch($heigher_level, 'J03')) selected @endif value="J03">J03</option>
                            <option @if(customSearch($heigher_level, 'J04')) selected @endif value="J04">J04</option>
                            <option @if(customSearch($heigher_level, 'J05')) selected @endif value="J05">J05</option>
                            <option @if(customSearch($heigher_level, 'J06')) selected @endif value="J06">J06</option>
                            <option @if(customSearch($heigher_level, 'J07')) selected @endif value="J07">J07</option>
                            <option @if(customSearch($heigher_level, 'J08')) selected @endif value="J08">J08</option>
                            <option @if(customSearch($heigher_level, 'J09')) selected @endif value="J09">J09</option>
                            <option @if(customSearch($heigher_level, 'J10')) selected @endif value="J10">J10</option>
                        </select>

                        {{-- Validation Alert Messages --}}
                        @error('heigher_level')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                    </div>
                </div>
            </div>

            {{-- Professional Bodies Tab --}}
            <div id="professional_bodies" class="tab-pane">
                @php
                    $data = json_decode($user->professionals, true);
                    // dd($data);
                @endphp
                <div class="form-group">
                    <div class="col-md-3">
                        <label class="control-label">100% Pre-Sale</label>
                        <div class="checkbox-custom checkbox-default">
                            <input type="checkbox" class="form-control  @error('pre_sale_100_percent') is-invalid @enderror" name="pre_sale_100_percent" @if(isset($data['pre_sale_100_percent']) && $data['pre_sale_100_percent'] == 'true') checked="true" @endif>
                            <label><strong>Yes</strong></label>
                        </div>

                        {{-- Validation Alert Messages --}}
                        @error('pre_sale_100_percent')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                    </div>

                    <div class="col-md-3">
                        <label class="control-label">Exclude from Auto Filecheck</label>
                        <div class="checkbox-custom checkbox-default">
                            <input type="checkbox" class="form-control  @error('exclude_from_auto_filecheck') is-invalid @enderror" name="exclude_from_auto_filecheck" @if( isset($data['exclude_from_auto_filecheck']) && $data['exclude_from_auto_filecheck'] == 'true') checked="true" @endif>
                            <label><strong>Yes</strong></label>
                        </div>

                        {{-- Validation Alert Messages --}}
                        @error('exclude_from_auto_filecheck')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                    </div>

                    <div class="col-md-3">
                        <label class="control-label">Exclude from Commission Check</label>
                        <div class="checkbox-custom checkbox-default">
                            <input type="checkbox" class="form-control  @error('exclude_from_commision_check') is-invalid @enderror" name="exclude_from_commision_check" @if(isset($data['exclude_from_commision_check']) && $data['exclude_from_commision_check'] == 'true') checked="true" @endif>
                            <label><strong>Yes</strong></label>
                        </div>

                        {{-- Validation Alert Messages --}}
                        @error('exclude_from_commision_check')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                    </div>

                    <div class="col-md-3">
                        <label class="control-label">Exclude from the advisers list (Policies and Clients)</label>
                        <div class="checkbox-custom checkbox-default">
                            <input type="checkbox" class="form-control  @error('exclude_from_adviser_list') is-invalid @enderror" name="exclude_from_adviser_list" @if(isset($data['exclude_from_adviser_list']) && $data['exclude_from_adviser_list'] == 'true') checked="true" @endif>
                            <label><strong>Yes</strong></label>
                        </div>

                        {{-- Validation Alert Messages --}}
                        @error('exclude_from_adviser_list')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                    </div>
                    <div class="col-md-12">
                        <hr>
                    </div>
                    <div class="col-md-3">
                        <label class="control-label">PFS (CII) - Personal Finance Society</label>
                        <div class="checkbox-custom checkbox-default">
                            <input type="checkbox" class="form-control  @error('personal_finance') is-invalid @enderror" name="personal_finance" @if(isset($data['personal_finance']) && $data['personal_finance'] == 'true') checked="true" @endif>
                            <label><strong>Yes</strong></label>
                        </div>

                        {{-- Validation Alert Messages --}}
                        @error('personal_finance')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                    </div>

                    <div class="col-md-3">
                        <label class="control-label">IFP - Institute of Financial Planners / CISI</label>
                        <div class="checkbox-custom checkbox-default">
                            <input type="checkbox" class="form-control  @error('institute_of_financial_planners') is-invalid @enderror" name="institute_of_financial_planners" @if(isset($data['institute_of_financial_planners']) && $data['institute_of_financial_planners'] == 'true') checked="true" @endif>
                            <label><strong>Yes</strong></label>
                        </div>

                        {{-- Validation Alert Messages --}}
                        @error('institute_of_financial_planners')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                    </div>

                    <div class="col-md-2">
                        <label class="control-label">IFS School of Finance / LIBF</label>
                        <div class="checkbox-custom checkbox-default">
                            <input type="checkbox" class="form-control  @error('school_of_finance') is-invalid @enderror" name="school_of_finance" @if(isset($data['school_of_finance']) && $data['school_of_finance'] == 'true') checked="true" @endif>
                            <label><strong>Yes</strong></label>
                        </div>

                        {{-- Validation Alert Messages --}}
                        @error('school_of_finance')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                    </div>

                    <div class="col-md-2">
                        <label class="control-label">Institute of Actuaries</label>
                        <div class="checkbox-custom checkbox-default">
                            <input type="checkbox" class="form-control  @error('institute_of_actuaries') is-invalid @enderror" name="institute_of_actuaries" @if(isset($data['institute_of_actuaries']) && $data['institute_of_actuaries'] == 'true') checked="true" @endif>
                            <label><strong>Yes</strong></label>
                        </div>

                        {{-- Validation Alert Messages --}}
                        @error('institute_of_actuaries')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                    </div>

                    <div class="col-md-2">
                        <label class="control-label">Institute of Bankers</label>
                        <div class="checkbox-custom checkbox-default">
                            <input type="checkbox" class="form-control  @error('institute_of_bankers') is-invalid @enderror" name="institute_of_bankers" @if(isset($data['institute_of_bankers']) && $data['institute_of_bankers'] == 'true') checked="true" @endif>
                            <label><strong>Yes</strong></label>
                        </div>

                        {{-- Validation Alert Messages --}}
                        @error('institute_of_bankers')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                    </div>
                </div>
            </div>

            {{-- Authentication Tab --}}
            <div id="authentication" class="tab-pane">
                <div class="col-md-4 col-centered">
                    <div class="panel">
                        <div class="panel-body">
                            <div class="form-group">
                                <div class="col-md-12">
                                    <label class="control-label">Job Role <span class="required">*</span></label>
                                    <select name="job_role" data-plugin-selectTwo class="form-control populate  @error('job_role') is-invalid @enderror" required>
                                        <option @if($user->role_id == 1) selected @endif value="1">Super Admin</option>
                                        <option @if($user->role_id == 3) selected @endif value="3">Director</option>
                                    </select>

                                    {{-- Validation Alert Messages --}}
                                    @error('job_role')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>

                                <div class="col-md-12">
                                    <label class="control-label">User Status <span class="required">*</span></label>
                                    <select name="user_status" data-plugin-selectTwo class="form-control populate  @error('user_status') is-invalid @enderror" required>
                                        <option @if($user->user_status == 1) selected @endif value="1">Active</option>
                                        <option @if($user->user_status == 0) selected @endif value="0">Inactive</option>
                                    </select>

                                    {{-- Validation Alert Messages --}}
                                    @error('user_status')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>

                                <div class="col-md-12">
                                    <br>
                                    <div class="checkbox-custom checkbox-default">
                                        <input type="checkbox" class="@error('access_status') is-invalid @enderror" name="send_access_info" id="sendMail">
                                        <label for="sendMail"><b>Send Mail To client_name's Email With All Access Information.</b></label>
                                    </div>

                                    {{-- Validation Alert Messages --}}
                                    @error('access_status')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="panel-footer">
        <ul class="pager">
            <li class="previous disabled pull-left">
                <a href="#" class="btn btn-dark btn-custom btn-sm">
                    <i class="fa fa-angle-left"></i> Previous
                </a>
            </li>
            <li class="finish hidden pull-right">
                <button type="submit" class="btn btn-dark btn-custom btn-sm submit-btn">
                    Update Info
                </button>
            </li>
            <li class="next">
                <a href="#" class="btn btn-dark btn-custom btn-sm">
                    Next <i class="fa fa-angle-right"></i>
                </a>
            </li>
        </ul>
    </div>

</form>
</section>
<!-- end: page -->

@endsection


@section('script_links')
    {{--  External Javascript  --}}
    <script src="{{ asset('custom/assets/vendor/select2/select2.js') }}"></script>
    <script src="{{ asset('custom/assets/vendor/bootstrap-multiselect/bootstrap-multiselect.js') }}"></script>
    <script src="{{ asset('custom/assets/vendor/bootstrap-datepicker/js/bootstrap-datepicker.js') }}"></script>

    <script src="{{ asset('custom/assets/vendor/jquery-validation/jquery.validate.js') }}"></script>
    <script src="{{ asset('custom/assets/vendor/bootstrap-wizard/jquery.bootstrap.wizard.js') }}"></script>
    <script src="{{ asset('custom/assets/javascripts/forms/examples.wizard.js') }}"></script>
    <script src="{{ asset('custom/assets/vendor/pnotify/pnotify.custom.js') }}"></script>

    <script src="{{ asset('custom/assets/javascripts/forms/examples.advanced.form.js') }}"></script>
@endsection

@section('scripts')
    {{--  External Javascript  --}}
    <script>
        $( document ).ready(function() {
            $( '#passGen' ).on( "click", function(){
                var text = "";
                var possible = "abcdefghijklmnopqrstuvwxyz!@#$%^&*()-+<>ABCDEFGHIJKLMNOP1234567890";

                for (var i = 0; i <= 12; i++)
                    text += possible.charAt(Math.floor(Math.random() * possible.length));

                var sendpass = document.getElementById("password_generator");
                sendpass.value = text;
            });


            $( '#copyPass' ).on( "click", function(){
                var copyPass = document.getElementById("password_generator");
                copyPass.select();
                document.execCommand("copy");
            });
        });
    </script>
@endsection

