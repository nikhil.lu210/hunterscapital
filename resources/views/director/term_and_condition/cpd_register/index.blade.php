@extends('layouts.director.app')

@section('page_title', '| Terms And Conditions | CPD Register')

@section('stylesheet_links')
    {{--  External CSS  --}}
    <link rel="stylesheet" href="{{ asset('custom/assets/vendor/select2/select2.css') }}" />
    <link rel="stylesheet" href="{{ asset('custom/assets/vendor/jquery-datatables-bs3/assets/css/datatables.css') }}" />

    <link rel="stylesheet" href="{{ asset('custom/assets/vendor/bootstrap-datepicker/css/datepicker3.css') }}" />

    <link rel="stylesheet" href="{{ asset('custom/assets/vendor/bootstrap-fileupload/bootstrap-fileupload.min.css') }}" />

@endsection

@section('stylesheet')
    {{--  External CSS  --}}
    <style>
        .dataTables_wrapper .DTTT.btn-group {
            display: none !important;
            position: absolute;
            top: -74px;
            right: 0px;
        }

        .member-active{
            background-color: green;
            color: #ffffff;
            padding: 2px 7px;
            border-radius: 3px;
            font-weight: 600;
        }
        .member-inactive{
            background-color: red;
            color: #ffffff;
            padding: 2px 7px;
            border-radius: 3px;
            font-weight: 600;
        }

        .silver{
            background-color: silver;
            color: #000000;
            padding: 2px 7px;
            border-radius: 3px;
            font-weight: 600;
        }
        .bronze{
            background-color: #ab4700;
            color: #ffffff;
            padding: 2px 7px;
            border-radius: 3px;
            font-weight: 600;
        }
        .gold{
            background-color: #ff7c1f;
            color: #ffffff;
            padding: 2px 7px;
            border-radius: 3px;
            font-weight: 600;
        }
        .platinum{
            background-color: #a0bfb4;
            color: #000000;
            padding: 2px 7px;
            border-radius: 3px;
            font-weight: 600;
        }

        .access-granted{
            background-color: green;
            color: #ffffff;
            padding: 2px 7px;
            border-radius: 3px;
            font-weight: 600;
        }
        .access-not-granted{
            background-color: red;
            color: #ffffff;
            padding: 2px 7px;
            border-radius: 3px;
            font-weight: 600;
        }
        .modal-header .close {
            outline: none !important;
            border: 1px solid #8e8e8e;
            padding: 0px 5px;
            border-radius: 0;
            opacity: 1;
            color: #8e8e8e;
            transition: 0.3s all ease-in-out;
        }
        .modal-header .close:hover {
            outline: none !important;
            border: 1px solid #0f1b25;
            background-color: #0f1b25;
            opacity: 1;
            color: #ffffff;
            transition: 0.3s all ease-in-out;
        }
        .modal-title{
            color: #0f1b25;
        }
        .modal-content {
            border: 1px solid #e5e5e5;
            border-radius: 0;
            box-shadow: 0 3px 9px rgba(0, 0, 0, 0.1);
        }
        .btn.btn-dark.btn-custom.btn-block {
            margin: 12px 0px;
            border-radius: 0px;
        }
        @media (min-width: 768px){
            .modal-dialog {
                width: 500px;
                margin: 11% auto;
            }
        }
        .fileupload .uneditable-input .fa {
            top: 38px;
            left: 25px;
        }
    </style>
@endsection

@section('content')

<header class="page-header">
    <h2>CPD Register</h2>

    <div class="right-wrapper pull-right">
        <ol class="breadcrumbs">
            <li>
                <a href="{{ route('director.dashboard.index') }}">
                    <i class="fa fa-home"></i>
                </a>
            </li>
            <li><span class="active">Terms And Conditions</span></li>
            <li><span>CPD Register</span></li>
        </ol>
    </div>
</header>

<!-- start: page -->
<section class="panel">
    <header class="panel-heading">
        <h2 class="panel-title">CPD Register</h2>
        <button class="btn btn-dark btn-custom btn-sm header-btn" data-toggle="modal" data-target="#newCPD">Add New CPD</button>
    </header>
    <div class="panel-body">
        <table class="table table-bordered table-striped mb-none" id="datatable-tabletools" data-swf-path="assets/vendor/jquery-datatables/extras/TableTools/swf/copy_csv_xls_pdf.swf">
            <thead>
                <tr class="table-header-row">
                    <th class="text-center">Sl.</th>
                    <th class="text-center">User</th>
                    <th class="text-center">CPD Topic</th>
                    <th class="text-center">CPD Activity</th>
                    <th class="text-center">Planned Completion Date</th>
                    <th class="text-center">File Download</th>
                    <th class="text-center">File Upload</th>
                    <th class="text-center">Actions</th>
                </tr>
            </thead>
            <tbody>
                @foreach ($cpds as $sl=>$cpd)
                <tr class="table-body-row">
                    <th class="text-center">{{ $sl+1 }}</th>
                    <th class="text-center">{{ $cpd->userDetails->saluation.' '.$cpd->userDetails->forname.' '.$cpd->userDetails->middle_name.' '.$cpd->userDetails->surname }}</th>
                    <th class="text-center">{{ $cpd->cpd_topic }}</th>
                    <th class="text-center">{{ $cpd->cpd_activity_type }}</th>

                    @php
                        $a = new DateTime($cpd->planned_completion_date);
                    @endphp
                    <th class="text-center">{{ $a->format('d M Y') }}</th>

                    <th class="text-center">
                        <button type="button" class="btn btn-dark btn-sm btn-sm-custom btn-custom-info" data-toggle="modal" data-target="#document_download_modal" data-todo="{{ $cpd->files }}" data-cpd_id="{{ $cpd->id }}"><i class="fa fa-cloud-download"></i></button>
                    </th>

                    <th class="text-center">
                        <button type="button" class="btn btn-dark btn-sm btn-sm-custom btn-custom-info" data-toggle="modal" data-target="#document_upload_modal" data-cpd_id="{{ $cpd->id }}"><i class="fa fa-cloud-upload"></i></button>
                    </th>

                    <td class="action-td text-center">
                        <a href="{{ route('director.term_and_condition.cpd_register.destroy', ['id' => $cpd->id ]) }}" class="btn btn-danger btn-custom btn-sm btn-sm-custom btn-custom-danger btn-delete" onclick="return confirm('Are you sure want to delete?');">
                            <i class="fa fa-trash"></i>
                        </a>

                        <button class="btn btn-dark btn-sm btn-sm-custom btn-custom-info" data-toggle="modal" data-target="#editCPD" data-information="{{ $cpd }}">
                            <i class="fa fa-pen"></i>
                        </button>
                    </td>
                </tr>
                @endforeach
            </tbody>
        </table>
    </div>
</section>
<!-- end: page -->




{{-- ========================< CPD Create Modal Starts >======================= --}}
<div class="modal fade" id="newCPD" tabindex="-1" role="dialog" aria-labelledby="gridSystemModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="gridSystemModalLabel"><b>Add New CPD</b></h4>
            </div>
            <form action="{{ route('director.term_and_condition.cpd_register.store') }}" method="POST">
                @csrf
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-12">
                            <label class="control-label">User <span class="required">*</span></label>
                            <select name="user" data-plugin-selectTwo class="form-control populate  @error('user') is-invalid @enderror" required>
                                <option disabled selected>Select User</option>
                                @foreach ($users as $user)
                                    <option value="{{ $user->id }}">{{ $user->saluation.' '.$user->forname.' '.$user->middle_name.' '.$user->surname }}</option>
                                @endforeach
                            </select>

                            {{-- Validation Alert Messages --}}
                            @error('user')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>

                        <div class="col-md-12">
                            <label class="control-label">Develoment Set By</label>
                            <select name="dev_set_by" data-plugin-selectTwo class="form-control populate  @error('dev_set_by') is-invalid @enderror">
                                <option disabled selected>Select Develoment Set By</option>
                                <optgroup label="AR Firm">
                                    @foreach ($users as $user)
                                        <option value="user-{{ $user->id }}">{{ $user->saluation.' '.$user->forname.' '.$user->middle_name.' '.$user->surname }}</option>
                                    @endforeach
                                </optgroup>
                                <optgroup label="Network">
                                    @foreach ($authors as $author)
                                        <option value="author-{{ $author->id }}">{{ $author->authors_name }}</option>
                                    @endforeach
                                </optgroup>
                            </select>

                            {{-- Validation Alert Messages --}}
                            @error('dev_set_by')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>

                        <div class="col-md-12">
                            <label class="control-label">CPD Topic <span class="required">*</span></label>
                            <select name="cpd_topic" data-plugin-selectTwo class="form-control populate  @error('cpd_topic') is-invalid @enderror" required>
                                <option disabled selected>Select CPD Topic</option>
                                <option value="Investment">Investment</option>
                                <option value="Pension">Pension</option>
                                <option value="Mortgage">Mortgage</option>
                                <option value="Protection">Protection</option>
                                <option value="Regulation">Regulation</option>
                                <option value="Taxation">Taxation</option>
                                <option value="License Investment">License Investment</option>
                                <option value="Core Subjects">Core Subjects</option>
                                <option value="Licence Pensions">Licence Pensions</option>
                                <option value="Specialist Advice">Specialist Advice</option>
                                <option value="Licence Mortgages">Licence Mortgages</option>
                                <option value="Other">Other</option>
                            </select>

                            {{-- Validation Alert Messages --}}
                            @error('cpd_topic')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>

                        <div class="col-md-12">
                            <label class="control-label">CPD Activity Type <span class="required">*</span></label>
                            <select name="cpd_activity_type" data-plugin-selectTwo class="form-control populate  @error('cpd_activity_type') is-invalid @enderror" required>
                                <option disabled selected>Select CPD Activity Type</option>
                                <option value="Seminar or Course">Seminar or Course</option>
                                <option value="Meeting">Meeting</option>
                                <option value="One to One Action">One to One Action</option>
                                <option value="Self Study">Self Study</option>
                                <option value="Compliance Visit">Compliance Visit</option>
                                <option value="Research">Research</option>
                                <option value="Computer Based Training">Computer Based Training</option>
                                <option value="File Check Learning">File Check Learning</option>
                                <option value="Reading of Financial Member Update">Reading of Financial Member Update</option>
                                <option value="Other">Other</option>
                            </select>

                            {{-- Validation Alert Messages --}}
                            @error('cpd_activity_type')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>

                        <div class="col-md-12">
                            <div class="form-group">
                                <label class="control-label"><b>Development Need</b> <span class="required">*</span></label>
                                <textarea name="development_needed" rows="2" class="form-control  @error('development_needed') is-invalid @enderror" placeholder="eg.: Development Need Details Here" required></textarea>

                                {{-- Validation Alert Messages --}}
                                @error('development_needed')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="col-md-12">
                            <label class="control-label">Planned Completion Date <span class="required">*</span></label>
                            <input type="text" autocomplete="off" data-plugin-datepicker="" name="planned_completion_date" class="form-control  @error('planned_completion_date') is-invalid @enderror" placeholder="eg.: dd/mm/yyyy" autocomplete="off" required>

                            {{-- Validation Alert Messages --}}
                            @error('planned_completion_date')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>

                        <div class="col-md-12">
                            <div class="form-group">
                                <label class="control-label"><b>CPD Activity Completed</b> <span class="required">*</span></label>
                                <textarea name="cpd_activity_completed" rows="2" class="form-control  @error('cpd_activity_completed') is-invalid @enderror" placeholder="eg.: CPD Activity Completed Details Here" required></textarea>

                                {{-- Validation Alert Messages --}}
                                @error('cpd_activity_completed')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="col-md-12">
                            <label class="control-label">Completed Date</label>
                            <input type="text" autocomplete="off" data-plugin-datepicker="" name="completed_date" class="form-control  @error('completed_date') is-invalid @enderror" placeholder="eg.: dd/mm/yyyy" autocomplete="off">

                            {{-- Validation Alert Messages --}}
                            @error('completed_date')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>

                        <div class="col-md-12">
                            <div class="form-group">
                                <label class="control-label"><b>Notes (Reflective Statement)</b> <span class="required">*</span></label>
                                <textarea name="note_reflective_statement" rows="2" class="form-control  @error('note_reflective_statement') is-invalid @enderror" placeholder="eg.: Notes (Reflective Statement) Details Here" required></textarea>

                                {{-- Validation Alert Messages --}}
                                @error('note_reflective_statement')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="col-md-6">
                            <label class="control-label">Structured Hours <span class="required">*</span></label>
                            <input type="number" autocomplete="off" name="structured_hour" class="form-control  @error('structured_hour') is-invalid @enderror" placeholder="eg.: 1.30" autocomplete="off" required>

                            {{-- Validation Alert Messages --}}
                            @error('structured_hour')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>

                        <div class="col-md-6">
                            <label class="control-label">Unstructured Hours <span class="required">*</span></label>
                            <input type="number" autocomplete="off" name="unstructured_hour" class="form-control  @error('unstructured_hour') is-invalid @enderror" placeholder="eg.: 1.30" autocomplete="off" required>

                            {{-- Validation Alert Messages --}}
                            @error('unstructured_hour')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                    </div>
                </div>

                <div class="modal-footer text-right">
                    <button type="submit" class="btn btn-dark btn-custom btn-sm">Assign As New task Type</button>
                </div>
            </form>
        </div>
    </div>
</div>
{{-- =========================< CPD Create Modal Ends >======================== --}}




{{-- ========================< CPD Edit Modal Starts >======================= --}}
<div class="modal fade" id="editCPD" tabindex="-1" role="dialog" aria-labelledby="gridSystemModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="gridSystemModalLabel"><b>Update CPD</b></h4>
            </div>
            <form action="{{ route('director.term_and_condition.cpd_register.update') }}" method="POST">
                @csrf
                <div class="modal-body">
                    <div class="row">
                        <input type="hidden" name="hiddenID" id="hiddenID">
                        <div class="col-md-12">
                            <label class="control-label">User <span class="required">*</span></label>
                            <select name="user" id="user" data-plugin-selectTwo class="form-control populate  @error('user') is-invalid @enderror" required>
                                <option disabled selected>Select User</option>
                                @foreach ($users as $user)
                                    <option value="{{ $user->id }}">{{ $user->saluation.' '.$user->forname.' '.$user->middle_name.' '.$user->surname }}</option>
                                @endforeach
                            </select>

                            {{-- Validation Alert Messages --}}
                            @error('user')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>

                        <div class="col-md-12">
                            <label class="control-label">Develoment Set By</label>
                            <select name="dev_set_by" id="dev_set_by" data-plugin-selectTwo class="form-control populate  @error('dev_set_by') is-invalid @enderror">
                                <option disabled selected>Select Develoment Set By</option>
                                <optgroup label="AR Firm">
                                    @foreach ($users as $user)
                                        <option value="user-{{ $user->id }}">{{ $user->saluation.' '.$user->forname.' '.$user->middle_name.' '.$user->surname }}</option>
                                    @endforeach
                                </optgroup>
                                <optgroup label="Network">
                                    @foreach ($authors as $author)
                                        <option value="author-{{ $author->id }}">{{ $author->authors_name }}</option>
                                    @endforeach
                                </optgroup>
                            </select>

                            {{-- Validation Alert Messages --}}
                            @error('dev_set_by')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>

                        <div class="col-md-12">
                            <label class="control-label">CPD Topic <span class="required">*</span></label>
                            <select name="cpd_topic" id="cpd_topic" data-plugin-selectTwo class="form-control populate  @error('cpd_topic') is-invalid @enderror" required>
                                <option disabled selected>Select CPD Topic</option>
                                <option value="Investment">Investment</option>
                                <option value="Pension">Pension</option>
                                <option value="Mortgage">Mortgage</option>
                                <option value="Protection">Protection</option>
                                <option value="Regulation">Regulation</option>
                                <option value="Taxation">Taxation</option>
                                <option value="License Investment">License Investment</option>
                                <option value="Core Subjects">Core Subjects</option>
                                <option value="Licence Pensions">Licence Pensions</option>
                                <option value="Specialist Advice">Specialist Advice</option>
                                <option value="Licence Mortgages">Licence Mortgages</option>
                                <option value="Other">Other</option>
                            </select>

                            {{-- Validation Alert Messages --}}
                            @error('cpd_topic')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>

                        <div class="col-md-12">
                            <label class="control-label">CPD Activity Type <span class="required">*</span></label>
                            <select name="cpd_activity_type" id="modal_cpd_activity_type" data-plugin-selectTwo class="form-control populate  @error('cpd_activity_type') is-invalid @enderror" required>
                                <option disabled>Select CPD Activity Type</option>
                                <option value="Seminar or Course">Seminar or Course</option>
                                <option value="Meeting">Meeting</option>
                                <option value="One to One Action">One to One Action</option>
                                <option value="Self Study">Self Study</option>
                                <option value="Compliance Visit">Compliance Visit</option>
                                <option value="Research">Research</option>
                                <option value="Computer Based Training">Computer Based Training</option>
                                <option value="File Check Learning">File Check Learning</option>
                                <option value="Reading of Financial Member Update">Reading of Financial Member Update</option>
                                <option value="Other">Other</option>
                            </select>

                            {{-- Validation Alert Messages --}}
                            @error('cpd_activity_type')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>

                        <div class="col-md-12">
                            <div class="form-group">
                                <label class="control-label"><b>Development Need</b> <span class="required">*</span></label>
                                <textarea name="development_needed" id="development_needed" rows="2" class="form-control  @error('development_needed') is-invalid @enderror" placeholder="eg.: Development Need Details Here" required></textarea>

                                {{-- Validation Alert Messages --}}
                                @error('development_needed')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="col-md-12">
                            <label class="control-label">Planned Completion Date <span class="required">*</span></label>
                            <input name="planned_completion_date" id="planned_completion_date" type="text" autocomplete="off" data-plugin-datepicker="" class="form-control  @error('planned_completion_date') is-invalid @enderror" placeholder="eg.: dd/mm/yyyy" autocomplete="off" required>

                            {{-- Validation Alert Messages --}}
                            @error('planned_completion_date')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>

                        <div class="col-md-12">
                            <div class="form-group">
                                <label class="control-label"><b>CPD Activity Completed</b> <span class="required">*</span></label>
                                <textarea name="cpd_activity_completed" id="cpd_activity_completed" rows="2" class="form-control  @error('cpd_activity_completed') is-invalid @enderror" placeholder="eg.: CPD Activity Completed Details Here" required></textarea>

                                {{-- Validation Alert Messages --}}
                                @error('cpd_activity_completed')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="col-md-12">
                            <label class="control-label">Completed Date</label>
                            <input name="completed_date" id="completed_date" type="text" autocomplete="off" data-plugin-datepicker="" class="form-control  @error('completed_date') is-invalid @enderror" placeholder="eg.: dd/mm/yyyy" autocomplete="off">

                            {{-- Validation Alert Messages --}}
                            @error('completed_date')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>

                        <div class="col-md-12">
                            <div class="form-group">
                                <label class="control-label"><b>Notes (Reflective Statement)</b> <span class="required">*</span></label>
                                <textarea name="note_reflective_statement" id="note_reflective_statement" rows="2" class="form-control  @error('note_reflective_statement') is-invalid @enderror" placeholder="eg.: Notes (Reflective Statement) Details Here" required></textarea>

                                {{-- Validation Alert Messages --}}
                                @error('note_reflective_statement')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="col-md-6">
                            <label class="control-label">Structured Hours <span class="required">*</span></label>
                            <input name="structured_hour" id="structured_hour" type="number" autocomplete="off" class="form-control  @error('structured_hour') is-invalid @enderror" placeholder="eg.: 1.30" autocomplete="off" required>

                            {{-- Validation Alert Messages --}}
                            @error('structured_hour')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>

                        <div class="col-md-6">
                            <label class="control-label">Unstructured Hours <span class="required">*</span></label>
                            <input name="unstructured_hour" id="unstructured_hour" type="number" autocomplete="off" class="form-control  @error('unstructured_hour') is-invalid @enderror" placeholder="eg.: 1.30" autocomplete="off" required>

                            {{-- Validation Alert Messages --}}
                            @error('unstructured_hour')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                    </div>
                </div>

                <div class="modal-footer text-right">
                    <button type="submit" class="btn btn-dark btn-custom btn-sm">Update CPD</button>
                </div>
            </form>
        </div>
    </div>
</div>
{{-- =========================< CPD Edit Modal Ends >======================== --}}


{{-- ========================< Document Download Modal Starts >======================= --}}
<div class="modal fade" id="document_download_modal" tabindex="-1" role="dialog" aria-labelledby="gridSystemModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="gridSystemModalLabel"><b>Complaint Documents</b></h4>
            </div>
            <div class="modal-body">
                <table class="table table-bordered table-striped mb-none">
                    <thead>
                        <tr class="table-header-row">
                            <th class="text-center">Sl.</th>
                            <th class="text-center">Document Name</th>
                            <th class="text-center">Upload Date</th>
                            <th class="text-center">Actions</th>
                        </tr>
                    </thead>
                    <tbody id="documents_download">
                        <tr class="table-body-row">
                            <th class="text-center" id="document_sl">01</th>
                            <th class="text-center" id="document_name"><b>document_name</b></th>
                            <th class="text-center" id="document_date">01_02_2019</th>

                            <td class="action-td text-center" id="document_action">
                                <a href="#" class="btn btn-danger btn-sm btn-sm-custom btn-custom-danger btn-delete" onclick="return confirm('Are You Sure Want To Delete This Document...?')">
                                    <i class="fa fa-trash" aria-hidden="true"></i>
                                </a>
                                <a href="#" class="btn btn-dark btn-sm btn-sm-custom btn-custom-info"><i class="fa fa-cloud-download"></i></a>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
{{-- =========================< Document Download Modal Ends >======================== --}}




{{-- ========================< Document Upload Create Modal Starts >======================= --}}
<div class="modal fade" id="document_upload_modal" tabindex="-1" role="dialog" aria-labelledby="gridSystemModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="gridSystemModalLabel"><b>Add New Document</b></h4>
            </div>
            <form action="{{ route('director.term_and_condition.cpd_register.upload') }}" method="POST" enctype="multipart/form-data">
                @csrf
                <div class="modal-body">
                    <div class="row">
                        <input type="hidden" name="id" id="cpd_file_upload_id">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label class="control-label">Document <span class="required">*</span></label>
                                <div class="fileupload fileupload-new" data-provides="fileupload">
                                    <div class="input-append">
                                        <div class="uneditable-input">
                                            <i class="fa fa-file fileupload-exists"></i>
                                            <span class="fileupload-preview"></span>
                                        </div>
                                        <span class="btn btn-default btn-file">
                                            <span class="fileupload-exists">Change</span>
                                            <span class="fileupload-new">Select file</span>
                                            <input type="file" name="document" class="@error('document') is-invalid @enderror" required/>
                                        </span>
                                        <a href="#" class="btn btn-default fileupload-exists" data-dismiss="fileupload">Remove</a>
                                    </div>
                                </div>

                                {{-- Validation Alert Messages --}}
                                @error('document')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>
                    </div>
                </div>

                <div class="modal-footer text-right">
                    <button type="submit" class="btn btn-dark btn-custom btn-sm">Upload Now</button>
                </div>
            </form>
        </div>
    </div>
</div>
{{-- =========================< Document Upload Create Modal Ends >======================== --}}
@endsection


@section('script_links')
    {{--  External Javascript  --}}
    <script src="{{ asset('custom/assets/vendor/select2/select2.js') }}"></script>
    <script src="{{ asset('custom/assets/vendor/jquery-datatables/media/js/jquery.dataTables.js') }}"></script>
    <script src="{{ asset('custom/assets/vendor/jquery-datatables/extras/TableTools/js/dataTables.tableTools.min.js') }}"></script>
    <script src="{{ asset('custom/assets/vendor/jquery-datatables-bs3/assets/js/datatables.js') }}"></script>

    <script src="{{ asset('custom/assets/javascripts/tables/examples.datatables.default.js') }}"></script>
    <script src="{{ asset('custom/assets/javascripts/tables/examples.datatables.row.with.details.js') }}"></script>
    <script src="{{ asset('custom/assets/javascripts/tables/examples.datatables.tabletools.js') }}"></script>
    <script src="{{ asset('custom/assets/vendor/bootstrap-datepicker/js/bootstrap-datepicker.js') }}"></script>

    <script src="{{ asset('custom/assets/vendor/bootstrap-fileupload/bootstrap-fileupload.min.js') }}"></script>
@endsection

@section('scripts')
    {{--  External Javascript  --}}
    <script>
        $('#editCPD').on('show.bs.modal', function (event) {
            var button = $(event.relatedTarget); // Button that triggered the modal
            var data = button.data('information');
            // console.log(data);
            var modal = $(this);
            modal.find('.modal-body #hiddenID').val(data.id);

            // User
            modal.find('.modal-body #user option[value=' + data.user_details.id + ']').attr('selected', 'selected');
            var name = modal.find('.modal-body #user option[value=' + data.user_details.id + ']').html();
            modal.find('.modal-body #s2id_user .select2-chosen').html(name);

            // Develoment Set By
            if (data.dev_set_by_user != null) {
                // Develoment Set By User
                modal.find('.modal-body #dev_set_by option[value="' + data.dev_set_by_user.id + '"]').attr('selected', 'selected');
                var name = modal.find('.modal-body #dev_set_by option[value="user-' + data.dev_set_by_user.id + '"]').html();
                modal.find('.modal-body #s2id_dev_set_by .select2-chosen').html(name);
            } else {
                // Develoment Set By Author
                modal.find('.modal-body #dev_set_by option[value="' + data.dev_set_by_author.id + '"]').attr('selected', 'selected');
                var name = modal.find('.modal-body #dev_set_by option[value="author-' + data.author_details.id + '"]').html();
                modal.find('.modal-body #s2id_dev_set_by .select2-chosen').html(name);
            }


            // CPD Topic
            modal.find('.modal-body #cpd_topic option[value="' + data.cpd_topic + '"]').attr('selected', 'selected');
            var name = modal.find('.modal-body #cpd_topic option[value="' + data.cpd_topic + '"]').html();
            modal.find('.modal-body #s2id_cpd_topic .select2-chosen').html(name);


            // Development Need
            modal.find('.modal-body #modal_cpd_activity_type option[value="'+data.cpd_activity_type+'"]');
            var name = modal.find('.modal-body #modal_cpd_activity_type option[value="'+data.cpd_activity_type+'"]').html();
            modal.find('.modal-body #s2id_modal_cpd_activity_type .select2-chosen').html(name);

            // Planned Completion Date
            modal.find('.modal-body #development_needed').html(data.development_needed);

            // Planned Completion Date
            var planned_completion_date = GetFormattedDate(data.planned_completion_date);
            modal.find('.modal-body #planned_completion_date').val(planned_completion_date);

            // CPD Activity Completed
            modal.find('.modal-body #cpd_activity_completed').html(data.cpd_activity_completed);

            // Completed Date
            var completed_date = GetFormattedDate(data.completed_date);
            modal.find('.modal-body #completed_date').val(completed_date);

            // Notes (Reflective Statement)
            modal.find('.modal-body #note_reflective_statement').html(data.note_reflective_statement);

            // Structured Hours
            modal.find('.modal-body #structured_hour').val(data.structured_hour);

            // Unstructured Hours
            modal.find('.modal-body #unstructured_hour').val(data.unstructured_hour);
        });

        function GetFormattedDate(date_format) {
            var format = date_format.split("-");
            // console.log(format);
            return format[1] + "/" + format[2] + "/" + format[0];
        }
    </script>



    <script>
        $('#document_upload_modal').on('show.bs.modal', function (event) {
            var button = $(event.relatedTarget); // Button that triggered the modal
            var cpd_id = button.data('cpd_id');
            // console.log(cpd_id);

            var modal = $(this);

            modal.find('.modal-body #cpd_file_upload_id').val(cpd_id);
        });
    </script>

    <script>
        $('#document_download_modal').on('show.bs.modal', function (event) {
            var button = $(event.relatedTarget); // Button that triggered the modal
            var documents = button.data('todo');
            var cpd_id = button.data('cpd_id');

            var modal = $(this);

            modal.find('.modal-body #documents_download').empty();

            for(var i = documents.length-1, sl=1; i>=0; i--,sl++){

                var urldes = window.location.origin+"/director/term_and_condition/cpd_register/destroy_file/"+ i +"/"+cpd_id;
                var urldown = window.location.origin+"/director/term_and_condition/cpd_register/download/"+ i+"/"+cpd_id;

                var tr = "<tr class='table-body-row'><th class='text-center'>"+ sl +"</th><th class='text-center'><b>"+ documents[i].name +"</b></th><th class='text-center'>"+ documents[i].date +"</th><td class='action-td text-center'><a href='"+urldes+"' onclick='return confirm(\"Are You Sure Want To Delete This File...?\")' class='btn btn-danger btn-sm btn-sm-custom btn-custom-danger btn-delete'><i class='fa fa-trash' aria-hidden='true'></i></a><a href='"+urldown+"' class='btn btn-dark btn-sm btn-sm-custom btn-custom-info'><i class='fa fa-cloud-download'></i></a></td></tr>";

                modal.find('.modal-body #documents_download').append(tr);
            }


        });
    </script>
@endsection
