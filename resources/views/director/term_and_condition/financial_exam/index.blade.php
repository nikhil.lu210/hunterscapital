@extends('layouts.director.app')

@section('page_title', '| Terms And Conditions | Financial Exam')

@section('stylesheet_links')
    {{--  External CSS  --}}
    
@endsection

@section('stylesheet')
    {{--  External CSS  --}}
    <style>

    </style>
@endsection

@section('content')

<header class="page-header">
    <h2>Financial Exam</h2>

    <div class="right-wrapper pull-right">
        <ol class="breadcrumbs">
            <li>
                <a href="{{ route('director.dashboard.index') }}">
                    <i class="fa fa-home"></i>
                </a>
            </li>
            <li><span class="active">Terms And Conditions</span></li>
            <li><span>Financial Exam</span></li>
        </ol>
    </div>
</header>

<!-- start: page -->

<!-- end: page -->
@endsection


@section('script_links')
{{--  External Javascript  --}}

@endsection

@section('scripts')
    {{--  External Javascript  --}}

@endsection