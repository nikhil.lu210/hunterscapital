@extends('layouts.director.app')

@section('page_title', '| Terms And Conditions | SPS')

@section('stylesheet_links')
    {{--  External CSS  --}}
    <link rel="stylesheet" href="{{ asset('custom/assets/vendor/select2/select2.css') }}" />
    <link rel="stylesheet" href="{{ asset('custom/assets/vendor/jquery-datatables-bs3/assets/css/datatables.css') }}" />

    <link rel="stylesheet" href="{{ asset('custom/assets/vendor/bootstrap-datepicker/css/datepicker3.css') }}" />

    <link rel="stylesheet" href="{{ asset('custom/assets/vendor/bootstrap-fileupload/bootstrap-fileupload.min.css') }}" />

@endsection

@section('stylesheet')
    {{--  External CSS  --}}
    <style>
        .dataTables_wrapper .DTTT.btn-group {
            display: none !important;
            position: absolute;
            top: -74px;
            right: 0px;
        }

        .member-active{
            background-color: green;
            color: #ffffff;
            padding: 2px 7px;
            border-radius: 3px;
            font-weight: 600;
        }
        .member-inactive{
            background-color: red;
            color: #ffffff;
            padding: 2px 7px;
            border-radius: 3px;
            font-weight: 600;
        }

        .silver{
            background-color: silver;
            color: #000000;
            padding: 2px 7px;
            border-radius: 3px;
            font-weight: 600;
        }
        .bronze{
            background-color: #ab4700;
            color: #ffffff;
            padding: 2px 7px;
            border-radius: 3px;
            font-weight: 600;
        }
        .gold{
            background-color: #ff7c1f;
            color: #ffffff;
            padding: 2px 7px;
            border-radius: 3px;
            font-weight: 600;
        }
        .platinum{
            background-color: #a0bfb4;
            color: #000000;
            padding: 2px 7px;
            border-radius: 3px;
            font-weight: 600;
        }

        .access-granted{
            background-color: green;
            color: #ffffff;
            padding: 2px 7px;
            border-radius: 3px;
            font-weight: 600;
        }
        .access-not-granted{
            background-color: red;
            color: #ffffff;
            padding: 2px 7px;
            border-radius: 3px;
            font-weight: 600;
        }
        .modal-header .close {
            outline: none !important;
            border: 1px solid #8e8e8e;
            padding: 0px 5px;
            border-radius: 0;
            opacity: 1;
            color: #8e8e8e;
            transition: 0.3s all ease-in-out;
        }
        .modal-header .close:hover {
            outline: none !important;
            border: 1px solid #0f1b25;
            background-color: #0f1b25;
            opacity: 1;
            color: #ffffff;
            transition: 0.3s all ease-in-out;
        }
        .modal-title{
            color: #0f1b25;
        }
        .modal-content {
            border: 1px solid #e5e5e5;
            border-radius: 0;
            box-shadow: 0 3px 9px rgba(0, 0, 0, 0.1);
        }
        .btn.btn-dark.btn-custom.btn-block {
            margin: 12px 0px;
            border-radius: 0px;
        }
        @media (min-width: 768px){
            .modal-dialog {
                width: 500px;
                margin: 11% auto;
            }
        }
        .fileupload .uneditable-input .fa {
            top: 38px;
            left: 25px;
        }
    </style>
@endsection

@section('content')

<header class="page-header">
    <h2>SPS</h2>

    <div class="right-wrapper pull-right">
        <ol class="breadcrumbs">
            <li>
                <a href="{{ route('director.dashboard.index') }}">
                    <i class="fa fa-home"></i>
                </a>
            </li>
            <li><span class="active">Terms And Conditions</span></li>
            <li><span>SPS</span></li>
        </ol>
    </div>
</header>

<!-- start: page -->
<section class="panel">
    <header class="panel-heading">
        <h2 class="panel-title">SPS</h2>
        <button class="btn btn-dark btn-custom btn-sm header-btn" data-toggle="modal" data-target="#newSPS">Add New SPS</button>
    </header>
    <div class="panel-body">
        <table class="table table-bordered table-striped mb-none" id="datatable-tabletools" data-swf-path="assets/vendor/jquery-datatables/extras/TableTools/swf/copy_csv_xls_pdf.swf">
            <thead>
                <tr class="table-header-row">
                    <th class="text-center">Sl.</th>
                    <th class="text-center">Name</th>
                    <th class="text-center">Issued By</th>
                    <th class="text-center">Issued Date</th>
                    <th class="text-center">Expiry Date</th>
                    <th class="text-center">File Download</th>
                    <th class="text-center">File Upload</th>
                    <th class="text-center">Actions</th>
                </tr>
            </thead>
            <tbody>
                @foreach ($spss as $sl=>$sps)
                <tr class="table-body-row">
                    <th class="text-center">{{ $sl+1 }}</th>
                    <th class="text-center">{{ $sps->userDetails->saluation.' '.$sps->userDetails->forname.' '.$sps->userDetails->middle_name.' '.$sps->userDetails->surname }}</th>
                    <th class="text-center">{{ $sps->issued_by }}</th>

                    @php
                        $a = new DateTime($sps->issued_date);
                        $b = new DateTime($sps->expiry_date);
                    @endphp
                    <th class="text-center">{{ $a->format('d M Y') }}</th>
                    <th class="text-center">{{ $b->format('d M Y') }}</th>

                    <th class="text-center">
                        <button type="button" class="btn btn-dark btn-sm btn-sm-custom btn-custom-info" data-toggle="modal" data-target="#document_download_modal" data-todo="{{ $sps->files }}" data-sps_id="{{ $sps->id }}"><i class="fa fa-cloud-download"></i></button>
                    </th>

                    <th class="text-center">
                        <button type="button" class="btn btn-dark btn-sm btn-sm-custom btn-custom-info" data-toggle="modal" data-target="#document_upload_modal" data-sps_id="{{ $sps->id }}"><i class="fa fa-cloud-upload"></i></button>
                    </th>

                    <td class="action-td text-center">
                        <a href="{{ route('director.term_and_condition.sps.destroy', ['id' => $sps->id ]) }}" class="btn btn-danger btn-custom btn-sm btn-sm-custom btn-custom-danger btn-delete" onclick="return confirm('Are you sure want to delete?');">
                            <i class="fa fa-trash"></i>
                        </a>

                        <button class="btn btn-dark btn-sm btn-sm-custom btn-custom-info" data-toggle="modal" data-target="#editSPS" data-information="{{ $sps }}">
                            <i class="fa fa-pen"></i>
                        </button>
                    </td>
                </tr>
                @endforeach
            </tbody>
        </table>
    </div>
</section>
<!-- end: page -->




{{-- ========================< SPS Create Modal Starts >======================= --}}
<div class="modal fade" id="newSPS" tabindex="-1" role="dialog" aria-labelledby="gridSystemModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="gridSystemModalLabel"><b>Add New SPS</b></h4>
            </div>
            <form action="{{ route('director.term_and_condition.sps.store') }}" method="POST">
                @csrf
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-12">
                            <label class="control-label">User <span class="required">*</span></label>
                            <select name="user" data-plugin-selectTwo class="form-control populate  @error('user') is-invalid @enderror" required>
                                <option disabled selected>Select User</option>
                                @foreach ($users as $user)
                                    <option value="{{ $user->id }}">{{ $user->saluation.' '.$user->forname.' '.$user->middle_name.' '.$user->surname }}</option>
                                @endforeach
                            </select>

                            {{-- Validation Alert Messages --}}
                            @error('user')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>

                        <div class="col-md-12">
                            <label class="control-label">Issued By <span class="required">*</span></label>
                            <select name="issued_by" data-plugin-selectTwo class="form-control populate  @error('issued_by') is-invalid @enderror" required>
                                <option disabled selected>Select Issued By</option>
                                <option value="PFS (CII) - Personal Financial Society">PFS (CII) - Personal Financial Society</option>
                                <option value="IFP - Institute of Financial Planners / CISI">IFP - Institute of Financial Planners / CISI</option>
                                <option value="IFS School of Finance / LIBF">IFS School of Finance / LIBF</option>
                                <option value="Institute of Actuaries">Institute of Actuaries</option>
                                <option value="Institute of Bankers">Institute of Bankers</option>
                            </select>

                            {{-- Validation Alert Messages --}}
                            @error('issued_by')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>

                        <div class="col-md-12">
                            <label class="control-label">Date Issued <span class="required">*</span></label>
                            <input type="text" autocomplete="off" data-plugin-datepicker="" name="date_issued" class="form-control  @error('date_issued') is-invalid @enderror" placeholder="eg.: dd/mm/yyyy" autocomplete="off" required>

                            {{-- Validation Alert Messages --}}
                            @error('date_issued')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>

                        <div class="col-md-12">
                            <label class="control-label">Expiry Date <span class="required">*</span></label>
                            <input type="text" autocomplete="off" data-plugin-datepicker="" name="expire_date" class="form-control  @error('expire_date') is-invalid @enderror" placeholder="eg.: dd/mm/yyyy" autocomplete="off" required>

                            {{-- Validation Alert Messages --}}
                            @error('expire_date')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                    </div>
                </div>

                <div class="modal-footer text-right">
                    <button type="submit" class="btn btn-dark btn-custom btn-sm">Create SPS</button>
                </div>
            </form>
        </div>
    </div>
</div>
{{-- =========================< SPS Create Modal Ends >======================== --}}




{{-- ========================< CPD Edit Modal Starts >======================= --}}
<div class="modal fade" id="editSPS" tabindex="-1" role="dialog" aria-labelledby="gridSystemModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="gridSystemModalLabel"><b>Update CPD</b></h4>
            </div>
            <form action="{{ route('director.term_and_condition.sps.update') }}" method="POST">
                @csrf
                <div class="modal-body">
                    <input type="hidden" name="hiddenID" id="hiddenID">
                    <div class="row">
                        <div class="col-md-12">
                            <label class="control-label">User <span class="required">*</span></label>
                            <select name="user" id="user" data-plugin-selectTwo class="form-control populate  @error('user') is-invalid @enderror" required>
                                <option disabled selected>Select User</option>
                                @foreach ($users as $user)
                                    <option value="{{ $user->id }}">{{ $user->saluation.' '.$user->forname.' '.$user->middle_name.' '.$user->surname }}</option>
                                @endforeach
                            </select>

                            {{-- Validation Alert Messages --}}
                            @error('user')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>

                        <div class="col-md-12">
                            <label class="control-label">Issued By <span class="required">*</span></label>
                            <select name="issued_by" id="issued_by" data-plugin-selectTwo class="form-control populate  @error('issued_by') is-invalid @enderror" required>
                                <option disabled selected>Select Issued By</option>
                                <option value="PFS (CII) - Personal Financial Society">PFS (CII) - Personal Financial Society</option>
                                <option value="IFP - Institute of Financial Planners / CISI">IFP - Institute of Financial Planners / CISI</option>
                                <option value="IFS School of Finance / LIBF">IFS School of Finance / LIBF</option>
                                <option value="Institute of Actuaries">Institute of Actuaries</option>
                                <option value="Institute of Bankers">Institute of Bankers</option>
                            </select>

                            {{-- Validation Alert Messages --}}
                            @error('issued_by')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>

                        <div class="col-md-12">
                            <label class="control-label">Date Issued <span class="required">*</span></label>
                            <input name="date_issued" id="date_issued" type="text" autocomplete="off" data-plugin-datepicker="" class="form-control  @error('date_issued') is-invalid @enderror" placeholder="eg.: dd/mm/yyyy" autocomplete="off" required>

                            {{-- Validation Alert Messages --}}
                            @error('date_issued')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>

                        <div class="col-md-12">
                            <label class="control-label">Expiry Date <span class="required">*</span></label>
                            <input name="expire_date" id="expire_date" type="text" autocomplete="off" data-plugin-datepicker="" class="form-control  @error('expire_date') is-invalid @enderror" placeholder="eg.: dd/mm/yyyy" autocomplete="off" required>

                            {{-- Validation Alert Messages --}}
                            @error('expire_date')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                    </div>
                </div>

                <div class="modal-footer text-right">
                    <button type="submit" class="btn btn-dark btn-custom btn-sm">Update SPS</button>
                </div>
            </form>
        </div>
    </div>
</div>
{{-- =========================< CPD Edit Modal Ends >======================== --}}


{{-- ========================< Document Download Modal Starts >======================= --}}
<div class="modal fade" id="document_download_modal" tabindex="-1" role="dialog" aria-labelledby="gridSystemModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="gridSystemModalLabel"><b>Complaint Documents</b></h4>
            </div>
            <div class="modal-body">
                <table class="table table-bordered table-striped mb-none">
                    <thead>
                        <tr class="table-header-row">
                            <th class="text-center">Sl.</th>
                            <th class="text-center">Document Name</th>
                            <th class="text-center">Upload Date</th>
                            <th class="text-center">Actions</th>
                        </tr>
                    </thead>
                    <tbody id="documents_download">
                        <tr class="table-body-row">
                            <th class="text-center" id="document_sl">01</th>
                            <th class="text-center" id="document_name"><b>document_name</b></th>
                            <th class="text-center" id="document_date">01_02_2019</th>

                            <td class="action-td text-center" id="document_action">
                                <a href="#" class="btn btn-danger btn-sm btn-sm-custom btn-custom-danger btn-delete" onclick="return confirm('Are You Sure Want To Delete This Document...?')">
                                    <i class="fa fa-trash" aria-hidden="true"></i>
                                </a>
                                <a href="#" class="btn btn-dark btn-sm btn-sm-custom btn-custom-info"><i class="fa fa-cloud-download"></i></a>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
{{-- =========================< Document Download Modal Ends >======================== --}}




{{-- ========================< Document Upload Create Modal Starts >======================= --}}
<div class="modal fade" id="document_upload_modal" tabindex="-1" role="dialog" aria-labelledby="gridSystemModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="gridSystemModalLabel"><b>Add New Document</b></h4>
            </div>
            <form action="{{ route('director.term_and_condition.sps.upload') }}" method="POST" enctype="multipart/form-data">
                @csrf
                <div class="modal-body">
                    <div class="row">
                        <input type="hidden" name="id" id="cpd_file_upload_id">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label class="control-label">Document <span class="required">*</span></label>
                                <div class="fileupload fileupload-new" data-provides="fileupload">
                                    <div class="input-append">
                                        <div class="uneditable-input">
                                            <i class="fa fa-file fileupload-exists"></i>
                                            <span class="fileupload-preview"></span>
                                        </div>
                                        <span class="btn btn-default btn-file">
                                            <span class="fileupload-exists">Change</span>
                                            <span class="fileupload-new">Select file</span>
                                            <input type="file" name="document" class="@error('document') is-invalid @enderror" required/>
                                        </span>
                                        <a href="#" class="btn btn-default fileupload-exists" data-dismiss="fileupload">Remove</a>
                                    </div>
                                </div>

                                {{-- Validation Alert Messages --}}
                                @error('document')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>
                    </div>
                </div>

                <div class="modal-footer text-right">
                    <button type="submit" class="btn btn-dark btn-custom btn-sm">Upload Now</button>
                </div>
            </form>
        </div>
    </div>
</div>
{{-- =========================< Document Upload Create Modal Ends >======================== --}}
@endsection


@section('script_links')
    {{--  External Javascript  --}}
    <script src="{{ asset('custom/assets/vendor/select2/select2.js') }}"></script>
    <script src="{{ asset('custom/assets/vendor/jquery-datatables/media/js/jquery.dataTables.js') }}"></script>
    <script src="{{ asset('custom/assets/vendor/jquery-datatables/extras/TableTools/js/dataTables.tableTools.min.js') }}"></script>
    <script src="{{ asset('custom/assets/vendor/jquery-datatables-bs3/assets/js/datatables.js') }}"></script>

    <script src="{{ asset('custom/assets/javascripts/tables/examples.datatables.default.js') }}"></script>
    <script src="{{ asset('custom/assets/javascripts/tables/examples.datatables.row.with.details.js') }}"></script>
    <script src="{{ asset('custom/assets/javascripts/tables/examples.datatables.tabletools.js') }}"></script>
    <script src="{{ asset('custom/assets/vendor/bootstrap-datepicker/js/bootstrap-datepicker.js') }}"></script>

    <script src="{{ asset('custom/assets/vendor/bootstrap-fileupload/bootstrap-fileupload.min.js') }}"></script>
@endsection

@section('scripts')
    {{--  External Javascript  --}}
    <script>
        $('#editSPS').on('show.bs.modal', function (event) {
            var button = $(event.relatedTarget); // Button that triggered the modal
            var data = button.data('information');
            console.log(data);
            var modal = $(this);
            modal.find('.modal-body #hiddenID').val(data.id);

            // User
            modal.find('.modal-body #user option[value=' + data.user_details.id + ']').attr('selected', 'selected');
            var name = modal.find('.modal-body #user option[value=' + data.user_details.id + ']').html();
            modal.find('.modal-body #s2id_user .select2-chosen').html(name);


            // Issued By
            modal.find('.modal-body #issued_by option[value="' + data.issued_by + '"]').attr('selected', 'selected');
            var name = modal.find('.modal-body #issued_by option[value="' + data.issued_by + '"]').html();
            modal.find('.modal-body #s2id_issued_by .select2-chosen').html(name);

            // Issued Date
            var date_issued = GetFormattedDate(data.date_issued);
            modal.find('.modal-body #date_issued').val(date_issued);

            // Expire Date
            var expire_date = GetFormattedDate(data.expire_date);
            modal.find('.modal-body #expire_date').val(expire_date);
        });

        function GetFormattedDate(date_format) {
            var format = date_format.split("-");
            // console.log(format);
            return format[1] + "/" + format[2] + "/" + format[0];
        }
    </script>



    <script>
        $('#document_upload_modal').on('show.bs.modal', function (event) {
            var button = $(event.relatedTarget); // Button that triggered the modal
            var sps_id = button.data('sps_id');
            // console.log(sps_id);

            var modal = $(this);

            modal.find('.modal-body #cpd_file_upload_id').val(sps_id);
        });
    </script>

    <script>
        $('#document_download_modal').on('show.bs.modal', function (event) {
            var button = $(event.relatedTarget); // Button that triggered the modal
            var documents = button.data('todo');
            var sps_id = button.data('sps_id');

            var modal = $(this);

            modal.find('.modal-body #documents_download').empty();

            for(var i = documents.length-1, sl=1; i>=0; i--,sl++){

                var urldes = window.location.origin+"/director/term_and_condition/sps/destroy_file/"+ i +"/"+sps_id;
                var urldown = window.location.origin+"/director/term_and_condition/sps/download/"+ i+"/"+sps_id;

                var tr = "<tr class='table-body-row'><th class='text-center'>"+ sl +"</th><th class='text-center'><b>"+ documents[i].name +"</b></th><th class='text-center'>"+ documents[i].date +"</th><td class='action-td text-center'><a href='"+urldes+"' onclick='return confirm(\"Are You Sure Want To Delete This File...?\")' class='btn btn-danger btn-sm btn-sm-custom btn-custom-danger btn-delete'><i class='fa fa-trash' aria-hidden='true'></i></a><a href='"+urldown+"' class='btn btn-dark btn-sm btn-sm-custom btn-custom-info'><i class='fa fa-cloud-download'></i></a></td></tr>";

                modal.find('.modal-body #documents_download').append(tr);
            }


        });
    </script>
@endsection
