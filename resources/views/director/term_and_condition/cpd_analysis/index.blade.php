@extends('layouts.director.app')

@section('page_title', '| Terms And Conditions | CPD Analysis')

@section('stylesheet_links')
    {{--  External CSS  --}}

@endsection

@section('stylesheet')
    {{--  External CSS  --}}
    <style>
        .chart.chart-md{
            margin-top: 30px;
        }
        td.legendLabel {
            padding-left: 5px;
        }
    </style>
@endsection

@section('content')

<header class="page-header">
    <h2>CPD Analysis</h2>

    <div class="right-wrapper pull-right">
        <ol class="breadcrumbs">
            <li>
                <a href="{{ route('director.dashboard.index') }}">
                    <i class="fa fa-home"></i>
                </a>
            </li>
            <li><span class="active">Terms And Conditions</span></li>
            <li><span>CPD Analysis</span></li>
        </ol>
    </div>
</header>

<!-- start: page -->
@php
    $script = ""
@endphp
@foreach ($cpds as $cpd)
{{-- {{ dd($cpd) }} --}}
<div class="col-md-6">
    <section class="panel">
        <header class="panel-heading">
            <div class="panel-actions">
                <a href="#" class="fa fa-caret-down"></a>
                <a href="#" class="fa fa-times"></a>
            </div>
            @if($cpd[0] != null)
                <h2 class="panel-title"><b>{{ $cpd[0]->userDetails->saluation.' '.$cpd[0]->userDetails->forname.' '.$cpd[0]->userDetails->middle_name.' '.$cpd[0]->userDetails->surname }}</b></h2>
            @endif
        </header>
        <div class="panel-body">
            <table class="table table-bordered table-striped mb-none">
                <thead>
                    <tr class="table-header-row">
                        <th class="text-center">CPD Activity</th>
                        <th class="text-center">Structured Hours</th>
                        <th class="text-center">Unstructured Hours</th>
                        <th class="text-center">Total</th>
                    </tr>
                </thead>
                <tbody>
                    @php
                        $struct_total = 0;
                        $unstruct_total = 0;
                        $cpd_array = array();
                        foreach($cpd as $list => $data){
                            if(empty($cpd_array)){
                                $abc = array(
                                    'id' => $data->user.$data->id,
                                    'cpd_activity' => $data->cpd_activity_type,
                                    'structured_hour' => $data->structured_hour,
                                    'unstructured_hour' => $data->unstructured_hour,
                                );
                                $cpd_array[] = $abc;
                            } else{
                                $temp = array_column($cpd_array, 'cpd_activity');

                                $id = array_search($data->cpd_activity_type, $temp);

                                $abc = gettype($id);

                                if(gettype($id) == 'integer'){
                                    $cpd_array[$id]['structured_hour'] += $data->structured_hour;
                                    $cpd_array[$id]['unstructured_hour'] += $data->unstructured_hour;
                                } else{
                                    // dd("dasd");
                                    $abc = array(
                                        'id' => $data->user.$data->id,
                                        'cpd_activity' => $data->cpd_activity_type,
                                        'structured_hour' => $data->structured_hour,
                                        'unstructured_hour' => $data->unstructured_hour,
                                    );
                                    $cpd_array[] = $abc;
                                }
                            }

                        }

                        $script .= "[";
                            foreach($cpd_array as $list => $data){
                                $script .= '{"id":"'.$data['id'].'",';
                                $script .= '"cpd_activity":"'.$data['cpd_activity'].'",';
                                $script .= '"structured_hour":'.$data['structured_hour'].',';
                                $script .= '"unstructured_hour":'.$data['unstructured_hour'].'}';
                                if((sizeof($cpd_array)-1) != $list) $script .= ',';
                            }
                        $script .= "]-";
                    @endphp


                    @foreach($cpd_array as $list => $data)
                        <tr class="table-body-row">
                            <td class="text-center">{{ $data['cpd_activity'] }}</td>
                            <td class="text-center">{{ $data['structured_hour'] }}</td>
                            <td class="text-center">{{ $data['unstructured_hour'] }}</td>

                            @php
                                $total = $data['structured_hour'] + $data['unstructured_hour'];
                                $struct_total += $data['structured_hour'];
                                $unstruct_total += $data['unstructured_hour'];
                            @endphp
                            <td class="text-center">{{ $total }}</td>
                        </tr>
                    @endforeach
                </tbody>
                <tfoot>
                    <tr class="table-footer-row">
                        <th class="text-center">Total</th>
                        <th class="text-center">{{ $struct_total }}</th>
                        <th class="text-center">{{ $unstruct_total }}</th>
                        <th class="text-center">{{ $struct_total + $unstruct_total }}</th>
                    </tr>
                </tfoot>
            </table>
            <!-- Flot: Pie -->
            <div class="chart chart-md" id="flotPie_{{ $cpd[0]->user.$cpd[0]->id }}"></div>

        </div>
    </section>
</div>
@endforeach
<!-- end: page -->
@endsection


@section('script_links')
{{--  External Javascript  --}}
    {{-- <script src="{{ asset('custom/assets/vendor/flot/jquery.flot.pie.js') }}"></script> --}}
    <script src="{{ asset('custom/assets/vendor/jquery-appear/jquery.appear.js') }}"></script>
    <script src="{{ asset('custom/assets/vendor/jquery-easypiechart/jquery.easypiechart.js') }}"></script>
    <script src="{{ asset('custom/assets/vendor/flot/jquery.flot.js') }}"></script>
    <script src="{{ asset('custom/assets/vendor/flot-tooltip/jquery.flot.tooltip.js') }}"></script>
    <script src="{{ asset('custom/assets/vendor/flot/jquery.flot.pie.js') }}"></script>
    <script src="{{ asset('custom/assets/vendor/flot/jquery.flot.categories.js') }}"></script>
    <script src="{{ asset('custom/assets/vendor/flot/jquery.flot.resize.js') }}"></script>
    <script src="{{ asset('custom/assets/vendor/jquery-sparkline/jquery.sparkline.js') }}"></script>
    <script src="{{ asset('custom/assets/vendor/raphael/raphael.js') }}"></script>
    <script src="{{ asset('custom/assets/vendor/morris/morris.js') }}"></script>
    <script src="{{ asset('custom/assets/vendor/gauge/gauge.js') }}"></script>
    <script src="{{ asset('custom/assets/vendor/snap-svg/snap.svg.js') }}"></script>
    <script src="{{ asset('custom/assets/vendor/liquid-meter/liquid.meter.js') }}"></script>
    {{-- <script src="{{ asset('custom/assets/javascripts/ui-elements/examples.charts.js') }}"></script> --}}

@endsection

@section('scripts')
    {{--  External Javascript  --}}

    <script type="text/javascript">
        $(document).ready(function(){

            var colors = {
                seminar_or_course : '#00ad7c',
                meeting : '#00818a',
                one_to_one : '#03414d',
                self_study : '#494ca2',
                compliance_visit : '#f0d43a',
                research : '#b5ff7d',
                computer_based_training : '#f33535',
                file_check_learning : '#65799b',
                reading_of_financial : '#0074e4',
                other : '#222831',
            };


            var cpds = "{{ $script }}";
            cpds = cpds.replace(/&quot;/g, '"');
            var cpd_arrays = cpds.split("-");

            cpd_arrays.splice(-1,1);

            for(var i =0; i< cpd_arrays.length; i++){
                var data = JSON.parse(cpd_arrays[i]);
                // console.log(data);
                var obj = [];
                for(var j=0; j< data.length; j++){
                    obj[j] ={
                        label : data[j].cpd_activity,
                        data : [1, (data[j].structured_hour+data[j].unstructured_hour)],
                        color: pickColor(colors, data[j].cpd_activity)
                    };
                }

                var flotPieData = obj;
                // console.log(flotPieData);
                var id = '#flotPie_'+data[0].id;

                var plot = $.plot(id, flotPieData, {
                    series: {
                        pie: {
                            show: true,
                            combine: {
                                // color: '#999',
                                // threshold: 0.1
                            }
                        }
                    },
                    legend: {
                        show: true
                    },
                    grid: {
                        hoverable: false,
                        clickable: false
                    }
                });
            }


            // console.log(cpd_arrays);.
            function pickColor(colors, name){
                if(name == 'Seminar or Course')
                    return colors.seminar_or_course;

                if(name == 'Meeting')
                    return colors.meeting;

                if(name == 'One to One Action')
                    return colors.one_to_one;

                if(name == 'Self Study')
                    return colors.self_study;

                if(name == 'Compliance Visit')
                    return colors.compliance_visit;

                if(name == 'Research')
                    return colors.research;

                if(name == 'Computer Based Training')
                    return colors.computer_based_training;

                if(name == 'File Check Learning')
                    return colors.file_check_learning;

                if(name == 'Reading of Financial Member Update')
                    return colors.reading_of_financial;

                if(name == 'Other')
                    return colors.other;

            }



            // var flotPieData = [{
            //     label: "Series 1",
            //     data: [
            //         [1, 79]
            //     ],
            //     color: colors.meeting
            // },{
            //     label: "Series 2",
            //     data: [
            //         [1, 47]
            //     ],
            //     color: colors.other
            // }];

            // console.log(flotPieData);

        });
    </script>
@endsection
