@extends('layouts.director.app')

@section('page_title', '| Business')

@section('stylesheet_links')
    {{--  External CSS  --}}
    <link rel="stylesheet" href="{{ asset('custom/assets/vendor/select2/select2.css') }}" />
    <link rel="stylesheet" href="{{ asset('custom/assets/vendor/jquery-datatables-bs3/assets/css/datatables.css') }}" />
@endsection

@section('stylesheet')
    {{--  External CSS  --}}
    <style>
        .dataTables_wrapper .DTTT.btn-group {
            display: none !important;
            position: absolute;
            top: -74px;
            right: 0px;
        }

        .member-active{
            background-color: green;
            color: #ffffff;
            padding: 2px 7px;
            border-radius: 3px;
            font-weight: 600;
        }
        .member-inactive{
            background-color: red;
            color: #ffffff;
            padding: 2px 7px;
            border-radius: 3px;
            font-weight: 600;
        }

        .access-granted{
            background-color: green;
            color: #ffffff;
            padding: 2px 7px;
            border-radius: 3px;
            font-weight: 600;
        }
        .access-not-granted{
            background-color: red;
            color: #ffffff;
            padding: 2px 7px;
            border-radius: 3px;
            font-weight: 600;
        }
        .modal-header .close {
            outline: none !important;
            border: 1px solid #8e8e8e;
            padding: 0px 5px;
            border-radius: 0;
            opacity: 1;
            color: #8e8e8e;
            transition: 0.3s all ease-in-out;
        }
        .modal-header .close:hover {
            outline: none !important;
            border: 1px solid #0f1b25;
            background-color: #0f1b25;
            opacity: 1;
            color: #ffffff;
            transition: 0.3s all ease-in-out;
        }
        .modal-title{
            color: #0f1b25;
        }
        .modal-content {
            border: 1px solid #e5e5e5;
            border-radius: 0;
            box-shadow: 0 3px 9px rgba(0, 0, 0, 0.1);
        }
        .btn.btn-dark.btn-custom.btn-block {
            margin: 12px 0px;
            border-radius: 0px;
        }
        @media (min-width: 768px){
            .modal-dialog {
                width: 500px;
                margin: 11% auto;
            }
        }
    </style>
@endsection

@section('content')
<header class="page-header">
    <h2>All Business</h2>

    <div class="right-wrapper pull-right">
        <ol class="breadcrumbs">
            <li>
                <a href="{{ Route('director.dashboard.index') }}">
                    <i class="fa fa-home"></i>
                </a>
            </li>
            <li><span class="active">Business</span></li>
            <li><span>All Business</span></li>
        </ol>
    </div>
</header>

<!-- start: page -->
<section class="panel">
    <header class="panel-heading">
        <h2 class="panel-title">All Business Lists</h2>
        <button class="btn btn-dark btn-custom btn-sm header-btn" data-toggle="modal" data-target="#myModal">Assign New Business</button>
    </header>
    <div class="panel-body">
        <table class="table table-bordered table-striped mb-none" id="datatable-tabletools" data-swf-path="assets/vendor/jquery-datatables/extras/TableTools/swf/copy_csv_xls_pdf.swf">
            <thead>
                <tr class="table-header-row">
                    <th>Sl.</th>
                    <th>Adviser Name</th>
                    <th>Client Full Name</th>
                    <th>Policy</th>
                    <th>Policy Type</th>
                    <th>Policy Status</th>
                    <th>Policy Request Date</th>
                    <th>Policy Start Date</th>
                    <th>Policy Ends Date</th>
                    <th class="text-center">Actions</th>
                </tr>
            </thead>
            <tbody>
                @foreach ($businesses as $sl=>$data)
                    <tr class="table-body-row">
                        <th>{{ $sl+1 }}</th>

                        <th>{{ $data->advisorDetails->saluation.' '.$data->advisorDetails->forname.' '.$data->advisorDetails->middle_name.' '.$data->advisorDetails->surname }}</th>

                        <th>{{ $data->clientDetails->client_title.' '.$data->clientDetails->client_forname.' '.$data->clientDetails->client_middle_name.' '.$data->clientDetails->client_surname }}</th>

                        <td>{{ $data->policy_name }}</td>
                        <td>{{ $data->specific_policy_type }}</td>
                        <td>{{ $data->policy_status }}</td>

                        @php
                            $a = new DateTime($data->policy_request_date);
                            $b = new DateTime($data->policy_start_date);
                            $c = new DateTime($data->policy_end_date);
                        @endphp
                        <td>{{ $a->format('d M Y') }}</td>
                        <td>{{ $b->format('d M Y') }}</td>
                        <td>{{ $c->format('d M Y') }}</td>

                        <td class="action-td text-center">
                            @php
                                // if($data->policy_name == 'Investment')
                                //     $route = 'director.business.all_business.showInvestment';

                                if($data->policy_name == 'Mortgage')
                                    $route = 'director.business.all_business.showMortgage';

                                // if($data->policy_name == 'Pension')
                                //     $route = 'director.business.all_business.showPension';

                                if($data->policy_name == 'Protection')
                                    $route = 'director.business.all_business.showProtection';
                            @endphp

                            <a href="{{ route($route, ['id' => $data->id]) }}" class="btn btn-dark btn-sm btn-sm-custom btn-custom-info">
                                <i class="fa fa-info"></i>
                            </a>
                        </td>
                    </tr>
                @endforeach
            </tbody>
        </table>
    </div>
</section>
<!-- end: page -->



{{-- ========================< Modal Starts >======================= --}}
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="gridSystemModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="gridSystemModalLabel"><b>Choose Your Policy</b></h4>
            </div>
            <div class="modal-body">
                <div class="row">
                    {{-- <div class="col-md-6">
                        <a href="{{ route('director.business.add_new_policy.investment.index') }}" class="btn btn-dark btn-custom btn-lg btn-block">Investment</a>
                    </div> --}}
                    <div class="col-md-6">
                        <a href="{{ route('director.business.add_new_policy.mortgage.index') }}" class="btn btn-dark btn-custom btn-lg btn-block">Mortgage</a>
                    </div>
                    {{-- <div class="col-md-6">
                        <a href="{{ route('director.business.add_new_policy.pension.index') }}" class="btn btn-dark btn-custom btn-lg btn-block">Pension</a>
                    </div> --}}
                    <div class="col-md-6">
                        <a href="{{ route('director.business.add_new_policy.protection.index') }}" class="btn btn-dark btn-custom btn-lg btn-block">Protection</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
{{-- =========================< Modal Ends >======================== --}}

@endsection


@section('script_links')
    {{--  External Javascript  --}}
    <script src="{{ asset('custom/assets/vendor/select2/select2.js') }}"></script>
    <script src="{{ asset('custom/assets/vendor/jquery-datatables/media/js/jquery.dataTables.js') }}"></script>
    <script src="{{ asset('custom/assets/vendor/jquery-datatables/extras/TableTools/js/dataTables.tableTools.min.js') }}"></script>
    <script src="{{ asset('custom/assets/vendor/jquery-datatables-bs3/assets/js/datatables.js') }}"></script>

    <script src="{{ asset('custom/assets/javascripts/tables/examples.datatables.default.js') }}"></script>
    <script src="{{ asset('custom/assets/javascripts/tables/examples.datatables.row.with.details.js') }}"></script>
    <script src="{{ asset('custom/assets/javascripts/tables/examples.datatables.tabletools.js') }}"></script>

@endsection

@section('scripts')
    {{--  External Javascript  --}}

@endsection
