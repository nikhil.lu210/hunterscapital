@extends('layouts.director.app')

@section('page_title', '| Business')

@section('stylesheet_links')
    {{--  External CSS  --}}

@endsection

@section('stylesheet')
    {{--  External CSS  --}}

@endsection

@section('content')

<header class="page-header">
    <h2>client_name</h2>

    <div class="right-wrapper pull-right">
        <ol class="breadcrumbs">
            <li>
                <a href="{{ Route('director.dashboard.index') }}">
                    <i class="fa fa-home"></i>
                </a>
            </li>
            <li><span class="active">Clients</span></li>
            <li>
                <a href="{{ Route('director.business.all_business.index') }}">
                    <span class="active">All Business</span>
                </a>
            </li>
            <li><span>client_name</span></li>
        </ol>
    </div>
</header>

<!-- start: page -->

<!-- end: page -->

@endsection


@section('script_links')
    {{--  External Javascript  --}}

@endsection

@section('scripts')
    {{--  External Javascript  --}}

@endsection
