@extends('layouts.director.app')

@section('page_title', '| Business | Update Turnover')

@section('stylesheet_links')
    {{--  External CSS  --}}
    <link rel="stylesheet" href="{{ asset('custom/assets/vendor/select2/select2.css') }}" />
    <link rel="stylesheet" href="{{ asset('custom/assets/vendor/bootstrap-datepicker/css/datepicker3.css') }}" />

@endsection

@section('stylesheet')
    {{--  External CSS  --}}
    <style>
        html .wizard-progress .wizard-steps li.active a span,
        html.dark .wizard-progress .wizard-steps li.active a span {
            color: #dd3333;
            border-color: #dd3333;
        }
        html .wizard-progress .wizard-steps li.completed a span,
        html.dark .wizard-progress .wizard-steps li.completed a span,
        html .wizard-progress .steps-progress .progress-indicator,
        html.dark .wizard-progress .steps-progress .progress-indicator{
            border-color: #dd3333;
            background: #dd3333;
        }
        .pager{
            margin: 0;
        }
        .pager li > a,
        .pager li > span,
        .pager li > a:hover,
        .pager li > span:hover{
            background-color: #02070a !important;
            border: 1px solid #02070a !important;
        }
        .pager .disabled > a,
        .pager .disabled > a:hover,
        .pager .disabled > a:focus,
        .pager .disabled > span{
            background-color: #ffffff !important;
            color: #02070a;
        }
        .btn.btn-dark.btn-custom.btn-block {
            margin: 12px 0px;
            border-radius: 0px;
        }

        html .wizard-progress,
        html.dark .wizard-progress {
            margin: 0 30%;
        }
    </style>
@endsection

@section('content')

<header class="page-header">
    <h2>Update Turnover</h2>

    <div class="right-wrapper pull-right">
        <ol class="breadcrumbs">
            <li>
                <a href="{{ Route('director.dashboard.index') }}">
                    <i class="fa fa-home"></i>
                </a>
            </li>
            <li><span class="active">Business</span></li>
            <li>
                <a href="{{ Route('director.business.turnover.index') }}">
                    <span class="active">Turnover</span>
                </a>
            </li>
            <li><span>Update Turnover</span></li>
        </ol>
    </div>
</header>

<!-- start: page -->
<section class="panel form-wizard" id="w3">
    <header class="panel-heading">
        <div class="panel-actions">
            <a href="#" class="fa fa-caret-down"></a>
        </div>

        <h2 class="panel-title">Update Turnover</h2>
    </header>

    <form class="form-horizontal" novalidate="novalidate" action="{{ route('director.business.turnover.update', ['id' => $turnover->id]) }}" method="POST">
        @csrf
        <div class="panel-body">
            <div class="wizard-progress">
                <div class="steps-progress">
                    <div class="progress-indicator" style="width: 0%;"></div>
                </div>
                <ul class="wizard-steps">
                    <li class="active">
                        <a href="#sec_01" data-toggle="tab"><span>1</span></a>
                    </li>
                    <li>
                        <a href="#sec_02" data-toggle="tab"><span>2</span></a>
                    </li>
                </ul>
            </div>
            <div class="tab-content">
                {{-- Part 01 --}}
                <div id="sec_01" class="tab-pane active">
                    <div class="form-group">
                        <div class="col-md-4">
                            <label class="control-label">Client <span class="required">*</span></label>
                            <select name="client" id="turnover_client" data-plugin-selectTwo class="form-control populate  @error('client') is-invalid @enderror" required>
                                <option disabled>Select Client</option>
                                <option value="Unrelated Client" @if($turnover->client == null) selected @endif>Unrelated Client</option>
                                @foreach ($clients as $client)
                                    <option value="{{ $client->id }}" @if($turnover->client != null && $turnover->client == $client->id) selected @endif>{{ $client->client_title.' '.$client->client_forname.' '.$client->client_middle_name.' '.$client->client_surname }}</option>
                                @endforeach
                            </select>

                            {{-- Validation Alert Messages --}}
                            @error('client')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>


                        <div class="col-md-4">
                            <label class="control-label">Policy</label>
                            <select name="client_policy" id="turnover_client_policies" data-plugin-selectTwo class="form-control populate  @error('client_policy') is-invalid @enderror">
                                <option disabled>Select Policy</option>
                                <option value="{{ $turnover->client_policy }}" selected>{{ $turnover->policyDetails->policy_name." ".$turnover->policyDetails->specific_policy_type }}</option>
                            </select>

                            {{-- Validation Alert Messages --}}
                            @error('client_policy')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>

                        <div class="col-md-4">
                            <label class="control-label">Advisor <span class="required">*</span></label>
                            <select name="advisor" data-plugin-selectTwo class="form-control populate  @error('advisor') is-invalid @enderror" required>
                                    <option disabled>Select Advisor</option>
                                    <option value="{{ $turnover->advisor }}" selected>{{ $turnover->advisorDetails->saluation.' '.$turnover->advisorDetails->forname.' '.$turnover->advisorDetails->middle_name.' '.$turnover->advisorDetails->surname }}</option>
                                @foreach ($advisors as $advisor)
                                    @php
                                        if($turnover->advisor == $advisor->id) continue;
                                    @endphp
                                    <option value="{{ $advisor->id }}">{{ $advisor->saluation.' '.$advisor->forname.' '.$advisor->middle_name.' '.$advisor->surname }}</option>
                                @endforeach
                            </select>

                            {{-- Validation Alert Messages --}}
                            @error('advisor')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>

                        <div class="col-md-4">
                            <label class="control-label">Turnover Date</label>
                            <input type="text" autocomplete="off" data-plugin-datepicker="" name="turnover_date" class="form-control  @error('turnover_date') is-invalid @enderror" placeholder="eg.: dd/mm/yyyy" value="{{ App\Http\Controllers\General::reverseDateFormatDashToSlash($turnover->turnover_date) }}">

                            {{-- Validation Alert Messages --}}
                            @error('turnover_date')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>

                        <div class="col-md-4">
                            <label class="control-label">Regulation Type <span class="required">*</span></label>
                            <select name="regulation_type" id="regulation_type" regulation_type data-plugin-selectTwo class="form-control populate  @error('regulation_type') is-invalid @enderror" required>
                                <option disabled>Select Regulation Type</option>
                                <option @if($turnover->regulation_type == 'Regulated') selected @endif value="Regulated">Regulated</option>
                                <option @if($turnover->regulation_type == 'Non-Regulated') selected @endif value="Non-Regulated">Non-Regulated</option>
                            </select>

                            {{-- Validation Alert Messages --}}
                            @error('regulation_type')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>

                        <div class="col-md-4">
                            <label class="control-label">Regulation Type Spec <span class="required">*</span></label>
                            <select name="regulation_type_spec" id="regulation_type_spec" data-plugin-selectTwo class="form-control populate @error('regulation_type_spec') is-invalid @enderror" required>
                                <option disabled>Select Regulation Type Spec</option>
                                <option selected value="{{ $turnover->regulation_type_spec }}">Advisory Business</option>
                            </select>

                            {{-- Validation Alert Messages --}}
                            @error('regulation_type_spec')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>

                        <div class="col-md-4">
                            <label class="control-label">Turnover Type <span class="required">*</span></label>
                            <select name="turnover_type" data-plugin-selectTwo class="form-control populate  @error('turnover_type') is-invalid @enderror" required>
                                <option disabled>Select Turnover Type</option>
                                <option @if($turnover->turnover_type == 'Mortgage/Proc Fee') selected @endif value="Mortgage/Proc Fee">Mortgage/Proc Fee</option>
                                <option @if($turnover->turnover_type == 'Pension') selected @endif value="Pension">Pension</option>
                                <option @if($turnover->turnover_type == 'Gen Ins') selected @endif value="Gen Ins">Gen Ins</option>
                                <option @if($turnover->turnover_type == 'Client Retainer/Fees') selected @endif value="Client Retainer/Fees">Client Retainer/Fees</option>
                                <option @if($turnover->turnover_type == 'Secondary Business Income') selected @endif value="Secondary Business Income">Secondary Business Income</option>
                                <option @if($turnover->turnover_type == 'GPP Consultancy Charges') selected @endif value="GPP Consultancy Charges">GPP Consultancy Charges</option>
                                <option @if($turnover->turnover_type == 'Clawback') selected @endif value="Clawback">Clawback</option>
                                <option @if($turnover->turnover_type == 'Investment') selected @endif value="Investment">Investment</option>
                                <option @if($turnover->turnover_type == 'Protection') selected @endif value="Protection">Protection</option>
                                <option @if($turnover->turnover_type == 'Mortgage/Broker Fee') selected @endif value="Mortgage/Broker Fee">Mortgage/Broker Fee</option>
                            </select>

                            {{-- Validation Alert Messages --}}
                            @error('turnover_type')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>

                        <div class="col-md-4">
                            <label class="control-label">Turnover Frequency <span class="required">*</span></label>
                            <select name="turnover_frequency" data-plugin-selectTwo class="form-control populate  @error('turnover_frequency') is-invalid @enderror" required>
                                <option disabled>Select Turnover Frequency</option>
                                <option @if($turnover->turnover_frequency == 'Initial or Renewal') selected @endif value="Initial or Renewal">Initial or Renewal</option>
                                <option @if($turnover->turnover_frequency == 'Ongoing') selected @endif value="Ongoing">Ongoing</option>
                                <option @if($turnover->turnover_frequency == 'Single') selected @endif value="Single">Single</option>
                            </select>

                            {{-- Validation Alert Messages --}}
                            @error('turnover_frequency')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>

                        <div class="col-md-4">
                            <label class="control-label">Adviser Reference</label>
                            <input type="text" autocomplete="off" name="adviser_reference" class="form-control  @error('adviser_reference') is-invalid @enderror" placeholder="eg.: Adviser Reference" value="{{ $turnover->adviser_reference }}"/>

                            {{-- Validation Alert Messages --}}
                            @error('adviser_reference')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>

                    </div>
                </div>

                {{-- Part 02 --}}
                <div id="sec_02" class="tab-pane">
                    <div class="form-group">
                        <div class="col-md-4">
                            <label class="control-label">RDR Status</label>
                            <select name="rdr_status" data-plugin-selectTwo class="form-control populate  @error('rdr_status') is-invalid @enderror">
                                <option @if($turnover->rdr_status == NULL) selected @endif disabled>Select RDR Status</option>
                                <option @if($turnover->rdr_status == 'Pre RDR (Legacy)') selected @endif value="Pre RDR (Legacy)">Pre RDR (Legacy)</option>
                                <option @if($turnover->rdr_status == 'Post RDR (Adviser Charging)') selected @endif value="Post RDR (Adviser Charging)">Post RDR (Adviser Charging)</option>
                            </select>

                            {{-- Validation Alert Messages --}}
                            @error('rdr_status')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>

                        <div class="col-md-4">
                            <label class="control-label">Revenue Source</label>
                            <select name="revenue_source" data-plugin-selectTwo class="form-control populate  @error('revenue_source') is-invalid @enderror">
                                <option @if($turnover->revenue_source == NULL) selected @endif disabled>Select Revenue Source</option>
                                <option @if($turnover->revenue_source == 'Retail Client') selected @endif value="Retail Client">Retail Client</option>
                                <option @if($turnover->revenue_source == 'Product Provider') selected @endif value="Product Provider">Product Provider</option>
                                <option @if($turnover->revenue_source == 'Platform') selected @endif value="Platform">Platform</option>
                            </select>

                            {{-- Validation Alert Messages --}}
                            @error('revenue_source')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>

                        <div class="col-md-4">
                            <label class="control-label">Payment Frequency</label>
                            <select name="payment_frequency" data-plugin-selectTwo class="form-control populate  @error('payment_frequency') is-invalid @enderror">
                                <option @if($turnover->payment_frequency == NULL) selected @endif disabled>Select Payment Frequency</option>
                                <option @if($turnover->payment_frequency == 'Lump Sum') selected @endif value="Lump Sum">Lump Sum</option>
                                <option @if($turnover->payment_frequency == 'Regular') selected @endif value="Regular">Regular</option>
                            </select>

                            {{-- Validation Alert Messages --}}
                            @error('payment_frequency')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>

                        <div class="col-md-4">
                            <label class="control-label">20% VAT Applied?</label>
                            <select name="vat_applied" data-plugin-selectTwo class="form-control populate  @error('vat_applied') is-invalid @enderror">
                                <option @if($turnover->vat_applied == NULL) selected @endif disabled>Select Answer</option>
                                <option @if($turnover->vat_applied == 1) selected @endif value="1">Yes</option>
                                <option @if($turnover->vat_applied == 0) selected @endif value="0">No</option>
                            </select>
                            <small><b>*If is exempt, leave this checkbox unchecked.</b></small>

                            {{-- Validation Alert Messages --}}
                            @error('vat_applied')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>

                        <div class="col-md-4">
                            <label class="control-label">Amount <span class="required">*</span></label>
                            <input type="number" autocomplete="off" name="amount" class="form-control  @error('amount') is-invalid @enderror" placeholder="eg.: 2100" required value="{{ $turnover->amount }}"/>

                            {{-- Validation Alert Messages --}}
                            @error('amount')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>

                        <div class="col-md-4">
                            <label class="control-label">Revenue Source Name</label>
                            <input type="text" autocomplete="off" name="revenue_source_name" class="form-control  @error('revenue_source_name') is-invalid @enderror" placeholder="eg.: Revenue Source Name" value="{{ $turnover->revenue_source_name }}"/>

                            {{-- Validation Alert Messages --}}
                            @error('revenue_source_name')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>

                        <div class="col-md-12">
                            <label class="control-label">Note</label>
                            <textarea name="note" class="form-control  @error('note') is-invalid @enderror" placeholder="eg.: Simple Note Here..." rows="4"/>{{ $turnover->note }}</textarea>

                            {{-- Validation Alert Messages --}}
                            @error('note')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="panel-footer">
            <ul class="pager">
                <li class="previous disabled pull-left">
                    <a href="#" class="btn btn-dark btn-custom btn-sm">
                        <i class="fa fa-angle-left"></i> Previous
                    </a>
                </li>
                <li class="finish hidden pull-right">
                    <button type="submit" class="btn btn-dark btn-custom btn-sm submit-btn">
                        Update Turnover
                    </button>
                </li>
                <li class="next">
                    <a href="#" class="btn btn-dark btn-custom btn-sm">
                        Next <i class="fa fa-angle-right"></i>
                    </a>
                </li>
            </ul>
        </div>

    </form>
</section>
<!-- end: page -->

@endsection


@section('script_links')
    {{--  External Javascript  --}}
    <script src="{{ asset('custom/assets/vendor/select2/select2.js') }}"></script>
    <script src="{{ asset('custom/assets/vendor/bootstrap-datepicker/js/bootstrap-datepicker.js') }}"></script>

    <script src="{{ asset('custom/assets/vendor/jquery-validation/jquery.validate.js') }}"></script>
    <script src="{{ asset('custom/assets/vendor/bootstrap-wizard/jquery.bootstrap.wizard.js') }}"></script>
    <script src="{{ asset('custom/assets/javascripts/forms/examples.wizard.js') }}"></script>
@endsection

@section('scripts')
    {{--  External Javascript  --}}
    <script>
        $(document).ready(function(){
            $('#turnover_client').change(function(){
                var client = $(this);

                var url = document.location.origin + "/director/client/document_center/policies/" + client.val();

                if(client.val() != 'Unrelated Client'){

                    $.ajax({
                        method: 'GET',
                        url: url,
                        success: function(data) {
                            let range = data;
                            let htm = "<option selected disabled>Select Policy</option>";

                            for(var i=0; i<range.length; i++){

                                htm += "<option value='"+ range[i].id +"'>"+ range[i].policy_name +" "+ range[i].specific_policy_type +"</option>";
                            }
                            var a = $('#turnover_client_policies');
                            a.empty();
                            a.append(htm);
                        },
                        error: function() {
                            console.log("their is an error !!!");
                        }
                    });
                } else{
                    let htm = "<option value='Unrelated Policy'>Unrelated Policy</option>";
                    var a = $('#turnover_client_policies');
                    a.empty();
                    a.append(htm);
                }

            });



            $('#regulation_type').change(function(){
                var arr = [
                    'Europian',
                    'Discretionary',
                    'Execution Only',
                    'Proffisonal Client',
                    'Non Regulationed Business'
                ];

                let regulation = $(this);
                if(regulation.val() == 'Regulated'){
                    let options ='<option value="Advisory Business">Advisory Business</option>';
                    var a =  $('#regulation_type_spec');
                    a.empty();
                    a.append(options);
                } else{
                    var options = '';
                    for(var i=0; i<arr.length; i++){
                        options += '<option value="'+arr[i]+'">'+arr[i]+'</option>';
                    }
                    var a =  $('#regulation_type_spec');
                    a.empty();
                    a.append(options);
                }
            });
        });
    </script>

@endsection
