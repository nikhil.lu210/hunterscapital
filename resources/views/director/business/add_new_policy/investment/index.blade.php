@extends('layouts.director.app')

@section('page_title', '| Business | New Policy')

@section('stylesheet_links')
    {{--  External CSS  --}}
    <link rel="stylesheet" href="{{ asset('custom/assets/vendor/select2/select2.css') }}" />
    <link rel="stylesheet" href="{{ asset('custom/assets/vendor/bootstrap-datepicker/css/datepicker3.css') }}" />

@endsection

@section('stylesheet')
    {{--  External CSS  --}}
    <style>
        html .wizard-progress .wizard-steps li.active a span,
        html.dark .wizard-progress .wizard-steps li.active a span {
            color: #dd3333;
            border-color: #dd3333;
        }
        html .wizard-progress .wizard-steps li.completed a span,
        html.dark .wizard-progress .wizard-steps li.completed a span,
        html .wizard-progress .steps-progress .progress-indicator,
        html.dark .wizard-progress .steps-progress .progress-indicator{
            border-color: #dd3333;
            background: #dd3333;
        }
        .pager{
            margin: 0;
        }
        .pager li > a,
        .pager li > span,
        .pager li > a:hover,
        .pager li > span:hover{
            background-color: #02070a !important;
            border: 1px solid #02070a !important;
        }
        .pager .disabled > a,
        .pager .disabled > a:hover,
        .pager .disabled > a:focus,
        .pager .disabled > span{
            background-color: #ffffff !important;
            color: #02070a;
        }
        .btn-custom{
            border-radius: 0px !important;
        }
        .panel{
            border: 1px solid #eee;
        }
        .panel .panel .panel-heading{
            padding: 10px 15px;
        }
        .panel .panel .panel-heading .panel-title{
            color: #ffffff;
        }
        .fileupload .uneditable-input .fa {
            top: 39px;
        }

        .modal-header .close {
            outline: none !important;
            border: 1px solid #8e8e8e;
            padding: 0px 5px;
            border-radius: 0;
            opacity: 1;
            color: #8e8e8e;
            transition: 0.3s all ease-in-out;
        }
        .modal-header .close:hover {
            outline: none !important;
            border: 1px solid #0f1b25;
            background-color: #0f1b25;
            opacity: 1;
            color: #ffffff;
            transition: 0.3s all ease-in-out;
        }
        .modal-title{
            color: #0f1b25;
        }
        .modal-content {
            border: 1px solid #e5e5e5;
            border-radius: 0;
            box-shadow: 0 3px 9px rgba(0, 0, 0, 0.1);
        }
        .btn.btn-dark.btn-custom.btn-block {
            margin: 12px 0px;
            border-radius: 0px;
        }
        @media (min-width: 768px){
            .modal-dialog {
                width: 500px;
                margin: 11% auto;
            }
        }

        .modal-footer {
            padding: 5px 15px;
        }
        .add-fund{
            margin-right: 5px;
        }
    </style>
@endsection

@section('content')

<header class="page-header">
    <h2>Investment</h2>

    <div class="right-wrapper pull-right">
        <ol class="breadcrumbs">
            <li>
                <a href="{{ Route('director.dashboard.index') }}">
                    <i class="fa fa-home"></i>
                </a>
            </li>
            <li><span class="active">Clients</span></li>
            <li><span class="active">Add New Policy</span></li>
            <li><span>Investment</span></li>
        </ol>
    </div>
</header>

<!-- start: page -->
<section class="panel form-wizard" id="w3">
    <header class="panel-heading">
        <div class="panel-actions">
            <a href="#" class="fa fa-caret-down"></a>
        </div>

        <h2 class="panel-title">New Investment Policy</h2>
    </header>

    <form class="form-horizontal" novalidate="novalidate" action="{{ Route('director.business.add_new_policy.investment.store') }}" method="POST">
        @csrf

        <div class="panel-body">
            <div class="wizard-progress">
                <div class="steps-progress">
                    <div class="progress-indicator" style="width: 0%;"></div>
                </div>
                <ul class="wizard-steps">
                    <li class="active">
                        <a href="#sec_01" data-toggle="tab"><span>1</span></a>
                    </li>
                    <li>
                        <a href="#sec_02" data-toggle="tab"><span>2</span></a>
                    </li>
                    <li>
                        <a href="#sec_03" data-toggle="tab"><span>3</span></a>
                    </li>
                </ul>
            </div>
            <div class="tab-content">
                {{-- Part 01 --}}
                <div id="sec_01" class="tab-pane active">
                    <div class="form-group">
                        <div class="col-md-3">
                            <label class="control-label">Client <span class="required">*</span></label>
                            <select name="client" data-plugin-selectTwo class="form-control populate  @error('client') is-invalid @enderror" required>
                                <option disabled selected>Select Client</option>
                                @foreach($clients as $client)
                                    <option value="{{ $client->id }}">{{ $client->client_title." ".$client->client_forname." ".$client->client_surname }}</option>
                                @endforeach
                            </select>

                            {{-- Validation Alert Messages --}}
                            @error('client')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>

                        <div class="col-md-3">
                            <label class="control-label">Adviser <span class="required">*</span></label>
                            <select name="advisor" data-plugin-selectTwo class="form-control populate  @error('advisor') is-invalid @enderror" required>
                                <option disabled selected>Select Adviser</option>
                                @foreach($advisors as $advisor)
                                    <option value="{{ $advisor->id }}">{{ $advisor->saluation." ".$advisor->forname." ".$advisor->surname }}</option>
                                @endforeach
                            </select>

                            {{-- Validation Alert Messages --}}
                            @error('advisor')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>

                        <div class="col-md-3">
                            <label class="control-label">Specific Policy Type <span class="required">*</span></label>
                            {{-- Include specific_policy_type Here --}}
                            @include('layouts.director.partials.dropdowns.specific_policy_type')
                        </div>

                        <div class="col-md-3">
                            <label class="control-label">New Business Type <span class="required">*</span></label>
                            <select name="new_business_type" data-plugin-selectTwo class="form-control populate  @error('new_business_type') is-invalid @enderror" required>
                                <option disabled selected>Select New Business Type</option>
                                <option value="New Business">New Business</option>
                                <option value="Execution Only/Non-Advised Sale">Execution Only/Non-Advised Sale</option>
                                <option value="Replacement">Replacement</option>
                                <option value="Direct Offer">Direct Offer</option>
                                <option value="Rebalancing">Rebalancing</option>
                                <option value="Fund Replacement">Fund Replacement</option>
                                <option value="Change in Circumstance">Change in Circumstance</option>
                                <option value="Change in ATR">Change in ATR</option>
                                <option value="Existing Policy (Not new Business)">Existing Policy (Not new Business)</option>
                            </select>

                            {{-- Validation Alert Messages --}}
                            @error('new_business_type')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>

                        <div class="col-md-4">
                            <label class="control-label">Adviser Reference</label>
                            <input type="text" autocomplete="off" name="adviser_reference" class="form-control  @error('adviser_reference') is-invalid @enderror" placeholder="eg.: Adviser Reference"/>

                            {{-- Validation Alert Messages --}}
                            @error('adviser_reference')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>

                        <div class="col-md-4">
                            <label class="control-label">Policy Valuation Amount</label>
                            <input type="number" name="policy_valuation_amount" class="form-control  @error('policy_valuation_amount') is-invalid @enderror" placeholder="eg.: 0" min="0"/>

                            {{-- Validation Alert Messages --}}
                            @error('policy_valuation_amount')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>

                        <div class="col-md-4">
                            <label class="control-label">Policy Request Date</label>
                            <input type="text" autocomplete="off" data-plugin-datepicker="" name="policy_request_date" class="form-control  @error('policy_request_date') is-invalid @enderror" placeholder="eg.: dd/mm/yyyy">

                            {{-- Validation Alert Messages --}}
                            @error('policy_request_date')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>

                        <div class="col-md-4">
                            <label class="control-label">Policy Start Date</label>
                            <input type="text" autocomplete="off" data-plugin-datepicker="" name="policy_start_date" class="form-control  @error('policy_start_date') is-invalid @enderror" placeholder="eg.: dd/mm/yyyy">

                            {{-- Validation Alert Messages --}}
                            @error('policy_start_date')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>

                        <div class="col-md-4">
                            <label class="control-label">Policy Ends Date</label>
                            <input type="text" autocomplete="off" data-plugin-datepicker="" name="policy_end_date" class="form-control  @error('policy_end_date') is-invalid @enderror" placeholder="eg.: dd/mm/yyyy">

                            {{-- Validation Alert Messages --}}
                            @error('policy_end_date')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>

                        <div class="col-md-4">
                            <label class="control-label">Policy Cancellation Date</label>
                            <input type="text" autocomplete="off" data-plugin-datepicker="" name="policy_cancellation_date" class="form-control  @error('policy_cancellation_date') is-invalid @enderror" placeholder="eg.: dd/mm/yyyy">

                            {{-- Validation Alert Messages --}}
                            @error('policy_cancellation_date')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>

                        <div class="col-md-4">
                            <label class="control-label">Policy Review Date <span class="required">(If applicable)</span></label>
                            <input type="text" autocomplete="off" data-plugin-datepicker="" name="policy_review_date" class="form-control  @error('policy_review_date') is-invalid @enderror" placeholder="eg.: dd/mm/yyyy">

                            {{-- Validation Alert Messages --}}
                            @error('policy_review_date')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>

                        <div class="col-md-4">
                            <label class="control-label">Policy Valuation Date <span class="required">(If applicable)</span></label>
                            <input type="text" autocomplete="off" data-plugin-datepicker="" name="policy_valuation_date" class="form-control  @error('policy_valuation_date') is-invalid @enderror" placeholder="eg.: dd/mm/yyyy">

                            {{-- Validation Alert Messages --}}
                            @error('policy_valuation_date')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>

                        <div class="col-md-12">
                            <label class="control-label"><b>Is there any other reason which may give rise to this client being classified as vulnerable? </b></label>
                            <small><br> For example (but not limited to) mental impairment (even if not fully incapable), recently bereaved, recent diagnose of major illness (or similar e.g. loss of limb). breakdown of marriage or other long term relationship.</small>
                            <div class="checkbox-custom checkbox-default">
                                <input type="checkbox" name="other_reason" class=" @error('other_reason') is-invalid @enderror">
                                <label for="other_reason">Yes</label>
                            </div>

                            {{-- Validation Alert Messages --}}
                            @error('other_reason')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>

                    </div>
                </div>

                {{-- Part 02 --}}
                <div id="sec_02" class="tab-pane">
                    <div class="col-md-5">
                        {{-- Provider Panel --}}
                        <section class="panel">
                            <header class="panel-heading bg-danger">
                                <h2 class="panel-title">Provider</h2>
                                <button class="btn btn-dark btn-custom btn-sm header-btn" data-toggle="modal" data-target="#providerModal">Request New Provider</button>
                            </header>
                            <div class="panel-body">
                                <div class="col-md-12">
                                    <label class="control-label">Provider <span class="required">*</span></label>
                                    <select name="provider_id" data-plugin-selectTwo class="form-control populate  @error('provider_id') is-invalid @enderror" required>
                                        <option disabled selected>Select Provider</option>
                                        @foreach($providers as $provider)
                                            <option value="{{ $provider->id }}">{{ $provider->provider_name }}</option>
                                        @endforeach
                                    </select>
                                    <small>To search for a provider, you can start typing the name once the dropdown list has been opened. If you cannot locate your required provider in the list, please use the 'Request Provider' below.</small>

                                    {{-- Validation Alert Messages --}}
                                    @error('provider_id')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                            </div>
                        </section>


                        {{-- Trust Panel --}}
                        <section class="panel">
                            <header class="panel-heading bg-danger">
                                <h2 class="panel-title">Trust</h2>
                                <a class="btn btn-dark btn-custom btn-sm header-btn" data-toggle="modal" data-target="#trustModal">Add New Trust</a>
                            </header>
                            <div class="panel-body">
                                <div class="col-md-12">
                                    <label class="control-label">Trust <span class="required">(If in Trust)</span></label>
                                    <select name="trust" data-plugin-selectTwo class="form-control populate  @error('trust') is-invalid @enderror">
                                        <option disabled selected>Select Trust</option>
                                        @foreach($trusts as $trust)
                                            <option value="{{ $trust->id }}">{{ $trust->trust_name }}</option>
                                        @endforeach
                                    </select>
                                    <small>To search for a trust, you can start typing the name once the dropdown list has been opened. If you want to add a new trust please use the 'Add Trust' below.</small>

                                    {{-- Validation Alert Messages --}}
                                    @error('trust')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>

                                <div class="col-md-12">
                                    <label class="control-label">Beneficiaries <span class="required">(If in Trust)</span></label>
                                    <input type="text" autocomplete="off" name="beneficiaries" class="form-control populate @error('beneficiaries') is-invalid @enderror" placeholder="Beneficiaries">

                                    {{-- Validation Alert Messages --}}
                                    @error('beneficiaries')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                            </div>
                        </section>
                    </div>


                    <div class="col-md-7">
                        {{-- Fund Panel --}}
                        <section class="panel">
                            <header class="panel-heading bg-danger">
                                <h2 class="panel-title">Fund</h2>
                                <button class="btn btn-dark btn-custom btn-sm header-btn" data-toggle="modal" data-target="#fundModal">Request A Fund</button>
                                <a class="btn btn-dark btn-custom btn-sm header-btn add-fund" data-toggle="modal" data-target="#addFundModal">Add New Fund</a>
                            </header>
                            <div class="panel-body">
                                <table class="table table-bordered table-striped mb-none">
                                    <thead>
                                        <tr>
                                            <th>Fund</th>
                                            <th>Invested</th>
                                            <th>Date</th>
                                            <th class="text-center">Actions</th>
                                        </tr>
                                    </thead>
                                    <tbody id="add_funds_list">
                                        {{-- <tr class="gradeX fund_list" id="">
                                            <th>01</th>
                                            <td>
                                                fund_01
                                                <input type="hidden" name="funds['+ id +'][fund_name]" value="">

                                            </td>
                                            <td>
                                                2100
                                                <input type="hidden" name="funds['+ id +'][invested]" value="">
                                            </td>
                                            <td>
                                                27-05-2019
                                                <input type="hidden" name="funds['+ id +'][date]" value="">
                                            </td>
                                            <td class="text-center">
                                                <a href="#" class="btn btn-danger btn-sm btn-sm-custom btn-custom-danger btn-delete">
                                                    <i class="fa fa-trash-o" aria-hidden="true"></i>
                                                </a>
                                            </td>
                                        </tr> --}}
                                    </tbody>
                                </table>
                            </div>
                        </section>


                        {{-- Others Panel --}}
                        <section class="panel">
                            <header class="panel-heading bg-danger">
                                <h2 class="panel-title">Policies & Investments</h2>
                            </header>
                            <div class="panel-body">
                                <div class="col-md-4">
                                    <label class="control-label">Policy Status <span class="required">*</span></label>
                                    <select name="policy_status" data-plugin-selectTwo class="form-control populate  @error('policy_status') is-invalid @enderror" required>
                                        <option disabled selected>Select Policy Status</option>
                                        <option value="Live">Live</option>
                                        <option value="Lapsed">Lapsed</option>
                                        <option value="Cancelled">Cancelled</option>
                                        <option value="Replaced">Replaced</option>
                                        <option value="Surrendered">Surrendered</option>
                                        <option value="Matured">Matured</option>
                                        <option value="Paid Up">Paid Up</option>
                                        <option value="Pending">Pending</option>
                                        <option value="Added in Error">Added in Error</option>
                                        <option value="NTU">NTU</option>
                                        <option value="DNP">DNP</option>
                                        <option value="Under Writing">Under Writing</option>
                                    </select>

                                    {{-- Validation Alert Messages --}}
                                    @error('policy_status')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>

                                <div class="col-md-4">
                                    <label class="control-label">Policy Number</label>
                                    <input type="text" autocomplete="off" name="policy_number" class="form-control  @error('policy_number') is-invalid @enderror" placeholder="eg.: Policy Number"/>

                                    {{-- Validation Alert Messages --}}
                                    @error('policy_number')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>

                                <div class="col-md-4">
                                    <label class="control-label">Risk Attitude <span class="required">*</span></label>
                                    <select name="risk_attitude" data-plugin-selectTwo class="form-control populate  @error('risk_attitude') is-invalid @enderror" required>
                                        <option disabled selected>Select Risk Attitude</option>
                                        @foreach($risk_lists as $risk_list)
                                            <option value="{{ $risk_list->id }}">{{ $risk_list->name }}</option>
                                        @endforeach
                                    </select>

                                    {{-- Validation Alert Messages --}}
                                    @error('risk_attitude')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>

                                <div class="col-md-12">
                                    <label class="control-label"><b>Do the Funds/Investments chosen include a UCIS products?</b></label>
                                    <div class="checkbox-custom checkbox-default">
                                        <input type="checkbox" name="ucis_products" class=" @error('ucis_products') is-invalid @enderror">
                                        <label for="ucis_products">Yes</label>
                                    </div>

                                    {{-- Validation Alert Messages --}}
                                    @error('ucis_products')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>

                                <div class="col-md-12">
                                    <label class="control-label"><b>Do the Funds/Investments selected include direct stocks or shares?</b></label>
                                    <div class="checkbox-custom checkbox-default">
                                        <input type="checkbox" name="direct_stock" class=" @error('direct_stock') is-invalid @enderror">
                                        <label for="direct_stock">Yes</label>
                                    </div>

                                    {{-- Validation Alert Messages --}}
                                    @error('direct_stock')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                            </div>
                        </section>
                    </div>
                </div>

                {{-- Part 03 --}}
                <div id="sec_03" class="tab-pane">
                    <div class="col-md-12 col-xl-6">
                        {{-- Commission Panel --}}
                        <section class="panel">
                            <header class="panel-heading bg-danger">
                                <h2 class="panel-title">Commission / Fee</h2>
                                <a class="btn btn-dark btn-custom btn-sm header-btn" data-toggle="modal" data-target="#addCommissionFeeModal">Add Commission/Fee</a>
                            </header>
                            <div class="panel-body">
                                <table class="table table-bordered table-striped mb-none">
                                    <thead>
                                        <tr>
                                            <th>Date Start</th>
                                            <th>Date End</th>
                                            <th>Paid by</th>
                                            <th>Amount</th>
                                            <th>Frequency</th>
                                            <th>Payment Status</th>
                                            <th class="text-center">Actions</th>
                                        </tr>
                                    </thead>
                                    <tbody id="add_commissions">
                                        {{-- <tr class="gradeX">
                                            <td>
                                                27_05_2019
                                                <input type="hidden" name="commissions['+ id +'][start_date]">
                                            </td>
                                            <td>
                                                27_05_2019
                                                <input type="hidden" name="commissions['+ id +'][end_date]">
                                            </td>
                                            <td>
                                                paid_by
                                                <input type="hidden" name="commissions['+ id +'][paid_by]">
                                            </td>
                                            <td>
                                                amount
                                                <input type="hidden" name="commissions['+ id +'][amount]">
                                            </td>
                                            <td>
                                                frequency
                                                <input type="hidden" name="commissions['+ id +'][frequency]">
                                            </td>
                                            <td>
                                                paid_unpaid
                                                <input type="hidden" name="commissions['+ id +'][paid_unpaid]">
                                            </td>
                                            <td class="text-center">
                                                <a href="#" class="btn btn-danger btn-sm btn-sm-custom btn-custom-danger btn-delete" onclick="destroy_commission(this);">
                                                    <i class="fa fa-trash-o" aria-hidden="true"></i>
                                                </a>
                                            </td>
                                        </tr> --}}
                                    </tbody>
                                </table>
                            </div>
                        </section>
                    </div>


                    <div class="col-md-12 col-xl-6">
                        {{-- Payment Panel --}}
                        <section class="panel">
                            <header class="panel-heading bg-danger">
                                <h2 class="panel-title">Payment</h2>
                                <a class="btn btn-dark btn-custom btn-sm header-btn" data-toggle="modal" data-target="#addPaymentModal">Add Payment</a>
                            </header>
                            <div class="panel-body">
                                <table class="table table-bordered table-striped mb-none">
                                    <thead>
                                        <tr>
                                            <th>Date Start</th>
                                            <th>Date End</th>
                                            <th>Paid by</th>
                                            <th>Amount</th>
                                            <th>Frequency</th>
                                            <th class="text-center">Actions</th>
                                        </tr>
                                    </thead>
                                    <tbody id="add_payments">
                                        {{-- <tr class="gradeX">
                                            <td>
                                                27_05_2019
                                                <input type="hidden" name="payments['+ id +'][start_date]">
                                            </td>
                                            <td>
                                                27_05_2019
                                                <input type="hidden" name="payments['+ id +'][end_date]">
                                            </td>
                                            <td>
                                                paid_by
                                                <input type="hidden" name="payments['+ id +'][paid_by]">
                                            </td>
                                            <td>
                                                amount
                                                <input type="hidden" name="payments['+ id +'][amount]">
                                            </td>
                                            <td>
                                                frequency
                                                <input type="hidden" name="payments['+ id +'][frequency]">
                                            </td>
                                            <td class="text-center">
                                                <a href="#" class="btn btn-danger btn-sm btn-sm-custom btn-custom-danger btn-delete" onclick="destroy_payment(this);">
                                                    <i class="fa fa-trash-o" aria-hidden="true"></i>
                                                </a>
                                            </td>
                                        </tr> --}}
                                    </tbody>
                                </table>
                            </div>
                        </section>
                    </div>


                    <div class="col-md-12 col-xl-6">
                        {{-- Withdrawal Panel --}}
                        <section class="panel">
                            <header class="panel-heading bg-danger">
                                <h2 class="panel-title">Withdrawal</h2>
                                <a class="btn btn-dark btn-custom btn-sm header-btn" data-toggle="modal" data-target="#addWithdrawalModal">Add Withdrawal</a>
                            </header>
                            <div class="panel-body">
                                <table class="table table-bordered table-striped mb-none">
                                    <thead>
                                        <tr>
                                            <th>Date Start</th>
                                            <th>Date End</th>
                                            <th>Paid by</th>
                                            <th>Amount</th>
                                            <th>Frequency</th>
                                            <th class="text-center">Actions</th>
                                        </tr>
                                    </thead>
                                    <tbody id="add_withdrawals">
                                        {{-- <tr class="gradeX">
                                            <td>
                                                27_05_2019
                                                <input type="hidden" name="withdrawals['+ id +'][start_date]">
                                            </td>
                                            <td>
                                                27_05_2019
                                                <input type="hidden" name="withdrawals['+ id +'][end_date]">
                                            </td>
                                            <td>
                                                paid_by
                                                <input type="hidden" name="withdrawals['+ id +'][paid_by]">
                                            </td>
                                            <td>
                                                amount
                                                <input type="hidden" name="withdrawals['+ id +'][amount]">
                                            </td>
                                            <td>
                                                frequency
                                                <input type="hidden" name="withdrawals['+ id +'][frequency]">
                                            </td>
                                            <td>
                                                paid_unpaid
                                                <input type="hidden" name="withdrawals['+ id +'][paid_unpaid]">
                                            </td>
                                            <td class="text-center">
                                                <a href="#" class="btn btn-danger btn-sm btn-sm-custom btn-custom-danger btn-delete" onclick="destroy_withdrawal(this);">
                                                    <i class="fa fa-trash-o" aria-hidden="true"></i>
                                                </a>
                                            </td>
                                        </tr> --}}
                                    </tbody>
                                </table>
                            </div>
                        </section>
                    </div>
                </div>
            </div>
        </div>

        <div class="panel-footer">
            <ul class="pager">
                <li class="previous disabled pull-left">
                    <a href="#" class="btn btn-dark btn-custom btn-sm">
                        <i class="fa fa-angle-left"></i> Previous
                    </a>
                </li>
                <li class="finish hidden pull-right">
                    <button type="submit" class="btn btn-dark btn-custom btn-sm submit-btn">
                        Submit
                    </button>
                </li>
                <li class="next">
                    <a href="#" class="btn btn-dark btn-custom btn-sm">
                        Next <i class="fa fa-angle-right"></i>
                    </a>
                </li>
            </ul>
        </div>

    </form>
</section>
<!-- end: page -->



{{-- ========================< Provider request Modal Starts >======================= --}}
<div class="modal fade" id="providerModal" tabindex="-1" role="dialog" aria-labelledby="gridSystemModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="gridSystemModalLabel"><b>Request For New Provider</b></h4>
            </div>

            <form action="{{ route('director.business.add_new_policy.request_for_provider') }}" method="POST">
                @csrf
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-12">
                            <label class="control-label"><b>Provider Name</b> <span class="required">*</span></label>
                            <input type="text" autocomplete="off" name="provider_name" class="form-control  @error('provider_name') is-invalid @enderror" placeholder="eg.: Jhon Doe" required/>

                            {{-- Validation Alert Messages --}}
                            @error('provider_name')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>

                        <div class="col-md-12">
                            <label class="control-label"><b>Email</b> <span class="required">*</span></label>
                            <input type="email" name="email" class="form-control  @error('email') is-invalid @enderror" placeholder="eg.: jhon_doe@mail.com" required/>

                            {{-- Validation Alert Messages --}}
                            @error('email')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>

                        <div class="col-md-12">
                            <div class="form-group">
                                <label class="control-label"><b>County</b> <span class="required">*</span></label>
                                <select name="county" data-plugin-selectTwo class="form-control populate  @error('county') is-invalid @enderror" required>
                                    <option>Please Select</option>
                                    <optgroup label="England">
                                        <option value="bath-and-north-east-somerset">Bath and North East Somerset</option>
                                        <option value="bedford">Bedford</option>
                                        <option value="berkshire">Berkshire</option>
                                        <option value="blackburn-with-darwen">Blackburn with Darwen</option>
                                        <option value="blackpool">Blackpool</option>
                                        <option value="bournemouth">Bournemouth</option>
                                        <option value="brighton-&amp;-hove">Brighton &amp; Hove</option>
                                        <option value="bristol">Bristol</option>
                                        <option value="buckinghamshire">Buckinghamshire</option>
                                        <option value="cambridgeshire">Cambridgeshire</option>
                                        <option value="central-bedfordshire">Central Bedfordshire</option>
                                        <option value="cheshire-east">Cheshire East</option>
                                        <option value="cheshire-west-and-chester">Cheshire West and Chester</option>
                                        <option value="cornwall">Cornwall</option>
                                        <option value="cumbria">Cumbria</option>
                                        <option value="darlington">Darlington</option>
                                        <option value="derby">Derby</option>
                                        <option value="derbyshire">Derbyshire</option>
                                        <option value="devon">Devon</option>
                                        <option value="dorset">Dorset</option>
                                        <option value="durham">Durham</option>
                                        <option value="east-riding-of-yorkshire">East Riding of Yorkshire</option>
                                        <option value="east-sussex">East Sussex</option>
                                        <option value="essex">Essex</option>
                                        <option value="gloucestershire">Gloucestershire</option>
                                        <option value="greater-london">Greater London</option>
                                        <option value="greater-manchester">Greater Manchester</option>
                                        <option value="halton">Halton</option>
                                        <option value="hampshire">Hampshire</option>
                                        <option value="hartlepool">Hartlepool</option>
                                        <option value="herefordshire">Herefordshire</option>
                                        <option value="hertfordshire">Hertfordshire</option>
                                        <option value="isle-of-wight">Isle of Wight</option>
                                        <option value="kent">Kent</option>
                                        <option value="kingston-upon-hull">Kingston upon Hull</option>
                                        <option value="lancashire">Lancashire</option>
                                        <option value="leicester">Leicester</option>
                                        <option value="leicestershire">Leicestershire</option>
                                        <option value="lincolnshire">Lincolnshire</option>
                                        <option value="luton">Luton</option>
                                        <option value="medway">Medway</option>
                                        <option value="merseyside">Merseyside</option>
                                        <option value="middlesbrough">Middlesbrough</option>
                                        <option value="milton-keynes">Milton Keynes</option>
                                        <option value="norfolk">Norfolk</option>
                                        <option value="north-east-lincolnshire">North East Lincolnshire</option>
                                        <option value="north-lincolnshire">North Lincolnshire</option>
                                        <option value="north-somerset">North Somerset</option>
                                        <option value="north-yorkshire">North Yorkshire</option>
                                        <option value="northamptonshire">Northamptonshire</option>
                                        <option value="northumberland">Northumberland</option>
                                        <option value="nottingham">Nottingham</option>
                                        <option value="nottinghamshire">Nottinghamshire</option>
                                        <option value="oxfordshire">Oxfordshire</option>
                                        <option value="peterborough">Peterborough</option>
                                        <option value="plymouth">Plymouth</option>
                                        <option value="poole">Poole</option>
                                        <option value="portsmouth">Portsmouth</option>
                                        <option value="redcar-and-cleveland">Redcar and Cleveland</option>
                                        <option value="rutland">Rutland</option>
                                        <option value="shropshire">Shropshire</option>
                                        <option value="somerset">Somerset</option>
                                        <option value="south-gloucestershire">South Gloucestershire</option>
                                        <option value="south-yorkshire">South Yorkshire</option>
                                        <option value="southampton">Southampton</option>
                                        <option value="southend-on-sea">Southend-on-Sea</option>
                                        <option value="staffordshire">Staffordshire</option>
                                        <option value="stockton-on-tees">Stockton-on-Tees</option>
                                        <option value="stoke-on-trent">Stoke-on-Trent</option>
                                        <option value="suffolk">Suffolk</option>
                                        <option value="surrey">Surrey</option>
                                        <option value="swindon">Swindon</option>
                                        <option value="telford-and-wrekin">Telford and Wrekin</option>
                                        <option value="thurrock">Thurrock</option>
                                        <option value="torbay">Torbay</option>
                                        <option value="tyne-and-wear">Tyne and Wear</option>
                                        <option value="warrington">Warrington</option>
                                        <option value="warwickshire">Warwickshire</option>
                                        <option value="west-midlands">West Midlands</option>
                                        <option value="west-sussex">West Sussex</option>
                                        <option value="west-yorkshire">West Yorkshire</option>
                                        <option value="wiltshire">Wiltshire</option>
                                        <option value="worcestershire">Worcestershire</option>
                                        <option value="york">York</option>
                                    </optgroup>
                                    <optgroup label="Scotland">
                                        <option value="aberdeen">Aberdeen</option>
                                        <option value="aberdeenshire">Aberdeenshire</option>
                                        <option value="angus">Angus</option>
                                        <option value="argyll-and-bute">Argyll and Bute</option>
                                        <option value="ayrshire-and-arran">Ayrshire and Arran</option>
                                        <option value="banffshire">Banffshire</option>
                                        <option value="berwickshire">Berwickshire</option>
                                        <option value="caithness">Caithness</option>
                                        <option value="clackmannanshire">Clackmannanshire</option>
                                        <option value="dumfries">Dumfries</option>
                                        <option value="dunbartonshire">Dunbartonshire</option>
                                        <option value="dundee">Dundee</option>
                                        <option value="east-lothian">East Lothian</option>
                                        <option value="edinburgh">Edinburgh</option>
                                        <option value="fife">Fife</option>
                                        <option value="glasgow">Glasgow</option>
                                        <option value="inverness">Inverness</option>
                                        <option value="kincardineshire">Kincardineshire</option>
                                        <option value="lanarkshire">Lanarkshire</option>
                                        <option value="midlothian">Midlothian</option>
                                        <option value="moray">Moray</option>
                                        <option value="nairn">Nairn</option>
                                        <option value="orkney-islands">Orkney Islands</option>
                                        <option value="perth-and-kinross">Perth and Kinross</option>
                                        <option value="renfrewshire">Renfrewshire</option>
                                        <option value="ross-and-cromarty">Ross and Cromarty</option>
                                        <option value="roxburgh,-ettrick-and-lauderdale">Roxburgh, Ettrick and Lauderdale</option>
                                        <option value="shetland-islands">Shetland Islands</option>
                                        <option value="stirling-and-falkirk">Stirling and Falkirk</option>
                                        <option value="sutherland">Sutherland</option>
                                        <option value="the-stewartry-of-kirkcudbright">The Stewartry of Kirkcudbright</option>
                                        <option value="tweeddale">Tweeddale</option>
                                        <option value="west-lothian">West Lothian</option>
                                        <option value="western-isles">Western Isles</option>
                                        <option value="wigtown">Wigtown</option>
                                    </optgroup>
                                    <optgroup label="Wales">
                                        <option value="blaenau-gwent">Blaenau Gwent</option>
                                        <option value="bridgend">Bridgend</option>
                                        <option value="caerphilly">Caerphilly</option>
                                        <option value="cardiff">Cardiff</option>
                                        <option value="carmarthenshire">Carmarthenshire</option>
                                        <option value="ceredigion">Ceredigion</option>
                                        <option value="conwy">Conwy</option>
                                        <option value="denbighshire">Denbighshire</option>
                                        <option value="flintshire">Flintshire</option>
                                        <option value="gwynedd">gwynedd</option>
                                        <option value="isle-of-anglesey">Isle of Anglesey</option>
                                        <option value="merthyr-tydfil">Merthyr Tydfil</option>
                                        <option value="monmouthshire">Monmouthshire</option>
                                        <option value="neath-port-talbot">Neath Port Talbot</option>
                                        <option value="newport">Newport</option>
                                        <option value="pembrokeshire">Pembrokeshire</option>
                                        <option value="powys">Powys</option>
                                        <option value="rhondda-cynon-taf">Rhondda Cynon Taf</option>
                                        <option value="swansea">Swansea</option>
                                        <option value="torfaen">Torfaen</option>
                                        <option value="vale-of-glamorgan">Vale of Glamorgan</option>
                                        <option value="wrexham">Wrexham</option>
                                    </optgroup>
                                    <optgroup label="Northern Ireland">
                                        <option value="antrim">Antrim</option>
                                        <option value="ards">Ards</option>
                                        <option value="armagh">Armagh</option>
                                        <option value="ballymena">Ballymena</option>
                                        <option value="ballymoney">Ballymoney</option>
                                        <option value="banbridge">Banbridge</option>
                                        <option value="belfast">Belfast</option>
                                        <option value="carrickfergus">Carrickfergus</option>
                                        <option value="castlereagh">Castlereagh</option>
                                        <option value="coleraine">Coleraine</option>
                                        <option value="cookstown">Cookstown</option>
                                        <option value="craigavon">Craigavon</option>
                                        <option value="derry">Derry</option>
                                        <option value="down">Down</option>
                                        <option value="dungannon-and-south-tyrone">Dungannon and South Tyrone</option>
                                        <option value="fermanagh">Fermanagh</option>
                                        <option value="larne">Larne</option>
                                        <option value="limavady">Limavady</option>
                                        <option value="lisburn">Lisburn</option>
                                        <option value="magherafelt">Magherafelt</option>
                                        <option value="moyle">Moyle</option>
                                        <option value="newry-and-mourne">Newry and Mourne</option>
                                        <option value="newtownabbey">Newtownabbey</option>
                                        <option value="north-down">North Down</option>
                                        <option value="omagh">Omagh</option>
                                        <option value="strabane">Strabane</option>
                                    </optgroup>
                                    <optgroup label="Ireland">
                                        <option value="carlow">Carlow</option>
                                        <option value="cavan">Cavan</option>
                                        <option value="clare">Clare</option>
                                        <option value="cork">Cork</option>
                                        <option value="donegal">Donegal</option>
                                        <option value="dublin">Dublin</option>
                                        <option value="dun-laoghaire-rathdown">Dún Laoghaire-Rathdown</option>
                                        <option value="fingal">Fingal</option>
                                        <option value="galway">Galway</option>
                                        <option value="kerry">Kerry</option>
                                        <option value="kildare">Kildare</option>
                                        <option value="kilkenny">Kilkenny</option>
                                        <option value="laois">Laois</option>
                                        <option value="leitrim">Leitrim</option>
                                        <option value="limerick">Limerick</option>
                                        <option value="longford">Longford</option>
                                        <option value="louth">Louth</option>
                                        <option value="mayo">Mayo</option>
                                        <option value="meath">Meath</option>
                                        <option value="monaghan">Monaghan</option>
                                        <option value="offaly">Offaly</option>
                                        <option value="roscommon">Roscommon</option>
                                        <option value="sligo">Sligo</option>
                                        <option value="south-dublin">South Dublin</option>
                                        <option value="tipperary">Tipperary</option>
                                        <option value="waterford">Waterford</option>
                                        <option value="westmeath">Westmeath</option>
                                        <option value="wexford">Wexford</option>
                                        <option value="wicklow">Wicklow</option>
                                    </optgroup>
                                </select>

                                {{-- Validation Alert Messages --}}
                                @error('county')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="col-md-12">
                            <label class="control-label"><b>Postal Code</b> <span class="required">*</span></label>
                            <input type="text" autocomplete="off" name="postal_code" class="form-control  @error('postal_code') is-invalid @enderror" placeholder="eg.: 112233" required/>

                            {{-- Validation Alert Messages --}}
                            @error('postal_code')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>

                        <div class="col-md-12">
                            <div class="form-group">
                                <label class="control-label"><b>Address</b> <span class="required">*</span></label>
                                <textarea name="address" rows="3" class="form-control  @error('address') is-invalid @enderror" placeholder="eg.: Jhon Doe Address" required></textarea>

                                {{-- Validation Alert Messages --}}
                                @error('address')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="col-md-12">
                                <label class="control-label"><b>Note</b></label>
                                <input type="text" autocomplete="off" name="note" class="form-control  @error('note') is-invalid @enderror" placeholder="eg.: 112233" required/>

                                {{-- Validation Alert Messages --}}
                                @error('note')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                    </div>
                </div>

                <div class="modal-footer text-right">
                    <button type="submit" class="btn btn-dark btn-custom btn-sm">Request Now</button>
                </div>
            </form>

        </div>
    </div>
</div>
{{-- =========================< Provider request Modal Ends >======================== --}}



{{-- ========================< Trust Modal Starts >======================= --}}
<div class="modal fade" id="trustModal" tabindex="-1" role="dialog" aria-labelledby="gridSystemModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="gridSystemModalLabel"><b>New Trust</b></h4>
            </div>

            <form action="{{ route('director.business.add_new_policy.request_for_trust') }}" method="POST">
                @csrf
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-12">
                            <label class="control-label"><b>Trust Name</b> <span class="required">*</span></label>
                            <input type="text" autocomplete="off" name="trust_name" class="form-control  @error('trust_name') is-invalid @enderror" placeholder="eg.: Jhon Doe" required/>

                            {{-- Validation Alert Messages --}}
                            @error('trust_name')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>

                        <div class="col-md-12 text-center">
                            <h2><b>Trustee</b></h2>
                            <hr>
                        </div>

                        <div class="col-md-12">
                            <label class="control-label"><b>Forename</b> <span class="required">*</span></label>
                            <input type="text" autocomplete="off" name="forename" class="form-control  @error('forename') is-invalid @enderror" placeholder="eg.: Jhon" required/>

                            {{-- Validation Alert Messages --}}
                            @error('forename')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>

                        <div class="col-md-12">
                            <label class="control-label"><b>Surname</b> <span class="required">*</span></label>
                            <input type="text" autocomplete="off" name="surname" class="form-control  @error('surname') is-invalid @enderror" placeholder="eg.: Doe" required/>

                            {{-- Validation Alert Messages --}}
                            @error('surname')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>

                        <div class="col-md-12">
                            <label class="control-label"><b>Contact Number</b> <span class="required">*</span></label>
                            <input type="text" name="contact_number" class="form-control  @error('contact_number') is-invalid @enderror" placeholder="eg.: 123456789" required/>

                            {{-- Validation Alert Messages --}}
                            @error('contact_number')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>

                        <div class="col-md-12">
                            <label class="control-label"><b>County</b> <span class="required">*</span></label>
                            <select name="county" data-plugin-selectTwo class="form-control populate  @error('county') is-invalid @enderror" required>
                                <option>Please Select</option>
                                <optgroup label="England">
                                    <option value="bath-and-north-east-somerset">Bath and North East Somerset</option>
                                    <option value="bedford">Bedford</option>
                                    <option value="berkshire">Berkshire</option>
                                    <option value="blackburn-with-darwen">Blackburn with Darwen</option>
                                    <option value="blackpool">Blackpool</option>
                                    <option value="bournemouth">Bournemouth</option>
                                    <option value="brighton-&amp;-hove">Brighton &amp; Hove</option>
                                    <option value="bristol">Bristol</option>
                                    <option value="buckinghamshire">Buckinghamshire</option>
                                    <option value="cambridgeshire">Cambridgeshire</option>
                                    <option value="central-bedfordshire">Central Bedfordshire</option>
                                    <option value="cheshire-east">Cheshire East</option>
                                    <option value="cheshire-west-and-chester">Cheshire West and Chester</option>
                                    <option value="cornwall">Cornwall</option>
                                    <option value="cumbria">Cumbria</option>
                                    <option value="darlington">Darlington</option>
                                    <option value="derby">Derby</option>
                                    <option value="derbyshire">Derbyshire</option>
                                    <option value="devon">Devon</option>
                                    <option value="dorset">Dorset</option>
                                    <option value="durham">Durham</option>
                                    <option value="east-riding-of-yorkshire">East Riding of Yorkshire</option>
                                    <option value="east-sussex">East Sussex</option>
                                    <option value="essex">Essex</option>
                                    <option value="gloucestershire">Gloucestershire</option>
                                    <option value="greater-london">Greater London</option>
                                    <option value="greater-manchester">Greater Manchester</option>
                                    <option value="halton">Halton</option>
                                    <option value="hampshire">Hampshire</option>
                                    <option value="hartlepool">Hartlepool</option>
                                    <option value="herefordshire">Herefordshire</option>
                                    <option value="hertfordshire">Hertfordshire</option>
                                    <option value="isle-of-wight">Isle of Wight</option>
                                    <option value="kent">Kent</option>
                                    <option value="kingston-upon-hull">Kingston upon Hull</option>
                                    <option value="lancashire">Lancashire</option>
                                    <option value="leicester">Leicester</option>
                                    <option value="leicestershire">Leicestershire</option>
                                    <option value="lincolnshire">Lincolnshire</option>
                                    <option value="luton">Luton</option>
                                    <option value="medway">Medway</option>
                                    <option value="merseyside">Merseyside</option>
                                    <option value="middlesbrough">Middlesbrough</option>
                                    <option value="milton-keynes">Milton Keynes</option>
                                    <option value="norfolk">Norfolk</option>
                                    <option value="north-east-lincolnshire">North East Lincolnshire</option>
                                    <option value="north-lincolnshire">North Lincolnshire</option>
                                    <option value="north-somerset">North Somerset</option>
                                    <option value="north-yorkshire">North Yorkshire</option>
                                    <option value="northamptonshire">Northamptonshire</option>
                                    <option value="northumberland">Northumberland</option>
                                    <option value="nottingham">Nottingham</option>
                                    <option value="nottinghamshire">Nottinghamshire</option>
                                    <option value="oxfordshire">Oxfordshire</option>
                                    <option value="peterborough">Peterborough</option>
                                    <option value="plymouth">Plymouth</option>
                                    <option value="poole">Poole</option>
                                    <option value="portsmouth">Portsmouth</option>
                                    <option value="redcar-and-cleveland">Redcar and Cleveland</option>
                                    <option value="rutland">Rutland</option>
                                    <option value="shropshire">Shropshire</option>
                                    <option value="somerset">Somerset</option>
                                    <option value="south-gloucestershire">South Gloucestershire</option>
                                    <option value="south-yorkshire">South Yorkshire</option>
                                    <option value="southampton">Southampton</option>
                                    <option value="southend-on-sea">Southend-on-Sea</option>
                                    <option value="staffordshire">Staffordshire</option>
                                    <option value="stockton-on-tees">Stockton-on-Tees</option>
                                    <option value="stoke-on-trent">Stoke-on-Trent</option>
                                    <option value="suffolk">Suffolk</option>
                                    <option value="surrey">Surrey</option>
                                    <option value="swindon">Swindon</option>
                                    <option value="telford-and-wrekin">Telford and Wrekin</option>
                                    <option value="thurrock">Thurrock</option>
                                    <option value="torbay">Torbay</option>
                                    <option value="tyne-and-wear">Tyne and Wear</option>
                                    <option value="warrington">Warrington</option>
                                    <option value="warwickshire">Warwickshire</option>
                                    <option value="west-midlands">West Midlands</option>
                                    <option value="west-sussex">West Sussex</option>
                                    <option value="west-yorkshire">West Yorkshire</option>
                                    <option value="wiltshire">Wiltshire</option>
                                    <option value="worcestershire">Worcestershire</option>
                                    <option value="york">York</option>
                                </optgroup>
                                <optgroup label="Scotland">
                                    <option value="aberdeen">Aberdeen</option>
                                    <option value="aberdeenshire">Aberdeenshire</option>
                                    <option value="angus">Angus</option>
                                    <option value="argyll-and-bute">Argyll and Bute</option>
                                    <option value="ayrshire-and-arran">Ayrshire and Arran</option>
                                    <option value="banffshire">Banffshire</option>
                                    <option value="berwickshire">Berwickshire</option>
                                    <option value="caithness">Caithness</option>
                                    <option value="clackmannanshire">Clackmannanshire</option>
                                    <option value="dumfries">Dumfries</option>
                                    <option value="dunbartonshire">Dunbartonshire</option>
                                    <option value="dundee">Dundee</option>
                                    <option value="east-lothian">East Lothian</option>
                                    <option value="edinburgh">Edinburgh</option>
                                    <option value="fife">Fife</option>
                                    <option value="glasgow">Glasgow</option>
                                    <option value="inverness">Inverness</option>
                                    <option value="kincardineshire">Kincardineshire</option>
                                    <option value="lanarkshire">Lanarkshire</option>
                                    <option value="midlothian">Midlothian</option>
                                    <option value="moray">Moray</option>
                                    <option value="nairn">Nairn</option>
                                    <option value="orkney-islands">Orkney Islands</option>
                                    <option value="perth-and-kinross">Perth and Kinross</option>
                                    <option value="renfrewshire">Renfrewshire</option>
                                    <option value="ross-and-cromarty">Ross and Cromarty</option>
                                    <option value="roxburgh,-ettrick-and-lauderdale">Roxburgh, Ettrick and Lauderdale</option>
                                    <option value="shetland-islands">Shetland Islands</option>
                                    <option value="stirling-and-falkirk">Stirling and Falkirk</option>
                                    <option value="sutherland">Sutherland</option>
                                    <option value="the-stewartry-of-kirkcudbright">The Stewartry of Kirkcudbright</option>
                                    <option value="tweeddale">Tweeddale</option>
                                    <option value="west-lothian">West Lothian</option>
                                    <option value="western-isles">Western Isles</option>
                                    <option value="wigtown">Wigtown</option>
                                </optgroup>
                                <optgroup label="Wales">
                                    <option value="blaenau-gwent">Blaenau Gwent</option>
                                    <option value="bridgend">Bridgend</option>
                                    <option value="caerphilly">Caerphilly</option>
                                    <option value="cardiff">Cardiff</option>
                                    <option value="carmarthenshire">Carmarthenshire</option>
                                    <option value="ceredigion">Ceredigion</option>
                                    <option value="conwy">Conwy</option>
                                    <option value="denbighshire">Denbighshire</option>
                                    <option value="flintshire">Flintshire</option>
                                    <option value="gwynedd">gwynedd</option>
                                    <option value="isle-of-anglesey">Isle of Anglesey</option>
                                    <option value="merthyr-tydfil">Merthyr Tydfil</option>
                                    <option value="monmouthshire">Monmouthshire</option>
                                    <option value="neath-port-talbot">Neath Port Talbot</option>
                                    <option value="newport">Newport</option>
                                    <option value="pembrokeshire">Pembrokeshire</option>
                                    <option value="powys">Powys</option>
                                    <option value="rhondda-cynon-taf">Rhondda Cynon Taf</option>
                                    <option value="swansea">Swansea</option>
                                    <option value="torfaen">Torfaen</option>
                                    <option value="vale-of-glamorgan">Vale of Glamorgan</option>
                                    <option value="wrexham">Wrexham</option>
                                </optgroup>
                                <optgroup label="Northern Ireland">
                                    <option value="antrim">Antrim</option>
                                    <option value="ards">Ards</option>
                                    <option value="armagh">Armagh</option>
                                    <option value="ballymena">Ballymena</option>
                                    <option value="ballymoney">Ballymoney</option>
                                    <option value="banbridge">Banbridge</option>
                                    <option value="belfast">Belfast</option>
                                    <option value="carrickfergus">Carrickfergus</option>
                                    <option value="castlereagh">Castlereagh</option>
                                    <option value="coleraine">Coleraine</option>
                                    <option value="cookstown">Cookstown</option>
                                    <option value="craigavon">Craigavon</option>
                                    <option value="derry">Derry</option>
                                    <option value="down">Down</option>
                                    <option value="dungannon-and-south-tyrone">Dungannon and South Tyrone</option>
                                    <option value="fermanagh">Fermanagh</option>
                                    <option value="larne">Larne</option>
                                    <option value="limavady">Limavady</option>
                                    <option value="lisburn">Lisburn</option>
                                    <option value="magherafelt">Magherafelt</option>
                                    <option value="moyle">Moyle</option>
                                    <option value="newry-and-mourne">Newry and Mourne</option>
                                    <option value="newtownabbey">Newtownabbey</option>
                                    <option value="north-down">North Down</option>
                                    <option value="omagh">Omagh</option>
                                    <option value="strabane">Strabane</option>
                                </optgroup>
                                <optgroup label="Ireland">
                                    <option value="carlow">Carlow</option>
                                    <option value="cavan">Cavan</option>
                                    <option value="clare">Clare</option>
                                    <option value="cork">Cork</option>
                                    <option value="donegal">Donegal</option>
                                    <option value="dublin">Dublin</option>
                                    <option value="dun-laoghaire-rathdown">Dún Laoghaire-Rathdown</option>
                                    <option value="fingal">Fingal</option>
                                    <option value="galway">Galway</option>
                                    <option value="kerry">Kerry</option>
                                    <option value="kildare">Kildare</option>
                                    <option value="kilkenny">Kilkenny</option>
                                    <option value="laois">Laois</option>
                                    <option value="leitrim">Leitrim</option>
                                    <option value="limerick">Limerick</option>
                                    <option value="longford">Longford</option>
                                    <option value="louth">Louth</option>
                                    <option value="mayo">Mayo</option>
                                    <option value="meath">Meath</option>
                                    <option value="monaghan">Monaghan</option>
                                    <option value="offaly">Offaly</option>
                                    <option value="roscommon">Roscommon</option>
                                    <option value="sligo">Sligo</option>
                                    <option value="south-dublin">South Dublin</option>
                                    <option value="tipperary">Tipperary</option>
                                    <option value="waterford">Waterford</option>
                                    <option value="westmeath">Westmeath</option>
                                    <option value="wexford">Wexford</option>
                                    <option value="wicklow">Wicklow</option>
                                </optgroup>
                            </select>

                            {{-- Validation Alert Messages --}}
                            @error('county')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>

                        <div class="col-md-12">
                            <label class="control-label"><b>Postal Code</b> <span class="required">*</span></label>
                            <input type="text" autocomplete="off" name="postcode" class="form-control  @error('postcode') is-invalid @enderror" placeholder="eg.: 112233" required/>

                            {{-- Validation Alert Messages --}}
                            @error('postcode')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>

                        <div class="col-md-12">
                            <div class="form-group">
                                <label class="control-label"><b>Address</b> <span class="required">*</span></label>
                                <textarea name="address" rows="3" class="form-control  @error('address') is-invalid @enderror" placeholder="eg.: Jhon Doe Address" required></textarea>

                                {{-- Validation Alert Messages --}}
                                @error('address')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="col-md-12">
                            <label class="control-label"><b>Notes</b></label>
                            <input type="text" autocomplete="off" name="note" class="form-control  @error('note') is-invalid @enderror" placeholder="eg.: 112233" required/>

                            {{-- Validation Alert Messages --}}
                            @error('note')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                    </div>
                </div>

                <div class="modal-footer text-right">
                    <button type="submit" class="btn btn-dark btn-custom btn-sm">Request Now</button>
                </div>
            </form>

        </div>
    </div>
</div>
{{-- =========================< Trust Modal Ends >======================== --}}



{{-- ========================< Fund Request Modal Starts >======================= --}}
<div class="modal fade" id="fundModal" tabindex="-1" role="dialog" aria-labelledby="gridSystemModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="gridSystemModalLabel"><b>Request For Fund</b></h4>
            </div>

            <form action="{{ route('director.business.add_new_policy.request_for_fund') }}" method="POST">
                @csrf
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-12">
                            <label class="control-label"><b>Fund Name</b> <span class="required">*</span></label>
                            <input type="text" autocomplete="off" name="fund_name" class="form-control  @error('fund_name') is-invalid @enderror" placeholder="eg.: Fund Name" required/>
                            <small>Please enter your requested fund name and any known codes below ensuring the fund name is 3 characters or greater.</small>

                            {{-- Validation Alert Messages --}}
                            @error('fund_name')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>

                        <div class="col-md-6">
                            <label class="control-label"><b>Fund Mex Code</b></label>
                            <input type="text" autocomplete="off" name="mex_code" class="form-control  @error('mex_code') is-invalid @enderror" placeholder="eg.: Fund Mex Code"/>

                            {{-- Validation Alert Messages --}}
                            @error('mex_code')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>

                        <div class="col-md-6">
                            <label class="control-label"><b>Fund ISIN Code</b></label>
                            <input type="text" autocomplete="off" name="isin_code" class="form-control  @error('isin_code') is-invalid @enderror" placeholder="eg.: Fund ISIN Code"/>

                            {{-- Validation Alert Messages --}}
                            @error('isin_code')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>

                        <div class="col-md-6">
                            <label class="control-label"><b>Fund SEDOL Code</b></label>
                            <input type="text" autocomplete="off" name="sedol_code" class="form-control  @error('sedol_code') is-invalid @enderror" placeholder="eg.: Fund SEDOL Code"/>

                            {{-- Validation Alert Messages --}}
                            @error('sedol_code')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>

                        <div class="col-md-6">
                            <label class="control-label"><b>Fund CITI Code</b></label>
                            <input type="text" autocomplete="off" name="citi_code" class="form-control  @error('citi_code') is-invalid @enderror" placeholder="eg.: Fund CITI Code"/>

                            {{-- Validation Alert Messages --}}
                            @error('citi_code')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>

                        <div class="col-md-12">
                            <label class="control-label"><b>Fund BB Ticker Code</b></label>
                            <input type="text" autocomplete="off" name="bb_ticker_code" class="form-control  @error('bb_ticker_code') is-invalid @enderror" placeholder="eg.: Fund BB Ticker Code"/>

                            {{-- Validation Alert Messages --}}
                            @error('bb_ticker_code')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>

                        <div class="col-md-12">
                            <div class="form-group">
                                <label class="control-label"><b>Notes</b></label>
                                <textarea name="note" rows="3" class="form-control  @error('note') is-invalid @enderror" placeholder="eg.: Describe any Notes"></textarea>
                                <small>Enter a reason (optionally) for the decision of approval/unapproval.</small>

                                {{-- Validation Alert Messages --}}
                                @error('note')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>
                    </div>
                </div>

                <div class="modal-footer text-right">
                    <button type="submit" class="btn btn-dark btn-custom btn-sm">Request Now</button>
                </div>
            </form>

        </div>
    </div>
</div>
{{-- =========================< Fund Request Modal Ends >======================== --}}


{{-- ========================< Fund Add Modal Starts >======================= --}}
<div class="modal fade" id="addFundModal" tabindex="-1" role="dialog" aria-labelledby="gridSystemModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="gridSystemModalLabel"><b>New Fund</b></h4>
            </div>

            <div class="modal-body">
                <div class="row">
                    <div class="col-md-12">
                        <label class="control-label"><b>Fund</b> <span class="required">*</span></label>
                        <select name="fund" id="add_fund_name" data-plugin-selectTwo class="form-control populate @error('fund') is-invalid @enderror" required>
                            <option disabled selected>Select Fund</option>
                            @foreach($funds as $fund)
                                <option value="{{ $fund->fund_name }}">{{ $fund->fund_name }}</option>
                            @endforeach
                        </select>

                        {{-- Validation Alert Messages --}}
                        @error('fund')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                    </div>

                    <div class="col-md-6">
                        <label class="control-label"><b>Invested</b> <span class="required">*</span></label>
                        <input type="number" autocomplete="off" name="invested" id="add_fund_invested" class="form-control  @error('invested') is-invalid @enderror" placeholder="eg.: 2520" required/>

                        {{-- Validation Alert Messages --}}
                        @error('invested')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                    </div>

                    <div class="col-md-6">
                        <label class="control-label"><b>Date</b> <span class="required">*</span></label>
                        <input type="text" autocomplete="off" data-plugin-datepicker="" name="date" id="add_fund_date" class="form-control  @error('date') is-invalid @enderror" placeholder="eg.: dd/mm/yyyy" required>

                        {{-- Validation Alert Messages --}}
                        @error('date')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                    </div>
                </div>
            </div>

            <div class="modal-footer text-right">
                <button id="add_fund_submit" type="button" data-dismiss="modal" aria-label="Close" class="btn btn-dark btn-custom btn-sm">Add Fund</button>
            </div>

        </div>
    </div>
</div>
{{-- =========================< Fund Add Modal Ends >======================== --}}


{{-- ========================< Add Commission Fee Modal Starts >======================= --}}
<div class="modal fade" id="addCommissionFeeModal" tabindex="-1" role="dialog" aria-labelledby="gridSystemModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="gridSystemModalLabel"><b>Add New Commission / Fee</b></h4>
            </div>

            <div class="modal-body">
                <div class="row">
                    <div class="col-md-6">
                        <label class="control-label"><b>Start Date</b> <span class="required">*</span></label>
                        <input type="text" id="add_commission_start_date" autocomplete="off" data-plugin-datepicker="" name="start_date" class="form-control  @error('start_date') is-invalid @enderror" placeholder="eg.: dd/mm/yyyy" required>

                        {{-- Validation Alert Messages --}}
                        @error('start_date')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                    </div>

                    <div class="col-md-6">
                        <label class="control-label"><b>End Date</b> <span class="required">*</span></label>
                        <input type="text" id="add_commission_end_date" autocomplete="off" data-plugin-datepicker="" name="end_date" class="form-control  @error('end_date') is-invalid @enderror" placeholder="eg.: dd/mm/yyyy" required>

                        {{-- Validation Alert Messages --}}
                        @error('end_date')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                    </div>

                    <div class="col-md-6">
                        <label class="control-label"><b>Paid By</b> <span class="required">*</span></label>
                        <select name="paid_by" id="add_commission_paid_by" data-plugin-selectTwo class="form-control populate @error('paid_by') is-invalid @enderror" required>
                            <option disabled selected>Select Paid By</option>
                            <option value="Cheque">Cheque</option>
                            <option value="Direct Debit">Direct Debit</option>
                            <option value="Transfer">Transfer</option>
                            <option value="Other">Other</option>
                        </select>

                        {{-- Validation Alert Messages --}}
                        @error('paid_by')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                    </div>

                    <div class="col-md-6">
                        <label class="control-label"><b>Amount</b> <span class="required">*</span></label>
                        <input type="text" id="add_commission_amount" autocomplete="off" name="amount" class="form-control  @error('amount') is-invalid @enderror" placeholder="eg.: 2000" required/>

                        {{-- Validation Alert Messages --}}
                        @error('amount')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                    </div>

                    <div class="col-md-6">
                        <label class="control-label"><b>Frequency</b> <span class="required">*</span></label>
                        <select name="frequency" id="add_commission_frequency" data-plugin-selectTwo class="form-control populate @error('frequency') is-invalid @enderror" required>
                            <option disabled selected>Select Frequency</option>
                            <option value="Single">Single</option>
                            <option value="Monthly">Monthly</option>
                            <option value="Quaterly">Quaterly</option>
                            <option value="Half-Yearly">Half-Yearly</option>
                            <option value="Annually">Annually</option>
                            <option value="Initial">Initial</option>
                        </select>

                        {{-- Validation Alert Messages --}}
                        @error('frequency')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                    </div>

                    <div class="col-md-6">
                        <label class="control-label"><b>Payment Status</b> <span class="required">*</span></label>
                        <select name="paid_unpaid" id="add_commission_paid_unpaid" data-plugin-selectTwo class="form-control populate @error('paid_unpaid') is-invalid @enderror" required>
                            <option disabled selected>Select Payment Status</option>
                            <option value="Paid">Paid</option>
                            <option value="Unpaid">Unpaid</option>
                        </select>

                        {{-- Validation Alert Messages --}}
                        @error('paid_unpaid')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                    </div>
                </div>
            </div>

            <div class="modal-footer text-right">
                <button type="button" id="add_commission_submit" data-dismiss="modal" aria-label="Close" class="btn btn-dark btn-custom btn-sm">Add Commission/Fee</button>
            </div>

        </div>
    </div>
</div>
{{-- =========================< Add Commission Fee Modal Ends >======================== --}}


{{-- ========================< Payment Modal Starts >======================= --}}
<div class="modal fade" id="addPaymentModal" tabindex="-1" role="dialog" aria-labelledby="gridSystemModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="gridSystemModalLabel"><b>Add Payment</b></h4>
            </div>

            <div class="modal-body">
                <div class="row">
                    <div class="col-md-6">
                        <label class="control-label"><b>Start Date</b> <span class="required">*</span></label>
                        <input type="text" id="add_payment_start_date" autocomplete="off" data-plugin-datepicker="" name="start_date" class="form-control  @error('start_date') is-invalid @enderror" placeholder="eg.: dd/mm/yyyy" required>

                        {{-- Validation Alert Messages --}}
                        @error('start_date')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                    </div>

                    <div class="col-md-6">
                        <label class="control-label"><b>End Date</b> <span class="required">*</span></label>
                        <input type="text" id="add_payment_end_date" autocomplete="off" data-plugin-datepicker="" name="end_date" class="form-control  @error('end_date') is-invalid @enderror" placeholder="eg.: dd/mm/yyyy" required>

                        {{-- Validation Alert Messages --}}
                        @error('end_date')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                    </div>

                    <div class="col-md-6">
                        <label class="control-label"><b>Paid By</b> <span class="required">*</span></label>
                        <select name="paid_by" id="add_payment_paid_by" data-plugin-selectTwo class="form-control populate @error('paid_by') is-invalid @enderror" required>
                            <option disabled selected>Select Paid By</option>
                            <option value="Cheque">Cheque</option>
                            <option value="Direct Debit">Direct Debit</option>
                            <option value="Transfer">Transfer</option>
                            <option value="Other">Other</option>
                        </select>

                        {{-- Validation Alert Messages --}}
                        @error('paid_by')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                    </div>

                    <div class="col-md-6">
                        <label class="control-label"><b>Amount</b> <span class="required">*</span></label>
                        <input type="text" id="add_payment_amount" autocomplete="off" name="amount" class="form-control  @error('amount') is-invalid @enderror" placeholder="eg.: 2000" required/>

                        {{-- Validation Alert Messages --}}
                        @error('amount')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                    </div>

                    <div class="col-md-6">
                        <label class="control-label"><b>Frequency</b> <span class="required">*</span></label>
                        <select name="frequency" id="add_payment_frequency" data-plugin-selectTwo class="form-control populate @error('frequency') is-invalid @enderror" required>
                            <option disabled selected>Select Frequency</option>
                            <option value="Single">Single</option>
                            <option value="Monthly">Monthly</option>
                            <option value="Quaterly">Quaterly</option>
                            <option value="Half-Yearly">Half-Yearly</option>
                            <option value="Annually">Annually</option>
                            <option value="Initial">Initial</option>
                        </select>

                        {{-- Validation Alert Messages --}}
                        @error('frequency')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                    </div>
                </div>
            </div>

            <div class="modal-footer text-right">
                <button type="submit" class="btn btn-dark btn-custom btn-sm" id="add_payment_submit" data-dismiss="modal" aria-label="Close">Add Payment</button>
            </div>

        </div>
    </div>
</div>
{{-- =========================< Payment Modal Ends >======================== --}}








{{-- ========================< Withdrawal Modal Starts >======================= --}}
<div class="modal fade" id="addWithdrawalModal" tabindex="-1" role="dialog" aria-labelledby="gridSystemModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="gridSystemModalLabel"><b>Add Withdrawal</b></h4>
            </div>

            <div class="modal-body">
                <div class="row">
                    <div class="col-md-6">
                        <label class="control-label"><b>Start Date</b> <span class="required">*</span></label>
                        <input type="text" id="add_withdrawal_start_date" autocomplete="off" data-plugin-datepicker="" name="start_date" class="form-control  @error('start_date') is-invalid @enderror" placeholder="eg.: dd/mm/yyyy" required>

                        {{-- Validation Alert Messages --}}
                        @error('start_date')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                    </div>

                    <div class="col-md-6">
                        <label class="control-label"><b>End Date</b> <span class="required">*</span></label>
                        <input type="text" id="add_withdrawal_end_date" autocomplete="off" data-plugin-datepicker="" name="end_date" class="form-control  @error('end_date') is-invalid @enderror" placeholder="eg.: dd/mm/yyyy" required>

                        {{-- Validation Alert Messages --}}
                        @error('end_date')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                    </div>

                    <div class="col-md-6">
                        <label class="control-label"><b>Paid By</b> <span class="required">*</span></label>
                        <select name="paid_by" id="add_withdrawal_paid_by" data-plugin-selectTwo class="form-control populate @error('paid_by') is-invalid @enderror" required>
                            <option disabled selected>Select Paid By</option>
                            <option value="Cheque">Cheque</option>
                            <option value="Direct Debit">Direct Debit</option>
                            <option value="Transfer">Transfer</option>
                            <option value="Other">Other</option>
                        </select>

                        {{-- Validation Alert Messages --}}
                        @error('paid_by')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                    </div>

                    <div class="col-md-6">
                        <label class="control-label"><b>Amount</b> <span class="required">*</span></label>
                        <input type="text" id="add_withdrawal_amount" autocomplete="off" name="amount" class="form-control  @error('amount') is-invalid @enderror" placeholder="eg.: 2000" required/>

                        {{-- Validation Alert Messages --}}
                        @error('amount')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                    </div>

                    <div class="col-md-6">
                        <label class="control-label"><b>Frequency</b> <span class="required">*</span></label>
                        <select name="frequency" id="add_withdrawal_frequency" data-plugin-selectTwo class="form-control populate @error('frequency') is-invalid @enderror" required>
                            <option disabled selected>Select Frequency</option>
                            <option value="Single">Single</option>
                            <option value="Monthly">Monthly</option>
                            <option value="Quaterly">Quaterly</option>
                            <option value="Half-Yearly">Half-Yearly</option>
                            <option value="Annually">Annually</option>
                            <option value="Initial">Initial</option>
                        </select>

                        {{-- Validation Alert Messages --}}
                        @error('frequency')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                    </div>
                </div>
            </div>

            <div class="modal-footer text-right">
                <button type="submit" class="btn btn-dark btn-custom btn-sm" data-dismiss="modal" aria-label="Close" id="add_withdrawal_submit">Add Withdrawal</button>
            </div>

        </div>
    </div>
</div>
{{-- =========================< Withdrawal Modal Ends >======================== --}}

@endsection


@section('script_links')
    {{--  External Javascript  --}}
    <script src="{{ asset('custom/assets/vendor/select2/select2.js') }}"></script>
    <script src="{{ asset('custom/assets/vendor/bootstrap-datepicker/js/bootstrap-datepicker.js') }}"></script>

    <script src="{{ asset('custom/assets/vendor/jquery-validation/jquery.validate.js') }}"></script>
    <script src="{{ asset('custom/assets/vendor/bootstrap-wizard/jquery.bootstrap.wizard.js') }}"></script>
    <script src="{{ asset('custom/assets/javascripts/forms/examples.wizard.js') }}"></script>
@endsection

@section('scripts')
    {{--  External Javascript  --}}

    {{-- Add Fund Row --}}
    <script>
        $('#add_fund_submit').click(function(){
            var fund_name = $('#add_fund_name').children("option:selected").val();
            var fund_invested = $('#add_fund_invested').val();
            var fund_date = $('#add_fund_date').val();

            var id = generateID();

            var fund = '<tr class="gradeX"><td>'+ fund_name +'<input type="hidden" name="funds['+ id +'][fund_name]" value="'+ fund_name +'"></td><td>'+ fund_invested +'<input type="hidden" name="funds['+ id +'][invested]" value="'+ fund_invested +'"></td><td>'+ fund_date +'<input type="hidden" name="funds['+ id +'][date]" value="'+ fund_date +'"></td><td class="text-center"><a onclick="destroy_fund(this);" class="btn btn-danger btn-sm btn-sm-custom btn-custom-danger btn-delete destroy_fund"><i class="fa fa-trash-o" aria-hidden="true"></i></a></td></tr>';

            $('#add_funds_list').append(fund);

        });

        function destroy_fund(that){
            $(that).parent().parent().remove();
        }
    </script>



    {{-- Add Commission Row --}}
    <script>
        $('#add_commission_submit').click(function(){
            var commission_start_date = $('#add_commission_start_date').val();
            var commission_end_date = $('#add_commission_end_date').val();
            var commission_paid_by = $('#add_commission_paid_by').children("option:selected").val();
            var commission_frequency = $('#add_commission_frequency').children("option:selected").val();
            var commission_paid_unpaid = $('#add_commission_paid_unpaid').children("option:selected").val();
            var commission_amount = $('#add_commission_amount').val();

            var id = generateID();

            var commission_tr ='<tr class="gradeX"><td>'+ commission_start_date +'<input type="hidden" name="commissions['+ id +'][start_date]" value="'+ commission_start_date +'"></td><td>'+ commission_end_date +'<input type="hidden" name="commissions['+ id +'][end_date]" value="'+ commission_end_date +'"></td><td>'+ commission_paid_by +'<input type="hidden" name="commissions['+ id +'][paid_by]" value="'+ commission_paid_by +'"></td><td>'+ commission_amount +'<input type="hidden" name="commissions['+ id +'][amount]" value="'+ commission_amount +'"></td><td>'+ commission_frequency +'<input type="hidden" name="commissions['+ id +'][frequency]" value="'+ commission_frequency +'"></td><td>'+ commission_paid_unpaid +'<input type="hidden" name="commissions['+ id +'][paid_unpaid]" value="'+ commission_paid_unpaid +'"></td><td class="text-center"><a href="#" class="btn btn-danger btn-sm btn-sm-custom btn-custom-danger btn-delete" onclick="destroy_commission(this);"><i class="fa fa-trash-o" aria-hidden="true"></i></a></td></tr>';

            $('#add_commissions').append(commission_tr);

        });

        function destroy_commission(that){
            $(that).parent().parent().remove();
        }
    </script>

    {{-- Add Payment Row --}}
    <script>
        $('#add_payment_submit').click(function(){
            var payment_start_date = $('#add_payment_start_date').val();
            var payment_end_date = $('#add_payment_end_date').val();
            var payment_paid_by = $('#add_payment_paid_by').children("option:selected").val();
            var payment_frequency = $('#add_payment_frequency').children("option:selected").val();
            var payment_amount = $('#add_payment_amount').val();


            var id = generateID();

            var payment_tr ='<tr class="gradeX"><td>'+ payment_start_date +'<input type="hidden" name="payments['+ id +'][start_date]" value="'+ payment_start_date +'"></td><td>'+ payment_end_date +'<input type="hidden" name="payments['+ id +'][end_date]" value="'+ payment_end_date +'"></td><td>'+ payment_paid_by +'<input type="hidden" name="payments['+ id +'][paid_by]" value="'+ payment_paid_by +'"></td><td>'+ payment_amount +'<input type="hidden" name="payments['+ id +'][amount]" value="'+ payment_amount +'"></td><td>'+ payment_frequency +'<input type="hidden" name="payments['+ id +'][frequency]" value="'+ payment_frequency +'"></td><td class="text-center"><a href="#" class="btn btn-danger btn-sm btn-sm-custom btn-custom-danger btn-delete" onclick="destroy_payment(this);"><i class="fa fa-trash-o" aria-hidden="true"></i></a></td></tr>';

            $('#add_payments').append(payment_tr);

        });

        function destroy_payment(that){
            $(that).parent().parent().remove();
        }
    </script>



    {{-- Add Withdrawal Row --}}
    <script>
        $('#add_withdrawal_submit').click(function(){
            var withdrawal_start_date = $('#add_withdrawal_start_date').val();
            var withdrawal_end_date = $('#add_withdrawal_end_date').val();
            var withdrawal_paid_by = $('#add_withdrawal_paid_by').children("option:selected").val();
            var withdrawal_frequency = $('#add_withdrawal_frequency').children("option:selected").val();
            var withdrawal_amount = $('#add_withdrawal_amount').val();



            var id = generateID();

            var withdrawal_tr ='<tr class="gradeX"><td>'+ withdrawal_start_date +'<input type="hidden" name="withdrawals['+ id +'][start_date]" value="'+ withdrawal_start_date +'"></td><td>'+ withdrawal_end_date +'<input type="hidden" name="withdrawals['+ id +'][end_date]" value="'+ withdrawal_end_date +'"></td><td>'+ withdrawal_paid_by +'<input type="hidden" name="withdrawals['+ id +'][paid_by]" value="'+ withdrawal_paid_by +'"></td><td>'+ withdrawal_amount +'<input type="hidden" name="withdrawals['+ id +'][amount]" value="'+ withdrawal_amount +'"></td><td>'+ withdrawal_frequency +'<input type="hidden" name="withdrawals['+ id +'][frequency]" value="'+ withdrawal_frequency +'"></td><td class="text-center"><a href="#" class="btn btn-danger btn-sm btn-sm-custom btn-custom-danger btn-delete" onclick="destroy_withdrawal(this);"><i class="fa fa-trash-o" aria-hidden="true"></i></a></td></tr>';

            $('#add_withdrawals').append(withdrawal_tr);

        });

        function destroy_withdrawal(that){
            $(that).parent().parent().remove();
        }
    </script>


    <script>
        function generateID() {
            // Math.random should be unique because of its seeding algorithm.
            // Convert it to base 36 (numbers + letters), and grab the first 9 characters
            // after the decimal.
            return '_' + Math.random().toString(36).substr(2, 9);
        };
    </script>

@endsection



