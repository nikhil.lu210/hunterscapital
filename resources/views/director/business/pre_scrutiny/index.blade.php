@extends('layouts.director.app')

@section('page_title', '| Business | Pre-Scrutiny')

@section('stylesheet_links')
    {{--  External CSS  --}}
    <link rel="stylesheet" href="{{ asset('custom/assets/vendor/select2/select2.css') }}" />
    <link rel="stylesheet" href="{{ asset('custom/assets/vendor/jquery-datatables-bs3/assets/css/datatables.css') }}" />
    <link rel="stylesheet" href="{{ asset('custom/assets/vendor/bootstrap-fileupload/bootstrap-fileupload.min.css') }}" />
@endsection

@section('stylesheet')
    {{--  External CSS  --}}
    <style>
        .dataTables_wrapper .DTTT.btn-group {
            display: none !important;
            position: absolute;
            top: -74px;
            right: 0px;
        }
        
        .member-active{
            background-color: green;
            color: #ffffff;
            padding: 2px 7px;
            border-radius: 3px;
            font-weight: 600;
        }
        .member-inactive{
            background-color: red;
            color: #ffffff;
            padding: 2px 7px;
            border-radius: 3px;
            font-weight: 600;
        }

        .access-granted{
            background-color: green;
            color: #ffffff;
            padding: 2px 7px;
            border-radius: 3px;
            font-weight: 600;
        }
        .access-not-granted{
            background-color: red;
            color: #ffffff;
            padding: 2px 7px;
            border-radius: 3px;
            font-weight: 600;
        }
        .modal-header .close {
            outline: none !important;
            border: 1px solid #8e8e8e;
            padding: 0px 5px;
            border-radius: 0;
            opacity: 1;
            color: #8e8e8e;
            transition: 0.3s all ease-in-out;
        }
        .modal-header .close:hover {
            outline: none !important;
            border: 1px solid #0f1b25;
            background-color: #0f1b25;
            opacity: 1;
            color: #ffffff;
            transition: 0.3s all ease-in-out;
        }
        .modal-title{
            color: #0f1b25;
        }
        .modal-content {
            border: 1px solid #e5e5e5;
            border-radius: 0;
            box-shadow: 0 3px 9px rgba(0, 0, 0, 0.1);
        }
        .btn.btn-dark.btn-custom.btn-block {
            margin: 12px 0px;
            border-radius: 0px;
        }
        @media (min-width: 768px){
            .modal-dialog {
                width: 500px;
                margin: 11% auto;
            }
        }

        .modal-footer {
            padding: 5px 15px;
        }
        .fileupload .uneditable-input .fa {
            top: 38px;
            left: 25px;
        }
    </style>
@endsection

@section('content')

<header class="page-header">
    <h2>Pre-Scrutiny</h2>

    <div class="right-wrapper pull-right">
        <ol class="breadcrumbs">
            <li>
                <a href="{{ Route('director.dashboard.index') }}">
                    <i class="fa fa-home"></i>
                </a>
            </li>
            <li><span class="active">Business</span></li>
            <li><span>Pre-Scrutiny</span></li>
        </ol>
    </div>
</header>

<!-- start: page -->
<section class="panel">
    <header class="panel-heading">
        <h2 class="panel-title">Pre-Scrutiny</h2>
        <button class="btn btn-dark btn-custom btn-sm header-btn" data-toggle="modal" data-target="#uploadModal">Upload File For Pre-Scrutiny</button>
    </header>
    <div class="panel-body">
        <table class="table table-bordered table-striped mb-none" id="datatable-tabletools" data-swf-path="assets/vendor/jquery-datatables/extras/TableTools/swf/copy_csv_xls_pdf.swf">
            <thead>
                <tr class="table-header-row">
                    <th>Sl.</th>
                    <th>IFA Name</th>
                    <th>Client</th>
                    <th>Business Type <sup>(Specific)</sup></th>
                    <th class="text-center">File</th>
                    <th>Upload Date</th>
                    <th>Work Status</th>
                    <th class="text-center">Amended File</th>
                    <th>Amended Date</th>
                    <th class="text-center">Actions</th>
                </tr>
            </thead>
            <tbody>
                <tr class="table-body-row">
                    <th>01</th>
                    <th>ifa_name</th>
                    <th>client_full_name</th>
                    <td>business_type</td>
                    <td class="text-center">
                        <a href="#" class="btn btn-dark btn-sm btn-sm-custom btn-custom-info btn-download">
                            <i class="fa fa-cloud-download"></i>
                        </a>
                    </td>
                    <td>12_02_2019</td>
                    <td>work_status</td>
                    <td class="text-center">
                        <a href="#" class="btn btn-dark btn-sm btn-sm-custom btn-custom-info btn-download">
                            <i class="fa fa-cloud-upload"></i>
                        </a>
                    </td>
                    <td>12_02_2019</td>
                    <td class="action-td text-center">
                        {{-- <a href="#" class="btn btn-dark btn-sm btn-sm-custom btn-custom-info">
                            <i class="fa fa-info"></i>
                        </a> --}}
                        <a href="#" class="btn btn-danger btn-sm btn-sm-custom btn-custom-danger btn-delete" onclick="return confirm('Are You Sure Want To Delete This Document...?')">
                            <i class="fa fa-trash"></i>
                        </a>
                    </td>
                </tr>
            </tbody>
        </table>
    </div>
</section>
<!-- end: page -->



{{-- ========================< Modal Starts >======================= --}}
<div class="modal fade" id="uploadModal" tabindex="-1" role="dialog" aria-labelledby="gridSystemModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="gridSystemModalLabel"><b>Add New Pre-Scrutiny File</b></h4>
            </div>

            <form action="#" method="POST">
                @csrf
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-12">
                            <label class="control-label">Document <span class="required">*</span></label>
                            <div class="fileupload fileupload-new" data-provides="fileupload">
                                <div class="input-append">
                                    <div class="uneditable-input">
                                        <i class="fa fa-file fileupload-exists"></i>
                                        <span class="fileupload-preview"></span>
                                    </div>
                                    <span class="btn btn-default btn-file">
                                        <span class="fileupload-exists">Change</span>
                                        <span class="fileupload-new">Select file</span>
                                        <input type="file" class="@error('document') is-invalid @enderror" name="document"/>
                                    </span>
                                    <a href="#" class="btn btn-default fileupload-exists" data-dismiss="fileupload">Remove</a>
                                </div>
                            </div>
                
                            {{-- Validation Alert Messages --}}
                            @error('document')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                        
                        <div class="col-md-12">
                            <label class="control-label"><b>Adviser</b> <span class="required">*</span></label>
                            <select name="adviser_id" data-plugin-selectTwo class="form-control populate @error('adviser_id') is-invalid @enderror" required>
                                <option disabled selected>Select Adviser</option>
                                <option value="adviser_id">adviser_name</option>
                            </select>

                            {{-- Validation Alert Messages --}}
                            @error('adviser_id')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                        
                        <div class="col-md-12">
                            <label class="control-label"><b>Client</b> <span class="required">*</span></label>
                            <select name="client_id" data-plugin-selectTwo class="form-control populate @error('client_id') is-invalid @enderror" required>
                                <option disabled selected>Select Client</option>
                                <option value="client_id">client_name</option>
                            </select>
                            <small class="pull-right">
                                <a href="{{ route('director.client.create') }}" target="_blank">
                                    <b>Add New Client</b>
                                </a>
                            </small>

                            {{-- Validation Alert Messages --}}
                            @error('client_id')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                        
                        <div class="col-md-12">
                            <label class="control-label"><b>Specific Business Type</b> <span class="required">*</span></label>
                            <select name="specific_business_type" data-plugin-selectTwo class="form-control populate @error('specific_business_type') is-invalid @enderror" required>
                                <option disabled selected>Select Specific Business Type</option>
                                <option value="specific_business_type">specific_business_type</option>
                            </select>

                            {{-- Validation Alert Messages --}}
                            @error('specific_business_type')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                    </div>
                </div>

                <div class="modal-footer text-right">
                    <button type="submit" class="btn btn-dark btn-custom btn-sm">Add Now</button>
                </div>
            </form>

        </div>
    </div>
</div>
{{-- =========================< Modal Ends >======================== --}}

@endsection


@section('script_links')
    {{--  External Javascript  --}}
    <script src="{{ asset('custom/assets/vendor/select2/select2.js') }}"></script>
    <script src="{{ asset('custom/assets/vendor/jquery-datatables/media/js/jquery.dataTables.js') }}"></script>
    <script src="{{ asset('custom/assets/vendor/jquery-datatables/extras/TableTools/js/dataTables.tableTools.min.js') }}"></script>
    <script src="{{ asset('custom/assets/vendor/jquery-datatables-bs3/assets/js/datatables.js') }}"></script>

    <script src="{{ asset('custom/assets/javascripts/tables/examples.datatables.default.js') }}"></script>
    <script src="{{ asset('custom/assets/javascripts/tables/examples.datatables.row.with.details.js') }}"></script>
    <script src="{{ asset('custom/assets/javascripts/tables/examples.datatables.tabletools.js') }}"></script>

    <script src="{{ asset('custom/assets/vendor/bootstrap-fileupload/bootstrap-fileupload.min.js') }}"></script>

@endsection

@section('scripts')
    {{--  External Javascript  --}}

@endsection
