@extends('layouts.director.app')

@section('page_title', '| Dashboard')

@section('stylesheet_links')
    {{--  External CSS  --}}

@endsection

@section('stylesheet')
    {{--  External CSS  --}}

@endsection

@section('content')

<header class="page-header">
    <h2>Dashboard</h2>

    <div class="right-wrapper pull-right">
        <ol class="breadcrumbs">
            <li>
                <a href="#">
                    <i class="fa fa-home"></i>
                </a>
            </li>
            <li><span>Dashboard</span></li>
        </ol>
    </div>
</header>

<!-- start: page -->
<div class="row">
    <div class="col-md-12 col-lg-6 col-xl-3">
        <section class="panel panel-featured-left panel-featured-warning">
            <div class="panel-body">
                <div class="widget-summary">
                    <div class="widget-summary-col widget-summary-col-icon">
                        <div class="summary-icon bg-warning">
                            <i class="fas fa-money"></i>
                        </div>
                    </div>
                    <div class="widget-summary-col">
                        <div class="summary">
                            <h4 class="title"><b>Total Businesses</b></h4>
                            <div class="info">
                                <strong class="amount">{{ $all_business }}</strong>
                            </div>
                        </div>
                        <div class="summary-footer">
                            <span class="text-warning">*Total Businesses of Our Company</span>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>

    <div class="col-md-12 col-lg-6 col-xl-3">
        <section class="panel panel-featured-left panel-featured-primary">
            <div class="panel-body">
                <div class="widget-summary">
                    <div class="widget-summary-col widget-summary-col-icon">
                        <div class="summary-icon bg-primary">
                            <i class="fab fa-battle-net"></i>
                        </div>
                    </div>
                    <div class="widget-summary-col">
                        <div class="summary">
                            <h4 class="title"><b>My Businesses</b></h4>
                            <div class="info">
                                <strong class="amount">{{ $my_business }}</strong>
                            </div>
                        </div>
                        <div class="summary-footer">
                            <span class="text-primary">*My Total Businesses</span>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>

    <div class="col-md-12 col-lg-6 col-xl-3">
        <section class="panel panel-featured-left panel-featured-success">
            <div class="panel-body">
                <div class="widget-summary">
                    <div class="widget-summary-col widget-summary-col-icon">
                        <div class="summary-icon bg-success">
                            <i class="fas fa-users"></i>
                        </div>
                    </div>
                    <div class="widget-summary-col">
                        <div class="summary">
                            <h4 class="title"><b>Total Clients</b></h4>
                            <div class="info">
                                <strong class="amount">{{ $all_clients }}</strong>
                            </div>
                        </div>
                        <div class="summary-footer">
                            <span class="text-success">*Total Clients of our Company</span>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>

    <div class="col-md-12 col-lg-6 col-xl-3">
        <section class="panel panel-featured-left panel-featured-danger">
            <div class="panel-body">
                <div class="widget-summary">
                    <div class="widget-summary-col widget-summary-col-icon">
                        <div class="summary-icon bg-danger">
                            <i class="fas fa-bezier-curve"></i>
                        </div>
                    </div>
                    <div class="widget-summary-col">
                        <div class="summary">
                            <h4 class="title"><b>My Clients</b></h4>
                            <div class="info">
                                <strong class="amount">{{ $my_clients }}</strong>
                            </div>
                        </div>
                        <div class="summary-footer">
                            <span class="text-danger">*Total Clients under me</span>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
</div>
<!-- end: page -->

@endsection


@section('script_links')
    {{--  External Javascript  --}}

@endsection

@section('scripts')
    {{--  External Javascript  --}}

    <script>
        $( document ).ready(function() {
            $.ajax({
                method: 'GET',
                url: "{{ route('director.dashboard.mailsend') }}" ,
                datatype: "JSON",
                success: function(data) {
                },
                error: function() {
                    console.log("Did not work on it.");
                }
            });

        });
    </script>

@endsection
