@extends('layouts.director.app')

@section('page_title', '| Clients')

@section('stylesheet_links')
    {{--  External CSS  --}}
    <link rel="stylesheet" href="{{ asset('custom/assets/vendor/select2/select2.css') }}" />
    <link rel="stylesheet" href="{{ asset('custom/assets/vendor/jquery-datatables-bs3/assets/css/datatables.css') }}" />

@endsection

@section('stylesheet')
    {{--  External CSS  --}}
    <style>
        .dataTables_wrapper .DTTT.btn-group {
            display: inline-block;
            position: absolute;
            top: -74px;
            right: 0px;
        }

        .member-active{
            background-color: green;
            color: #ffffff;
            padding: 2px 7px;
            border-radius: 3px;
            font-weight: 600;
        }
        .member-inactive{
            background-color: red;
            color: #ffffff;
            padding: 2px 7px;
            border-radius: 3px;
            font-weight: 600;
        }

        .silver{
            background-color: silver;
            color: #000000;
            padding: 2px 7px;
            border-radius: 3px;
            font-weight: 600;
        }
        .bronze{
            background-color: #ab4700;
            color: #ffffff;
            padding: 2px 7px;
            border-radius: 3px;
            font-weight: 600;
        }
        .gold{
            background-color: #ff7c1f;
            color: #ffffff;
            padding: 2px 7px;
            border-radius: 3px;
            font-weight: 600;
        }
        .platinum{
            background-color: #a0bfb4;
            color: #000000;
            padding: 2px 7px;
            border-radius: 3px;
            font-weight: 600;
        }

        .access-granted{
            background-color: green;
            color: #ffffff;
            padding: 2px 7px;
            border-radius: 3px;
            font-weight: 600;
        }
        .access-not-granted{
            background-color: red;
            color: #ffffff;
            padding: 2px 7px;
            border-radius: 3px;
            font-weight: 600;
        }
    </style>
@endsection

@section('content')

<header class="page-header">
    <h2>Client Access</h2>

    <div class="right-wrapper pull-right">
        <ol class="breadcrumbs">
            <li>
                <a href="{{ Route('director.dashboard.index') }}">
                    <i class="fa fa-home"></i>
                </a>
            </li>
            <li><span class="active">Clients</span></li>
            <li><span>Client Access</span></li>
        </ol>
    </div>
</header>

<!-- start: page -->
<section class="panel">
    <header class="panel-heading">
        <h2 class="panel-title">My Client Lists</h2>
    </header>
    <div class="panel-body">
        <table class="table table-bordered table-striped mb-none" id="datatable-tabletools" data-swf-path="assets/vendor/jquery-datatables/extras/TableTools/swf/copy_csv_xls_pdf.swf">
            <thead>
                <tr class="table-header-row">
                    <th>Sl.</th>
                    <th>Full Name</th>
                    <th>Email</th>
                    <th>Mobile No.</th>
                    <th class="hide-status">Client Access</th>
                    <th>Access Status</th>
                    <th>Access Granted Date</th>
                    <th class="text-center hide-status">Actions</th>
                </tr>
            </thead>
            <tbody>
                @foreach($clients as $key => $client)
                    <tr class="table-body-row">
                        <th>{{ $key+1 }}</th>
                        <th>{{ $client->client_title." ".$client->client_forname." ".$client->client_surname }}</th>
                        <td>{{ $client->email }}</td>
                        <td>{{ $client->mobile_phone }}</td>

                        @if($client->client_access == 1)
                            <td class="hide-status"><span class="access-granted">Granted</span></td>
                        @else
                            <td class="hide-status"><span class="access-not-granted">Not Granted</span></td>
                        @endif

                        @if($client->client_status == 1)
                            <td><span class="member-active"><i class="fa fa-check"></i></span></td>
                        @else
                            <td><span class="member-inactive"><i class="fa fa-times"></i></span></td>
                        @endif
                        @php
                            $date = new DateTime($client->updated_at);
                        @endphp
                        <td>{{ $date->format('d M Y') }}</td>
                        <td class="action-td text-center hide-status">
                            @if($client->client_access == 0)
                            <a href="{{ route('director.client.client_access.show', ['id' => $client->id]) }}" class="btn btn-dark btn-sm btn-sm-custom btn-custom-info">
                                <i class="fa fa-info"></i>
                            </a>
                            @else
                                <a href="{{ route('director.client.client_access.refuse', ['id' => $client->id]) }}" class="btn btn-danger btn-sm btn-sm-custom btn-custom-danger btn-delete" onclick="return confirm('Do You Want to Refuse user Access of this Client?')">
                                    <i class="fa fa-trash"></i>
                                </a>
                            @endif
                        </td>
                    </tr>
                @endforeach
            </tbody>
        </table>
    </div>
</section>
<!-- end: page -->

@endsection


@section('script_links')
    {{--  External Javascript  --}}
    <script src="{{ asset('custom/assets/vendor/select2/select2.js') }}"></script>
    <script src="{{ asset('custom/assets/vendor/jquery-datatables/media/js/jquery.dataTables.js') }}"></script>
    <script src="{{ asset('custom/assets/vendor/jquery-datatables/extras/TableTools/js/dataTables.tableTools.min.js') }}"></script>
    <script src="{{ asset('custom/assets/vendor/jquery-datatables-bs3/assets/js/datatables.js') }}"></script>

    <script src="{{ asset('custom/assets/javascripts/tables/examples.datatables.default.js') }}"></script>
    <script src="{{ asset('custom/assets/javascripts/tables/examples.datatables.row.with.details.js') }}"></script>
    <script src="{{ asset('custom/assets/javascripts/tables/examples.datatables.tabletools.js') }}"></script>
@endsection

@section('scripts')
    {{--  External Javascript  --}}

@endsection
