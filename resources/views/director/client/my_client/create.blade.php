@extends('layouts.director.app')

@section('page_title', '| Clients')

@section('stylesheet_links')
    {{--  External CSS  --}}
    <link rel="stylesheet" href="{{ asset('custom/assets/vendor/bootstrap-datepicker/css/datepicker3.css') }}" />
    <link rel="stylesheet" href="{{ asset('custom/assets/vendor/select2/select2.css') }}" />
    <link rel="stylesheet" href="{{ asset('custom/assets/vendor/bootstrap-fileupload/bootstrap-fileupload.min.css') }}" />

@endsection

@section('stylesheet')
    {{--  External CSS  --}}
    <style>
    .panel{
        border: 1px solid #eee;
    }
    .panel .panel .panel-heading{
        padding: 10px 15px;
    }
    .fileupload .uneditable-input .fa {
        top: 39px;
    }
    </style>
@endsection

@section('content')

<header class="page-header">
    <h2>Create New Client</h2>

    <div class="right-wrapper pull-right">
        <ol class="breadcrumbs">
            <li>
                <a href="{{ Route('director.dashboard.index') }}">
                    <i class="fa fa-home"></i>
                </a>
            </li>
            <li><span class="active">Clients</span></li>
            <li><span>Create New Client</span></li>
        </ol>
    </div>
</header>

<!-- start: page -->
<section class="detials-page">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12 col-lg-12 col-xl-12">
                {{-- Profile Details  --}}
                <section class="panel panel-danger">
                    <form action="{{ route('director.client.store') }}" method="post" enctype="multipart/form-data">
                    @csrf
                        <header class="panel-heading">
                            <div class="panel-actions">
                                <a href="#" class="fa fa-caret-down"></a>
                            </div>

                            <h2 class="panel-title">Assign New Client</h2>
                        </header>

                        <div class="panel-body">
                            <div class="row">
                                <div class="col-md-12 col-xl-6">

                                    {{-- Profile Information --}}
                                    <section class="panel">
                                        <header class="panel-heading bg-warning">
                                            <h2 class="panel-title">Profile Information</h2>
                                        </header>

                                        <div class="panel-body">
                                            <div class="row">

                                                <div class="col-md-5">
                                                    <div class="form-group">
                                                        <label class="control-label">Title <span class="required">*</span></label>
                                                        <select name="client_title" data-plugin-selectTwo class="form-control populate  @error('client_title') is-invalid @enderror" required>
                                                            <option value="Mr">Mr</option>
                                                            <option value="Mrs">Mrs</option>
                                                            <option value="Miss">Miss</option>
                                                            <option value="Ms">Ms</option>
                                                            <option value="Lord">Lord</option>
                                                            <option value="Lady">Lady</option>
                                                            <option value="Dr">Dr</option>
                                                            <option value="Professor">Professor</option>
                                                            <option value="Professor Dame">Professor Dame</option>
                                                            <option value="Professor Sir">Professor Sir</option>
                                                            <option value="Air Chief Marshal Sir">Air Chief Marshal Sir</option>
                                                            <option value="Air Marshal">Air Marshal</option>
                                                            <option value="Air Marshal Sir">Air Marshal Sir</option>
                                                            <option value="Air Vice Marshal">Air Vice Marshal</option>
                                                            <option value="Air Vice Marshal Sir">Air Vice Marshal Sir</option>
                                                            <option value="Air Commodore">Air Commodore</option>
                                                            <option value="Group Captain">Group Captain</option>
                                                            <option value="Wing Commander">Wing Commander</option>
                                                            <option value="Squadron Leader">Squadron Leader</option>
                                                            <option value="Flight Lieutenant">Flight Lieutenant</option>
                                                            <option value="Flying Officer">Flying Officer</option>
                                                            <option value="Pilot Officer">Pilot Officer</option>
                                                            <option value="Admiral">Admiral</option>
                                                            <option value="Admiral Sir">Admiral Sir</option>
                                                            <option value="Vice Admiral">Vice Admiral</option>
                                                            <option value="Vice Admiral Sir">Vice Admiral Sir</option>
                                                            <option value="Rear Admiral">Rear Admiral</option>
                                                            <option value="Rear Admiral Sir">Rear Admiral Sir</option>
                                                            <option value="Commodore">Commodore</option>
                                                            <option value="Captain">Captain</option>
                                                            <option value="Commander">Commander</option>
                                                            <option value="Lieutenant Commander">Lieutenant Commander</option>
                                                            <option value="Lieutenant">Lieutenant</option>
                                                            <option value="Sub Lieutenant">Sub Lieutenant</option>
                                                            <option value="General">General</option>
                                                            <option value="General Sir">General Sir</option>
                                                            <option value="Lieutenant General Sir">Lieutenant General Sir</option>
                                                            <option value="Lieutenant General">Lieutenant General</option>
                                                            <option value="Major General">Major General</option>
                                                            <option value="Major General Sir">Major General Sir</option>
                                                            <option value="Brigadier">Brigadier</option>
                                                            <option value="Brigadier Sir">Brigadier Sir</option>
                                                            <option value="Colonel">Colonel</option>
                                                            <option value="Lieutenant Colonel">Lieutenant Colonel</option>
                                                            <option value="Major">Major</option>
                                                            <option value="Second Lieutenant">Second Lieutenant</option>
                                                            <option value="Baron">Baron</option>
                                                            <option value="Baroness">Baroness</option>
                                                            <option value="Count">Count</option>
                                                            <option value="Countess">Countess</option>
                                                            <option value="Dame">Dame</option>
                                                            <option value="Sir">Sir</option>
                                                            <option value="The Honourable">The Honourable</option>
                                                            <option value="The Reverend Canon">The Reverend Canon</option>
                                                            <option value="The Reverend">The Reverend</option>
                                                            <option value="Viscount">Viscount</option>
                                                            <option value="Viscountess">Viscountess</option>
                                                            <option value="The One and Only">The One and Only</option>
                                                        </select>

                                                        {{-- Validation Alert Messages --}}
                                                        @error('client_title')
                                                            <span class="invalid-feedback" role="alert">
                                                                <strong>{{ $message }}</strong>
                                                            </span>
                                                        @enderror
                                                    </div>
                                                </div>

                                                <div class="col-md-7">
                                                    <div class="form-group">
                                                        <label class="control-label">Forename <span class="required">*</span></label>
                                                        <input type="text" autocomplete="off" name="client_forname" class="form-control  @error('client_forname') is-invalid @enderror" placeholder="eg.: John" required/>

                                                        {{-- Validation Alert Messages --}}
                                                        @error('client_forname')
                                                            <span class="invalid-feedback" role="alert">
                                                                <strong>{{ $message }}</strong>
                                                            </span>
                                                        @enderror
                                                    </div>
                                                </div>

                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label class="control-label">Middle Name</label>
                                                        <input type="text" autocomplete="off" name="client_middle_name" class="form-control  @error('client_middle_name') is-invalid @enderror" placeholder="eg.: John"/>

                                                        {{-- Validation Alert Messages --}}
                                                        @error('client_middle_name')
                                                            <span class="invalid-feedback" role="alert">
                                                                <strong>{{ $message }}</strong>
                                                            </span>
                                                        @enderror
                                                    </div>
                                                </div>

                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label class="control-label">Surname <span class="required">*</span></label>
                                                        <input type="text" autocomplete="off" name="client_surname" class="form-control  @error('client_surname') is-invalid @enderror" placeholder="eg.: Doe" required/>

                                                        {{-- Validation Alert Messages --}}
                                                        @error('client_surname')
                                                            <span class="invalid-feedback" role="alert">
                                                                <strong>{{ $message }}</strong>
                                                            </span>
                                                        @enderror
                                                    </div>
                                                </div>

                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label class="control-label">Home Phone Number</label>
                                                        <input type="text" autocomplete="off" name="home_phone" class="form-control  @error('home_phone') is-invalid @enderror" placeholder="eg.: 158 587 1548"/>

                                                        {{-- Validation Alert Messages --}}
                                                        @error('home_phone')
                                                            <span class="invalid-feedback" role="alert">
                                                                <strong>{{ $message }}</strong>
                                                            </span>
                                                        @enderror
                                                    </div>
                                                </div>

                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label class="control-label">Mobile Number <span class="required">*</span></label>
                                                        <input type="text" autocomplete="off" name="mobile_phone" class="form-control  @error('mobile_phone') is-invalid @enderror" placeholder="eg.: +880 1712 345 654" required/>

                                                        {{-- Validation Alert Messages --}}
                                                        @error('mobile_phone')
                                                            <span class="invalid-feedback" role="alert">
                                                                <strong>{{ $message }}</strong>
                                                            </span>
                                                        @enderror
                                                    </div>
                                                </div>

                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label class="control-label">Email Address <span class="required">*</span></label>
                                                        <input type="email" autocomplete="off" name="email" class="form-control  @error('email') is-invalid @enderror" placeholder="eg.: jhon_doe@mail.com" required/>

                                                        {{-- Validation Alert Messages --}}
                                                        @error('email')
                                                            <span class="invalid-feedback" role="alert">
                                                                <strong>{{ $message }}</strong>
                                                            </span>
                                                        @enderror
                                                    </div>
                                                </div>

                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label class="control-label">Birthdate <span class="required">*</span></label>
                                                        <input type="text" autocomplete="off" data-plugin-datepicker="" name="date_of_birth" class="form-control  @error('date_of_birth') is-invalid @enderror" placeholder="eg.: dd/mm/yyyy" required>

                                                        {{-- Validation Alert Messages --}}
                                                        @error('date_of_birth')
                                                            <span class="invalid-feedback" role="alert">
                                                                <strong>{{ $message }}</strong>
                                                            </span>
                                                        @enderror
                                                    </div>
                                                </div>

                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label class="control-label">County <span class="required">*</span></label>
                                                        <select name="county" data-plugin-selectTwo class="form-control populate  @error('county') is-invalid @enderror" required>
                                                            <option>Please Select</option>
                                                            <optgroup label="England">
                                                                <option value="bath-and-north-east-somerset">Bath and North East Somerset</option>
                                                                <option value="bedford">Bedford</option>
                                                                <option value="berkshire">Berkshire</option>
                                                                <option value="blackburn-with-darwen">Blackburn with Darwen</option>
                                                                <option value="blackpool">Blackpool</option>
                                                                <option value="bournemouth">Bournemouth</option>
                                                                <option value="brighton-&amp;-hove">Brighton &amp; Hove</option>
                                                                <option value="bristol">Bristol</option>
                                                                <option value="buckinghamshire">Buckinghamshire</option>
                                                                <option value="cambridgeshire">Cambridgeshire</option>
                                                                <option value="central-bedfordshire">Central Bedfordshire</option>
                                                                <option value="cheshire-east">Cheshire East</option>
                                                                <option value="cheshire-west-and-chester">Cheshire West and Chester</option>
                                                                <option value="cornwall">Cornwall</option>
                                                                <option value="cumbria">Cumbria</option>
                                                                <option value="darlington">Darlington</option>
                                                                <option value="derby">Derby</option>
                                                                <option value="derbyshire">Derbyshire</option>
                                                                <option value="devon">Devon</option>
                                                                <option value="dorset">Dorset</option>
                                                                <option value="durham">Durham</option>
                                                                <option value="east-riding-of-yorkshire">East Riding of Yorkshire</option>
                                                                <option value="east-sussex">East Sussex</option>
                                                                <option value="essex">Essex</option>
                                                                <option value="gloucestershire">Gloucestershire</option>
                                                                <option value="greater-london">Greater London</option>
                                                                <option value="greater-manchester">Greater Manchester</option>
                                                                <option value="halton">Halton</option>
                                                                <option value="hampshire">Hampshire</option>
                                                                <option value="hartlepool">Hartlepool</option>
                                                                <option value="herefordshire">Herefordshire</option>
                                                                <option value="hertfordshire">Hertfordshire</option>
                                                                <option value="isle-of-wight">Isle of Wight</option>
                                                                <option value="kent">Kent</option>
                                                                <option value="kingston-upon-hull">Kingston upon Hull</option>
                                                                <option value="lancashire">Lancashire</option>
                                                                <option value="leicester">Leicester</option>
                                                                <option value="leicestershire">Leicestershire</option>
                                                                <option value="lincolnshire">Lincolnshire</option>
                                                                <option value="luton">Luton</option>
                                                                <option value="medway">Medway</option>
                                                                <option value="merseyside">Merseyside</option>
                                                                <option value="middlesbrough">Middlesbrough</option>
                                                                <option value="milton-keynes">Milton Keynes</option>
                                                                <option value="norfolk">Norfolk</option>
                                                                <option value="north-east-lincolnshire">North East Lincolnshire</option>
                                                                <option value="north-lincolnshire">North Lincolnshire</option>
                                                                <option value="north-somerset">North Somerset</option>
                                                                <option value="north-yorkshire">North Yorkshire</option>
                                                                <option value="northamptonshire">Northamptonshire</option>
                                                                <option value="northumberland">Northumberland</option>
                                                                <option value="nottingham">Nottingham</option>
                                                                <option value="nottinghamshire">Nottinghamshire</option>
                                                                <option value="oxfordshire">Oxfordshire</option>
                                                                <option value="peterborough">Peterborough</option>
                                                                <option value="plymouth">Plymouth</option>
                                                                <option value="poole">Poole</option>
                                                                <option value="portsmouth">Portsmouth</option>
                                                                <option value="redcar-and-cleveland">Redcar and Cleveland</option>
                                                                <option value="rutland">Rutland</option>
                                                                <option value="shropshire">Shropshire</option>
                                                                <option value="somerset">Somerset</option>
                                                                <option value="south-gloucestershire">South Gloucestershire</option>
                                                                <option value="south-yorkshire">South Yorkshire</option>
                                                                <option value="southampton">Southampton</option>
                                                                <option value="southend-on-sea">Southend-on-Sea</option>
                                                                <option value="staffordshire">Staffordshire</option>
                                                                <option value="stockton-on-tees">Stockton-on-Tees</option>
                                                                <option value="stoke-on-trent">Stoke-on-Trent</option>
                                                                <option value="suffolk">Suffolk</option>
                                                                <option value="surrey">Surrey</option>
                                                                <option value="swindon">Swindon</option>
                                                                <option value="telford-and-wrekin">Telford and Wrekin</option>
                                                                <option value="thurrock">Thurrock</option>
                                                                <option value="torbay">Torbay</option>
                                                                <option value="tyne-and-wear">Tyne and Wear</option>
                                                                <option value="warrington">Warrington</option>
                                                                <option value="warwickshire">Warwickshire</option>
                                                                <option value="west-midlands">West Midlands</option>
                                                                <option value="west-sussex">West Sussex</option>
                                                                <option value="west-yorkshire">West Yorkshire</option>
                                                                <option value="wiltshire">Wiltshire</option>
                                                                <option value="worcestershire">Worcestershire</option>
                                                                <option value="york">York</option>
                                                            </optgroup>
                                                            <optgroup label="Scotland">
                                                                <option value="aberdeen">Aberdeen</option>
                                                                <option value="aberdeenshire">Aberdeenshire</option>
                                                                <option value="angus">Angus</option>
                                                                <option value="argyll-and-bute">Argyll and Bute</option>
                                                                <option value="ayrshire-and-arran">Ayrshire and Arran</option>
                                                                <option value="banffshire">Banffshire</option>
                                                                <option value="berwickshire">Berwickshire</option>
                                                                <option value="caithness">Caithness</option>
                                                                <option value="clackmannanshire">Clackmannanshire</option>
                                                                <option value="dumfries">Dumfries</option>
                                                                <option value="dunbartonshire">Dunbartonshire</option>
                                                                <option value="dundee">Dundee</option>
                                                                <option value="east-lothian">East Lothian</option>
                                                                <option value="edinburgh">Edinburgh</option>
                                                                <option value="fife">Fife</option>
                                                                <option value="glasgow">Glasgow</option>
                                                                <option value="inverness">Inverness</option>
                                                                <option value="kincardineshire">Kincardineshire</option>
                                                                <option value="lanarkshire">Lanarkshire</option>
                                                                <option value="midlothian">Midlothian</option>
                                                                <option value="moray">Moray</option>
                                                                <option value="nairn">Nairn</option>
                                                                <option value="orkney-islands">Orkney Islands</option>
                                                                <option value="perth-and-kinross">Perth and Kinross</option>
                                                                <option value="renfrewshire">Renfrewshire</option>
                                                                <option value="ross-and-cromarty">Ross and Cromarty</option>
                                                                <option value="roxburgh,-ettrick-and-lauderdale">Roxburgh, Ettrick and Lauderdale</option>
                                                                <option value="shetland-islands">Shetland Islands</option>
                                                                <option value="stirling-and-falkirk">Stirling and Falkirk</option>
                                                                <option value="sutherland">Sutherland</option>
                                                                <option value="the-stewartry-of-kirkcudbright">The Stewartry of Kirkcudbright</option>
                                                                <option value="tweeddale">Tweeddale</option>
                                                                <option value="west-lothian">West Lothian</option>
                                                                <option value="western-isles">Western Isles</option>
                                                                <option value="wigtown">Wigtown</option>
                                                            </optgroup>
                                                            <optgroup label="Wales">
                                                                <option value="blaenau-gwent">Blaenau Gwent</option>
                                                                <option value="bridgend">Bridgend</option>
                                                                <option value="caerphilly">Caerphilly</option>
                                                                <option value="cardiff">Cardiff</option>
                                                                <option value="carmarthenshire">Carmarthenshire</option>
                                                                <option value="ceredigion">Ceredigion</option>
                                                                <option value="conwy">Conwy</option>
                                                                <option value="denbighshire">Denbighshire</option>
                                                                <option value="flintshire">Flintshire</option>
                                                                <option value="gwynedd">gwynedd</option>
                                                                <option value="isle-of-anglesey">Isle of Anglesey</option>
                                                                <option value="merthyr-tydfil">Merthyr Tydfil</option>
                                                                <option value="monmouthshire">Monmouthshire</option>
                                                                <option value="neath-port-talbot">Neath Port Talbot</option>
                                                                <option value="newport">Newport</option>
                                                                <option value="pembrokeshire">Pembrokeshire</option>
                                                                <option value="powys">Powys</option>
                                                                <option value="rhondda-cynon-taf">Rhondda Cynon Taf</option>
                                                                <option value="swansea">Swansea</option>
                                                                <option value="torfaen">Torfaen</option>
                                                                <option value="vale-of-glamorgan">Vale of Glamorgan</option>
                                                                <option value="wrexham">Wrexham</option>
                                                            </optgroup>
                                                            <optgroup label="Northern Ireland">
                                                                <option value="antrim">Antrim</option>
                                                                <option value="ards">Ards</option>
                                                                <option value="armagh">Armagh</option>
                                                                <option value="ballymena">Ballymena</option>
                                                                <option value="ballymoney">Ballymoney</option>
                                                                <option value="banbridge">Banbridge</option>
                                                                <option value="belfast">Belfast</option>
                                                                <option value="carrickfergus">Carrickfergus</option>
                                                                <option value="castlereagh">Castlereagh</option>
                                                                <option value="coleraine">Coleraine</option>
                                                                <option value="cookstown">Cookstown</option>
                                                                <option value="craigavon">Craigavon</option>
                                                                <option value="derry">Derry</option>
                                                                <option value="down">Down</option>
                                                                <option value="dungannon-and-south-tyrone">Dungannon and South Tyrone</option>
                                                                <option value="fermanagh">Fermanagh</option>
                                                                <option value="larne">Larne</option>
                                                                <option value="limavady">Limavady</option>
                                                                <option value="lisburn">Lisburn</option>
                                                                <option value="magherafelt">Magherafelt</option>
                                                                <option value="moyle">Moyle</option>
                                                                <option value="newry-and-mourne">Newry and Mourne</option>
                                                                <option value="newtownabbey">Newtownabbey</option>
                                                                <option value="north-down">North Down</option>
                                                                <option value="omagh">Omagh</option>
                                                                <option value="strabane">Strabane</option>
                                                            </optgroup>
                                                            <optgroup label="Ireland">
                                                                <option value="carlow">Carlow</option>
                                                                <option value="cavan">Cavan</option>
                                                                <option value="clare">Clare</option>
                                                                <option value="cork">Cork</option>
                                                                <option value="donegal">Donegal</option>
                                                                <option value="dublin">Dublin</option>
                                                                <option value="dun-laoghaire-rathdown">Dún Laoghaire-Rathdown</option>
                                                                <option value="fingal">Fingal</option>
                                                                <option value="galway">Galway</option>
                                                                <option value="kerry">Kerry</option>
                                                                <option value="kildare">Kildare</option>
                                                                <option value="kilkenny">Kilkenny</option>
                                                                <option value="laois">Laois</option>
                                                                <option value="leitrim">Leitrim</option>
                                                                <option value="limerick">Limerick</option>
                                                                <option value="longford">Longford</option>
                                                                <option value="louth">Louth</option>
                                                                <option value="mayo">Mayo</option>
                                                                <option value="meath">Meath</option>
                                                                <option value="monaghan">Monaghan</option>
                                                                <option value="offaly">Offaly</option>
                                                                <option value="roscommon">Roscommon</option>
                                                                <option value="sligo">Sligo</option>
                                                                <option value="south-dublin">South Dublin</option>
                                                                <option value="tipperary">Tipperary</option>
                                                                <option value="waterford">Waterford</option>
                                                                <option value="westmeath">Westmeath</option>
                                                                <option value="wexford">Wexford</option>
                                                                <option value="wicklow">Wicklow</option>
                                                            </optgroup>
                                                        </select>

                                                        {{-- Validation Alert Messages --}}
                                                        @error('county')
                                                            <span class="invalid-feedback" role="alert">
                                                                <strong>{{ $message }}</strong>
                                                            </span>
                                                        @enderror
                                                    </div>
                                                </div>

                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label class="control-label">Client Nationality <span class="required">*</span></label>
                                                        <input type="text" autocomplete="off" name="client_nationality" class="form-control  @error('client_nationality') is-invalid @enderror" placeholder="eg.: Bangladeshi" required/>

                                                        {{-- Validation Alert Messages --}}
                                                        @error('client_nationality')
                                                            <span class="invalid-feedback" role="alert">
                                                                <strong>{{ $message }}</strong>
                                                            </span>
                                                        @enderror
                                                    </div>
                                                </div>

                                                <div class="col-md-3">
                                                    <div class="form-group">
                                                        <label class="control-label">Home Town / City <span class="required">*</span></label>
                                                        <input type="text" autocomplete="off" name="town_city" class="form-control  @error('town_city') is-invalid @enderror" placeholder="eg.: Home Town" required/>

                                                        {{-- Validation Alert Messages --}}
                                                        @error('town_city')
                                                            <span class="invalid-feedback" role="alert">
                                                                <strong>{{ $message }}</strong>
                                                            </span>
                                                        @enderror
                                                    </div>
                                                </div>

                                                <div class="col-md-3">
                                                    <div class="form-group">
                                                        <label class="control-label">Postal Code <span class="required">*</span></label>
                                                        <input type="text" autocomplete="off" name="post_code" class="form-control  @error('post_code') is-invalid @enderror" placeholder="eg.: 3224" required/>

                                                        {{-- Validation Alert Messages --}}
                                                        @error('post_code')
                                                            <span class="invalid-feedback" role="alert">
                                                                <strong>{{ $message }}</strong>
                                                            </span>
                                                        @enderror
                                                    </div>
                                                </div>

                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label class="control-label">House No / Name <span class="required">*</span></label>
                                                        <input type="text" autocomplete="off" name="house_no_name" class="form-control  @error('house_no_name') is-invalid @enderror" placeholder="eg.: 12/B Bilash Bhaban, Uganda" required/>

                                                        {{-- Validation Alert Messages --}}
                                                        @error('house_no_name')
                                                            <span class="invalid-feedback" role="alert">
                                                                <strong>{{ $message }}</strong>
                                                            </span>
                                                        @enderror
                                                    </div>
                                                </div>

                                                <div class="col-md-12">
                                                    <div class="form-group">
                                                        <label class="control-label">Address <span class="required">*</span></label>
                                                        <textarea name="address" rows="3" class="form-control  @error('address') is-invalid @enderror" placeholder="eg.: Jhon Doe Address" required></textarea>

                                                        {{-- Validation Alert Messages --}}
                                                        @error('address')
                                                            <span class="invalid-feedback" role="alert">
                                                                <strong>{{ $message }}</strong>
                                                            </span>
                                                        @enderror
                                                    </div>
                                                </div>

                                            </div>
                                        </div>
                                    </section>

                                </div>


                                <div class="col-md-12 col-xl-6">

                                    {{-- Extra Information --}}
                                    <section class="panel">
                                        <header class="panel-heading bg-warning">
                                            <h2 class="panel-title">Extra Information</h2>
                                        </header>

                                        <div class="panel-body">
                                            <div class="row">

                                                <div class="col-md-12">
                                                    <div class="form-group">
                                                        <label class="control-label">Client Corporate Name </label>
                                                        <input type="text" autocomplete="off" name="corporate_name" class="form-control  @error('corporate_name') is-invalid @enderror" placeholder="eg.: Client Corporate Name" />
                                                        <small><span class="required">**</span> Fillup This Field if The Client is a Corporate Client <span class="required">**</span></small>

                                                        {{-- Validation Alert Messages --}}
                                                        @error('corporate_name')
                                                            <span class="invalid-feedback" role="alert">
                                                                <strong>{{ $message }}</strong>
                                                            </span>
                                                        @enderror
                                                    </div>
                                                </div>

                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label class="control-label">Client Status <span class="required">*</span></label>
                                                        <select name="client_status" data-plugin-selectTwo class="form-control populate  @error('client_status') is-invalid @enderror" required>
                                                            <option value="1">Active</option>
                                                            <option value="0">Non-Active</option>
                                                        </select>

                                                        {{-- Validation Alert Messages --}}
                                                        @error('client_status')
                                                            <span class="invalid-feedback" role="alert">
                                                                <strong>{{ $message }}</strong>
                                                            </span>
                                                        @enderror
                                                    </div>
                                                </div>

                                                {{-- <div class="col-md-3">
                                                    <div class="form-group">
                                                        <label class="control-label">Client Priority <span class="required">*</span></label>
                                                        <select name="grading" data-plugin-selectTwo class="form-control populate  @error('grading') is-invalid @enderror" required>
                                                            <option disabled selected>Select Grading</option>
                                                            @foreach($gradings as $grading)
                                                            <option value="{{ $grading->id }}">{{ $grading->name }}</option>
                                                            @endforeach
                                                        </select>

                                                        <!-- Validation Alert Messages -->
                                                        @error('grading')
                                                            <span class="invalid-feedback" role="alert">
                                                                <strong>{{ $message }}</strong>
                                                            </span>
                                                        @enderror
                                                    </div>
                                                </div> --}}

                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label class="control-label">Adviser <span class="required">*</span></label>
                                                        <select name="advisor" data-plugin-selectTwo class="form-control populate  @error('adviser') is-invalid @enderror" required>
                                                            <option disabled selected>Select Advisor</option>
                                                            @foreach($advisors as $advisor)
                                                            <option value="{{ $advisor->id }}">{{ $advisor->saluation." ".$advisor->forname." ".$advisor->surname }}</option>
                                                            @endforeach
                                                        </select>

                                                        {{-- Validation Alert Messages --}}
                                                        @error('adviser')
                                                            <span class="invalid-feedback" role="alert">
                                                                <strong>{{ $message }}</strong>
                                                            </span>
                                                        @enderror
                                                    </div>
                                                </div>

                                                <div class="col-md-12">
                                                    <div class="form-group">
                                                        <label class="control-label">Contactable By</label>
                                                        <div class="row">
                                                            <div class="col-md-2">
                                                                <div class="checkbox-custom checkbox-default">
                                                                    <input type="checkbox" name="contactable_by[mail]" class=" @error('con_by_mail') is-invalid @enderror">
                                                                    <label for="con_by_mail">Mail</label>
                                                                </div>
                                                            </div>
                                                            <div class="col-md-2">
                                                                <div class="checkbox-custom checkbox-default">
                                                                    <input type="checkbox" name="contactable_by[email]" class=" @error('con_by_email') is-invalid @enderror">
                                                                    <label for="con_by_email">E-Mail</label>
                                                                </div>
                                                            </div>
                                                            <div class="col-md-2">
                                                                <div class="checkbox-custom checkbox-default">
                                                                    <input type="checkbox" name="contactable_by[phone]" class=" @error('con_by_phone') is-invalid @enderror">
                                                                    <label for="con_by_phone">Phone</label>
                                                                </div>
                                                            </div>
                                                            <div class="col-md-2">
                                                                <div class="checkbox-custom checkbox-default">
                                                                    <input type="checkbox" name="contactable_by[sms]" class=" @error('con_by_sms') is-invalid @enderror">
                                                                    <label for="con_by_sms">SMS</label>
                                                                </div>
                                                            </div>
                                                        </div>

                                                        {{-- Validation Alert Messages --}}
                                                        @error('con_by_mail')
                                                            <span class="invalid-feedback" role="alert">
                                                                <strong>{{ $message }}</strong>
                                                            </span>
                                                        @enderror

                                                        {{-- Validation Alert Messages --}}
                                                        @error('con_by_email')
                                                            <span class="invalid-feedback" role="alert">
                                                                <strong>{{ $message }}</strong>
                                                            </span>
                                                        @enderror

                                                        {{-- Validation Alert Messages --}}
                                                        @error('con_by_phone')
                                                            <span class="invalid-feedback" role="alert">
                                                                <strong>{{ $message }}</strong>
                                                            </span>
                                                        @enderror

                                                        {{-- Validation Alert Messages --}}
                                                        @error('con_by_sms')
                                                            <span class="invalid-feedback" role="alert">
                                                                <strong>{{ $message }}</strong>
                                                            </span>
                                                        @enderror

                                                        {{-- Validation Alert Messages --}}
                                                        @error('con_by_social')
                                                            <span class="invalid-feedback" role="alert">
                                                                <strong>{{ $message }}</strong>
                                                            </span>
                                                        @enderror
                                                    </div>
                                                </div>

                                            </div>
                                        </div>
                                    </section>

                                    {{-- Client Partners --}}
                                    <section class="panel">
                                        <header class="panel-heading bg-warning">
                                            <h2 class="panel-title">Associate Partner</h2>
                                        </header>
                                        <div class="panel-body">
                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <label class="control-label">Select Partner</label>
                                                    <select name="partner" data-plugin-selectTwo class="form-control populate remove-disable @error('partner') is-invalid @enderror" required>
                                                        <option disabled selected>Select Partner</option>
                                                        @foreach($clients as $client)

                                                        <option value="{{ $client->id }}">{{ $client->client_title." ".$client->client_surname." ".$client->client_forname }}</option>
                                                        @endforeach
                                                    </select>
                                                    {{-- Validation Alert Messages --}}
                                                    @error('partner')
                                                        <span class="invalid-feedback" role="alert">
                                                            <strong>{{ $message }}</strong>
                                                        </span>
                                                    @enderror
                                                </div>
                                            </div>
                                        </div>
                                    </section>

                                </div>
                            </div>
                        </div>

                        <div class="panel-footer text-right">
                            <button type="submit" class="btn btn-dark btn-custom btn-sm">Assign Client</button>
                        </div>
                    </form>
                </section>
            </div>
        </div>
    </div>
</section>
<!-- end: page -->

@endsection


@section('script_links')
    {{--  External Javascript  --}}
    <script src="{{ asset('custom/assets/vendor/bootstrap-datepicker/js/bootstrap-datepicker.js') }}"></script>
    <script src="{{ asset('custom/assets/vendor/select2/select2.js') }}"></script>

    <script src="{{ asset('custom/assets/vendor/bootstrap-fileupload/bootstrap-fileupload.min.js') }}"></script>

@endsection

@section('scripts')
    {{--  External Javascript  --}}
    <script>

    </script>
@endsection
