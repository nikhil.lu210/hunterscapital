@extends('layouts.director.app')

@section('page_title', '| My Client | client_name')

@section('stylesheet_links')
    {{--  External CSS  --}}
    <link rel="stylesheet" href="{{ asset('custom/assets/vendor/bootstrap-datepicker/css/datepicker3.css') }}" />
    <link rel="stylesheet" href="{{ asset('custom/assets/vendor/select2/select2.css') }}" />
    <link rel="stylesheet" href="{{ asset('custom/assets/vendor/jquery-datatables-bs3/assets/css/datatables.css') }}" />
    <link rel="stylesheet" href="{{ asset('custom/assets/vendor/bootstrap-fileupload/bootstrap-fileupload.min.css') }}" />

@endsection

@section('stylesheet')
    {{--  External CSS  --}}
    <style>
    .panel{
        border: 1px solid #eee;
    }
    .panel .panel .panel-heading{
        padding: 10px 15px;
    }
    #datatable-default_filter{
        text-align: right;
    }
    #datatable-default_paginate{
        float: right;
    }
    .dataTables_wrapper .DTTT.btn-group {
        display: inline-block;
        position: absolute;
        top: -65px;
        right: 0px;
        display: none !important; /*Remove it If Want to Print*/
    }
    .fileupload .uneditable-input .fa {
        top: 39px;
    }
    </style>
@endsection

@section('content')

<header class="page-header">
    <h2>client_name</h2>

    <div class="right-wrapper pull-right">
        <ol class="breadcrumbs">
            <li>
                <a href="{{ Route('director.dashboard.index') }}">
                    <i class="fa fa-home"></i>
                </a>
            </li>
            <li><span class="active">Clients</span></li>
            <li><span>client_name</span></li>
        </ol>
    </div>
</header>

<!-- start: page -->
<section class="detials-page">
    <div class="container-fluid">
        <div class="row">

            <div class="col-md-12 col-xl-6">
                <form action="{{ route('director.client.update', ['id' => $client->id]) }}" method="post">
                @csrf
                    <section class="panel">
                        <div class="panel-body">
                            {{-- Profile Details  --}}
                            <section class="panel panel-danger">
                                <header class="panel-heading">
                                    <h2 class="panel-title">Profile Details</h2>
                                </header>

                                <div class="panel-body">
                                    <div class="row">

                                        <div class="col-md-5">
                                            <div class="form-group">
                                                <label class="control-label">Title <span class="required">*</span></label>
                                                <select name="client_title" id="client_title" data-plugin-selectTwo class="form-control populate  @error('client_title') is-invalid @enderror" required>
                                                    <option value="Mr">Mr</option>
                                                    <option value="Mrs">Mrs</option>
                                                    <option value="Miss">Miss</option>
                                                    <option value="Ms">Ms</option>
                                                    <option value="Lord">Lord</option>
                                                    <option value="Lady">Lady</option>
                                                    <option value="Dr">Dr</option>
                                                    <option value="Professor">Professor</option>
                                                    <option value="Professor Dame">Professor Dame</option>
                                                    <option value="Professor Sir">Professor Sir</option>
                                                    <option value="Air Chief Marshal Sir">Air Chief Marshal Sir</option>
                                                    <option value="Air Marshal">Air Marshal</option>
                                                    <option value="Air Marshal Sir">Air Marshal Sir</option>
                                                    <option value="Air Vice Marshal">Air Vice Marshal</option>
                                                    <option value="Air Vice Marshal Sir">Air Vice Marshal Sir</option>
                                                    <option value="Air Commodore">Air Commodore</option>
                                                    <option value="Group Captain">Group Captain</option>
                                                    <option value="Wing Commander">Wing Commander</option>
                                                    <option value="Squadron Leader">Squadron Leader</option>
                                                    <option value="Flight Lieutenant">Flight Lieutenant</option>
                                                    <option value="Flying Officer">Flying Officer</option>
                                                    <option value="Pilot Officer">Pilot Officer</option>
                                                    <option value="Admiral">Admiral</option>
                                                    <option value="Admiral Sir">Admiral Sir</option>
                                                    <option value="Vice Admiral">Vice Admiral</option>
                                                    <option value="Vice Admiral Sir">Vice Admiral Sir</option>
                                                    <option value="Rear Admiral">Rear Admiral</option>
                                                    <option value="Rear Admiral Sir">Rear Admiral Sir</option>
                                                    <option value="Commodore">Commodore</option>
                                                    <option value="Captain">Captain</option>
                                                    <option value="Commander">Commander</option>
                                                    <option value="Lieutenant Commander">Lieutenant Commander</option>
                                                    <option value="Lieutenant">Lieutenant</option>
                                                    <option value="Sub Lieutenant">Sub Lieutenant</option>
                                                    <option value="General">General</option>
                                                    <option value="General Sir">General Sir</option>
                                                    <option value="Lieutenant General Sir">Lieutenant General Sir</option>
                                                    <option value="Lieutenant General">Lieutenant General</option>
                                                    <option value="Major General">Major General</option>
                                                    <option value="Major General Sir">Major General Sir</option>
                                                    <option value="Brigadier">Brigadier</option>
                                                    <option value="Brigadier Sir">Brigadier Sir</option>
                                                    <option value="Colonel">Colonel</option>
                                                    <option value="Lieutenant Colonel">Lieutenant Colonel</option>
                                                    <option value="Major">Major</option>
                                                    <option value="Second Lieutenant">Second Lieutenant</option>
                                                    <option value="Baron">Baron</option>
                                                    <option value="Baroness">Baroness</option>
                                                    <option value="Count">Count</option>
                                                    <option value="Countess">Countess</option>
                                                    <option value="Dame">Dame</option>
                                                    <option value="Sir">Sir</option>
                                                    <option value="The Honourable">The Honourable</option>
                                                    <option value="The Reverend Canon">The Reverend Canon</option>
                                                    <option value="The Reverend">The Reverend</option>
                                                    <option value="Viscount">Viscount</option>
                                                    <option value="Viscountess">Viscountess</option>
                                                    <option value="The One and Only">The One and Only</option>
                                                </select>

                                                {{-- Validation Alert Messages --}}
                                                @error('client_title')
                                                    <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $message }}</strong>
                                                    </span>
                                                @enderror
                                            </div>
                                        </div>

                                        <div class="col-md-7">
                                            <div class="form-group">
                                                <label class="control-label">ForName <span class="required">*</span></label>
                                            <input type="text" autocomplete="off" name="client_forname" class="form-control  @error('client_forname') is-invalid @enderror" placeholder="eg.: John" value="{{ $client->client_forname }}" required/>

                                                {{-- Validation Alert Messages --}}
                                                @error('client_forname')
                                                    <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $message }}</strong>
                                                    </span>
                                                @enderror
                                            </div>
                                        </div>

                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label class="control-label">Middle Name</label>
                                                <input type="text" autocomplete="off" name="client_middle_name" class="form-control  @error('client_middle_name') is-invalid @enderror" value="{{ $client->client_middle_name }}" placeholder="eg.: John"/>

                                                {{-- Validation Alert Messages --}}
                                                @error('client_middle_name')
                                                    <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $message }}</strong>
                                                    </span>
                                                @enderror
                                            </div>
                                        </div>

                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label class="control-label">Surname <span class="required">*</span></label>
                                                <input type="text" autocomplete="off" name="client_surname" class="form-control  @error('client_surname') is-invalid @enderror" value="{{ $client->client_surname }}" placeholder="eg.: Doe" required/>

                                                {{-- Validation Alert Messages --}}
                                                @error('client_surname')
                                                    <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $message }}</strong>
                                                    </span>
                                                @enderror
                                            </div>
                                        </div>

                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label class="control-label">Home Phone Number</label>
                                                <input type="text" autocomplete="off" name="home_phone" class="form-control  @error('home_phone') is-invalid @enderror" value="{{ $client->home_phone }}" placeholder="eg.: 158 587 1548"/>

                                                {{-- Validation Alert Messages --}}
                                                @error('home_phone')
                                                    <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $message }}</strong>
                                                    </span>
                                                @enderror
                                            </div>
                                        </div>

                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label class="control-label">Mobile Number <span class="required">*</span></label>
                                                <input type="text" autocomplete="off" name="mobile_phone" class="form-control  @error('mobile_phone') is-invalid @enderror" value="{{ $client->mobile_phone }}" placeholder="eg.: +880 1712 345 654" required/>

                                                {{-- Validation Alert Messages --}}
                                                @error('mobile_phone')
                                                    <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $message }}</strong>
                                                    </span>
                                                @enderror
                                            </div>
                                        </div>

                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label class="control-label">Email Address <span class="required">*</span></label>
                                                <input type="text" autocomplete="off" name="email" class="form-control  @error('email') is-invalid @enderror" value="{{ $client->email }}" placeholder="eg.: jhon_doe@mail.com" required/>

                                                {{-- Validation Alert Messages --}}
                                                @error('email')
                                                    <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $message }}</strong>
                                                    </span>
                                                @enderror
                                            </div>
                                        </div>

                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label class="control-label">Birthdate <span class="required">*</span></label>
                                                <input type="text" autocomplete="off" data-plugin-datepicker="" name="date_of_birth" class="form-control  @error('date_of_birth') is-invalid @enderror" value="{{ App\Http\Controllers\Director\Client\ClientController::reverseDateFormat($client->date_of_birth) }}" placeholder="eg.: dd/mm/yyyy" required>

                                                {{-- Validation Alert Messages --}}
                                                @error('date_of_birth')
                                                    <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $message }}</strong>
                                                    </span>
                                                @enderror
                                            </div>
                                        </div>

                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label class="control-label">County <span class="required">*</span></label>
                                                <select name="county" id="client_county" data-plugin-selectTwo class="form-control populate  @error('county') is-invalid @enderror" required>
                                                    <option>Please Select</option>
                                                    <optgroup label="England">
                                                        <option value="bath-and-north-east-somerset">Bath and North East Somerset</option>
                                                        <option value="bedford">Bedford</option>
                                                        <option value="berkshire">Berkshire</option>
                                                        <option value="blackburn-with-darwen">Blackburn with Darwen</option>
                                                        <option value="blackpool">Blackpool</option>
                                                        <option value="bournemouth">Bournemouth</option>
                                                        <option value="brighton-&amp;-hove">Brighton &amp; Hove</option>
                                                        <option value="bristol">Bristol</option>
                                                        <option value="buckinghamshire">Buckinghamshire</option>
                                                        <option value="cambridgeshire">Cambridgeshire</option>
                                                        <option value="central-bedfordshire">Central Bedfordshire</option>
                                                        <option value="cheshire-east">Cheshire East</option>
                                                        <option value="cheshire-west-and-chester">Cheshire West and Chester</option>
                                                        <option value="cornwall">Cornwall</option>
                                                        <option value="cumbria">Cumbria</option>
                                                        <option value="darlington">Darlington</option>
                                                        <option value="derby">Derby</option>
                                                        <option value="derbyshire">Derbyshire</option>
                                                        <option value="devon">Devon</option>
                                                        <option value="dorset">Dorset</option>
                                                        <option value="durham">Durham</option>
                                                        <option value="east-riding-of-yorkshire">East Riding of Yorkshire</option>
                                                        <option value="east-sussex">East Sussex</option>
                                                        <option value="essex">Essex</option>
                                                        <option value="gloucestershire">Gloucestershire</option>
                                                        <option value="greater-london">Greater London</option>
                                                        <option value="greater-manchester">Greater Manchester</option>
                                                        <option value="halton">Halton</option>
                                                        <option value="hampshire">Hampshire</option>
                                                        <option value="hartlepool">Hartlepool</option>
                                                        <option value="herefordshire">Herefordshire</option>
                                                        <option value="hertfordshire">Hertfordshire</option>
                                                        <option value="isle-of-wight">Isle of Wight</option>
                                                        <option value="kent">Kent</option>
                                                        <option value="kingston-upon-hull">Kingston upon Hull</option>
                                                        <option value="lancashire">Lancashire</option>
                                                        <option value="leicester">Leicester</option>
                                                        <option value="leicestershire">Leicestershire</option>
                                                        <option value="lincolnshire">Lincolnshire</option>
                                                        <option value="luton">Luton</option>
                                                        <option value="medway">Medway</option>
                                                        <option value="merseyside">Merseyside</option>
                                                        <option value="middlesbrough">Middlesbrough</option>
                                                        <option value="milton-keynes">Milton Keynes</option>
                                                        <option value="norfolk">Norfolk</option>
                                                        <option value="north-east-lincolnshire">North East Lincolnshire</option>
                                                        <option value="north-lincolnshire">North Lincolnshire</option>
                                                        <option value="north-somerset">North Somerset</option>
                                                        <option value="north-yorkshire">North Yorkshire</option>
                                                        <option value="northamptonshire">Northamptonshire</option>
                                                        <option value="northumberland">Northumberland</option>
                                                        <option value="nottingham">Nottingham</option>
                                                        <option value="nottinghamshire">Nottinghamshire</option>
                                                        <option value="oxfordshire">Oxfordshire</option>
                                                        <option value="peterborough">Peterborough</option>
                                                        <option value="plymouth">Plymouth</option>
                                                        <option value="poole">Poole</option>
                                                        <option value="portsmouth">Portsmouth</option>
                                                        <option value="redcar-and-cleveland">Redcar and Cleveland</option>
                                                        <option value="rutland">Rutland</option>
                                                        <option value="shropshire">Shropshire</option>
                                                        <option value="somerset">Somerset</option>
                                                        <option value="south-gloucestershire">South Gloucestershire</option>
                                                        <option value="south-yorkshire">South Yorkshire</option>
                                                        <option value="southampton">Southampton</option>
                                                        <option value="southend-on-sea">Southend-on-Sea</option>
                                                        <option value="staffordshire">Staffordshire</option>
                                                        <option value="stockton-on-tees">Stockton-on-Tees</option>
                                                        <option value="stoke-on-trent">Stoke-on-Trent</option>
                                                        <option value="suffolk">Suffolk</option>
                                                        <option value="surrey">Surrey</option>
                                                        <option value="swindon">Swindon</option>
                                                        <option value="telford-and-wrekin">Telford and Wrekin</option>
                                                        <option value="thurrock">Thurrock</option>
                                                        <option value="torbay">Torbay</option>
                                                        <option value="tyne-and-wear">Tyne and Wear</option>
                                                        <option value="warrington">Warrington</option>
                                                        <option value="warwickshire">Warwickshire</option>
                                                        <option value="west-midlands">West Midlands</option>
                                                        <option value="west-sussex">West Sussex</option>
                                                        <option value="west-yorkshire">West Yorkshire</option>
                                                        <option value="wiltshire">Wiltshire</option>
                                                        <option value="worcestershire">Worcestershire</option>
                                                        <option value="york">York</option>
                                                    </optgroup>
                                                    <optgroup label="Scotland">
                                                        <option value="aberdeen">Aberdeen</option>
                                                        <option value="aberdeenshire">Aberdeenshire</option>
                                                        <option value="angus">Angus</option>
                                                        <option value="argyll-and-bute">Argyll and Bute</option>
                                                        <option value="ayrshire-and-arran">Ayrshire and Arran</option>
                                                        <option value="banffshire">Banffshire</option>
                                                        <option value="berwickshire">Berwickshire</option>
                                                        <option value="caithness">Caithness</option>
                                                        <option value="clackmannanshire">Clackmannanshire</option>
                                                        <option value="dumfries">Dumfries</option>
                                                        <option value="dunbartonshire">Dunbartonshire</option>
                                                        <option value="dundee">Dundee</option>
                                                        <option value="east-lothian">East Lothian</option>
                                                        <option value="edinburgh">Edinburgh</option>
                                                        <option value="fife">Fife</option>
                                                        <option value="glasgow">Glasgow</option>
                                                        <option value="inverness">Inverness</option>
                                                        <option value="kincardineshire">Kincardineshire</option>
                                                        <option value="lanarkshire">Lanarkshire</option>
                                                        <option value="midlothian">Midlothian</option>
                                                        <option value="moray">Moray</option>
                                                        <option value="nairn">Nairn</option>
                                                        <option value="orkney-islands">Orkney Islands</option>
                                                        <option value="perth-and-kinross">Perth and Kinross</option>
                                                        <option value="renfrewshire">Renfrewshire</option>
                                                        <option value="ross-and-cromarty">Ross and Cromarty</option>
                                                        <option value="roxburgh,-ettrick-and-lauderdale">Roxburgh, Ettrick and Lauderdale</option>
                                                        <option value="shetland-islands">Shetland Islands</option>
                                                        <option value="stirling-and-falkirk">Stirling and Falkirk</option>
                                                        <option value="sutherland">Sutherland</option>
                                                        <option value="the-stewartry-of-kirkcudbright">The Stewartry of Kirkcudbright</option>
                                                        <option value="tweeddale">Tweeddale</option>
                                                        <option value="west-lothian">West Lothian</option>
                                                        <option value="western-isles">Western Isles</option>
                                                        <option value="wigtown">Wigtown</option>
                                                    </optgroup>
                                                    <optgroup label="Wales">
                                                        <option value="blaenau-gwent">Blaenau Gwent</option>
                                                        <option value="bridgend">Bridgend</option>
                                                        <option value="caerphilly">Caerphilly</option>
                                                        <option value="cardiff">Cardiff</option>
                                                        <option value="carmarthenshire">Carmarthenshire</option>
                                                        <option value="ceredigion">Ceredigion</option>
                                                        <option value="conwy">Conwy</option>
                                                        <option value="denbighshire">Denbighshire</option>
                                                        <option value="flintshire">Flintshire</option>
                                                        <option value="gwynedd">gwynedd</option>
                                                        <option value="isle-of-anglesey">Isle of Anglesey</option>
                                                        <option value="merthyr-tydfil">Merthyr Tydfil</option>
                                                        <option value="monmouthshire">Monmouthshire</option>
                                                        <option value="neath-port-talbot">Neath Port Talbot</option>
                                                        <option value="newport">Newport</option>
                                                        <option value="pembrokeshire">Pembrokeshire</option>
                                                        <option value="powys">Powys</option>
                                                        <option value="rhondda-cynon-taf">Rhondda Cynon Taf</option>
                                                        <option value="swansea">Swansea</option>
                                                        <option value="torfaen">Torfaen</option>
                                                        <option value="vale-of-glamorgan">Vale of Glamorgan</option>
                                                        <option value="wrexham">Wrexham</option>
                                                    </optgroup>
                                                    <optgroup label="Northern Ireland">
                                                        <option value="antrim">Antrim</option>
                                                        <option value="ards">Ards</option>
                                                        <option value="armagh">Armagh</option>
                                                        <option value="ballymena">Ballymena</option>
                                                        <option value="ballymoney">Ballymoney</option>
                                                        <option value="banbridge">Banbridge</option>
                                                        <option value="belfast">Belfast</option>
                                                        <option value="carrickfergus">Carrickfergus</option>
                                                        <option value="castlereagh">Castlereagh</option>
                                                        <option value="coleraine">Coleraine</option>
                                                        <option value="cookstown">Cookstown</option>
                                                        <option value="craigavon">Craigavon</option>
                                                        <option value="derry">Derry</option>
                                                        <option value="down">Down</option>
                                                        <option value="dungannon-and-south-tyrone">Dungannon and South Tyrone</option>
                                                        <option value="fermanagh">Fermanagh</option>
                                                        <option value="larne">Larne</option>
                                                        <option value="limavady">Limavady</option>
                                                        <option value="lisburn">Lisburn</option>
                                                        <option value="magherafelt">Magherafelt</option>
                                                        <option value="moyle">Moyle</option>
                                                        <option value="newry-and-mourne">Newry and Mourne</option>
                                                        <option value="newtownabbey">Newtownabbey</option>
                                                        <option value="north-down">North Down</option>
                                                        <option value="omagh">Omagh</option>
                                                        <option value="strabane">Strabane</option>
                                                    </optgroup>
                                                    <optgroup label="Ireland">
                                                        <option value="carlow">Carlow</option>
                                                        <option value="cavan">Cavan</option>
                                                        <option value="clare">Clare</option>
                                                        <option value="cork">Cork</option>
                                                        <option value="donegal">Donegal</option>
                                                        <option value="dublin">Dublin</option>
                                                        <option value="dun-laoghaire-rathdown">Dún Laoghaire-Rathdown</option>
                                                        <option value="fingal">Fingal</option>
                                                        <option value="galway">Galway</option>
                                                        <option value="kerry">Kerry</option>
                                                        <option value="kildare">Kildare</option>
                                                        <option value="kilkenny">Kilkenny</option>
                                                        <option value="laois">Laois</option>
                                                        <option value="leitrim">Leitrim</option>
                                                        <option value="limerick">Limerick</option>
                                                        <option value="longford">Longford</option>
                                                        <option value="louth">Louth</option>
                                                        <option value="mayo">Mayo</option>
                                                        <option value="meath">Meath</option>
                                                        <option value="monaghan">Monaghan</option>
                                                        <option value="offaly">Offaly</option>
                                                        <option value="roscommon">Roscommon</option>
                                                        <option value="sligo">Sligo</option>
                                                        <option value="south-dublin">South Dublin</option>
                                                        <option value="tipperary">Tipperary</option>
                                                        <option value="waterford">Waterford</option>
                                                        <option value="westmeath">Westmeath</option>
                                                        <option value="wexford">Wexford</option>
                                                        <option value="wicklow">Wicklow</option>
                                                    </optgroup>
                                                </select>

                                                {{-- Validation Alert Messages --}}
                                                @error('county')
                                                    <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $message }}</strong>
                                                    </span>
                                                @enderror
                                            </div>
                                        </div>

                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label class="control-label">Client Nationality <span class="required">*</span></label>
                                                <input type="text" autocomplete="off" name="client_nationality" class="form-control  @error('client_nationality') is-invalid @enderror" value="{{ $client->client_nationality }}" placeholder="eg.: Bangladeshi" required/>

                                                {{-- Validation Alert Messages --}}
                                                @error('client_nationality')
                                                    <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $message }}</strong>
                                                    </span>
                                                @enderror
                                            </div>
                                        </div>

                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label class="control-label">Home Town / City <span class="required">*</span></label>
                                                <input type="text" autocomplete="off" name="town_city" value="{{ $client->town_city }}" class="form-control  @error('town_city') is-invalid @enderror" placeholder="eg.: Home Town" required/>

                                                {{-- Validation Alert Messages --}}
                                                @error('town_city')
                                                    <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $message }}</strong>
                                                    </span>
                                                @enderror
                                            </div>
                                        </div>

                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label class="control-label">Postal Code <span class="required">*</span></label>
                                                <input type="text" autocomplete="off" name="post_code" value="{{ $client->post_code }}" class="form-control  @error('post_code') is-invalid @enderror" placeholder="eg.: 3224" required/>

                                                {{-- Validation Alert Messages --}}
                                                @error('post_code')
                                                    <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $message }}</strong>
                                                    </span>
                                                @enderror
                                            </div>
                                        </div>

                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label class="control-label">House No / Name <span class="required">*</span></label>
                                                <input type="text" autocomplete="off" name="house_no_name" value="{{ $client->house_no_name }}" class="form-control  @error('house_no_name') is-invalid @enderror" placeholder="eg.: 12/B Bilash Bhaban, Uganda" required/>

                                                {{-- Validation Alert Messages --}}
                                                @error('house_no_name')
                                                    <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $message }}</strong>
                                                    </span>
                                                @enderror
                                            </div>
                                        </div>

                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <label class="control-label">Address <span class="required">*</span></label>
                                                <textarea name="address" rows="3" class="form-control  @error('address') is-invalid @enderror" placeholder="eg.: Jhon Doe Address" required>{{ $client->address }}</textarea>

                                                {{-- Validation Alert Messages --}}
                                                @error('address')
                                                    <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $message }}</strong>
                                                    </span>
                                                @enderror
                                            </div>
                                        </div>

                                    </div>
                                </div>
                            </section>


                            {{-- Extra Informations  --}}
                            <section class="panel panel-danger">
                                <header class="panel-heading">
                                    <h2 class="panel-title">Extra Informations</h2>
                                </header>

                                <div class="panel-body">
                                    <div class="row">

                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <label class="control-label">Client Corporate Name </label>
                                                <input type="text" autocomplete="off" name="corporate_name" class="form-control  @error('corporate_name') is-invalid @enderror" placeholder="eg.: Client Corporate Name" value="{{ $client->corporate_name }}" />
                                                <small><span class="required">**</span> Fillup This Field if The Client is a Corporate Client <span class="required">**</span></small>

                                                {{-- Validation Alert Messages --}}
                                                @error('corporate_name')
                                                    <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $message }}</strong>
                                                    </span>
                                                @enderror
                                            </div>
                                        </div>

                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label class="control-label">Client Status <span class="required">*</span></label>
                                                <select name="client_status" data-plugin-selectTwo class="form-control populate  @error('client_status') is-invalid @enderror" required>
                                                    <option value="1" @if($client->client_status == 1) selected @endif>Active</option>
                                                    <option value="0" @if($client->client_status == 0) selected @endif>Non-Active</option>
                                                </select>

                                                {{-- Validation Alert Messages --}}
                                                @error('client_status')
                                                    <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $message }}</strong>
                                                    </span>
                                                @enderror
                                            </div>
                                        </div>

                                        {{-- <div class="col-md-3">
                                            <div class="form-group">
                                                <label class="control-label">Client Priority <span class="required">*</span></label>
                                                <select name="grading" data-plugin-selectTwo class="form-control populate  @error('grading') is-invalid @enderror" required>
                                                    <option disabled selected>Select Grading</option>
                                                    @foreach($gradings as $grading)
                                                    <option value="{{ $grading->id }}" @if($client->grading == $grading->id ) selected @endif>{{ $grading->name }}</option>
                                                    @endforeach
                                                </select>

                                                <!-- Validation Alert Messages -->
                                                @error('grading')
                                                    <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $message }}</strong>
                                                    </span>
                                                @enderror
                                            </div>
                                        </div> --}}

                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label class="control-label">Adviser <span class="required">*</span></label>
                                                <select name="advisor" data-plugin-selectTwo class="form-control populate  @error('adviser') is-invalid @enderror" required>
                                                    <option disabled selected>Select Advisor</option>
                                                    @foreach($advisors as $advisor)
                                                    <option value="{{ $advisor->id }}" @if($client->advisor == $advisor->id )selected @endif>{{ $advisor->saluation." ".$advisor->forname." ".$advisor->surname }}</option>
                                                    @endforeach
                                                </select>

                                                {{-- Validation Alert Messages --}}
                                                @error('adviser')
                                                    <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $message }}</strong>
                                                    </span>
                                                @enderror
                                            </div>
                                        </div>

                                        <?php
                                            $contactBy = json_decode($client->contactable_by, true);
                                            // dd($contactBy);
                                        ?>

                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <label class="control-label">Contactable By</label>
                                                <div class="row">
                                                    <div class="col-md-2">
                                                        <div class="checkbox-custom checkbox-default">
                                                            <input type="checkbox" name="contactable_by[mail]" class=" @error('con_by_mail') is-invalid @enderror" @if(isset($contactBy['mail']) && $contactBy['mail'] == 'on') checked="true" @endif>
                                                            <label for="con_by_mail">Mail</label>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-2">
                                                        <div class="checkbox-custom checkbox-default">
                                                            <input type="checkbox" name="contactable_by[email]" class=" @error('con_by_email') is-invalid @enderror" @if(isset($contactBy['email']) && $contactBy['email'] == 'on') checked="true" @endif>
                                                            <label for="con_by_email">E-Mail</label>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-2">
                                                        <div class="checkbox-custom checkbox-default">
                                                            <input type="checkbox" name="contactable_by[phone]" class=" @error('con_by_phone') is-invalid @enderror" @if(isset($contactBy['phone']) &&$contactBy['phone'] == 'on') checked="true" @endif>
                                                            <label for="con_by_phone">Phone</label>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-2">
                                                        <div class="checkbox-custom checkbox-default">
                                                            <input type="checkbox" name="contactable_by[sms]" class=" @error('con_by_sms') is-invalid @enderror" @if(isset($contactBy['sms']) && $contactBy['sms'] == 'on') checked="true" @endif>
                                                            <label for="con_by_sms">SMS</label>
                                                        </div>
                                                    </div>
                                                </div>

                                                {{-- Validation Alert Messages --}}
                                                @error('con_by_mail')
                                                    <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $message }}</strong>
                                                    </span>
                                                @enderror

                                                {{-- Validation Alert Messages --}}
                                                @error('con_by_email')
                                                    <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $message }}</strong>
                                                    </span>
                                                @enderror

                                                {{-- Validation Alert Messages --}}
                                                @error('con_by_phone')
                                                    <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $message }}</strong>
                                                    </span>
                                                @enderror

                                                {{-- Validation Alert Messages --}}
                                                @error('con_by_sms')
                                                    <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $message }}</strong>
                                                    </span>
                                                @enderror

                                                {{-- Validation Alert Messages --}}
                                                @error('con_by_social')
                                                    <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $message }}</strong>
                                                    </span>
                                                @enderror
                                            </div>
                                        </div>

                                    </div>
                                </div>
                            </section>
                        </div>

                        <footer class="panel-footer text-right">
                            <button type="submit" class="btn btn-dark btn-custom btn-sm">
                                Update Now
                            </button>
                        </footer>
                    </section>
                </form>


                {{-- Supervisor Checklists --}}
                <section class="panel panel-danger">
                    <header class="panel-heading">
                        <div class="panel-actions">
                            <a href="#" class="fa fa-caret-down"></a>
                        </div>

                        <h2 class="panel-title">Supervisor Checklists</h2>
                    </header>


                    @php
                        $checklists = $client->checklists;

                        $item_lists = [
                            "Fact Find / Know Your Customer / Attitude To Mortgage Risk",
                            "Evidence of client verification – ID VA",
                            "Sanction Search / High Risk / High Net Worth Clients",
                            "Income verification / Proof of Income including benefits statements, tenancy agr",
                            "4 or more BTL SA302 and tax year overview.",
                            "Last 3 months bank statements",
                            "Proof of deposit: purchase only",
                            "Product Recommendation",
                            "Disclosure Document issued/BTL TOB",
                            "Research (e.g. Mortgage Brain, Trigold, Webline, Assureweb etc)",
                            "KFI / Illustration",
                            "Client Authority to Proceed",
                            "Fee Agreement ",
                            "Scheme Abuse Declaration",
                            "DIP & Application",
                            "Suitability Report / Demands & Needs Statement issued within 5 working days"

                        ];

                        function isUpload($checklists, $id){
                            foreach($checklists as $list){
                                if($list->checklist_id == $id) return false;
                            }
                            return true;
                        }
                    @endphp

                    <div class="panel-body">
                        <section class="panel">
                            <header class="panel-heading bg-warning">
                                <h2 class="panel-title">Upload Checklist Document</h2>
                            </header>

                            <form action="{{ route('director.client.upload.checklist', ['client_id' => $client->id]) }}" method="post" enctype="multipart/form-data">
                                @csrf
                                <div class="panel-body">

                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label class="control-label">Checklist Item <span class="required">*</span></label>
                                            <select name="checklist_id" data-plugin-selectTwo class="form-control populate  @error('checklist_id') is-invalid @enderror" required>
                                                <option disabled selected>Select Checklist Item</option>
                                                @if( isUpload($checklists, 1) )<option value="1">Fact Find / Know Your Customer / Attitude To Mortgage Risk</option> @endif
                                                @if( isUpload($checklists, 2) )<option value="2">Evidence of client verification – ID VA</option> @endif
                                                @if( isUpload($checklists, 3) )<option value="3">Sanction Search / High Risk / High Net Worth Clients</option> @endif
                                                @if( isUpload($checklists, 4) )<option value="4">Income verification / Proof of Income including benefits statements, tenancy agreement if BTL.</option> @endif
                                                @if( isUpload($checklists, 5) )<option value="5">4 or more BTL SA302 and tax year overview.</option> @endif
                                                @if( isUpload($checklists, 6) )<option value="6">Last 3 months bank statements</option> @endif
                                                @if( isUpload($checklists, 7) )<option value="7">Proof of deposit: purchase only</option> @endif
                                                @if( isUpload($checklists, 8) )<option value="8">Product Recommendation</option> @endif
                                                @if( isUpload($checklists, 9) )<option value="9">Disclosure Document issued/BTL TOB</option> @endif
                                                @if( isUpload($checklists, 10) )<option value="10">Research (e.g. Mortgage Brain, Trigold, Webline, Assureweb etc)</option> @endif
                                                @if( isUpload($checklists, 11) )<option value="11">KFI / Illustration</option> @endif
                                                @if( isUpload($checklists, 12) )<option value="12">Client Authority to Proceed</option> @endif
                                                @if( isUpload($checklists, 13) )<option value="13">Fee Agreement </option> @endif
                                                @if( isUpload($checklists, 14) )<option value="14">Scheme Abuse Declaration</option> @endif
                                                @if( isUpload($checklists, 15) )<option value="15">DIP & Application</option> @endif
                                                @if( isUpload($checklists, 16) )<option value="16">Suitability Report / Demands & Needs Statement issued within 5 working days</option> @endif

                                            </select>

                                            {{-- Validation Alert Messages --}}
                                            @error('checklist_id')
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                            @enderror
                                        </div>
                                    </div>

                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label class="control-label">Checklist Document <span class="required">*</span></label>
                                            <div class="fileupload fileupload-new" data-provides="fileupload">
                                                <div class="input-append">
                                                    <div class="uneditable-input">
                                                        <i class="fa fa-file fileupload-exists"></i>
                                                        <span class="fileupload-preview"></span>
                                                    </div>
                                                    <span class="btn btn-default btn-file">
                                                        <span class="fileupload-exists">Change</span>
                                                        <span class="fileupload-new">Select file</span>
                                                        <input type="file" name="checklist_file"/>
                                                    </span>
                                                    <a href="#" class="btn btn-default fileupload-exists" data-dismiss="fileupload">Remove</a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                </div>

                                <div class="panel-footer text-right">
                                    <button type="submit" class="btn btn-dark btn-custom btn-upload btn-sm" id="upload_btn">Upload Now</button>
                                </div>
                            </form>
                        </section>

                        <section class="panel">
                            <header class="panel-heading bg-warning">
                                <h2 class="panel-title">All Checklist Items</h2>
                            </header>

                            <div class="panel-body">
                                <table class="table table-bordered table-striped mb-none" id="datatable-default">
                                    <thead>
                                        <tr>
                                            <th>Sl.</th>
                                            <th>Type</th>
                                            <th>Checklist</th>
                                            <th>Upload Date</th>
                                            <th>Action</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach($checklists as $srl => $item)
                                            <tr class="table-body-row">
                                                <th>{{ $srl+1 }}</th>
                                                <th>{{ $item_lists[$item->checklist_id-1] }}</th>
                                                <th class="document"><a href="{{ route('director.client.download.checklist', ['id' => $item->id]) }}">{{ $item->file_name }} <i class="fa fa-download"></i></a></th>
                                                <td>{{ App\Http\Controllers\General::reverseDateFormatDashToSlash($item->updated_at) }}</td>
                                                <td class="text-center">

                                                    <a href="{{ route('director.client.delete.checklist', ['id' => $item->id]) }}" class="btn btn-danger btn-sm btn-sm-custom btn-custom-danger btn-delete" onclick="return isDelete()">
                                                        <i class="fa fa-trash"></i>
                                                    </a>
                                                    <a href="{{ route('director.client.download.checklist', ['id' => $item->id]) }}" class="btn btn-dark btn-sm btn-sm-custom btn-custom-info btn-download">
                                                        <i class="fa fa-cloud-download"></i>
                                                    </a>
                                                </td>
                                            </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                        </section>
                    </div>
                </section>
            </div>



            <div class="col-md-12 col-xl-6">
                {{-- Client Documents --}}
                <section class="panel panel-danger">
                    <header class="panel-heading">
                        <div class="panel-actions">
                            <a href="#" class="fa fa-caret-down"></a>
                        </div>

                        <h2 class="panel-title">Client Documents</h2>
                    </header>

                    <div class="panel-body">
                        <section class="panel">
                            <header class="panel-heading bg-warning">
                                <h2 class="panel-title">Upload New Document</h2>
                            </header>

                            <form action="{{ route('director.client.document_center.store') }}" method="post" enctype="multipart/form-data">
                                @csrf
                                <div class="panel-body">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label class="control-label">Document Title <span class="required">*</span></label>
                                            <input type="text" autocomplete="off" name="note" class="form-control @error('note') is-invalid @enderror" placeholder="eg.: Document Title"/>

                                            {{-- Validation Alert Messages --}}
                                            @error('note')
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                            @enderror
                                        </div>
                                    </div>

                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label class="control-label">Document <span class="required">*</span></label>
                                            <div class="fileupload fileupload-new" data-provides="fileupload">
                                                <div class="input-append">
                                                    <div class="uneditable-input">
                                                        <i class="fa fa-file fileupload-exists"></i>
                                                        <span class="fileupload-preview"></span>
                                                    </div>
                                                    <span class="btn btn-default btn-file">
                                                        <span class="fileupload-exists">Change</span>
                                                        <span class="fileupload-new">Select file</span>
                                                        <input type="file" name="document"/>
                                                    </span>
                                                    <a href="#" class="btn btn-default fileupload-exists" data-dismiss="fileupload">Remove</a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label class="control-label">Client Policies</label>
                                            <select name="policy" data-plugin-selectTwo class="form-control populate  @error('category') is-invalid @enderror">
                                                <option disabled selected>select Policy</option>
                                                @foreach ($policies as $policy)
                                                    <option value="{{ $policy->id }}">{{ $policy->policy_name." ".$policy->specific_policy_type }}</option>
                                                @endforeach
                                            </select>

                                            {{-- Validation Alert Messages --}}
                                            @error('category')
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                            @enderror
                                        </div>
                                    </div>
                                    <input type="hidden" name="category" value="2">
                                    <input type="hidden" name="client" value="{{ $client->id }}">
                                    <input type="hidden" name="client_page" value="on">

                                </div>

                                <div class="panel-footer text-right">
                                    <button type="submit" class="btn btn-dark btn-custom btn-upload btn-sm" id="upload_btn">Upload Now</button>
                                </div>
                            </form>
                        </section>

                        <section class="panel">
                            <header class="panel-heading bg-warning">
                                <h2 class="panel-title">All Documents</h2>
                            </header>

                            <div class="panel-body">
                                <table class="table table-bordered table-striped mb-none" id="datatable-default">
                                    <thead>
                                        <tr>
                                            <th>Sl.</th>
                                            <th>Document</th>
                                            <th>Upload Date</th>
                                            <th>Action</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach($documents as $key => $document)
                                            <tr class="table-body-row">
                                                <th>{{ $key+1 }}</th>
                                                <th class="document"><a href="{{ route('director.client.document_center.download', ['id' => $document->id]) }}">{{ $document->file_name }} <i class="fa fa-download"></i></a></th>
                                                <td>{{ App\Http\Controllers\General::reverseDateFormatDashToSlash($document->updated_at) }}</td>
                                                <td class="text-center">
                                                    <a href="{{ route('director.client.document_center.destroy', ['id' => $document->id]) }}" class="btn btn-danger btn-sm btn-sm-custom btn-custom-danger btn-delete" onclick="return isDelete();">
                                                        <i class="fa fa-trash"></i>
                                                    </a>
                                                    <a href="{{ route('director.client.document_center.download', ['id' => $document->id]) }}" class="btn btn-dark btn-sm btn-sm-custom btn-custom-info btn-download">
                                                        <i class="fa fa-cloud-download"></i>
                                                    </a>
                                                    {{-- <a href="{{ route('director.client.document_center.show', ['id' =>$document->id]) }}" class="btn btn-dark btn-sm btn-sm-custom btn-custom-info">
                                                        <i class="fa fa-info"></i> --}}
                                                    </a>
                                                </td>
                                            </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                        </section>
                    </div>
                </section>


                {{-- Client Documents --}}
                <section class="panel panel-danger">
                    <header class="panel-heading">
                        <div class="panel-actions">
                            <a href="#" class="fa fa-caret-down"></a>
                        </div>

                        <h2 class="panel-title">Client Policies</h2>
                    </header>

                    <div class="panel-body">

                        <section class="panel">
                            <header class="panel-heading bg-warning">
                                <h2 class="panel-title">All Policies</h2>
                            </header>

                            <div class="panel-body">
                                <table class="table table-bordered table-striped mb-none" id="datatable-default">
                                    <thead>
                                        <tr>
                                            <th>Sl.</th>
                                            <th>Policy</th>
                                            <th>Date Case Opened</th>
                                            <th>Action</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach($policies as $list => $policy)
                                            <tr class="table-body-row">
                                                <th>{{ $list+1 }}</th>
                                                <th class="document">{{ $policy->policy_name }}</th>
                                                <td>{{ App\Http\Controllers\General::reverseDateFormatDashToSlash($policy->policy_request_date) }}</td>
                                                <td class="text-center">
                                                    @php
                                                        // if($policy->policy_name == 'Investment')
                                                        //     $route = 'director.business.all_business.showInvestment';

                                                        if($policy->policy_name == 'Mortgage')
                                                            $route = 'director.business.all_business.showMortgage';

                                                        // if($policy->policy_name == 'Pension')
                                                        //     $route = 'director.business.all_business.showPension';

                                                        if($policy->policy_name == 'Protection')
                                                            $route = 'director.business.all_business.showProtection';
                                                    @endphp

                                                    <a href="{{ route($route, ['id' => $policy->id]) }}" target="_blank" class="btn btn-dark btn-sm btn-sm-custom btn-custom-info">
                                                        <i class="fa fa-info"></i>
                                                    </a>
                                                </td>
                                            </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                        </section>
                    </div>
                </section>


                {{-- {{ dd($client->partners) }} --}}

                {{-- Client Partners --}}
                <section class="panel panel-danger">
                    <header class="panel-heading">
                        <div class="panel-actions">
                            <a href="#" class="fa fa-caret-down"></a>
                        </div>

                        <h2 class="panel-title">Associated Client</h2>
                    </header>

                    @php
                        $partners = array();
                        $partners = (array) json_decode($client->partners, true);
                    @endphp

                    <div class="panel-body">
                        <section class="panel">
                            <header class="panel-heading bg-warning">
                                <h2 class="panel-title">Add Associated Client</h2>
                            </header>

                            <form action="{{ route('director.client.assign_partner', ['client_id' => $client->id]) }}" method="post">
                                @csrf
                                <div class="panel-body">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label class="control-label">Select Associated Client <span class="required">*</span></label>
                                            <select name="partner" data-plugin-selectTwo class="form-control populate @error('partner') is-invalid @enderror" required>
                                                <option selected>Select Associated Client</option>
                                                @foreach($clients as $list)
                                                    @if(!in_array($list->id, $partners))
                                                    <option value="{{ $list->id }}">{{ $list->client_title." ".$list->client_surname." ".$list->client_forname }}</option>
                                                    @endif
                                                @endforeach
                                            </select>

                                            {{-- Validation Alert Messages --}}
                                            @error('partner')
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                            @enderror
                                        </div>
                                    </div>
                                </div>

                                <div class="panel-footer text-right">
                                    <button type="submit" class="btn btn-dark btn-custom btn-sm">Assign Associated Client</button>
                                </div>
                            </form>
                        </section>

                        <section class="panel">
                            <header class="panel-heading bg-warning">
                                <h2 class="panel-title">Associated Clients</h2>
                            </header>
                            <div class="panel-body">
                                <table class="table table-bordered table-striped mb-none">
                                    <thead>
                                        <tr>
                                            <th>Name</th>
                                            <th>Mobile Number</th>
                                            <th>Email</th>
                                            <th class="text-center">Action</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach ($partners as $partner_id)
                                            @php
                                                $partner = App\Models\Director\Clients\Client\Client::find($partner_id);
                                            @endphp
                                            @if($partner != null)
                                            <tr>
                                                <td>{{ $partner->client_title." ".$partner->client_forname." ".$partner->client_surname }}</td>
                                                <td>{{ $partner->mobile_phone }}</td>
                                                <td>{{ $partner->email }}</td>
                                                <td class="text-center">
                                                    <a href="{{ route('director.client.delete_partner', ['client' => $client->id, 'partner_id' => $partner_id]) }}" class="btn btn-danger btn-sm btn-sm-custom btn-custom-danger btn-delete" onclick="return confirm('Are You Sure Want To Delete The Partner...?')">
                                                        <i class="fa fa-trash"></i>
                                                    </a>
                                                </td>
                                            </tr>
                                            @else
                                                <tr>
                                                    <td>Partner is Disclose !!!</td>
                                                    <td>-</td>
                                                    <td>-</td>
                                                    <td class="text-center">
                                                        <a href="{{ route('director.client.delete_partner', ['client' => $client->id, 'partner_id' => $partner_id]) }}" class="btn btn-danger btn-sm btn-sm-custom btn-custom-danger btn-delete" onclick="return confirm('Are You Sure Want To Delete The Partner...?')">
                                                            <i class="fa fa-trash"></i>
                                                        </a>
                                                    </td>
                                                </tr>
                                            @endif
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </section>
                    </div>
                </section>


                {{-- Other Informations --}}
                <section class="panel panel-danger">
                    <header class="panel-heading">
                        <div class="panel-actions">
                            <a href="#" class="fa fa-caret-down"></a>
                        </div>

                        <h2 class="panel-title">Notes</h2>
                    </header>

                    <div class="panel-body">
                        <section class="panel">
                            <header class="panel-heading bg-warning">
                                <h2 class="panel-title">Add New Note</h2>
                            </header>

                            <form action="{{ route('director.client.add_note', ['client' => $client->id]) }}" method="post">
                                @csrf
                                <div class="panel-body">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label class="control-label">Note <span class="required">*</span></label>
                                            <textarea name="note" placeholder="Note is here..."  class="form-control populate @error('note') is-invalid @enderror" required></textarea>

                                            {{-- Validation Alert Messages --}}
                                            @error('note')
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                            @enderror
                                        </div>
                                    </div>
                                </div>

                                <div class="panel-footer text-right">
                                    <button type="submit" class="btn btn-dark btn-custom btn-sm">Add Note</button>
                                </div>
                            </form>
                        </section>
                        <table class="table table-no-more table-bordered table-striped mb-none">
                            <thead>
                                <tr>
                                    <th>Date</th>
                                    <th>Time</th>
                                    <th>Notes</th>
                                    <th>User</th>
                                    <th class="text-center">Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                @php
                                    $notes = array();
                                    $notes = (array) json_decode($client->notes, true);
                                @endphp
                                @foreach($notes as $srl => $note)
                                    <tr>
                                        <td>{{ $note['date'] }}</td>
                                        <td>{{ $note['time'] }}</td>
                                        <td>{{ $note['note'] }}</td>
                                        @php
                                            $admin = App\User::find($note['admin']);
                                        @endphp
                                        <td>{{ $admin->forname." ".$admin->surname }}</td>
                                        <td class="text-center">
                                            @if (Auth::user()->id == $note['admin'])
                                                <a href="{{ route('director.client.delete_note', ['client' => $client->id, 'key' => $srl]) }}" class="btn btn-danger btn-sm btn-sm-custom btn-custom-danger btn-delete" onclick="return isDelete();">
                                                    <i class="fa fa-trash"></i>
                                                </a>
                                            @endif
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </section>
            </div>

        </div>
    </div>
</section>
<!-- end: page -->

@endsection


@section('script_links')
    {{--  External Javascript  --}}
    <script src="{{ asset('custom/assets/vendor/bootstrap-datepicker/js/bootstrap-datepicker.js') }}"></script>
    <script src="{{ asset('custom/assets/vendor/select2/select2.js') }}"></script>
    <script src="{{ asset('custom/assets/vendor/jquery-datatables/media/js/jquery.dataTables.js') }}"></script>
    <script src="{{ asset('custom/assets/vendor/jquery-datatables/extras/TableTools/js/dataTables.tableTools.min.js') }}"></script>
    <script src="{{ asset('custom/assets/vendor/jquery-datatables-bs3/assets/js/datatables.js') }}"></script>

    <script src="{{ asset('custom/assets/javascripts/tables/examples.datatables.default.js') }}"></script>
    <script src="{{ asset('custom/assets/javascripts/tables/examples.datatables.row.with.details.js') }}"></script>
    <script src="{{ asset('custom/assets/javascripts/tables/examples.datatables.tabletools.js') }}"></script>

    <script src="{{ asset('custom/assets/vendor/bootstrap-fileupload/bootstrap-fileupload.min.js') }}"></script>

@endsection

@section('scripts')
    {{--  External Javascript  --}}
    <script>
    $(document).ready(function(){
        var remove = $('.remove-disable');
        var edit = $('#edit_btn');
        var cancel = $('#cancel_btn');
        var update = $('#update_btn');

        edit.click(function() {
            remove.removeAttr('disabled');
            edit.addClass('d-none');
            cancel.removeClass('d-none');
            update.removeClass('d-none');
        });

        cancel.click(function() {
            remove.attr('disabled', 'true');
            edit.removeClass('d-none');
            cancel.addClass('d-none');
            update.addClass('d-none');
        });
    });
    </script>

    <script>
        $(document).ready(function(){
            var client_title = '{{ $client->client_title }}';
            $("#client_title").find('option[value=' + client_title + ']').attr('selected', 'selected');
            var name = $("#client_title").find('option[value=' + client_title + ']').html();
            // console.log(name);
            $('#s2id_client_title .select2-chosen').html(name);


            var client_county = '{{ $client->county }}';
            $("#client_county").find('option[value=' + client_county + ']').attr('selected', 'selected');
            var name = $("#client_county").find('option[value=' + client_county + ']').html();
            console.log(name);
            $('#s2id_client_county .select2-chosen').html(name);
        });
    </script>
@endsection
