@extends('layouts.director.app')

@section('page_title', '| Clients')

@section('stylesheet_links')
    {{--  External CSS  --}}
    <link rel="stylesheet" href="{{ asset('custom/assets/vendor/select2/select2.css') }}" />
    <link rel="stylesheet" href="{{ asset('custom/assets/vendor/jquery-datatables-bs3/assets/css/datatables.css') }}" />

@endsection

@section('stylesheet')
    {{--  External CSS  --}}
    <style>
        .dataTables_wrapper .DTTT.btn-group {
            display: none !important;
            position: absolute;
            top: -74px;
            right: 0px;
        }
        .approved{
            background-color: green;
            color: #ffffff;
            padding: 2px 7px;
            border-radius: 3px;
            font-weight: 600;
        }
        .unapproved{
            background-color: red;
            color: #ffffff;
            padding: 2px 7px;
            border-radius: 3px;
            font-weight: 600;
        }
        .pending{
            background-color: yellow;
            color: #000000;
            padding: 2px 7px;
            border-radius: 3px;
            font-weight: 600;
        }
    </style>
@endsection

@section('content')

<header class="page-header">
    <h2>Quote Center</h2>

    <div class="right-wrapper pull-right">
        <ol class="breadcrumbs">
            <li>
                <a href="{{ Route('director.dashboard.index') }}">
                    <i class="fa fa-home"></i>
                </a>
            </li>
            <li><span class="active">Clients</span></li>
            <li><span>Quote Center</span></li>
        </ol>
    </div>
</header>

<!-- start: page -->
{{-- <section class="panel">
    <div class="panel-body">
        <div class="col-md-12">
            <form action="#" method="get">
                @csrf
                <div class="row">
                    <div class="col-md-8">
                        <select name="site" data-plugin-selectTwo class="form-control populate" required>
                            <option selected disabled>Select User</option>
                            <option value="user_01">user_01</option>
                            <option value="user_02">user_02</option>
                            <option value="user_03">user_03</option>
                        </select>
                    </div>
                    <div class="col-md-4">
                        <button class="btn btn-dark btn-custom btn-block" type="submit">Filter By user</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
</section>

<section class="panel">
    <header class="panel-heading">
        <h2 class="panel-title">All Complaints</h2>
        <a href="#" class="btn btn-dark btn-custom btn-sm header-btn">Add New Quote</a>
    </header>
    <div class="panel-body">
        <table class="table table-bordered table-striped mb-none" id="datatable-tabletools" data-swf-path="assets/vendor/jquery-datatables/extras/TableTools/swf/copy_csv_xls_pdf.swf">
            <thead>
                <tr class="table-header-row">
                    <th class="text-center">Sl.</th>
                    <th class="text-center">Client</th>
                    <th class="text-center">Date</th>
                    <th class="text-center">Name</th>
                    <th class="text-center">Risk Level</th>
                    <th class="text-center">Invest</th>
                    <th class="text-center">Risk Profile Status</th>
                    <th class="text-center">Actions</th>
                </tr>
            </thead>
            <tbody>
                <tr class="table-body-row">
                    <th class="text-center">01</th>
                    <th class="text-center">Client_name</th>
                    <th class="text-center">20_12_2019</th>
                    <th class="text-center">name</th>
                    <th class="text-center">10</th>
                    <th class="text-center">invest</th>

                    <th class="text-center"><span class="approved">Complete</span></th>
                    <th class="text-center"><span class="unapproved">Incomplete</span></th>
                    <th class="text-center"><span class="pending">Pending</span></th>

                    <td class="action-td text-center">
                        <a href="#" class="btn btn-danger btn-sm btn-sm-custom btn-custom-danger btn-delete" onclick="return confirm('Are You Sure Want To Delete This Quote...?')">
                            <i class="fa fa-trash" aria-hidden="true"></i>
                        </a>
                        <a href="#" class="btn btn-dark btn-sm btn-sm-custom btn-custom-info"><i class="fa fa-info"></i></a>
                    </td>
                </tr>
            </tbody>
        </table>
    </div>
</section> --}}
<!-- end: page -->

@endsection


@section('script_links')
    {{--  External Javascript  --}}
    <script src="{{ asset('custom/assets/vendor/select2/select2.js') }}"></script>
    <script src="{{ asset('custom/assets/vendor/jquery-datatables/media/js/jquery.dataTables.js') }}"></script>
    <script src="{{ asset('custom/assets/vendor/jquery-datatables/extras/TableTools/js/dataTables.tableTools.min.js') }}"></script>
    <script src="{{ asset('custom/assets/vendor/jquery-datatables-bs3/assets/js/datatables.js') }}"></script>

    <script src="{{ asset('custom/assets/javascripts/tables/examples.datatables.default.js') }}"></script>
    <script src="{{ asset('custom/assets/javascripts/tables/examples.datatables.row.with.details.js') }}"></script>
    <script src="{{ asset('custom/assets/javascripts/tables/examples.datatables.tabletools.js') }}"></script>

@endsection

@section('scripts')
    {{--  External Javascript  --}}

@endsection
