@extends('layouts.director.app')

@section('page_title', '| Clients')

@section('stylesheet_links')
    {{--  External CSS  --}}
    <link rel="stylesheet" href="{{ asset('custom/assets/vendor/select2/select2.css') }}" />
    <link rel="stylesheet" href="{{ asset('custom/assets/vendor/jquery-datatables-bs3/assets/css/datatables.css') }}" />
@endsection

@section('stylesheet')
    {{--  External CSS  --}}
    <style>
        .dataTables_wrapper .DTTT.btn-group {
            display: inline-block;
            position: absolute;
            top: -74px;
            right: 0px;
        }
        .member-active{
            background-color: green;
            color: #ffffff;
            padding: 2px 7px;
            border-radius: 3px;
            font-weight: 600;
        }
        .member-inactive{
            background-color: red;
            color: #ffffff;
            padding: 2px 7px;
            border-radius: 3px;
            font-weight: 600;
        }
        .panel{
            border: 1px solid #eee;
        }
    </style>
@endsection

@section('content')

<header class="page-header">
    <h2>Risk List Details</h2>

    <div class="right-wrapper pull-right">
        <ol class="breadcrumbs">
            <li>
                <a href="{{ Route('director.dashboard.index') }}">
                    <i class="fa fa-home"></i>
                </a>
            </li>
            <li><span class="active">Clients</span></li>
            <li>
                <a href="{{ Route('director.client.risk_list.index') }}"><span class="active">Risk Lists</span></a>
            </li>
            <li><span>risk_name</span></li>
        </ol>
    </div>
</header>

<!-- start: page -->
<section class="detials-page">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-4 col-lg-4 col-xl-4 col-centered">
                {{-- Profile Details  --}}
                <section class="panel panel-danger">
                    <form action="{{ route('director.client.risk_list.update', ['id' => $risk_list->id]) }}" method="post">
                    @csrf
                        <header class="panel-heading">
                            <div class="panel-actions">
                                <a href="#" class="fa fa-caret-down"></a>
                            </div>

                            <h2 class="panel-title">risk_name</h2>
                        </header>

                        <div class="panel-body">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label class="control-label">Risk Name <span class="required">*</span></label>
                                        <input type="text" autocomplete="off" name="risk_name" class="form-control remove-disable @error('risk_name') is-invalid @enderror" placeholder="Eg.: Platinum +" value="{{ $risk_list->name }}" required disabled>

                                        {{-- Validation Alert Messages --}}
                                        @error('risk_name')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                        @enderror
                                    </div>
                                </div>

                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label class="control-label">Status <span class="required">*</span></label>
                                        <select name="risk_status" data-plugin-selectTwo class="form-control populate remove-disable  @error('risk_status') is-invalid @enderror" required disabled>
                                            <option value="1" @if($risk_list->status == 1)selected @endif>Active</option>
                                            <option value="0" @if($risk_list->status == 0)selected @endif>Inactive</option>
                                        </select>

                                        {{-- Validation Alert Messages --}}
                                        @error('risk_status')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                        @enderror
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="panel-footer text-right">
                            <button type="button" class="btn btn-dark btn-custom btn-edit btn-sm" id="edit_btn">Edit</button>
                            <button type="button" class="btn btn-danger btn-custom btn-cancel btn-sm d-none" id="cancel_btn">Cancel</button>
                            <button class="btn btn-success btn-custom btn-update btn-sm d-none" id="update_btn" type="submit">Update</button>
                        </div>
                    </form>
                </section>
            </div>
        </div>
    </div>
</section>

@endsection


@section('script_links')
    {{--  External Javascript  --}}
    <script src="{{ asset('custom/assets/vendor/select2/select2.js') }}"></script>
    <script src="{{ asset('custom/assets/vendor/jquery-datatables/media/js/jquery.dataTables.js') }}"></script>
    <script src="{{ asset('custom/assets/vendor/jquery-datatables/extras/TableTools/js/dataTables.tableTools.min.js') }}"></script>
    <script src="{{ asset('custom/assets/vendor/jquery-datatables-bs3/assets/js/datatables.js') }}"></script>

    <script src="{{ asset('custom/assets/javascripts/tables/examples.datatables.default.js') }}"></script>
    <script src="{{ asset('custom/assets/javascripts/tables/examples.datatables.row.with.details.js') }}"></script>
    <script src="{{ asset('custom/assets/javascripts/tables/examples.datatables.tabletools.js') }}"></script>

    <script src="{{ asset('custom/assets/vendor/bootstrap-fileupload/bootstrap-fileupload.min.js') }}"></script>

@endsection

@section('scripts')
    {{--  External Javascript  --}}
    <script>
        $(document).ready(function(){
            var remove = $('.remove-disable');
            var edit = $('#edit_btn');
            var cancel = $('#cancel_btn');
            var update = $('#update_btn');

            edit.click(function() {
                remove.removeAttr('disabled');
                edit.addClass('d-none');
                cancel.removeClass('d-none');
                update.removeClass('d-none');
            });

            cancel.click(function() {
                remove.attr('disabled', 'true');
                edit.removeClass('d-none');
                cancel.addClass('d-none');
                update.addClass('d-none');
            });
        });
    </script>
@endsection
