@extends('layouts.director.app')

@section('page_title', '| Clients')

@section('stylesheet_links')
    {{--  External CSS  --}}
    <link rel="stylesheet" href="{{ asset('custom/assets/vendor/select2/select2.css') }}" />
@endsection

@section('stylesheet')
    {{--  External CSS  --}}
    <style>
    .panel{
        border: 1px solid #eee;
    }
    </style>
@endsection

@section('content')

<header class="page-header">
    <h2>Create New Risk</h2>

    <div class="right-wrapper pull-right">
        <ol class="breadcrumbs">
            <li>
                <a href="{{ Route('director.dashboard.index') }}">
                    <i class="fa fa-home"></i>
                </a>
            </li>
            <li><span class="active">Clients</span></li>
            <li><span>Create New Risk</span></li>
        </ol>
    </div>
</header>

<!-- start: page -->
<section class="detials-page">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-4 col-lg-4 col-xl-4 col-centered">
                {{-- Profile Details  --}}
                <section class="panel panel-danger">
                    <form action="{{ route('director.client.risk_list.store') }}" method="post">
                    @csrf
                        <header class="panel-heading">
                            <div class="panel-actions">
                                <a href="#" class="fa fa-caret-down"></a>
                            </div>

                            <h2 class="panel-title">Create New Risk</h2>
                        </header>

                        <div class="panel-body">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label class="control-label">Risk Name <span class="required">*</span></label>
                                        <input type="text" autocomplete="off" name="risk_name" class="form-control @error('risk_name') is-invalid @enderror" placeholder="Eg.: Conservative" required>

                                        {{-- Validation Alert Messages --}}
                                        @error('risk_name')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                        @enderror
                                    </div>
                                </div>

                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label class="control-label">Status <span class="required">*</span></label>
                                        <select name="risk_status" data-plugin-selectTwo class="form-control populate  @error('risk_status') is-invalid @enderror" required>
                                            <option value="1">Active</option>
                                            <option value="0">Inactive</option>
                                        </select>

                                        {{-- Validation Alert Messages --}}
                                        @error('risk_status')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                        @enderror
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="panel-footer text-right">
                            <button type="submit" class="btn btn-dark btn-custom">Create Now</button>
                        </div>
                    </form>
                </section>
            </div>
        </div>
    </div>
</section>
<!-- end: page -->

@endsection


@section('script_links')
    {{--  External Javascript  --}}
    <script src="{{ asset('custom/assets/vendor/select2/select2.js') }}"></script>
@endsection

@section('scripts')
    {{--  External Javascript  --}}
    <script>

    </script>
@endsection
