@extends('layouts.director.app')

@section('page_title', '| Clients')

@section('stylesheet_links')
    {{--  External CSS  --}}
    <link rel="stylesheet" href="{{ asset('custom/assets/vendor/select2/select2.css') }}" />
    <link rel="stylesheet" href="{{ asset('custom/assets/vendor/jquery-datatables-bs3/assets/css/datatables.css') }}" />

@endsection

@section('stylesheet')
    {{--  External CSS  --}}
    <style>
        .dataTables_wrapper .DTTT.btn-group {
            display: none !important;
            position: absolute;
            top: -74px;
            right: 0px;
        }
        .member-active{
            background-color: green;
            color: #ffffff;
            padding: 2px 7px;
            border-radius: 3px;
            font-weight: 600;
        }
        .member-inactive{
            background-color: red;
            color: #ffffff;
            padding: 2px 7px;
            border-radius: 3px;
            font-weight: 600;
        }
    </style>

@endsection

@section('content')

<header class="page-header">
    <h2>Risk Lists</h2>

    <div class="right-wrapper pull-right">
        <ol class="breadcrumbs">
            <li>
                <a href="{{ Route('director.dashboard.index') }}">
                    <i class="fa fa-home"></i>
                </a>
            </li>
            <li><span class="active">Clients</span></li>
            <li><span>Risk Lists</span></li>
        </ol>
    </div>
</header>

<!-- start: page -->
<section class="detials-page">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12 col-lg-12 col-xl-12 col-centered">
                <section class="panel">
                    <header class="panel-heading">
                        <h2 class="panel-title">All Risk Lists</h2>
                        <a href="{{ route('director.client.risk_list.create') }}" class="btn btn-dark btn-custom btn-sm header-btn">Create New Risk</a>
                    </header>
                    <div class="panel-body">
                        <table class="table table-bordered table-striped mb-none" id="datatable-tabletools" data-swf-path="assets/vendor/jquery-datatables/extras/TableTools/swf/copy_csv_xls_pdf.swf">
                            <thead>
                                <tr class="table-header-row">
                                    <th>Sl.</th>
                                    <th>Risk Title</th>
                                    <th>Status</th>
                                    <th class="text-center">Actions</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($risk_lists as $key => $risk_list)
                                    <tr class="table-body-row">
                                        <th>{{ $key+1 }}</th>
                                        <th>{{ $risk_list->name }}</th>
                                        @if($risk_list->status == 1)
                                            <td><span class="member-active"><i class="fa fa-check"></i></span></td>
                                        @else
                                            <td><span class="member-inactive"><i class="fa fa-times"></i></span></td>
                                        @endif
                                        <td class="action-td text-center">
                                            <a href="{{ route('director.client.risk_list.show', ['id' => $risk_list->id]) }}" class="btn btn-dark btn-sm btn-sm-custom btn-custom-info">
                                                <i class="fa fa-info"></i>
                                            </a>
                                            <a href="{{ route('director.client.risk_list.destroy', ['id' => $risk_list->id]) }}" class="btn btn-danger btn-sm btn-sm-custom btn-custom-danger btn-delete">
                                                <i class="fa fa-trash"></i>
                                            </a>
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </section>
            </div>
        </div>
    </div>
</section>
<!-- end: page -->

@endsection


@section('script_links')
    {{--  External Javascript  --}}
    <script src="{{ asset('custom/assets/vendor/select2/select2.js') }}"></script>
    <script src="{{ asset('custom/assets/vendor/jquery-datatables/media/js/jquery.dataTables.js') }}"></script>
    <script src="{{ asset('custom/assets/vendor/jquery-datatables/extras/TableTools/js/dataTables.tableTools.min.js') }}"></script>
    <script src="{{ asset('custom/assets/vendor/jquery-datatables-bs3/assets/js/datatables.js') }}"></script>

    <script src="{{ asset('custom/assets/javascripts/tables/examples.datatables.default.js') }}"></script>
    <script src="{{ asset('custom/assets/javascripts/tables/examples.datatables.row.with.details.js') }}"></script>
    <script src="{{ asset('custom/assets/javascripts/tables/examples.datatables.tabletools.js') }}"></script>

@endsection

@section('scripts')
    {{--  External Javascript  --}}

@endsection
