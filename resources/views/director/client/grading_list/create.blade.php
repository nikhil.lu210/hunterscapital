@extends('layouts.director.app')

@section('page_title', '| Clients')

@section('stylesheet_links')
    {{--  External CSS  --}}
    <link rel="stylesheet" href="{{ asset('custom/assets/vendor/select2/select2.css') }}" />
    <link rel="stylesheet" href="{{ asset('custom/assets/vendor/bootstrap-fileupload/bootstrap-fileupload.min.css') }}" />
@endsection

@section('stylesheet')
    {{--  External CSS  --}}
    <style>
    .panel{
        border: 1px solid #eee;
    }
    .panel .panel .panel-heading{
        padding: 10px 15px;
    }
    .fileupload .uneditable-input .fa {
        top: 39px;
    }
    .col-centered{
        float: none !important;
        margin: 0 auto;
    }
    </style>
@endsection

@section('content')

<header class="page-header">
    <h2>Create New Greading</h2>

    <div class="right-wrapper pull-right">
        <ol class="breadcrumbs">
            <li>
                <a href="{{ Route('director.dashboard.index') }}">
                    <i class="fa fa-home"></i>
                </a>
            </li>
            <li><span class="active">Clients</span></li>
            <li><span>Create New Greading</span></li>
        </ol>
    </div>
</header>

<!-- start: page -->
<section class="detials-page">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-6 col-lg-6 col-xl-6 col-centered">
                {{-- Profile Details  --}}
                <section class="panel panel-danger">
                    <form action="{{ route('director.client.grading_list.store') }}" method="post" enctype="multipart/form-data">
                    @csrf
                        <header class="panel-heading">
                            <div class="panel-actions">
                                <a href="#" class="fa fa-caret-down"></a>
                            </div>

                            <h2 class="panel-title">Create New Greading</h2>
                        </header>

                        <div class="panel-body">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label class="control-label">Greading Icon <span class="required">*</span></label>
                                        <div class="fileupload fileupload-new" data-provides="fileupload">
                                            <div class="input-append">
                                                <div class="uneditable-input">
                                                    <i class="fa fa-file fileupload-exists"></i>
                                                    <span class="fileupload-preview"></span>
                                                </div>
                                                <span class="btn btn-default btn-file">
                                                    <span class="fileupload-exists">Change</span>
                                                    <span class="fileupload-new">Select file</span>
                                                    <input type="file" class="@error('greading_icon') is-invalid @enderror" name="greading_icon" accept=".png, .jpg, .jpeg" required/>
                                                </span>
                                                <a href="#" class="btn btn-default fileupload-exists" data-dismiss="fileupload">Remove</a>
                                            </div>
                                        </div>
                                        @error('greading_icon')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                        @enderror
                                    </div>
                                </div>

                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label class="control-label">Greading Name <span class="required">*</span></label>
                                        <input type="text" autocomplete="off" name="greading_name" class="form-control @error('greading_name') is-invalid @enderror" placeholder="Eg.: Platinum +" required>

                                        {{-- Validation Alert Messages --}}
                                        @error('greading_name')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                        @enderror
                                    </div>
                                </div>

                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label class="control-label">Status <span class="required">*</span></label>
                                        <select name="greading_status" data-plugin-selectTwo class="form-control populate  @error('greading_status') is-invalid @enderror" required>
                                            <option value="1">Active</option>
                                            <option value="-1">Inactive</option>
                                        </select>

                                        {{-- Validation Alert Messages --}}
                                        @error('greading_status')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                        @enderror
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="panel-footer text-right">
                            <button type="submit" class="btn btn-dark btn-custom">Create Now</button>
                        </div>
                    </form>
                </section>
            </div>
        </div>
    </div>
</section>
<!-- end: page -->

@endsection


@section('script_links')
    {{--  External Javascript  --}}
    <script src="{{ asset('custom/assets/vendor/select2/select2.js') }}"></script>

    <script src="{{ asset('custom/assets/vendor/bootstrap-fileupload/bootstrap-fileupload.min.js') }}"></script>

@endsection

@section('scripts')
    {{--  External Javascript  --}}
    <script>
    
    </script>
@endsection
