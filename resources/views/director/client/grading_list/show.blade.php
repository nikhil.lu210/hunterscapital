@extends('layouts.director.app')

@section('page_title', '| Clients')

@section('stylesheet_links')
    {{--  External CSS  --}}
    <link rel="stylesheet" href="{{ asset('custom/assets/vendor/select2/select2.css') }}" />
    <link rel="stylesheet" href="{{ asset('custom/assets/vendor/bootstrap-fileupload/bootstrap-fileupload.min.css') }}" />
    <link rel="stylesheet" href="{{ asset('custom/assets/vendor/jquery-datatables-bs3/assets/css/datatables.css') }}" />
@endsection

@section('stylesheet')
    {{--  External CSS  --}}
    <style>
        .dataTables_wrapper .DTTT.btn-group {
            display: inline-block;
            position: absolute;
            top: -74px;
            right: 0px;
        }
        .member-active{
            background-color: green;
            color: #ffffff;
            padding: 2px 7px;
            border-radius: 3px;
            font-weight: 600;
        }
        .member-inactive{
            background-color: red;
            color: #ffffff;
            padding: 2px 7px;
            border-radius: 3px;
            font-weight: 600;
        }
        .panel{
            border: 1px solid #eee;
        }
        .panel .panel .panel-heading{
            padding: 10px 15px;
        }
        .fileupload .uneditable-input .fa {
            top: 39px;
        }
        .col-centered{
            float: none !important;
            margin: 0 auto;
        }
    </style>
@endsection

@section('content')

<header class="page-header">
    <h2>Greading List Details</h2>

    <div class="right-wrapper pull-right">
        <ol class="breadcrumbs">
            <li>
                <a href="{{ Route('director.dashboard.index') }}">
                    <i class="fa fa-home"></i>
                </a>
            </li>
            <li><span class="active">Clients</span></li>
            <li>
                <a href="{{ Route('director.client.grading_list.index') }}"><span class="active">Grading Lists</span></a>
            </li>
            <li><span>greading_name</span></li>
        </ol>
    </div>
</header>

<!-- start: page -->
<section class="detials-page">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-6 col-lg-6 col-xl-6 col-centered">
                {{-- Profile Details  --}}
                <section class="panel panel-danger">
                    <form action="{{ route('director.client.grading_list.update', ['id' => $grading->id]) }}" method="post" enctype="multipart/form-data">
                    @csrf
                        <header class="panel-heading">
                            <div class="panel-actions">
                                <a href="#" class="fa fa-caret-down"></a>
                            </div>

                            <h2 class="panel-title">greading_name</h2>
                        </header>

                        <div class="panel-body">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label class="control-label">Greading Icon <span class="required">*</span></label>
                                        <div class="fileupload fileupload-exists" data-provides="fileupload"><input type="hidden" value="" name="">
                                            <div class="input-append">
                                                <div class="uneditable-input">
                                                    <i class="fa fa-file fileupload-exists" aria-hidden="true"></i>
                                                    <span class="fileupload-preview"></span>
                                                </div>
                                                <span class="btn btn-default btn-file">
                                                    <span class="fileupload-exists">Change</span>
                                                    <span class="fileupload-new">Select file</span>
                                                    <input type="file" class="remove-disable" class="@error('greading_icon') is-invalid @enderror" name="greading_icon" accept=".png, .jpg, .jpeg" disabled>
                                                </span>
                                                <a href="#" class="btn btn-default fileupload-exists remove-disable" data-dismiss="fileupload" disabled>Remove</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label class="control-label">Greading Name <span class="required">*</span></label>
                                        <input type="text" autocomplete="off" name="greading_name" class="form-control remove-disable @error('greading_name') is-invalid @enderror" placeholder="Eg.: Platinum +" value="{{ $grading->name }}" required disabled>

                                        {{-- Validation Alert Messages --}}
                                        @error('greading_name')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                        @enderror
                                    </div>
                                </div>

                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label class="control-label">Status <span class="required">*</span></label>
                                        <select name="greading_status" data-plugin-selectTwo class="form-control populate remove-disable  @error('greading_status') is-invalid @enderror" required disabled>
                                            <option value="1" @if($grading->status == 1)selected @endif>Active</option>
                                            <option value="-1" @if($grading->status == -1)selected @endif>Inactive</option>
                                        </select>

                                        {{-- Validation Alert Messages --}}
                                        @error('greading_status')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                        @enderror
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="panel-footer text-right">
                            <button type="button" class="btn btn-dark btn-custom btn-edit btn-sm" id="edit_btn">Edit</button>
                            <button type="button" class="btn btn-danger btn-custom btn-cancel btn-sm d-none" id="cancel_btn">Cancel</button>
                            <button class="btn btn-success btn-custom btn-update btn-sm d-none" id="update_btn" type="submit">Update</button>
                        </div>
                    </form>
                </section>
            </div>
        </div>
    </div>
</section>



<section class="panel">
    <header class="panel-heading">
        <h2 class="panel-title">All My {{ $grading->name }} Clients</h2>
    </header>
    <div class="panel-body">
        <table class="table table-bordered table-striped mb-none" id="datatable-tabletools" data-swf-path="assets/vendor/jquery-datatables/extras/TableTools/swf/copy_csv_xls_pdf.swf">
            <thead>
                <tr class="table-header-row">
                    <th>Sl.</th>
                    <th>Status</th>
                    <th>Full Name</th>
                    <th>Grading</th>
                    <th>Email</th>
                    <th>Mobile No.</th>
                    <th>Joined From</th>
                    <th class="text-center">Actions</th>
                </tr>
            </thead>
            <tbody>
                @foreach ($grading->clients as $sl => $data)
                <tr class="table-body-row">
                    <th>@if ($sl < 10) 0{{ $sl+1 }} @else {{ $sl+1 }} @endif</th>
                    @if ($data->client_status == 1)
                        <td><span class="member-active"><i class="fa fa-check"></i></span></td>
                    @else
                        <td><span class="member-inactive"><i class="fa fa-times"></i></span></td>
                    @endif

                    <td>{{ $data->client_title.' '.$data->client_forname.' '.$data->client_middle_name.' '.$data->client_surname }}</td>
                    <td><img src="data:image/png;base64,{{ $grading->icon }}" alt="" class="grading-icon"></td>
                    <td>{{ $data->email }}</td>
                    <td>{{ $data->mobile_phone }}</td>
                    <td>{{ $data->created_at->format('d-M Y') }}</td>
                    <td class="action-td text-center">
                        <a href="{{ route('director.client.show', ['id' => $data->id]) }}" class="btn btn-dark btn-sm btn-sm-custom btn-custom-info">
                            <i class="fa fa-info"></i>
                        </a>
                    </td>
                </tr>
                @endforeach
            </tbody>
        </table>
    </div>
</section>
<!-- end: page -->

@endsection


@section('script_links')
    {{--  External Javascript  --}}
    <script src="{{ asset('custom/assets/vendor/select2/select2.js') }}"></script>
    <script src="{{ asset('custom/assets/vendor/jquery-datatables/media/js/jquery.dataTables.js') }}"></script>
    <script src="{{ asset('custom/assets/vendor/jquery-datatables/extras/TableTools/js/dataTables.tableTools.min.js') }}"></script>
    <script src="{{ asset('custom/assets/vendor/jquery-datatables-bs3/assets/js/datatables.js') }}"></script>

    <script src="{{ asset('custom/assets/javascripts/tables/examples.datatables.default.js') }}"></script>
    <script src="{{ asset('custom/assets/javascripts/tables/examples.datatables.row.with.details.js') }}"></script>
    <script src="{{ asset('custom/assets/javascripts/tables/examples.datatables.tabletools.js') }}"></script>

    <script src="{{ asset('custom/assets/vendor/bootstrap-fileupload/bootstrap-fileupload.min.js') }}"></script>

@endsection

@section('scripts')
    {{--  External Javascript  --}}
    <script>
        $(document).ready(function(){
            var remove = $('.remove-disable');
            var edit = $('#edit_btn');
            var cancel = $('#cancel_btn');
            var update = $('#update_btn');

            edit.click(function() {
                remove.removeAttr('disabled');
                edit.addClass('d-none');
                cancel.removeClass('d-none');
                update.removeClass('d-none');
            });

            cancel.click(function() {
                remove.attr('disabled', 'true');
                edit.removeClass('d-none');
                cancel.addClass('d-none');
                update.addClass('d-none');
            });
        });
    </script>
@endsection
