@extends('layouts.director.app')

@section('page_title', '| Clients | Fact Find')

@section('stylesheet_links')
    {{--  External CSS  --}}
    <link rel="stylesheet" href="{{ asset('custom/assets/vendor/bootstrap-datepicker/css/datepicker3.css') }}" />
    <link rel="stylesheet" href="{{ asset('custom/assets/vendor/select2/select2.css') }}" />
    <link rel="stylesheet" href="{{ asset('custom/assets/vendor/bootstrap-fileupload/bootstrap-fileupload.min.css') }}" />

@endsection

@section('stylesheet')
    {{--  External CSS  --}}
    <style>
    .panel{
        border: 1px solid #eee;
    }
    .panel .panel .panel-heading{
        padding: 10px 15px;
    }
    .fileupload .uneditable-input .fa {
        top: 39px;
    }
    </style>
@endsection

@section('content')

<header class="page-header">
    <h2>Fact Find Result</h2>

    <div class="right-wrapper pull-right">
        <ol class="breadcrumbs">
            <li>
                <a href="{{ Route('director.dashboard.index') }}">
                    <i class="fa fa-home"></i>
                </a>
            </li>
            <li><span class="active">Clients</span></li>
            <li>
                <a href="{{ route('director.client.fact_find.index') }}">
                    <span class="active">Fact Find</span>
                </a>
            </li>
            <li><span>{{ $client->client_title." ".$client->client_forname." ".$client->client_surname }}</span></li>
        </ol>
    </div>
</header>

<!-- start: page -->
<section class="detials-page">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12 col-lg-12 col-xl-12">
                {{-- Profile Details  --}}
                <section class="panel panel-danger">
                    <header class="panel-heading">
                        <div class="panel-actions">
                            <a href="#" class="fa fa-caret-down"></a>
                        </div>

                        <h2 class="panel-title">{{ $fact_module->factfind_name }}</h2>
                    </header>

                    <div class="panel-body">
                        <div class="row">
                            @php
                                $factfinds = array();
                                $factfinds = (array) json_decode($fact_module->factfinds, true);

                                $d_factfinds = array();
                                if($fact_data != null){
                                    $d_factfinds = (array) json_decode($fact_data->factfinds, true);
                                    // dd($d_factfinds);
                                }
                                // dd($factfinds);
                                // dd($d_factfinds);
                            @endphp
                            @foreach($factfinds as $key => $factfind)
                            <div class="col-md-6 col-xl-4">
                                <form action="{{ route('director.client.fact_find.user_store', ['client_id' => $client->id, 'fact_module_id' => $fact_module->id, 'section_id' => $key]) }}" method="post">
                                    @csrf
                                    {{-- Section Name --}}
                                    <section class="panel">
                                        <header class="panel-heading bg-warning">
                                            <h2 class="panel-title">{{ $factfind['name'] }}</h2>
                                        </header>

                                        <div class="panel-body">
                                            <div class="row">
                                                {{-- Text --}}
                                                @php
                                                    $questions = array();
                                                    $questions =(array)$factfind['module'];
                                                    // dd($factfind['module']);
                                                @endphp

                                                @foreach($questions as $list => $question)

                                                @php
                                                    if($fact_data != null){
                                                        if(array_key_exists($key, $d_factfinds))
                                                            if(array_key_exists($list, $d_factfinds[$key])) $data_flag = true;
                                                            else $data_flag = false;
                                                        else
                                                            $data_flag = false;
                                                    } else $data_flag = false;
                                                @endphp
                                                {{-- {{ dd($question) }} --}}
                                                {{-- {{ dd($d_factfinds[$key][$list]) }} --}}
                                                    @if($question['field_type'] == 'text')
                                                        <div class="col-md-12">
                                                            <div class="form-group">
                                                                <label class="control-label">{{ $question['question_name'] }} @if($question['mendatory'] == 'on')<span class="required">*</span>@endif</label>
                                                                <input type="text" autocomplete="off" name="question[{{ $list }}]" class="form-control  @error('question[{{ $list }}]') is-invalid @enderror" @if($data_flag == true)value="{{ $d_factfinds[$key][$list] }}"@endif @if($question['mendatory'] == 'on')required @endif @if($question['allow_client_edit'] == null)disabled @endif/>
                                                                @if($question['allow_client_edit'] == null)
                                                                    <input type="hidden" autocomplete="off" name="question[{{ $list }}]" class="form-control  @error('question[{{ $list }}]') is-invalid @enderror" @if($data_flag == true)value="{{ $d_factfinds[$key][$list] }}" @endif/>
                                                                @endif


                                                                {{-- Validation Alert Messages --}}
                                                                @error('question[{{ $list }}]')
                                                                    <span class="invalid-feedback" role="alert">
                                                                        <strong>{{ $message }}</strong>
                                                                    </span>
                                                                @enderror
                                                            </div>
                                                        </div>
                                                    @endif

                                                    {{-- Multi-Line Text Area --}}
                                                    @if($question['field_type'] == 'textarea')
                                                        <div class="col-md-12">
                                                            <div class="form-group">
                                                                <label class="control-label">{{ $question['question_name'] }} @if($question['mendatory'] == 'on')<span class="required">*</span>@endif</label>
                                                                <textarea name="question[{{ $list }}]" class="form-control  @error('question[{{ $list }}]') is-invalid @enderror"  @if($question['mendatory'] == 'on')required @endif @if($question['allow_client_edit'] == null)disabled @endif>@if($data_flag == true){{ $d_factfinds[$key][$list] }}@endif</textarea>
                                                                @if($question['allow_client_edit'] == null)
                                                                    <input type="hidden" autocomplete="off" name="question[{{ $list }}]" class="form-control  @error('question[{{ $list }}]') is-invalid @enderror" @if($data_flag == true)value="{{ $d_factfinds[$key][$list] }}" @endif/>
                                                                @endif

                                                                {{-- Validation Alert Messages --}}
                                                                @error('question_type')
                                                                    <span class="invalid-feedback" role="alert">
                                                                        <strong>{{ $message }}</strong>
                                                                    </span>
                                                                @enderror
                                                            </div>
                                                        </div>
                                                    @endif

                                                    {{-- Date --}}
                                                    @if($question['field_type'] == 'date')
                                                        <div class="col-md-12">
                                                            <div class="form-group">
                                                                <label class="control-label">{{ $question['question_name'] }} @if($question['mendatory'] == 'on')<span class="required">*</span>@endif</label>
                                                                <input type="text" autocomplete="off" data-plugin-datepicker="" name="question[{{ $list }}]" class="form-control  @error('question[{{ $list }}]') is-invalid @enderror" placeholder="eg.: dd/mm/yyyy" @if($data_flag == true)value="{{ $d_factfinds[$key][$list] }}" @endif @if($question['mendatory'] == 'on')required @endif @if($question['allow_client_edit'] == null)disabled @endif/>
                                                                @if($question['allow_client_edit'] == null)
                                                                    <input type="hidden" autocomplete="off" name="question[{{ $list }}]" class="form-control  @error('question[{{ $list }}]') is-invalid @enderror" @if($data_flag == true)value="{{ $d_factfinds[$key][$list] }}"@endif/>
                                                                @endif

                                                                {{-- Validation Alert Messages --}}
                                                                @error('question[{{ $list }}]')
                                                                    <span class="invalid-feedback" role="alert">
                                                                        <strong>{{ $message }}</strong>
                                                                    </span>
                                                                @enderror
                                                            </div>
                                                        </div>
                                                    @endif

                                                    {{-- Check-Box --}}
                                                    @if($question['field_type'] == 'checkbox')
                                                        <div class="col-md-12" style="margin-bottom: 10px;">
                                                            <label class="control-label">{{ $question['question_name'] }} @if($question['mendatory'] == 'on')<span class="required">*</span>@endif</label>
                                                            <div class="checkbox-custom checkbox-default">
                                                                <input type="checkbox" name="question[{{ $list }}]" class=" @error('question[{{ $list }}]') is-invalid @enderror" @if($question['mendatory'] == 'on')required @endif @if($question['allow_client_edit'] == null)disabled @endif @if($data_flag == true) @if( $d_factfinds[$key][$list] == 'on') checked="" @endif @endif/>
                                                                @if($question['allow_client_edit'] == null)
                                                                    <input type="hidden" autocomplete="off" name="question[{{ $list }}]" class="form-control  @error('question[{{ $list }}]') is-invalid @enderror" @if($data_flag == true) @if( $d_factfinds[$key][$list] == 'on') checked="" @endif @endif/>
                                                                @endif
                                                                <label for="question[{{ $list }}]">{{ $question['question_name'] }}</label>
                                                            </div>

                                                            {{-- Validation Alert Messages --}}
                                                            @error('question[{{ $list }}]')
                                                                <span class="invalid-feedback" role="alert">
                                                                    <strong>{{ $message }}</strong>
                                                                </span>
                                                            @enderror
                                                        </div>
                                                    @endif

                                                    {{-- Select-Box --}}
                                                    @if($question['field_type'] == 'select')
                                                        <div class="col-md-12">
                                                            <div class="form-group">
                                                                <label class="control-label">{{ $question['question_name'] }} @if($question['mendatory'] == 'on')<span class="required">*</span>@endif</label>
                                                                <select name="question[{{ $list }}]" data-plugin-selectTwo class="form-control populate  @error('question[{{ $list }}]') is-invalid @enderror" @if($question['mendatory'] == 'on')required @endif>

                                                                    <option disabled @if($data_flag == true) @if( $d_factfinds[$key][$list] == null)selected @endif @endif>Select Option</option>
                                                                    @php
                                                                        $options = array();
                                                                        $options = (array)$question['options'];
                                                                    @endphp
                                                                    @foreach($options as $option)
                                                                        <option value="{{$option}}" @if($data_flag == true) @if( $d_factfinds[$key][$list] == $option)selected @endif @endif>{{$option}}</option>
                                                                    @endforeach
                                                                </select>

                                                                {{-- Validation Alert Messages --}}
                                                                @error('question[{{ $list }}]')
                                                                    <span class="invalid-feedback" role="alert">
                                                                        <strong>{{ $message }}</strong>
                                                                    </span>
                                                                @enderror
                                                            </div>
                                                        </div>
                                                    @endif

                                                    {{-- Numeric --}}
                                                    @if($question['field_type'] == 'number')
                                                        <div class="col-md-12">
                                                            <div class="form-group">
                                                                <label class="control-label">{{ $question['question_name'] }} @if($question['mendatory'] == 'on')<span class="required">*</span>@endif</label>
                                                                <input type="number" autocomplete="off" name="question[{{ $list }}]" class="form-control  @error('question[{{ $list }}]') is-invalid @enderror" @if($data_flag == true)value="{{ $d_factfinds[$key][$list] }}" @endif @if($question['mendatory'] == 'on')required @endif @if($question['allow_client_edit'] == null)disabled @endif/>
                                                                @if($question['allow_client_edit'] == null)
                                                                    <input type="hidden" autocomplete="off" name="question[{{ $list }}]" class="form-control  @error('question[{{ $list }}]') is-invalid @enderror" @if($data_flag == true)value="{{ $d_factfinds[$key][$list] }}" @endif/>
                                                                @endif

                                                                {{-- Validation Alert Messages --}}
                                                                @error('question[{{ $list }}]')
                                                                    <span class="invalid-feedback" role="alert">
                                                                        <strong>{{ $message }}</strong>
                                                                    </span>
                                                                @enderror
                                                            </div>
                                                        </div>
                                                    @endif
                                                @endforeach

                                            </div>
                                        </div>

                                        <div class="panel-footer text-right">
                                            @if($factfind['module'] != null)
                                                <button type="submit" class="btn btn-dark btn-custom btn-sm">Update</button>
                                            @endif
                                        </div>
                                    </section>
                                </form>
                            </div>
                            @endforeach
                        </div>
                    </div>
                </section>
            </div>
        </div>
    </div>
</section>
<!-- end: page -->

@endsection


@section('script_links')
    {{--  External Javascript  --}}
    <script src="{{ asset('custom/assets/vendor/bootstrap-datepicker/js/bootstrap-datepicker.js') }}"></script>
    <script src="{{ asset('custom/assets/vendor/select2/select2.js') }}"></script>

    <script src="{{ asset('custom/assets/vendor/bootstrap-fileupload/bootstrap-fileupload.min.js') }}"></script>

@endsection

@section('scripts')
    {{--  External Javascript  --}}
    <script>

    </script>
@endsection
