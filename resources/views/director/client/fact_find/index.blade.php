@extends('layouts.director.app')

@section('page_title', '| Clients')

@section('stylesheet_links')
    {{--  External CSS  --}}
    <link rel="stylesheet" href="{{ asset('custom/assets/vendor/select2/select2.css') }}" />

@endsection

@section('stylesheet')
    {{--  External CSS  --}}

@endsection

@section('content')

<header class="page-header">
    <h2>Fact Find</h2>

    <div class="right-wrapper pull-right">
        <ol class="breadcrumbs">
            <li>
                <a href="{{ Route('director.dashboard.index') }}">
                    <i class="fa fa-home"></i>
                </a>
            </li>
            <li><span class="active">Clients</span></li>
            <li><span>Fact Find</span></li>
        </ol>
    </div>
</header>

<!-- start: page -->
<section class="detials-page">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-4 col-lg-4 col-xl-4 col-centered">
                {{-- Profile Details  --}}
                <section class="panel panel-danger">
                    <form action="{{ route('director.client.fact_find.search') }}" method="POST">
                    @csrf
                        <header class="panel-heading">
                            <div class="panel-actions">
                                <a href="#" class="fa fa-caret-down"></a>
                            </div>

                            <h2 class="panel-title">Search Fact Find</h2>
                        </header>

                        <div class="panel-body">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label class="control-label">Client <span class="required">*</span></label>
                                        <select name="client_id" data-plugin-selectTwo class="form-control populate  @error('client_id') is-invalid @enderror" required>
                                            <option disabled selected>Select Client</option>
                                            @foreach($clients as $client)
                                                <option value="{{ $client->id }}">{{ $client->client_title." ".$client->client_forname." ".$client->client_surname }}</option>
                                            @endforeach
                                        </select>

                                        {{-- Validation Alert Messages --}}
                                        @error('client_id')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                        @enderror
                                    </div>
                                </div>

                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label class="control-label">Fact Find <span class="required">*</span></label>
                                        <select name="factfind_id" data-plugin-selectTwo class="form-control populate  @error('factfind_id') is-invalid @enderror" required>
                                            <option disabled selected>Select Fact Find</option>
                                            @foreach($fact_modules as $fact_module)
                                                <option value="{{ $fact_module->id }}">{{ $fact_module->factfind_name }}</option>
                                            @endforeach
                                        </select>

                                        {{-- Validation Alert Messages --}}
                                        @error('factfind_id')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                        @enderror
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="panel-footer text-right">
                            <button type="submit" class="btn btn-dark btn-custom">Search</button>
                        </div>
                    </form>
                </section>
            </div>
        </div>
    </div>
</section>
<!-- end: page -->

@endsection


@section('script_links')
    {{--  External Javascript  --}}
    <script src="{{ asset('custom/assets/vendor/select2/select2.js') }}"></script>

@endsection

@section('scripts')
    {{--  External Javascript  --}}

@endsection
