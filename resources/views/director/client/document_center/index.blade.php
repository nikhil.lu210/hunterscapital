@extends('layouts.director.app')

@section('page_title', '| Clients')

@section('stylesheet_links')
    {{--  External CSS  --}}
    <link rel="stylesheet" href="{{ asset('custom/assets/vendor/select2/select2.css') }}" />
    <link rel="stylesheet" href="{{ asset('custom/assets/vendor/jquery-datatables-bs3/assets/css/datatables.css') }}" />
@endsection

@section('stylesheet')
    {{--  External CSS  --}}
    <style>
        .dataTables_wrapper .DTTT.btn-group {
            display: inline-block;
            position: absolute;
            top: -74px;
            right: 0px;
        }

        .member-active{
            background-color: green;
            color: #ffffff;
            padding: 2px 7px;
            border-radius: 3px;
            font-weight: 600;
        }
        .member-inactive{
            background-color: red;
            color: #ffffff;
            padding: 2px 7px;
            border-radius: 3px;
            font-weight: 600;
        }

        .silver{
            background-color: silver;
            color: #000000;
            padding: 2px 7px;
            border-radius: 3px;
            font-weight: 600;
        }
        .bronze{
            background-color: #ab4700;
            color: #ffffff;
            padding: 2px 7px;
            border-radius: 3px;
            font-weight: 600;
        }
        .gold{
            background-color: #ff7c1f;
            color: #ffffff;
            padding: 2px 7px;
            border-radius: 3px;
            font-weight: 600;
        }
        .platinum{
            background-color: #a0bfb4;
            color: #000000;
            padding: 2px 7px;
            border-radius: 3px;
            font-weight: 600;
        }

        .access-granted{
            background-color: green;
            color: #ffffff;
            padding: 2px 7px;
            border-radius: 3px;
            font-weight: 600;
        }
        .access-not-granted{
            background-color: red;
            color: #ffffff;
            padding: 2px 7px;
            border-radius: 3px;
            font-weight: 600;
        }
    </style>
@endsection

@section('content')

<header class="page-header">
    <h2>Documents Center</h2>

    <div class="right-wrapper pull-right">
        <ol class="breadcrumbs">
            <li>
                <a href="{{ Route('director.dashboard.index') }}">
                    <i class="fa fa-home"></i>
                </a>
            </li>
            <li><span class="active">Clients</span></li>
            <li><span>Documents Center</span></li>
        </ol>
    </div>
</header>

<!-- start: page -->
<section class="panel">
    <header class="panel-heading">
        <h2 class="panel-title">All Documents List</h2>
        <a href="{{ route('director.client.document_center.create') }}" class="btn btn-dark btn-custom btn-sm header-btn">New Document</a>
    </header>
    <div class="panel-body">
        <table class="table table-bordered table-striped mb-none" id="datatable-default">
            <thead>
                <tr class="table-header-row">
                    <th>Sl.</th>
                    <th>Category</th>
                    <th>Client Name</th>
                    <th>Uploaded File</th>
                    <th>Uploaded Date</th>
                    <th class="text-center">Actions</th>
                </tr>
            </thead>
            <tbody>
                @foreach($documents as $key => $document)
                    <tr class="table-body-row">
                        <th>{{ $key+1 }}</th>
                        <td>
                            @if($document->category == 1) General
                            @elseif($document->category == 2) Client File
                            @elseif($document->category == 3) Banking
                            @else Mail Merge
                            @endif
                        </td>
                        @if($document->category == 2)
                        <td>{{ $document->documentClient->client_title." ".$document->documentClient->client_forname." ".$document->documentClient->client_surname }}</td>
                        @else <td></td>
                        @endif
                        <th class="document"><a href="{{ route('director.client.document_center.download', ['id' => $document->id]) }}">{{ $document->file_name }} <i class="fa fa-download"></i></a></th>
                        <td>{{ App\Http\Controllers\General::reverseDateFormatDashToSlash($document->updated_at) }}</td>
                        <td class="text-center">
                            <a href="{{ route('director.client.document_center.destroy', ['id' => $document->id]) }}" class="btn btn-danger btn-sm btn-sm-custom btn-custom-danger btn-delete" onclick="return confirm('Are You Sure Want To Delete This Document...?')">
                                <i class="fa fa-trash"></i>
                            </a>
                            <a href="{{ route('director.client.document_center.download', ['id' => $document->id]) }}" class="btn btn-dark btn-sm btn-sm-custom btn-custom-info btn-download">
                                <i class="fa fa-cloud-download"></i>
                            </a>
                            {{-- <a href="{{ route('director.client.document_center.show', ['id' =>$document->id]) }}" class="btn btn-dark btn-sm btn-sm-custom btn-custom-info">
                                <i class="fa fa-info"></i> --}}
                            </a>
                        </td>
                    </tr>
                @endforeach
            </tbody>
        </table>
    </div>
</section>
<!-- end: page -->

@endsection


@section('script_links')
    {{--  External Javascript  --}}
    <script src="{{ asset('custom/assets/vendor/select2/select2.js') }}"></script>
    <script src="{{ asset('custom/assets/vendor/jquery-datatables/media/js/jquery.dataTables.js') }}"></script>
    <script src="{{ asset('custom/assets/vendor/jquery-datatables/extras/TableTools/js/dataTables.tableTools.min.js') }}"></script>
    <script src="{{ asset('custom/assets/vendor/jquery-datatables-bs3/assets/js/datatables.js') }}"></script>

    <script src="{{ asset('custom/assets/javascripts/tables/examples.datatables.default.js') }}"></script>
    <script src="{{ asset('custom/assets/javascripts/tables/examples.datatables.row.with.details.js') }}"></script>
    <script src="{{ asset('custom/assets/javascripts/tables/examples.datatables.tabletools.js') }}"></script>
@endsection

@section('scripts')
    {{--  External Javascript  --}}

@endsection
