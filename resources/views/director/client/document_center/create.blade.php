@extends('layouts.director.app')

@section('page_title', '| Clients')

@section('stylesheet_links')
    {{--  External CSS  --}}
    <link rel="stylesheet" href="{{ asset('custom/assets/vendor/select2/select2.css') }}" />
    <link rel="stylesheet" href="{{ asset('custom/assets/vendor/bootstrap-fileupload/bootstrap-fileupload.min.css') }}" />
@endsection

@section('stylesheet')
    {{--  External CSS  --}}
    <style>
    .panel{
        border: 1px solid #eee;
    }
    .panel .panel .panel-heading{
        padding: 10px 15px;
    }
    .fileupload .uneditable-input .fa {
        top: 39px;
    }
    .col-centered{
        float: none !important;
        margin: 0 auto;
    }
    .d-none{
        display: none !important;
    }
    </style>
@endsection

@section('content')

<header class="page-header">
    <h2>Upload New Document</h2>

    <div class="right-wrapper pull-right">
        <ol class="breadcrumbs">
            <li>
                <a href="{{ Route('director.dashboard.index') }}">
                    <i class="fa fa-home"></i>
                </a>
            </li>
            <li><span class="active">Clients</span></li>
            <li><span>Upload New Document</span></li>
        </ol>
    </div>
</header>

<!-- start: page -->
<section class="detials-page">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-6 col-lg-6 col-xl-6 col-centered">
                {{-- Profile Details  --}}
                <section class="panel panel-danger">
                    <form action="{{ route('director.client.document_center.store') }}" method="post" enctype="multipart/form-data">
                    @csrf
                        <header class="panel-heading">
                            <div class="panel-actions">
                                <a href="#" class="fa fa-caret-down"></a>
                            </div>

                            <h2 class="panel-title">Upload New Document</h2>
                        </header>

                        <div class="panel-body">
                            <div class="row">
                                <div class="col-md-7">
                                    <div class="form-group">
                                        <label class="control-label">Document <span class="required">*</span></label>
                                        <div class="fileupload fileupload-new" data-provides="fileupload">
                                            <div class="input-append">
                                                <div class="uneditable-input">
                                                    <i class="fa fa-file fileupload-exists"></i>
                                                    <span class="fileupload-preview"></span>
                                                </div>
                                                <span class="btn btn-default btn-file">
                                                    <span class="fileupload-exists">Change</span>
                                                    <span class="fileupload-new">Select file</span>
                                                    <input type="file" name="document" class="@error('document') is-invalid @enderror"/>
                                                </span>
                                                <a href="#" class="btn btn-default fileupload-exists" data-dismiss="fileupload">Remove</a>
                                            </div>
                                        </div>

                                        {{-- Validation Alert Messages --}}
                                        @error('document')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                        @enderror
                                    </div>
                                </div>

                                <div class="col-md-5">
                                    <div class="form-group">
                                        <label class="control-label">Category <span class="required">*</span></label>
                                        <select name="category" id="document_category" data-plugin-selectTwo class="form-control populate  @error('category') is-invalid @enderror" required>
                                            <option value="1">General</option>
                                            <option value="2">Client File</option>
                                            <option value="3">Banking</option>
                                            <option value="4">Mail Merge</option>
                                        </select>

                                        {{-- Validation Alert Messages --}}
                                        @error('category')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                        @enderror
                                    </div>
                                </div>

                                <div class="col-md-12 d-none" id="document_client">
                                    <div class="form-group">
                                        <label class="control-label">Client Name</label>
                                        <select name="client" id="selected_client_id" data-plugin-selectTwo class="form-control populate  @error('category') is-invalid @enderror">
                                            <option disabled selected>Select client</option>
                                            @foreach($clients as $client)
                                                <option value="{{ $client->id }}">{{ $client->client_title." ".$client->client_forname." ".$client->client_surname }}</option>
                                            @endforeach
                                        </select>

                                        {{-- Validation Alert Messages --}}
                                        @error('category')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                        @enderror
                                    </div>
                                </div>

                                <div class="col-md-12 d-none" id="document_client_policy">
                                    <div class="form-group">
                                        <label class="control-label">Client Business</label>
                                        <select name="policy" id="client_policies" data-plugin-selectTwo class="form-control populate  @error('category') is-invalid @enderror">
                                            <option disabled selected>select Business</option>
                                        </select>

                                        {{-- Validation Alert Messages --}}
                                        @error('category')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                        @enderror
                                    </div>
                                </div>

                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label class="control-label">Document Description <span class="required">*</span></label>
                                        <textarea class="form-control @error('note') is-invalid @enderror" rows="3" id="textareaDefault" name="note" required></textarea>

                                        {{-- Validation Alert Messages --}}
                                        @error('note')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                        @enderror
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="panel-footer text-right">
                            <button type="submit" class="btn btn-dark btn-custom">Upload This Document</button>
                        </div>
                    </form>
                </section>
            </div>
        </div>
    </div>
</section>
<!-- end: page -->

@endsection


@section('script_links')
    {{--  External Javascript  --}}
    <script src="{{ asset('custom/assets/vendor/select2/select2.js') }}"></script>

    <script src="{{ asset('custom/assets/vendor/bootstrap-fileupload/bootstrap-fileupload.min.js') }}"></script>

@endsection

@section('scripts')
    {{--  External Javascript  --}}
    <script>
        $(document).ready(function(){
            $('#document_category').change(function(){
                var category = $(this);

                if(category.val() == 2){
                    $('#document_client').removeClass('d-none');
                    $('#document_client_policy').removeClass('d-none');
                } else {
                    $('#document_client').addClass('d-none');
                    $('#document_client_policy').addClass('d-none');
                }
            });

            $('#selected_client_id').change(function(){
                var client = $(this);

                var url = document.location.origin + "/director/client/document_center/policies/" + client.val();

                $.ajax({
                    method: 'GET',
                    url: url,
                    success: function(data) {
                        let range = data;
                        let htm = "<option selected disabled>Select Policy</option>";

                        for(var i=0; i<range.length; i++){

                            htm += "<option value='"+ range[i].id +"'>"+ range[i].policy_name +" "+ range[i].specific_policy_type +"</option>";
                        }
                        var a = $('#client_policies');
                        a.empty();
                        a.append(htm);
                    },
                    error: function() {
                        console.log("their is an error !!!");
                    }
                });

            });


        });

    </script>
@endsection
