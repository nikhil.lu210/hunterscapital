{{-- <!-- start: header --> --}}
<header class="header">
    <div class="logo-container">
        <a href="{{ Route('director.dashboard.index') }}" class="logo">
            <img src="{{ asset('custom/images/logo.png') }}" height="35" alt="Veechi Technologies (NIKHIL)" />
            {{-- <h4><b>HUNTERS CAPITAL</b></h4> --}}
        </a>
        <div class="visible-xs toggle-sidebar-left" data-toggle-class="sidebar-left-opened" data-target="html" data-fire-event="sidebar-left-opened">
            <i class="fa fa-bars" aria-label="Toggle sidebar"></i>
        </div>
    </div>

    {{-- <!-- start: search & user box --> --}}
    <div class="header-right">

        <div id="userbox" class="userbox">
            <a href="#" data-toggle="dropdown">
                <figure class="profile-picture">
                    <img src="{{ asset('custom/assets/images/!logged-user.jpg') }}" alt="Joseph Doe" class="img-circle" data-lock-picture="assets/images/!logged-user.jpg" />
                </figure>
                <div class="profile-info" data-lock-name="Nikhil Kurmi" data-lock-email="nikhil.lu210@gmail.com">
                    <span class="name">{{ Auth::user()->forname.' '.Auth::user()->surname }}</span>
                    <span class="role">{{ Auth::user()->role->name }}</span>
                </div>

                <i class="fa custom-caret"></i>
            </a>

            <div class="dropdown-menu">
                <ul class="list-unstyled">
                    <li class="divider"></li>
                    <li>
                        <a role="menuitem" tabindex="-1" href="#"><i class="fa fa-user"></i> My Profile</a>
                    </li>
                    <li>
                        <a role="menuitem" tabindex="-1" href="{{ route('logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
                            <i class="fa fa-power-off"></i>
                            Logout
                        </a>

                        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                            @csrf
                        </form>
                    </li>
                </ul>
            </div>
        </div>
    </div>
    {{-- <!-- end: search & user box --> --}}
</header>
{{-- <!-- end: header --> --}}
