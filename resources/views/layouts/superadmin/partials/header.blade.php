<!-- Basic -->
<meta charset="UTF-8">

{{-- CSRF Token--}}
<meta name="csrf-token" content="{{ csrf_token() }}">

{{--  Page Title  --}}
{{-- <title> HUNTERS CAPITAL | {{ App\Models\Role\Role::find(Auth::user()->role_id)->name }} @yield('page_title') </title> --}}
<title> HUNTERS CAPITAL | SUPER ADMIN @yield('page_title') </title>
<link rel="shortcut icon" href="{{ asset('custom/images/logo.png') }}" type="image/x-icon">

<meta name="keywords" content="Veechi Technologies" />
<meta name="description" content="Veechi Technologies">
<meta name="author" content="Veechi Technologies">

<!-- Mobile Metas -->
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />

<!-- Web Fonts  -->
<link href="http://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700,800|Shadows+Into+Light" rel="stylesheet" type="text/css">

<!-- Vendor CSS -->
<link rel="stylesheet" href="{{ asset('custom/assets/vendor/bootstrap/css/bootstrap.css') }}" />
<link rel="stylesheet" href="{{ asset('custom/assets/vendor/font-awesome/css/font-awesome.css') }}" />

@yield('stylesheet_links')

<!-- Theme CSS -->
<link rel="stylesheet" href="{{ asset('custom/assets/stylesheets/theme.css') }}" />

<!-- Skin CSS -->
<link rel="stylesheet" href="{{ asset('custom/assets/stylesheets/skins/default.css') }}" />

<!-- Theme Custom CSS -->
<link rel="stylesheet" href="{{ asset('custom/assets/stylesheets/theme-custom.css') }}">

<!-- Custom CSS -->
<link rel="stylesheet" href="{{ asset('custom/css/style.css') }}" />
<link rel="stylesheet" href="{{ asset('custom/css/responsive.css') }}" />

<!-- Head Libs -->
<script src="{{ asset('custom/assets/vendor/modernizr/modernizr.js') }}"></script>


@yield('stylesheet')
