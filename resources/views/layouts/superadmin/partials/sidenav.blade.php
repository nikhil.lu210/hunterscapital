<!-- start: sidebar -->
<aside id="sidebar-left" class="sidebar-left">

    <div class="sidebar-header">
        <div class="sidebar-title">
            Navigation
        </div>
        <div class="sidebar-toggle hidden-xs" data-toggle-class="sidebar-left-collapsed" data-target="html" data-fire-event="sidebar-left-toggle">
            <i class="fa fa-bars" aria-label="Toggle sidebar"></i>
        </div>
    </div>

    <div class="nano">
        <div class="nano-content">
            <nav id="menu" class="nav-main" role="navigation">
                <ul class="nav nav-main">
                    {{-- Dashboard --}}
                    <li class="{{ Request::is('superadmin/dashboard*') ? 'nav-active' : '' }}">
                        <a href="{{ Route('superadmin.dashboard.index') }}">
                            <i class="fa fa-home" aria-hidden="true"></i>
                            <span>Dashboard</span>
                        </a>
                    </li>

                    {{-- Assessors --}}
                    <li class="nav-parent {{ Request::is('superadmin/assessor*') ? 'nav-active nav-expanded' : '' }}">
                        <a>
                            <i class="fas fa-users" aria-hidden="true"></i>
                            <span>Assessors</span>
                        </a>
                        <ul class="nav nav-children">
                            <li class="{{ Request::is('superadmin/assessor/all_assessors*') ? 'nav-active' : '' }}">
                                <a href="{{ route('superadmin.assessor.index') }}">All Assessors</a>
                            </li>
                            <li class="{{ Request::is('superadmin/assessor/create*') ? 'nav-active' : '' }}">
                                <a href="{{ route('superadmin.assessor.create') }}">Assign New Assessor</a>
                            </li>
                        </ul>
                    </li>

                    {{-- Authors --}}
                    <li class="nav-parent {{ Request::is('superadmin/author*') ? 'nav-active nav-expanded' : '' }}">
                        <a>
                            <i class="fas fa-users" aria-hidden="true"></i>
                            <span>Authors</span>
                        </a>
                        <ul class="nav nav-children">
                            <li class="{{ Request::is('superadmin/author/all_authors*') ? 'nav-active' : '' }}">
                                <a href="{{ route('superadmin.author.index') }}">All Authors</a>
                            </li>
                            <li class="{{ Request::is('superadmin/author/create*') ? 'nav-active' : '' }}">
                                <a href="{{ route('superadmin.author.create') }}">Assign New Author</a>
                            </li>
                        </ul>
                    </li>

                    {{-- Funds --}}
                    {{-- <li class="nav-parent {{ Request::is('superadmin/fund*') ? 'nav-active nav-expanded' : '' }}">
                        <a>
                            <i class="fas fa-cubes" aria-hidden="true"></i>
                            <span>Funds</span>
                        </a>
                        <ul class="nav nav-children">
                            <li class="{{ Request::is('superadmin/fund/all_funds*') ? 'nav-active' : '' }}">
                                <a href="{{ route('superadmin.fund.index') }}">All Funds</a>
                            </li>
                            <li class="{{ Request::is('superadmin/fund/non_approved_fund*') ? 'nav-active' : '' }}">
                                <a href="{{ route('superadmin.fund.non_approved_fund') }}">Non Approved Funds</a>
                            </li>
                            <li class="{{ Request::is('superadmin/fund/create') ? 'nav-active' : '' }}">
                                <a href="{{ route('superadmin.fund.create') }}">Assign New Fund</a>
                            </li>
                        </ul>
                    </li> --}}

                    {{-- Providers --}}
                    <li class="nav-parent {{ Request::is('superadmin/provider*') ? 'nav-active nav-expanded' : '' }}">
                        <a>
                            <i class="fas fa-users" aria-hidden="true"></i>
                            <span>Providers</span>
                        </a>
                        <ul class="nav nav-children">
                            <li class="{{ Request::is('superadmin/provider/all_provider*') ? 'nav-active' : '' }}">
                                <a href="{{ route('superadmin.provider.index') }}">All Providers</a>
                            </li>
                            <li class="{{ Request::is('superadmin/provider/non_approved_provider*') ? 'nav-active' : '' }}">
                                <a href="{{ route('superadmin.provider.non_approved_provider') }}">Non Approved Providers</a>
                            </li>
                            <li class="{{ Request::is('superadmin/provider/create*') ? 'nav-active' : '' }}">
                                <a href="{{ route('superadmin.provider.create') }}">Assign New Provider</a>
                            </li>
                        </ul>
                    </li>

                    {{-- Trusts --}}
                    {{-- <li class="nav-parent {{ Request::is('superadmin/trust*') ? 'nav-active nav-expanded' : '' }}">
                        <a>
                            <i class="fas fa-users" aria-hidden="true"></i>
                            <span>Trusts</span>
                        </a>
                        <ul class="nav nav-children">
                            <li class="{{ Request::is('superadmin/trust/all_trust*') ? 'nav-active' : '' }}">
                                <a href="{{ route('superadmin.trust.index') }}">All Trusts</a>
                            </li>
                            <li class="{{ Request::is('superadmin/trust/non_approved_trust*') ? 'nav-active' : '' }}">
                                <a href="{{ route('superadmin.trust.non_approved_trust') }}">Non Approved Trusts</a>
                            </li>
                            <li class="{{ Request::is('superadmin/trust/create*') ? 'nav-active' : '' }}">
                                <a href="{{ route('superadmin.trust.create') }}">Assign New Trust</a>
                            </li>
                        </ul>
                    </li> --}}
                </ul>
            </nav>

        </div>

    </div>

</aside>
<!-- end: sidebar -->
