<!-- start: sidebar -->
<aside id="sidebar-left" class="sidebar-left">

    <div class="sidebar-header">
        <div class="sidebar-title">
            Navigation
        </div>
        <div class="sidebar-toggle hidden-xs" data-toggle-class="sidebar-left-collapsed" data-target="html" data-fire-event="sidebar-left-toggle">
            <i class="fa fa-bars" aria-label="Toggle sidebar"></i>
        </div>
    </div>

    <div class="nano">
        <div class="nano-content">
            <nav id="menu" class="nav-main" role="navigation">
                <ul class="nav nav-main">
                    {{-- Dashboard --}}
                    <li class="{{ Request::is('admin/dashboard*') ? 'nav-active' : '' }}">
                        <a href="#">
                            <i class="fa fa-home" aria-hidden="true"></i>
                            <span>Dashboard</span>
                        </a>
                    </li>

                    {{-- Administration --}}
                    <li class="nav-parent {{ Request::is('admin/administration/*') ? 'nav-active nav-expanded' : '' }}">
                        <a>
                            <i class="fas fa-user-shield" aria-hidden="true"></i>
                            <span>Administration</span>
                        </a>
                        <ul class="nav nav-children">
                            <li class="{{ Request::is('admin/administration/admin*') ? 'nav-active' : '' }}">
                                <a href="#">All Demos</a>
                            </li>
                        </ul>
                    </li>
                </ul>
            </nav>

        </div>

    </div>

</aside>
<!-- end: sidebar -->
