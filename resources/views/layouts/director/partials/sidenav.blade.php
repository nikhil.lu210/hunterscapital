<!-- start: sidebar -->
<aside id="sidebar-left" class="sidebar-left">

    <div class="sidebar-header">
        <div class="sidebar-title">
            Navigation
        </div>
        <div class="sidebar-toggle hidden-xs" data-toggle-class="sidebar-left-collapsed" data-target="html" data-fire-event="sidebar-left-toggle">
            <i class="fa fa-bars" aria-label="Toggle sidebar"></i>
        </div>
    </div>

    <div class="nano">
        <div class="nano-content">
            <nav id="menu" class="nav-main" role="navigation">
                <ul class="nav nav-main">
                    {{-- Dashboard --}}
                    <li class="{{ Request::is('director/dashboard*') ? 'nav-active' : '' }}">
                        <a href="{{ Route('director.dashboard.index') }}">
                            <i class="fa fa-home" aria-hidden="true"></i>
                            <span>Dashboard</span>
                        </a>
                    </li>

                    {{-- Clients --}}
                    <li class="nav-parent {{ Request::is('director/client*') ? 'nav-active nav-expanded' : '' }}">
                        <a>
                            <i class="fas fa-users" aria-hidden="true"></i>
                            <span>Clients</span>
                        </a>
                        <ul class="nav nav-children">
                            <li class="{{ Request::is('director/client/my_clients*') ? 'nav-active' : '' }}">
                                <a href="{{ route('director.client.index') }}">All My Clients</a>
                            </li>
                            <li class="{{ Request::is('director/client/create*') ? 'nav-active' : '' }}">
                                <a href="{{ route('director.client.create') }}">Create New Client</a>
                            </li>
                            <li class="{{ Request::is('director/client/fact_find*') ? 'nav-active' : '' }}">
                                <a href="{{ route('director.client.fact_find.index') }}">Fact Find</a>
                            </li>
                            {{-- <li class="{{ Request::is('director/client/quote_center*') ? 'nav-active' : '' }}">
                                <a href="{{ route('director.client.quote_center.index') }}">Quote Center</a>
                            </li> --}}
                            {{-- <li class="{{ Request::is('director/client/letter_builder*') ? 'nav-active' : '' }}">
                                <a href="{{ route('director.client.letter_builder.index') }}">Letter Builder</a>
                            </li> --}}
                            <li class="{{ Request::is('director/client/document_center*') ? 'nav-active' : '' }}">
                                <a href="{{ route('director.client.document_center.index') }}">Documents Center</a>
                            </li>
                            {{-- <li class="{{ Request::is('director/client/client_access*') ? 'nav-active' : '' }}">
                                <a href="{{ route('director.client.client_access.index') }}">Client Access</a>
                            </li> --}}
                            {{-- <li class="{{ Request::is('director/client/grading_list*') ? 'nav-active' : '' }}">
                                <a href="{{ route('director.client.grading_list.index') }}">Grading Lists</a>
                            </li> --}}
                            {{-- <li class="{{ Request::is('director/client/risk_list*') ? 'nav-active' : '' }}">
                                <a href="{{ route('director.client.risk_list.index') }}">Risk Lists</a>
                            </li> --}}
                        </ul>
                    </li>

                    {{-- Business --}}
                    <li class="nav-parent {{ Request::is('director/business*') ? 'nav-active nav-expanded' : '' }}">
                        <a>
                            <i class="fas fa-money" aria-hidden="true"></i>
                            <span>Business</span>
                        </a>
                        <ul class="nav nav-children">
                            <li class="{{ Request::is('director/business/all_business*') ? 'nav-active' : '' }}">
                                <a href="{{ route('director.business.all_business.index') }}">All Business</a>
                            </li>
                            <li class="{{ Request::is('director/business/turnovers*') ? 'nav-active' : '' }}">
                                <a href="{{ route('director.business.turnover.index') }}">Turnover</a>
                            </li>
                            <li class="nav-parent nav-custom-parent {{ Request::is('director/business/add_new_business*') ? 'nav-active nav-expanded' : '' }}">
                                <a href="#">Add New Business</a>
                                <ul class="nav nav-children" style="">
                                    {{-- <li class="{{ Request::is('director/business/add_new_business/investment*') ? 'nav-active' : '' }}"><a href="{{ route('director.business.add_new_policy.investment.index') }}">Investment</a></li> --}}

                                    <li class="{{ Request::is('director/business/add_new_business/mortgage*') ? 'nav-active' : '' }}"><a href="{{ route('director.business.add_new_policy.mortgage.index') }}">Mortgage</a></li>

                                    {{-- <li class="{{ Request::is('director/business/add_new_business/pension*') ? 'nav-active' : '' }}"><a href="{{ route('director.business.add_new_policy.pension.index') }}">Pension</a></li> --}}

                                    <li class="{{ Request::is('director/business/add_new_business/protection*') ? 'nav-active' : '' }}"><a href="{{ route('director.business.add_new_policy.protection.index') }}">Protection</a></li>
                                </ul>
                            </li>
                            <li class="{{ Request::is('director/business/turnover/create*') ? 'nav-active' : '' }}">
                                <a href="{{ route('director.business.turnover.create') }}">Add Turnover</a>
                            </li>
                            {{-- <li class="{{ Request::is('director/business/pre_scrutiny*') ? 'nav-active' : '' }}">
                                <a href="{{ route('director.business.pre_scrutiny.index') }}">Pre Scrutiny</a>
                            </li> --}}
                        </ul>
                    </li>

                    {{-- Company --}}
                    <li class="nav-parent {{ Request::is('director/company*') ? 'nav-active nav-expanded' : '' }}">
                        <a>
                            <i class="fas fa-building" aria-hidden="true"></i>
                            <span>Company</span>
                        </a>
                        <ul class="nav nav-children">
                            <li class="{{ Request::is('director/company/my_company*') ? 'nav-active' : '' }}">
                                <a href="{{ route('director.company.my_company.index') }}">My Company</a>
                            </li>
                            <li class="{{ Request::is('director/company/users*') ? 'nav-active' : '' }}">
                                <a href="{{ route('director.company.user.index') }}">All Users</a>
                            </li>
                            <li class="{{ Request::is('director/company/user/create') ? 'nav-active' : '' }}">
                                <a href="{{ route('director.company.user.create') }}">New User</a>
                            </li>
                            <li class="{{ Request::is('director/company/business_model*') ? 'nav-active' : '' }}">
                                <a href="{{ route('director.company.business_model.index') }}">Business Model</a>
                            </li>
                            <li class="{{ Request::is('director/company/task_manager*') ? 'nav-active' : '' }}">
                                <a href="{{ route('director.company.task_manager.index') }}">Task Manager</a>
                            </li>
                            <li class="{{ Request::is('director/company/task_type*') ? 'nav-active' : '' }}">
                                <a href="{{ route('director.company.task_type.index') }}">Task Types</a>
                            </li>

                            <li class="nav-parent nav-custom-parent {{ Request::is('director/company/compliance*') ? 'nav-active nav-expanded' : '' }}">
                                <a href="#">Compliance</a>
                                <ul class="nav nav-children" style="">
                                    <li class="{{ Request::is('director/company/compliance/online_account*') ? 'nav-active' : '' }}"><a href="{{ route('director.company.compliance.online_account.index') }}">Online Accounts</a></li>
                                    <li class="{{ Request::is('director/company/compliance/complaint*') ? 'nav-active' : '' }}"><a href="{{ route('director.company.compliance.complaint.index') }}">Complaints</a></li>
                                    <li class="{{ Request::is('director/company/compliance/promotion*') ? 'nav-active' : '' }}"><a href="{{ route('director.company.compliance.promotion.index') }}">Promotions</a></li>
                                    <li class="{{ Request::is('director/company/compliance/visit*') ? 'nav-active' : '' }}"><a href="{{ route('director.company.compliance.visit.index') }}">Visits</a></li>
                                    {{-- <li class="{{ Request::is('director/company/compliance/file_check*') ? 'nav-active' : '' }}"><a href="{{ route('director.company.compliance.file_check.index') }}">File Check</a></li> --}}

                                    {{-- <li class="nav-parent nav-custom-parent {{ Request::is('director/company/compliance/in_and_out*') ? 'nav-active nav-expanded' : '' }}">
                                        <a href="#">In & Out</a>
                                        <ul class="nav nav-children" style="">
                                            <li class="{{ Request::is('director/company/compliance/in_and_out/application*') ? 'nav-active' : '' }}"><a href="{{ route('director.company.compliance.in_and_out.application.index') }}">Applications</a></li>
                                            <li class="{{ Request::is('director/company/compliance/in_and_out/approval*') ? 'nav-active' : '' }}"><a href="{{ route('director.company.compliance.in_and_out.approval.index') }}">Approval</a></li>
                                            <li class="{{ Request::is('director/company/compliance/in_and_out/leaving*') ? 'nav-active' : '' }}"><a href="{{ route('director.company.compliance.in_and_out.leaving.index') }}">Leaving</a></li>
                                        </ul>
                                    </li> --}}
                                </ul>
                            </li>

                            <li class="{{ Request::is('director/company/fact_find*') ? 'nav-active' : '' }}">
                                <a href="{{ route('director.company.fact_find.index') }}">Fact Find</a>
                            </li>
                        </ul>
                    </li>

                    {{-- Settings --}}
                    <li class="nav-parent {{ Request::is('director/setting*') ? 'nav-active nav-expanded' : '' }}">
                        <a>
                            <i class="fas fa-cogs" aria-hidden="true"></i>
                            <span>Settings</span>
                        </a>
                        <ul class="nav nav-children">
                            <li class="{{ Request::is('director/setting/my_account*') ? 'nav-active' : '' }}">
                                <a href="{{ route('director.setting.my_account.index') }}">My Account</a>
                            </li>
                            {{-- <li class="{{ Request::is('director/setting/my_credential*') ? 'nav-active' : '' }}">
                                <a href="{{ route('director.setting.my_credential.index') }}">My Credentials</a>
                            </li> --}}
                        </ul>
                    </li>

                    {{-- Terms and Conditions --}}
                    <li class="nav-parent {{ Request::is('director/term_and_condition*') ? 'nav-active nav-expanded' : '' }}">
                        <a>
                            <i class="fas fa-question-circle" aria-hidden="true"></i>
                            <span>Terms and Conditions</span>
                        </a>
                        <ul class="nav nav-children">
                            <li class="{{ Request::is('director/term_and_condition/cpd_register*') ? 'nav-active' : '' }}">
                                <a href="{{ route('director.term_and_condition.cpd_register.index') }}">CPD Register</a>
                            </li>
                            <li class="{{ Request::is('director/term_and_condition/cpd_analysis*') ? 'nav-active' : '' }}">
                                <a href="{{ route('director.term_and_condition.cpd_analysis.index') }}">CPD Analysis</a>
                            </li>
                            {{-- <li class="{{ Request::is('director/term_and_condition/qualification*') ? 'nav-active' : '' }}">
                                <a href="{{ route('director.term_and_condition.qualification.index') }}">Qualifications</a>
                            </li>
                            <li class="{{ Request::is('director/term_and_condition/professional_body*') ? 'nav-active' : '' }}">
                                <a href="{{ route('director.term_and_condition.professional_body.index') }}">Professional Bodies</a>
                            </li> --}}
                            <li class="{{ Request::is('director/term_and_condition/sps*') ? 'nav-active' : '' }}">
                                <a href="{{ route('director.term_and_condition.sps.index') }}">SPS</a>
                            </li>
                            {{-- <li class="{{ Request::is('director/term_and_condition/financial_exam*') ? 'nav-active' : '' }}">
                                <a href="{{ route('director.term_and_condition.financial_exam.index') }}">Financial Exam</a>
                            </li> --}}
                        </ul>
                    </li>

                    {{-- Document Library --}}
                    {{-- <li class="{{ Request::is('director/document_library*') ? 'nav-active' : '' }}">
                        <a href="{{ Route('director.document_library.index') }}">
                            <i class="fa fa-book" aria-hidden="true"></i>
                            <span>Document Library</span>
                        </a>
                    </li> --}}

                    {{-- Auto Suitablity Report --}}
                    {{-- <li class="{{ Request::is('director/auto_suitability_report*') ? 'nav-active' : '' }}">
                        <a href="{{ Route('director.auto_suitability_report.index') }}">
                            <i class="fa fa-file-text" aria-hidden="true"></i>
                            <span>Auto Suitability Report</span>
                        </a>
                    </li> --}}

                    {{-- GDPR --}}
                    {{-- <li class="{{ Request::is('director/gdpr*') ? 'nav-active' : '' }}">
                        <a href="{{ Route('director.gdpr.index') }}"">
                            <i class="fa fa-shield" aria-hidden="true"></i>
                            <span>GDPR</span>
                        </a>
                    </li> --}}
                </ul>
            </nav>

        </div>

    </div>

</aside>
<!-- end: sidebar -->
