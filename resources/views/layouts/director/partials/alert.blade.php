<div class="row">
    <div class="col-md-12">
        @if (session('danger'))
            <div class="alert alert-danger" role="alert">
                {{ session('danger') }}
            </div>
        @elseif (session('success'))
            <div class="alert alert-success" role="alert">
                {{ session('success') }}
            </div>
        @elseif (session('info'))
            <div class="alert alert-info" role="alert">
                {{ session('info') }}
            </div>
        @elseif (session('primary'))
            <div class="alert alert-primary" role="alert">
                {{ session('primary') }}
            </div>
        @elseif (session('dark'))
            <div class="alert alert-dark" role="alert">
                {{ session('dark') }}
            </div>
        @endif
    </div>
</div>
