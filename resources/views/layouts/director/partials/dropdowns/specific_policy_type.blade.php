<select name="specific_policy_type" id="specific_policy_type" data-plugin-selectTwo class="form-control populate placeholder @error('specific_policy_type') is-invalid @enderror" required data-plugin-options='{ "placeholder": "Specific Policy Type", "allowClear": true }'>
    <option disabled @if( !isset($business) )selected @endif>Select Policy Type</option>
    @if( isset($business) and $business != null)
    <option selected value="{{ $business->specific_policy_type }}">{{ $business->specific_policy_type }}</option>
    @endif
    <optgroup label="">
        <option value="Fixed">Fixed</option>
        <option value="Variable">Variable</option>
        <option value="Libor">Libor</option>
        <option value="Interest Only">Interest Only</option>
        <option value="Tracker">Tracker</option>
        <option value="Discounted">Discounted</option>
        <option value="Flexible">Flexible</option>
        <option value="Other">Other</option>
    </optgroup>
</select>

{{-- Validation Alert Messages --}}
@error('specific_policy_type')
    <span class="invalid-feedback" role="alert">
        <strong>{{ $message }}</strong>
    </span>
@enderror
