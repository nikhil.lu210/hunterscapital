<script src="{{ asset('custom/assets/vendor/jquery/jquery.js') }}"></script>
<script src="{{ asset('custom/assets/vendor/jquery-browser-mobile/jquery.browser.mobile.js') }}"></script>
<script src="{{ asset('custom/assets/vendor/bootstrap/js/bootstrap.js') }}"></script>
<script src="{{ asset('custom/assets/vendor/nanoscroller/nanoscroller.js') }}"></script>

{{-- Font Awesome 5 --}}
<script src="https://kit.fontawesome.com/b9bdb37ea9.js"></script>

{{-- Scripts Links --}}
@yield('script_links')

<!-- Theme Base, Components and Settings -->
<script src="{{ asset('custom/assets/javascripts/theme.js') }}"></script>

<!-- Theme Custom -->
<script src="{{ asset('custom/assets/javascripts/theme.custom.js') }}"></script>

<!-- Theme Initialization Files -->
<script src="{{ asset('custom/assets/javascripts/theme.init.js') }}"></script>

<script src="{{ asset('custom/js/toastr.min.js') }}"></script>
<!-- custom js -->
<script src="{{ asset('custom/js/script.js') }}"></script>
<script src="{{ asset('custom/js/responsive.js') }}"></script>

<script>
    $(document).ready(function(){
        $('#ToolTables_datatable-tabletools_3').click(function(){
            $('.hide-status').addClass('d-none');
        })
        $(document).on('keydown', function(event) {
            if (event.key == "Escape") {
                $('.hide-status').removeClass('d-none');
            }
        });
    });

    //check autheticate user email for confirmation
    function isDelete(){
        var email = "{{ Auth::user()->email }}";
        var take_email = prompt("Please Give Your Authenticate Email here...");

        if(email == take_email){
            confirm('Are You Sure Want To Delete...?');
        } else{
            alert("Your Email Does Not Match ...");
            return false;
        }
    }

</script>

@if (Session::has('success'))
<script>
    var msg = "{{ Session::get('success') }}";
    toastr.success(msg);
</script>
@endif

@if (Session::has('danger'))
<script>
    var msg = "{{ Session::get('danger') }}";
    toastr.error(msg);
</script>
@endif

@if (Session::has('info'))
<script>
    var msg = "{{ Session::get('info') }}";
    toastr.info(msg);
</script>
@endif

@if (Session::has('warning'))
<script>
    var msg = "{{ Session::get('warning') }}";
    toastr.warning(msg);
</script>
@endif

@if (Session::has('primary'))
<script>
    var msg = "{{ Session::get('primary') }}";
    toastr.primary(msg);
</script>
@endif


{{-- Custom Scripts --}}
@yield('scripts')
