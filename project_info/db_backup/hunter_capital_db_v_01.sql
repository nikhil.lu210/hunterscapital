CREATE TABLE IF NOT EXISTS `adviser_biz_models` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;



CREATE TABLE IF NOT EXISTS `assessors` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `assessor_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `note` text COLLATE utf8mb4_unicode_ci,
  `address` text COLLATE utf8mb4_unicode_ci,
  `county` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `postcode` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `contact_number` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;



CREATE TABLE IF NOT EXISTS `authors` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `authors_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `note` text COLLATE utf8mb4_unicode_ci,
  `address` text COLLATE utf8mb4_unicode_ci,
  `county` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `postcode` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `contact_number` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;



CREATE TABLE IF NOT EXISTS `checklists` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `client_id` bigint(20) unsigned DEFAULT NULL,
  `checklist_id` tinyint(4) NOT NULL,
  `file` blob NOT NULL,
  `file_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `checklists_client_id_foreign` (`client_id`),
  CONSTRAINT `checklists_client_id_foreign` FOREIGN KEY (`client_id`) REFERENCES `clients` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;



CREATE TABLE IF NOT EXISTS `clients` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `client_status` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `client_access` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '0',
  `advisor` bigint(20) unsigned DEFAULT NULL,
  `corporate_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `client_nationality` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `house_no_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `town_city` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `county` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `post_code` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `address` text COLLATE utf8mb4_unicode_ci,
  `client_title` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `client_forname` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `client_middle_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `client_surname` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `date_of_birth` date DEFAULT NULL,
  `home_phone` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `mobile_phone` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `grading` bigint(20) unsigned DEFAULT NULL,
  `contactable_by` json DEFAULT NULL,
  `partners` json DEFAULT NULL,
  `notes` json DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `clients_advisor_foreign` (`advisor`),
  KEY `clients_grading_foreign` (`grading`),
  CONSTRAINT `clients_advisor_foreign` FOREIGN KEY (`advisor`) REFERENCES `users` (`id`) ON DELETE CASCADE,
  CONSTRAINT `clients_grading_foreign` FOREIGN KEY (`grading`) REFERENCES `client_gradings` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;



CREATE TABLE IF NOT EXISTS `client_gradings` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `icon` mediumblob,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;



CREATE TABLE IF NOT EXISTS `complaints` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `advisor` bigint(20) unsigned NOT NULL,
  `business_type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `complaint_type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `client_advice_date` date DEFAULT NULL,
  `client` bigint(20) unsigned NOT NULL,
  `assessor` bigint(20) unsigned DEFAULT NULL,
  `client_received` date NOT NULL,
  `internally_received` date NOT NULL,
  `confirmation_sent` date DEFAULT NULL,
  `verbar_complaint` tinyint(1) NOT NULL DEFAULT '1',
  `details` text COLLATE utf8mb4_unicode_ci,
  `four_week_later` date DEFAULT NULL,
  `eight_week_later` date DEFAULT NULL,
  `final_response` date DEFAULT NULL,
  `to_assessor` date DEFAULT NULL,
  `from_assessor` date DEFAULT NULL,
  `to_client_assessor` date DEFAULT NULL,
  `redress_amount_assessor` int(11) DEFAULT NULL,
  `assessor_decision` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `assessor_note` text COLLATE utf8mb4_unicode_ci,
  `to_adjudicator` date DEFAULT NULL,
  `from_adjudicator` date DEFAULT NULL,
  `to_client_adjudicator` date DEFAULT NULL,
  `redress_amount_adjudicator` int(11) DEFAULT NULL,
  `adjudicator_decision` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `adjudicator_note` text COLLATE utf8mb4_unicode_ci,
  `to_ombudsman` date DEFAULT NULL,
  `from_ombudsman` date DEFAULT NULL,
  `to_client_ombudsman` date DEFAULT NULL,
  `redress_amount_ombudsman` int(11) DEFAULT NULL,
  `ombudsman_decision` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `ombudsman_note` text COLLATE utf8mb4_unicode_ci,
  `documents` json DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `complaints_advisor_foreign` (`advisor`),
  KEY `complaints_client_foreign` (`client`),
  KEY `complaints_assessor_foreign` (`assessor`),
  CONSTRAINT `complaints_advisor_foreign` FOREIGN KEY (`advisor`) REFERENCES `users` (`id`) ON DELETE CASCADE,
  CONSTRAINT `complaints_assessor_foreign` FOREIGN KEY (`assessor`) REFERENCES `assessors` (`id`) ON DELETE CASCADE,
  CONSTRAINT `complaints_client_foreign` FOREIGN KEY (`client`) REFERENCES `clients` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;



CREATE TABLE IF NOT EXISTS `customer_biz_models` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;



CREATE TABLE IF NOT EXISTS `c_p_d_s` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `user` bigint(20) unsigned NOT NULL,
  `cpd_topic` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `dev_set_by_user` bigint(20) unsigned DEFAULT NULL,
  `dev_set_by_author` bigint(20) unsigned DEFAULT NULL,
  `cpd_activity_type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `development_needed` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `planned_completion_date` date NOT NULL,
  `cpd_activity_completed` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `completed_date` date DEFAULT NULL,
  `note_reflective_statement` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `structured_hour` double(8,2) NOT NULL,
  `unstructured_hour` double(8,2) NOT NULL,
  `files` json DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `c_p_d_s_user_foreign` (`user`),
  KEY `c_p_d_s_dev_set_by_user_foreign` (`dev_set_by_user`),
  KEY `c_p_d_s_dev_set_by_author_foreign` (`dev_set_by_author`),
  CONSTRAINT `c_p_d_s_dev_set_by_author_foreign` FOREIGN KEY (`dev_set_by_author`) REFERENCES `authors` (`id`) ON DELETE CASCADE,
  CONSTRAINT `c_p_d_s_dev_set_by_user_foreign` FOREIGN KEY (`dev_set_by_user`) REFERENCES `users` (`id`) ON DELETE CASCADE,
  CONSTRAINT `c_p_d_s_user_foreign` FOREIGN KEY (`user`) REFERENCES `users` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;



CREATE TABLE IF NOT EXISTS `documents` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `category` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `file_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `client` bigint(20) unsigned DEFAULT NULL,
  `client_policy` bigint(20) unsigned DEFAULT NULL,
  `description` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `file` blob NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `documents_client_foreign` (`client`),
  KEY `documents_client_policy_foreign` (`client_policy`),
  CONSTRAINT `documents_client_foreign` FOREIGN KEY (`client`) REFERENCES `clients` (`id`) ON DELETE CASCADE,
  CONSTRAINT `documents_client_policy_foreign` FOREIGN KEY (`client_policy`) REFERENCES `policies` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;



CREATE TABLE IF NOT EXISTS `fact_find_data` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `factfind_id` bigint(20) unsigned NOT NULL,
  `client_id` bigint(20) unsigned NOT NULL,
  `factfinds` json DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fact_find_data_factfind_id_foreign` (`factfind_id`),
  KEY `fact_find_data_client_id_foreign` (`client_id`),
  CONSTRAINT `fact_find_data_client_id_foreign` FOREIGN KEY (`client_id`) REFERENCES `clients` (`id`) ON DELETE CASCADE,
  CONSTRAINT `fact_find_data_factfind_id_foreign` FOREIGN KEY (`factfind_id`) REFERENCES `fact_find_modules` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;



CREATE TABLE IF NOT EXISTS `fact_find_modules` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `factfind_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `factfinds` json DEFAULT NULL,
  `isDefault` tinyint(1) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;



CREATE TABLE IF NOT EXISTS `funds` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `fund_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `note` text COLLATE utf8mb4_unicode_ci,
  `mex_code` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `isin_code` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `sedol_code` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `citi_code` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `bb_ticker_code` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `approved_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;



CREATE TABLE IF NOT EXISTS `letter_builders` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `letter_content` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `footer_display` tinyint(1) NOT NULL,
  `footer_display_content` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `client` bigint(20) unsigned NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `letter_builders_client_foreign` (`client`),
  CONSTRAINT `letter_builders_client_foreign` FOREIGN KEY (`client`) REFERENCES `clients` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;



CREATE TABLE IF NOT EXISTS `libraries` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `category` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `manual_writer` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `date_of_submission` date NOT NULL,
  `read_by` json NOT NULL,
  `file` blob NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;



CREATE TABLE IF NOT EXISTS `migrations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=35 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;



INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
	(1, '2014_09_24_100150_create_roles_table', 1),
	(2, '2014_10_12_000000_create_users_table', 1),
	(3, '2014_10_12_100000_create_password_resets_table', 1),
	(4, '2019_09_25_073648_create_trusts_table', 1),
	(5, '2019_09_25_073906_create_providers_table', 1),
	(6, '2019_09_25_073915_create_funds_table', 1),
	(7, '2019_09_25_073945_create_assessors_table', 1),
	(8, '2019_09_25_073951_create_authors_table', 1),
	(9, '2019_09_25_073956_create_suitable_reports_table', 1),
	(10, '2019_09_25_074000_create_client_gradings_table', 1),
	(11, '2019_09_25_074005_create_clients_table', 1),
	(12, '2019_09_25_074009_create_my_companies_table', 1),
	(13, '2019_09_25_074013_create_task_types_table', 1),
	(14, '2019_09_25_074018_create_tasks_table', 1),
	(15, '2019_09_25_074022_create_customer_biz_models_table', 1),
	(16, '2019_09_25_074026_create_adviser_biz_models_table', 1),
	(17, '2019_09_25_074031_create_online_accounts_table', 1),
	(18, '2019_09_25_074035_create_complaints_table', 1),
	(19, '2019_09_25_074039_create_promotions_table', 1),
	(20, '2019_09_25_074044_create_visits_table', 1),
	(21, '2019_09_25_074049_create_visit_actions_table', 1),
	(22, '2019_09_25_074053_create_visit_files_table', 1),
	(23, '2019_09_25_074057_create_fact_find_modules_table', 1),
	(24, '2019_09_25_074058_create_risk_lists_table', 1),
	(25, '2019_09_25_074102_create_policies_table', 1),
	(26, '2019_09_25_074120_create_turnovers_table', 1),
	(27, '2019_09_25_074125_create_pre_scrutinies_table', 1),
	(28, '2019_09_25_074130_create_c_p_d_s_table', 1),
	(29, '2019_09_25_074134_create_s_p_s_s_table', 1),
	(30, '2019_09_25_074204_create_libraries_table', 1),
	(31, '2019_09_25_074215_create_fact_find_data_table', 1),
	(32, '2019_09_25_074220_create_letter_builders_table', 1),
	(33, '2019_09_25_074225_create_documents_table', 1),
	(34, '2020_01_07_054900_create_checklists_table', 1);




CREATE TABLE IF NOT EXISTS `my_companies` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `company_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `company_website` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `phone_number` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `mobile_number` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `vat_status` tinyint(1) NOT NULL,
  `principal_email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `filechecking_email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `office_email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `secondary_filechecking_email` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `county` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `town_city` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `postal_code` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `area_location` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `house_plot_no` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `address` text COLLATE utf8mb4_unicode_ci,
  `fca_number` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `financial_year_end` date DEFAULT NULL,
  `pii_renewal_date` date DEFAULT NULL,
  `business` json DEFAULT NULL,
  `company_footer` text COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `company_logo` mediumblob,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;



INSERT INTO `my_companies` (`id`, `company_name`, `company_website`, `phone_number`, `mobile_number`, `vat_status`, `principal_email`, `filechecking_email`, `office_email`, `secondary_filechecking_email`, `county`, `town_city`, `postal_code`, `area_location`, `house_plot_no`, `address`, `fca_number`, `financial_year_end`, `pii_renewal_date`, `business`, `company_footer`, `created_at`, `updated_at`, `deleted_at`, `company_logo`) VALUES
	(1, 'Your Company Name', NULL, NULL, '01234567891', 0, 'abc@mail.com', 'filecheck@mail.com', 'official@mail.com', 'secondary@mail.com', 'Lodon', 'Your Town City', 'abc1020', 'your area location', 'comapony plot number', 'compony address', 'compony fca number', '2020-02-27', '2020-02-27', NULL, 'your company footer', '2020-02-27 08:33:02', '2020-02-27 08:33:02', NULL, NULL);



CREATE TABLE IF NOT EXISTS `online_accounts` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `site` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `decision` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `decision_note` text COLLATE utf8mb4_unicode_ci,
  `site_checked` tinyint(1) NOT NULL DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;



CREATE TABLE IF NOT EXISTS `password_resets` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  KEY `password_resets_email_index` (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;



CREATE TABLE IF NOT EXISTS `policies` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `policy_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `client` bigint(20) unsigned NOT NULL,
  `advisor` bigint(20) unsigned NOT NULL,
  `specific_policy_type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `new_business_type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `adviser_reference` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `policy_valuation_amount` double(8,2) DEFAULT NULL,
  `policy_request_date` date DEFAULT NULL,
  `policy_valuation_date` date DEFAULT NULL,
  `policy_start_date` date DEFAULT NULL,
  `policy_end_date` date DEFAULT NULL,
  `policy_cancellation_date` date DEFAULT NULL,
  `policy_review_date` date DEFAULT NULL,
  `is_send_mail` enum('Yes','No') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'No',
  `other_reason` tinyint(1) NOT NULL DEFAULT '0',
  `note` text COLLATE utf8mb4_unicode_ci,
  `provider` bigint(20) unsigned NOT NULL,
  `trust` bigint(20) unsigned DEFAULT NULL,
  `beneficiaries` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `funds` json DEFAULT NULL,
  `policy_status` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `policy_number` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `risk_attitude` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `ucis_product` tinyint(1) NOT NULL DEFAULT '0',
  `direct_stock_share` tinyint(1) NOT NULL DEFAULT '0',
  `commission_fees` json DEFAULT NULL,
  `payments` json DEFAULT NULL,
  `withdrawals` json DEFAULT NULL,
  `mortgage_amount` double(8,2) DEFAULT NULL,
  `mortgage_monthly_amount` double(8,2) DEFAULT NULL,
  `mortgage_term` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `mortgage_fixed_until` date DEFAULT NULL,
  `mortgage_protected` tinyint(1) DEFAULT NULL,
  `county` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `postal_code` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `address` text COLLATE utf8mb4_unicode_ci,
  `interest` double(8,2) DEFAULT NULL,
  `loan_to_value` double(8,2) DEFAULT NULL,
  `property_value` double(8,2) DEFAULT NULL,
  `deposit_amount` double(8,2) DEFAULT NULL,
  `valuation_date` date DEFAULT NULL,
  `mortgage_offer_date` date DEFAULT NULL,
  `willing_to_pay` tinyint(1) NOT NULL DEFAULT '0',
  `repayment_on_track` tinyint(1) NOT NULL DEFAULT '0',
  `mortgage_portable` tinyint(1) NOT NULL DEFAULT '0',
  `indexed_criteria` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `widow_pension` double(8,2) DEFAULT NULL,
  `waiver_included` tinyint(1) NOT NULL DEFAULT '0',
  `contracted_out` tinyint(1) NOT NULL DEFAULT '0',
  `product_type` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `protection_basis` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `renewal_date` date DEFAULT NULL,
  `premium_period` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `total_protection_sum` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `protection_excess` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `plan_purpose` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `policies_client_foreign` (`client`),
  KEY `policies_advisor_foreign` (`advisor`),
  KEY `policies_provider_foreign` (`provider`),
  KEY `policies_trust_foreign` (`trust`),
  CONSTRAINT `policies_advisor_foreign` FOREIGN KEY (`advisor`) REFERENCES `users` (`id`) ON DELETE CASCADE,
  CONSTRAINT `policies_client_foreign` FOREIGN KEY (`client`) REFERENCES `clients` (`id`) ON DELETE CASCADE,
  CONSTRAINT `policies_provider_foreign` FOREIGN KEY (`provider`) REFERENCES `providers` (`id`) ON DELETE CASCADE,
  CONSTRAINT `policies_trust_foreign` FOREIGN KEY (`trust`) REFERENCES `trusts` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;



CREATE TABLE IF NOT EXISTS `pre_scrutinies` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `advisor` bigint(20) unsigned NOT NULL,
  `client` bigint(20) unsigned NOT NULL,
  `specific_business_type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `file` blob NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `pre_scrutinies_advisor_foreign` (`advisor`),
  KEY `pre_scrutinies_client_foreign` (`client`),
  CONSTRAINT `pre_scrutinies_advisor_foreign` FOREIGN KEY (`advisor`) REFERENCES `users` (`id`) ON DELETE CASCADE,
  CONSTRAINT `pre_scrutinies_client_foreign` FOREIGN KEY (`client`) REFERENCES `clients` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;



CREATE TABLE IF NOT EXISTS `promotions` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `promotion_category` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `promotion_type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `promotion_medium` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `promotion_title` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `requested_date` date NOT NULL,
  `deadline` date DEFAULT NULL,
  `promotion_closed` tinyint(1) NOT NULL DEFAULT '0',
  `closed_by` bigint(20) unsigned DEFAULT NULL,
  `closed_date` date DEFAULT NULL,
  `overall_result` json DEFAULT NULL,
  `note` text COLLATE utf8mb4_unicode_ci,
  `requested_by` bigint(20) unsigned NOT NULL,
  `ifac_advisor` bigint(20) unsigned DEFAULT NULL,
  `jurisdiction` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `product_area_covered` text COLLATE utf8mb4_unicode_ci,
  `reviewer` bigint(20) unsigned DEFAULT NULL,
  `reviewed_date` date DEFAULT NULL,
  `signed_of_by` bigint(20) unsigned DEFAULT NULL,
  `approval_date` date DEFAULT NULL,
  `approval_to` date DEFAULT NULL,
  `documents` json DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `promotions_closed_by_foreign` (`closed_by`),
  KEY `promotions_requested_by_foreign` (`requested_by`),
  KEY `promotions_ifac_advisor_foreign` (`ifac_advisor`),
  KEY `promotions_reviewer_foreign` (`reviewer`),
  KEY `promotions_signed_of_by_foreign` (`signed_of_by`),
  CONSTRAINT `promotions_closed_by_foreign` FOREIGN KEY (`closed_by`) REFERENCES `authors` (`id`) ON DELETE CASCADE,
  CONSTRAINT `promotions_ifac_advisor_foreign` FOREIGN KEY (`ifac_advisor`) REFERENCES `authors` (`id`) ON DELETE CASCADE,
  CONSTRAINT `promotions_requested_by_foreign` FOREIGN KEY (`requested_by`) REFERENCES `users` (`id`) ON DELETE CASCADE,
  CONSTRAINT `promotions_reviewer_foreign` FOREIGN KEY (`reviewer`) REFERENCES `authors` (`id`) ON DELETE CASCADE,
  CONSTRAINT `promotions_signed_of_by_foreign` FOREIGN KEY (`signed_of_by`) REFERENCES `authors` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;



CREATE TABLE IF NOT EXISTS `providers` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `provider_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `address` text COLLATE utf8mb4_unicode_ci,
  `county` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `postcode` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email_address` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `note` text COLLATE utf8mb4_unicode_ci,
  `approved_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;



CREATE TABLE IF NOT EXISTS `risk_lists` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;



CREATE TABLE IF NOT EXISTS `roles` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;



INSERT INTO `roles` (`id`, `name`, `slug`, `created_at`, `updated_at`, `deleted_at`) VALUES
	(1, 'Super Admin', 'super admin', '2020-02-27 08:33:01', '2020-02-27 08:33:01', NULL),
	(2, 'Admin', 'admin', '2020-02-27 08:33:01', '2020-02-27 08:33:01', NULL),
	(3, 'Director', 'director', '2020-02-27 08:33:02', '2020-02-27 08:33:02', NULL),
	(4, 'Client', 'client', '2020-02-27 08:33:02', '2020-02-27 08:33:02', NULL);



CREATE TABLE IF NOT EXISTS `suitable_reports` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `download` int(11) NOT NULL,
  `file` blob NOT NULL,
  `note` text COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;



CREATE TABLE IF NOT EXISTS `s_p_s_s` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `user` bigint(20) unsigned NOT NULL,
  `issued_by` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `date_issued` date NOT NULL,
  `expire_date` date NOT NULL,
  `files` json DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `s_p_s_s_user_foreign` (`user`),
  CONSTRAINT `s_p_s_s_user_foreign` FOREIGN KEY (`user`) REFERENCES `users` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;



CREATE TABLE IF NOT EXISTS `tasks` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `task_title` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `task_type` bigint(20) unsigned NOT NULL,
  `assigned_to` bigint(20) unsigned NOT NULL,
  `start_date` date NOT NULL,
  `end_date` date DEFAULT NULL,
  `client` bigint(20) unsigned DEFAULT NULL,
  `task_status` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `note` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `tasks_task_type_foreign` (`task_type`),
  KEY `tasks_assigned_to_foreign` (`assigned_to`),
  KEY `tasks_client_foreign` (`client`),
  CONSTRAINT `tasks_assigned_to_foreign` FOREIGN KEY (`assigned_to`) REFERENCES `users` (`id`) ON DELETE CASCADE,
  CONSTRAINT `tasks_client_foreign` FOREIGN KEY (`client`) REFERENCES `clients` (`id`) ON DELETE CASCADE,
  CONSTRAINT `tasks_task_type_foreign` FOREIGN KEY (`task_type`) REFERENCES `task_types` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;



CREATE TABLE IF NOT EXISTS `task_types` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` text COLLATE utf8mb4_unicode_ci,
  `status` tinyint(1) NOT NULL DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;



CREATE TABLE IF NOT EXISTS `trusts` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `trust_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `forename` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `surname` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `address` text COLLATE utf8mb4_unicode_ci,
  `county` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `postcode` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `contact_number` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `note` text COLLATE utf8mb4_unicode_ci,
  `approved_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;



CREATE TABLE IF NOT EXISTS `turnovers` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `client` bigint(20) unsigned DEFAULT NULL,
  `client_policy` bigint(20) unsigned DEFAULT NULL,
  `advisor` bigint(20) unsigned NOT NULL,
  `turnover_date` date DEFAULT NULL,
  `regulation_type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `regulation_type_spec` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `turnover_type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `turnover_frequency` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `adviser_reference` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `rdr_status` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `revenue_source` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `payment_frequency` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `amount` double(8,2) NOT NULL,
  `vat_applied` tinyint(1) NOT NULL DEFAULT '0',
  `revenue_source_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `note` text COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `turnovers_client_foreign` (`client`),
  KEY `turnovers_client_policy_foreign` (`client_policy`),
  KEY `turnovers_advisor_foreign` (`advisor`),
  CONSTRAINT `turnovers_advisor_foreign` FOREIGN KEY (`advisor`) REFERENCES `users` (`id`) ON DELETE CASCADE,
  CONSTRAINT `turnovers_client_foreign` FOREIGN KEY (`client`) REFERENCES `clients` (`id`) ON DELETE CASCADE,
  CONSTRAINT `turnovers_client_policy_foreign` FOREIGN KEY (`client_policy`) REFERENCES `policies` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;



CREATE TABLE IF NOT EXISTS `users` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `role_id` bigint(20) unsigned NOT NULL DEFAULT '3',
  `client_id` bigint(20) unsigned DEFAULT NULL,
  `saluation` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `forname` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `middle_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `surname` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `date_of_birth` date DEFAULT NULL,
  `email_address` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `mobile_number` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `phone_number` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `fax_number` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `ni_number` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `pre_sale_type` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `fca_number` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `county_old` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `town_city` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `post_code` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `address` text COLLATE utf8mb4_unicode_ci,
  `job_title` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `specials` json DEFAULT NULL,
  `basics` json DEFAULT NULL,
  `mortgages` json DEFAULT NULL,
  `lifetimes` json DEFAULT NULL,
  `others` json DEFAULT NULL,
  `heigher_levels` json DEFAULT NULL,
  `professionals` json DEFAULT NULL,
  `job_role` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_status` tinyint(1) NOT NULL DEFAULT '1',
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_email_unique` (`email`),
  KEY `users_role_id_foreign` (`role_id`),
  CONSTRAINT `users_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;



INSERT INTO `users` (`id`, `role_id`, `client_id`, `saluation`, `forname`, `middle_name`, `surname`, `date_of_birth`, `email_address`, `mobile_number`, `phone_number`, `fax_number`, `ni_number`, `pre_sale_type`, `fca_number`, `county_old`, `town_city`, `post_code`, `address`, `job_title`, `specials`, `basics`, `mortgages`, `lifetimes`, `others`, `heigher_levels`, `professionals`, `job_role`, `user_status`, `email`, `email_verified_at`, `password`, `remember_token`, `created_at`, `updated_at`, `deleted_at`) VALUES
	(1, 1, NULL, 'Mr.', 'Super', NULL, 'Admin', NULL, 'superadmin@mail.com', '01234567899', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Super', 1, 'superadmin@mail.com', NULL, '$2y$10$Aw0cT0Qv9./FS9pukXT7V.BUye4R.iMxYQ/4eJ9AHFwpyUM9m30YW', NULL, '2020-02-27 08:33:02', '2020-02-27 08:33:02', NULL),
	(2, 3, NULL, 'Mr.', 'Director', NULL, 'Babu', NULL, 'director@mail.com', '01234567899', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Director', 1, 'director@mail.com', NULL, '$2y$10$tyWzmbwIaz0HsOcaDg.vVehqOGyTAPatlTDOIUtkzApPSPV1JqW9u', NULL, '2020-02-27 08:33:02', '2020-02-27 08:33:02', NULL);



CREATE TABLE IF NOT EXISTS `visits` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `visit_date` date NOT NULL,
  `signed_off_on` date DEFAULT NULL,
  `nature_of_visit` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `signed_off_by` bigint(20) unsigned DEFAULT NULL,
  `grading` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `visits_signed_off_by_foreign` (`signed_off_by`),
  CONSTRAINT `visits_signed_off_by_foreign` FOREIGN KEY (`signed_off_by`) REFERENCES `authors` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;



CREATE TABLE IF NOT EXISTS `visit_actions` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `visit_id` bigint(20) unsigned NOT NULL,
  `agenda` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `outcome` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `deadline` date NOT NULL,
  `completed_on` date DEFAULT NULL,
  `issue` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `related_action` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `note` text COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `visit_actions_visit_id_foreign` (`visit_id`),
  CONSTRAINT `visit_actions_visit_id_foreign` FOREIGN KEY (`visit_id`) REFERENCES `visits` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;



CREATE TABLE IF NOT EXISTS `visit_files` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `visit_id` bigint(20) unsigned NOT NULL,
  `file_type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `file` blob NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `visit_files_visit_id_foreign` (`visit_id`),
  CONSTRAINT `visit_files_visit_id_foreign` FOREIGN KEY (`visit_id`) REFERENCES `visits` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
